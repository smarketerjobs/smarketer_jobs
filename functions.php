<?php
	function random_string($mode, $len){
		$str = '';
		if(strpos($mode, 'A') !== false){
			$str .= 'QWERTYUIOPASDFGHJKLZXCVBNM';
		}
		if(strpos($mode, 'a') !== false){
			$str .= 'qwertyuiopasdfghjklzxcvbnm';
		}
		if(strpos($mode, '&') !== false){
			$str .= '`~!@#$%^&*()-_=+,./<>?|';
		}
		return mb_substr(str_shuffle($str), 0, $len);
	}
?>