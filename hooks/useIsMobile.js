import React from 'react';

function useIsMobile(ww = 992) {
    const [isMobile, setIsMobile] = React.useState(window.innerWidth < ww);

    function handleSizeChange() {
        return setIsMobile(window.innerWidth < ww);
    }

    React.useEffect(() => {
        window.addEventListener("resize", handleSizeChange);

        return () => {
            window.removeEventListener("resize", handleSizeChange);
        };
    }, [isMobile]);

    return isMobile;
}

export default useIsMobile;
