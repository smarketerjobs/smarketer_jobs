<!DOCTYPE html>
<html>
<head>
    <title>Smarketer.Jobs</title>
    <link rel="shortcut icon" href="../media/logoSquare.png" type="image/png">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:site_name" content="Smarketer.Jobs"/>
    <?php
        if($this->page_url == '/overview'){
            echo $this->job()->get_job_posting($this->job_id);
        }
    ?>
    <style>
        .full-page-loader {
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            position: absolute;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
        }
        .full-page-loader > img {
            animation: 1.8s infinite heartbeat;
        }

        @keyframes heartbeat {
            0% {
                transform: scale(1);
            }
            25% {
                transform: scale(1.05);
            }
            50% {
                transform: scale(1);
            }
            75% {
                transform: scale(1.05);
            }
            100% {
                transform: scale(1);
            }
        }
    </style>
</head>
<body>
<div id="root">
    <div class="full-page-loader">
        <img width="200" src="../media/logo-search.svg" alt="AIP.Trade logo"/>
    </div>
</div>

<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
      rel="stylesheet">
<link href="../css/toggle-style.css" rel="stylesheet">
<link href="../css/Accordion.css" rel="stylesheet">

<script defer type="application/ld+json">
    {
        "@context":"http://schema.org",
        "@type":"WebSite",
        "name":"Smarketer.Jobs",
        "url":"<?= SITE_ADDR; ?>"
    }
</script>
<script defer type="application/ld+json">
    {
        "@context":"http://schema.org",
        "@type":"Organization",
        "url":"<?= SITE_ADDR; ?>",
        "logo":"<?= SITE_ADDR; ?>/media/logoSquare.png"
    }
</script>
<script defer>
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        const f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WGWKMRR');
</script>
</body>
</html>
