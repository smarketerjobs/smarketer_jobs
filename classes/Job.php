<?php

use Medoo\Medoo;

include "Helpers.php";

class Job extends Main {
    /** @var Medoo $database */
    private $database;

    public function __construct()
    {
        $this->database = $this->mysql();
        parent::__construct();
    }

    public function save($data = null){
		$locale_code = $this->locale()->get_locale();
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return false;
		}else{
			$row = $this->database->select(
				'users',
				['user_id'],
				['email' => $this->user()->get_email()]
			);
			if(count($row)){
				$user_id_fk = $row[0]['user_id'];
			}else{
				$this->set_data('');
				$this->set_error($this->locale()->get_by_key("User not found"));
				return false;
			}
		}
		if(!$data){
			$data = $this->get_input_data();
		}

		if($user_id_fk){
			$job_id = isset($data['job_id']) ? (int)$data['job_id'] : null;
			$title = isset($data['title']) ? $data['title'] : '';
			$start_description = isset($data['start_description']) ? $data['start_description'] : '';
			$description = isset($data['description']) ? $data['description'] : '';
			$qualifications = isset($data['qualifications']) ? $data['qualifications'] : '';
			$benefits = isset($data['benefits']) ? $data['benefits'] : '';
			$final_description = isset($data['final_description']) ? $data['final_description'] : '';
			$category_id_fk = isset($data['category_id']) ? $data['category_id'] : null;
			$industry_id_fk = isset($data['industry_id']) ? $data['industry_id'] : null;
			$employments = isset($data['employments']) ? $data['employments'] : '';
			$company_id_fk = isset($data['company_id_fk']) ? $data['company_id_fk'] : null;

			$company = $this->company()->get_company_data($company_id_fk);
			if (!$company || $this->user()->get_logged_user_id() != $company['user_id_fk']) {
				$this->set_data('');
				$this->set_error('Company not found or disabled.');
				return false;
			}

			$link = isset($data['link']) ? $data['link'] : '';
			$type_id_fk = isset($data['type_id']) ? $data['type_id'] : null;
			$location_id_fk = isset($data['location_id']) ? $data['location_id'] : 1;
			$street = isset($data['street']) ? $data['street'] : '';
			$post_code = isset($data['post_code']) ? $data['post_code'] : null;
			$city = isset($data['city']) ? $data['city'] : '';
			$lat = isset($data['lat']) ? $data['lat'] : '';
			$lng = isset($data['lng']) ? $data['lng'] : '';
			$state_code_fk = isset($data['state_code']) ? $data['state_code'] : null;
			$country_code_fk = isset($data['country_code']) ? $data['country_code'] : null;
			$salary_gross = isset($data['salary_gross']) ? str_replace(',', '.', $data['salary_gross']) : null;
			$salary_from = isset($data['salary_from']) ? str_replace(',', '.', $data['salary_from']) : null;
			$salary_to = isset($data['salary_to']) ? str_replace(',', '.', $data['salary_to']) : null;
			$salary_mode = isset($data['salary_mode']) ? $data['salary_mode'] : 1;
			$salary_period_id_fk = isset($data['salary_period_id']) ? $data['salary_period_id'] : null;
			$currency_id_fk = isset($data['currency_id']) ? $data['currency_id'] : 1;
			$active = isset($data['active']) ? $data['active'] : 0;
			$auto_active = isset($data['auto_active']) ? $data['auto_active'] : 0;

			$date_start = gmdate('Y-m-d');
			$date_end = isset($data['date_end']) ? $data['date_end'] : gmdate('Y-m-d', strtotime('+30 day'));

			$addr_str = '';
			if($street){
				$addr_str .= ','.$street;
			}

			if($post_code){
				$addr_str .= ','.$post_code;
			}

			if($city){
				$addr_str .= ','.$city;
			}

			if($state_code_fk){
				$state = $this->catalog()->get_state_by_state_code($state_code_fk);
				if($state){
					$addr_str .= ','.$state;
				}
			}

			if($country_code_fk){
				$country = $this->catalog()->get_country_by_country_code($country_code_fk);
				if($country){
					$addr_str .= ','.$country;
				}
			}

			$required = [
				'title' => 'Job title',
				'description' => 'Description',
				'company_id_fk' => 'Company',
				'category_id' => 'Category',
				'industry_id' => 'Industry',
				'employments' => 'Employment',
			];
			$this->set_data('');
			foreach ($required as $key => $value) {
				if(isset($data[$key])){
					if(!is_array($data[$key]) && !$data[$key]){
						$this->set_data('');
						$this->set_error($value.' cannot be empty');
						return false;
					}elseif(is_array($data[$key]) && count($data[$key]) == 0){
						$this->set_data('');
						$this->set_error($value.' cannot be empty');
						return false;
					}elseif(is_array($data[$key])){
						foreach ($data[$key] as $value2) {
							if(!trim($value2)){
								$this->set_data('');
								$this->set_error($value.' cannot be empty');
								return false;
							}
						}
					}
				}else{
					$this->set_data('');
					$this->set_error($value.' is not present');
					return false;
				}
			}

			if(!$job_id){
				$this->database->insert(
					'jobs',
					[
						"user_id_fk"=>$user_id_fk,
						"company_id_fk" => $company_id_fk,
						"title"=>$title,
						"start_description"=>$start_description,
						"description"=>$description,
						"qualifications"=>$qualifications,
						"benefits"=>$benefits,
						"final_description"=>$final_description,
						"category_id_fk"=>$category_id_fk,
						"industry_id_fk"=>$industry_id_fk,
						"employments"=>json_encode($employments),
						"date_start"=>$date_start,
						"date_end"=>$date_end,
						"date_posted"=>date("Y-m-d H:i:s", $GLOBALS['now']),
						"link"=>$link,
						"type_id_fk"=>$type_id_fk,
						"location_id_fk"=>$location_id_fk,
						"street"=>$street,
						"post_code"=>$post_code,
						"city"=>$city,
						"lat"=>$lat,
						"lng"=>$lng,
						"state_code_fk"=>$state_code_fk,
						"country_code_fk"=>$country_code_fk,
						"salary_gross"=>$salary_gross,
						"salary_from"=>$salary_from,
						"salary_to"=>$salary_to,
						"salary_mode"=>$salary_mode,
						"salary_period_id_fk"=>$salary_period_id_fk,
						"currency_id_fk"=>$currency_id_fk,
						"locale_code"=>$locale_code,
						"active"=>$active,
						"auto_active"=>$auto_active,
					]
				);
				$job_id = $this->database->id();

				$this->stats()->job_published();

				// $mysql->error();
				if(!$this->database->error()[1]){
					$this->set_data(['job_id' => $job_id]);
				}else{
					$this->set_data('');
					$this->set_error($this->database->error());
				}
				return $this->database->id();
			}else{
				// check if company is active
				if ($company['active'] != 1) {
					$this->set_error('You can enable only job in active company.');
					$this->set_data('');
					return false;
				}

				$this->database->update(
					'jobs',
					[
						"user_id_fk"=>$user_id_fk,
						"company_id_fk" => $company_id_fk,
						"title"=>$title,
						"start_description"=>$start_description,
						"description"=>$description,
						"qualifications"=>$qualifications,
						"benefits"=>$benefits,
						"final_description"=>$final_description,
						"category_id_fk"=>$category_id_fk,
						"industry_id_fk"=>$industry_id_fk,
						"employments"=>json_encode($employments),
						"date_start"=>$date_start,
						"date_end"=>$date_end,
						"date_posted"=>gmdate("Y-m-d H:i:s", $GLOBALS['now']),
						"link"=>$link,
						"type_id_fk"=>$type_id_fk,
						"location_id_fk"=>$location_id_fk,
						"street"=>$street,
						"post_code"=>$post_code,
						"city"=>$city,
						"lat"=>$lat,
						"lng"=>$lng,
						"state_code_fk"=>$state_code_fk,
						"country_code_fk"=>$country_code_fk,
						"salary_gross"=>$salary_gross,
						"salary_from"=>$salary_from,
						"salary_to"=>$salary_to,
						"salary_mode"=>$salary_mode,
						"salary_period_id_fk"=>$salary_period_id_fk,
						"currency_id_fk"=>$currency_id_fk,
						"locale_code"=>$locale_code,
						"active"=>$active,
						"auto_active"=>$auto_active,
					],
					['job_id'=>$job_id]
				);
				if(!$this->database->error()[1]){
					if($active){
                        try {
                            $this->update_activity($job_id);
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                    }
					$this->set_data(['job_id' => $job_id]);
					return true;
				}else{
					$this->set_data('');
					$this->set_error($this->database->error());
					return false;
				}
			}
		}else{
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User not found"));
			return false;
		}
	}

	public function auto_deactivate($job_id){

	}

    /**
     * @throws Google_Exception
     */
    public function deactivate_all() {
		if (!$this->user()->is_logged_user()) return;

		$user_id = $this->user()->get_logged_user_id();
		$jobs = $this->database->select(
			'jobs',
			['job_id'],
			[
			    'active' => 1,
                'deleted' => 0
            ]
		);

		if ($jobs && count($jobs)) {
			foreach ($jobs as $job) {
				$this->google_delete($job['job_id']);
			}
		}

		$this->database->update(
			'jobs',
			['active' => 0],
			[
			    'user_id_fk' => $user_id,
                'deleted' => 0
            ]
		);

        return;
	}

    /**
     * @param $job_id
     * @return array|bool
     * @throws Exception
     */
    public function update_activity($job_id){
		$status = [
			'active' => 1,
			'message' => "success",
		];

		$row = $this->database->get(
			'jobs',
			['job_id', 'user_id_fk', 'company_id_fk', 'date_start', 'date_end', 'auto_active', 'active'],
			['job_id' => $job_id]
		);

		if($row && count($row)){
			$user_id = $row['user_id_fk'];

			// Check if company is active
			$company = $this->database->get(
				'companies',
				['company_id', 'active'],
				['company_id' => $row['company_id_fk']]
			);

			if ($company && !$company['active']) {
				$status = [
					'active' => 0,
					'message' => "You can't activate job when you have no active companies",
				];
				return $status;
			}
		}else{
			return false;
		}

		$active_jobs_count = $this->user()->get_active_jobs_count($user_id, $job_id);
		$current_plan = $this->user()->get_current_plan($user_id);
		$max_jobs_count = $current_plan['max_jobs'];

        date_default_timezone_set('UTC');
		$date_start = strtotime($row['date_start']);
		if (!$date_start) {
		    $date_start = 0;
        }

        $date_end = strtotime($row['date_end']);
		$today = time();

		if(($row['auto_active'] == 1 || $row['active'] == 1) || $today >= $date_start && $today <= $date_end && $active_jobs_count < $max_jobs_count) {
			$status['active'] = 1;
			$this->google_update($job_id);

			$this->mysql()->update(
			    'jobs',
                ['date_start' => gmdate('Y-m-d', $today)],
                ['jobs.job_id' => $job_id]
            );

		}else{
			if ($today < $date_start) {
				$status['message'] = "You can't activate job with future start date";
			} else if($today > $date_end ) {

				$status['active'] = 1;
				$status['message'] = "Application date is set to 30 days in the future";

				$args = [
					'active' => 1, 
					'date_start' => gmdate('Y-m-d', time()),
					'date_end' => gmdate('Y-m-d', strtotime('+1 month'))
				];

				$this->database->update(
					'jobs',
					$args,
					['job_id' => $job_id]
				);

				return $status;

			} else if ($active_jobs_count >= $max_jobs_count) {
				$status['message'] = "You can't activate more jobs on current plan";
			}

			$status['active'] = 0;
			$this->google_delete($job_id);
		}

		$this->database->update(
			'jobs',
			['active' => $status['active']],
			['job_id' => $job_id]
		);

		return $status;
	}

    /**
     * @param $user_id
     * @return bool|int
     * @throws Exception
     */
    public function update_all_activity($user_id){
		$res = $this->database->select(
			'jobs',
			['[><]users' => ['user_id_fk' => 'user_id']],
			['job_id'],
			['OR' => ['user_id_fk' => $user_id, 'stripe_user_id' => $user_id]]
		);
		if(!$this->database->error()[1] && count($res)){
			$active_jobs = 0;
			foreach ($res as $key => $row) {
				$status = $this->update_activity($row['job_id']);
				if ($status['active']) {
					$active_jobs += 1;
				}
			}
			return $active_jobs;
		}else{
			return false;
		}
	}

	public function get_job_slug($job_id){
		$row = $this->database->get(
			'jobs',
			[
				'[><]users' => ['user_id_fk' => 'user_id'],
			],
			['job_id', 'user_id', 'company_slug'],
			[
				'AND' => [
					'job_id' => $job_id,
				]
			]
		);

		if(count($row)){
			return SITE_ADDR.'/overview?job_id='.$row['job_id'];
		}else{
			return false;
		}
	}

    /**
     * @param null $job_id
     * @return bool
     * @throws Google_Exception
     */
    public function google_check($job_id = null){
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			if(isset($data['job_id'])) {
				$job_id = $data['job_id'];
			}else{
				$this->set_data('');
				$this->set_error('No job id');
				return false;
			}
		}
		$job_url = $this->get_job_url($job_id);

		if($job_url){
			require_once DOCUMENT_ROOT.'/classes/Google/vendor/autoload.php';
			$client = new Google_Client();
			$client->setAuthConfig(DOCUMENT_ROOT.'/keys/google-service-account.json');
			$client->addScope('https://www.googleapis.com/auth/indexing');

			/** @var GuzzleHttp\Client $httpClient */
			$httpClient = $client->authorize();
			$endpoint = 'https://indexing.googleapis.com/v3/urlNotifications/metadata';

			$encoded_url = urlencode($job_url);

			$response = $httpClient->get($endpoint.'?url='.$encoded_url);
			$status_code = $response->getStatusCode();

			if($status_code == 200) {
				$this->set_data($status_code);
				print_r($response);
				return true;
			}else{
				$this->set_data('');
				$this->set_error($status_code.": ".$response->getReasonPhrase());
				return false;
			}
		}else{
			$this->set_data('');
			$this->set_error('Some error');
			return false;
		}
	}

    /**
     * @param null $job_id
     * @return bool
     * @throws Google_Exception
     */
    private function google_update($job_id = null){
		if(SITE_ADDR != 'https://smarketer.jobs'){
			return false;
		}
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			if(isset($data['job_id'])){
				$job_id = $data['job_id'];
			}else{
				$this->set_data('');
				$this->set_error('No job id');
				return false;
			}
		}

		if(!$job_id){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}

		$job_url = $this->get_job_url($job_id);

		file_get_contents("https://www.google.com/ping?sitemap=https://smarketer.jobs/sitemap.xml");

		if($job_url){
			require_once DOCUMENT_ROOT.'/classes/Google/vendor/autoload.php';
			$client = new Google_Client();
			$client->setAuthConfig(DOCUMENT_ROOT.'/keys/google-service-account.json');
			$client->addScope('https://www.googleapis.com/auth/indexing');

			/** @var GuzzleHttp\Client $httpClient */
			$httpClient = $client->authorize();
			$endpoint = 'https://indexing.googleapis.com/v3/urlNotifications:publish';

			$content = '{
				"url": "'.$job_url.'",
				"type": "URL_UPDATED"
			}';

			$response = $httpClient->post($endpoint, [ 'body' => $content ]);
			$status_code = $response->getStatusCode();

			if($status_code == 200){
				$this->set_data('ok');
				return true;
			}else{
				$this->set_data('');
				$this->set_error($status_code);
				return false;
			}

		}else{
			$this->set_data('');
			$this->set_error('Job url not found');
			return false;
		}

	}

    /**
     * @param null $job_id
     * @return bool
     * @throws Google_Exception
     */
    private function google_delete($job_id = null){
		if(SITE_ADDR != 'https://smarketer.jobs'){
			return true;
		}
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			if(isset($data['job_id'])){
				$job_id = $data['job_id'];
			}else{
				$this->set_data('');
				$this->set_error('No job id');
				return false;
			}
		}

		if(!$job_id){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}

		file_get_contents("https://www.google.com/ping?sitemap=https://smarketer.jobs/sitemap.xml");

		$job_url = $this->get_job_url($job_id);
		if($job_url){
			require_once DOCUMENT_ROOT.'/classes/Google/vendor/autoload.php';
			$client = new Google_Client();
			$client->setAuthConfig(DOCUMENT_ROOT.'/keys/google-service-account.json');
			$client->addScope('https://www.googleapis.com/auth/indexing');

			/** @var GuzzleHttp\Client $httpClient */
			$httpClient = $client->authorize();
			$endpoint = 'https://indexing.googleapis.com/v3/urlNotifications:publish';

			$content = '{
				"url": "'.$job_url.'",
				"type": "URL_DELETED"
			}';

			$response = $httpClient->post($endpoint, [ 'body' => $content ]);
			$status_code = $response->getStatusCode();

			if($status_code == 200){
				$this->set_data('ok');
				return true;
			}else{
				$this->set_data("");
				$this->set_error("error_".$status_code);
				return false;
			}
		}else{
			$this->set_error("Job url not found");
			return false;
		}

	}

	public function get_job_url($job_id){
		$row = $this->database->get(
			'jobs',
			['job_id'],
			[
				'job_id' => $job_id,
			]
		);
		if($row){
			return SITE_ADDR.'/overview?job_id='.$row['job_id'];
		}else{
			return false;
		}
	}

    /**
     * @param null $job_id
     * @return bool
     * @throws Exception
     */
    public function activate($job_id = null){
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return false;
		}
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			$job_id = $data['job_id'];
		}

		if(!$job_id){
			$this->set_data('');
			$this->set_error('job_id is not defined');
			return false;
		}

		$status = $this->update_activity($job_id);
		if($status['active']){
			$this->set_data($status['message']);
			$this->set_error('');
			return true;
		}else{
			$this->set_data('');
			if (!$this->get_error()) {
				$this->set_error($status['message']);
			}
			return false;
		}
	}

    /**
     * @return bool
     * @throws Google_Exception
     */
    public function deactivate(){
		if(!$this->user()->is_logged_user()){
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			$this->set_data('');
			return false;
		}
		/** @var array $data */
		$data = $this->get_input_data();
		$this->database->update(
			'jobs',
			['active'=>0],
			['job_id'=>$data['job_id'], 'user_id_fk'=>$this->user()->get_logged_user_id()]
		);

		if(!$this->database->error()[1]){
			$this->set_data('success');
			$this->set_error('');
			$this->google_delete($data['job_id']);
			return true;
		}else{
			$this->set_error($this->database->error());
			$this->set_data('');
			return false;
		}
	}

    /**
     * @param null $job_id
     * @return bool
     * @throws Google_Exception
     */
    public function delete($job_id = null){
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return false;
		}
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			if(isset($data['job_id'])){
				$job_id = $data['job_id'];
			}
		}

		if(!$job_id){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}

		$this->database->update(
			'jobs',
			[
				'deleted' => 1
			],
			[
				'job_id' => $job_id,
				'user_id_fk' => $this->user()->get_logged_user_id(),
			]
		);
		if(!$this->database->error()[1]){
			if($this->google_delete($job_id)){
				$this->set_data('success');
				return true;
			}else{
				$this->set_data('');
				if ($this->get_error() == "") {
					$this->set_error('Google delete error');
				}
				return false;
			}
		}else{
			$this->set_data('');
			$this->set_error('error');
			return false;
		}
	}

	public function get_job($job_id = null) {
	    /** @var array $data */
	    $data = $this->get_input_data();
	    if ($job_id) {
	        $data['job_id'] = $job_id;
        }
	    $data['active'] = [0, 1];
        $this->set_input_data($data);

        $jobs_info = $this->search_jobs();

        $jobs = $jobs_info['result'];

        if (!count($jobs)) {
            $this->set_error('No job found');
            $this->set_data("");
            return;
        }

        $this->set_data($jobs[0]);
        $this->set_error("");
    }

	public function sort_by($array, $key, $mode){
		usort($array, function($a, $b) use($key, $mode){
			if($mode == 'desc'){
				if(isset($a[$key]) && isset($b[$key]) && $a[$key] < $b[$key]){
					return 1;
				}else{
					return -1;
				}
			}else{
				if(isset($a[$key]) && isset($b[$key]) && $a[$key] > $b[$key]){
					return 1;
				}else{
					return -1;
				}
			}
		});
		return $array;
	}

	public function get_user_jobs() {
		$user_id = $this->user()->get_logged_user_id();

		if (!$user_id) {
			$this->set_error("User is not logged");
			$this->set_data("");
			return false;
		}

		$rows = $this->database->select(
			'jobs',
			[
				'[><]companies' => ['company_id_fk' => 'company_id'],
			],
			[
				"jobs.job_id",
				"jobs.title",
				"companies.company_name",
				"jobs.job_view",
				"jobs.job_conversion",
				"jobs.job_share",
				"jobs.job_redirect",
				"jobs.active",
			],
			[
				'jobs.user_id_fk' => $user_id,
				'jobs.deleted' => 0,
			]
		);


		if($this->database->error()[1]){
			$this->set_data("");
			$this->set_error($this->database->error());
			return false;
		}

		if (!count($rows)) {
			$this->set_error('No jobs found');
			$this->set_data("");
			return false;
		}

		$this->set_error("");
		$this->set_data($rows);
		return true;
	}

	public function get_all_jobs() {
        /** @var array $data */
        $data = $this->get_input_data();
        if(!isset($data['sort_by'])){
            $data['sort_by'] = 'job_id';
        }

        if(!isset($data['user_id'])){
            $this->set_data('');
            $this->set_error('No user_id');
            return;
        }

        $user_id = $data['user_id'];

        if($this->user()->is_logged_user() && $this->user()->get_logged_user_id() == $user_id) {
            if ($data['active'] === "all") {
                $data['active'] = [0, 1];
            }
        }else{
            $data['active'] = [1];
        }

        $data['sort_by'] = $data['sort_by'] == 'category_id' ? 'category_id_fk' : $data['sort_by'];
        $data['sort_by'] = $data['sort_by'] == 'industry_id' ? 'industry_id_fk' : $data['sort_by'];
        $data['sort_by'] = $data['sort_by'] == 'type_id' ? 'type_id_fk' : $data['sort_by'];
        $data['sort_by'] = $data['sort_by'] == 'state_id' ? 'state_id_fk' : $data['sort_by'];
        $data['sort_by'] = $data['sort_by'] == 'country_id' ? 'country_id_fk' : $data['sort_by'];

        if(!isset($data['sort_method']) || !in_array($data['sort_method'], ['asc', 'desc'])){
            $data['sort_method'] = 'asc';
        }
        $data['sort_method'] = mb_strtoupper($data['sort_method']);

        $this->set_input_data($data);

        $jobs = $this->search_jobs();
        $jobs = $jobs['result'];

        if (!count($jobs)) {
            $this->set_error('No jobs found');
            $this->set_data("");
            return;
        }
        $this->set_error("");
        $this->set_data($jobs);
        return;
    }

	public function get_job_posting($job_id = null) {
        /** @var array $data */
        $data = $this->get_input_data();
		if(!$job_id && isset($data['job_id'])){
			$job_id = $data['job_id'];
		}

		if(!$job_id){
			$this->set_data('');
			$this->set_error('No job id');
			return false;
		}
		$today = gmdate("Y-m-d H:i:s", $GLOBALS['now']);
		$row = $this->database->get(
			'jobs',
			[
				'[><]users' => ['user_id_fk' => 'user_id'],
				'[><]companies' => ['company_id_fk' => 'company_id'],
				'[><]job_currencies' => ['currency_id_fk' => 'currency_id'],
				'[><]job_salary_periods' => ['salary_period_id_fk' => 'period_id'],
				'[><]states' => ['state_code_fk' => 'state_code'],
				'[>]job_types' => ['type_id_fk' => 'type_id'],
			],
			[
				'jobs.job_id',
				'jobs.user_id_fk',
				'jobs.title',
				'jobs.start_description',
				'jobs.description',
				'jobs.qualifications',
				'jobs.benefits',
				'jobs.final_description',
				'jobs.category_id_fk',
				'jobs.industry_id_fk',
				'jobs.employments',
				'jobs.date_start',
				'jobs.date_end',
				'jobs.date_posted',
				'jobs.link',
				'jobs.type_id_fk',
				'jobs.location_id_fk',
				'jobs.street',
				'jobs.post_code',
				'jobs.city',
				'jobs.lat',
				'jobs.lng',
				'jobs.state_code_fk',
				'jobs.country_code_fk',
				'jobs.salary_gross',
				'jobs.salary_from',
				'jobs.salary_to',
				'jobs.salary_mode',
				'jobs.salary_period_id_fk',
				'jobs.locale_code',
				'jobs.active ',
				'companies.company_name',
				'companies.company_website',
				'companies.company_logo',
				'companies.company_description',
				'job_currencies.currency_code',
				'job_salary_periods.period_title_en(salary_period)',
				'job_types.google_type(employment_type)',
				'states.state',
			],
			[
				'AND' => [
					'job_id' => $job_id,
					'jobs.active' => 1,
					'date_start[<=]' => $today,
					'date_end[>=]' => $today,
				]
			]
		);

		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}


		if(!is_array($row) || !count($row)){
			$this->set_data('');
			$this->set_error('No jobs');
			return false;
		}

		$ld_json = [
			'@context' => 'http://schema.org',
			'@type' => 'JobPosting',
			'datePosted' => $row['date_posted'],
		];

		if($row['date_end']){
			$ld_json['validThrough'] = $row['date_end'];
		}

		$ld_json['title'] = $row['title'];

		$ld_json['baseSalary'] = [
			'@type' => 'MonetaryAmount',
			'currency' => $row['currency_code'],
			'value' => [
				'@type' => 'QuantitativeValue',
				'unitText' => strtoupper($row['salary_period']),
			],
		];
		if($row['salary_mode'] == 1){
			$ld_json['baseSalary']['value']['value'] = $row['salary_gross'];
		}elseif($row['salary_mode'] == 2){
			$ld_json['baseSalary']['value']['minValue'] = $row['salary_from'];
			$ld_json['baseSalary']['value']['maxValue'] = $row['salary_to'];
		}

		$ld_json['hiringOrganization'] = [
			'@type' => 'Organization',
		];

		if($row['company_name']){
			$ld_json['hiringOrganization']['name'] = $row['company_name'];
		}
		if($row['company_website']){
			$ld_json['hiringOrganization']['sameAs'] = $row['company_website'];
		}
		if($row['company_logo'] && file_exists(DOCUMENT_ROOT.'/logotypes/'.$row['company_logo'])){
			$ld_json['hiringOrganization']['logo'] = SITE_ADDR.'/logotypes/'.$row['company_logo'];
		}

		$ld_json['jobLocation'] = [
			'@type' => 'Place',
			'address' => [
				'@type' => 'PostalAddress',
				'streetAddress' => $row['street'],
				'addressLocality' => $row['city'], //city
				'addressRegion' => $row['state'], //MI
				'addressCountry' => $row['country_code_fk'], //2 letter country code

			],
		];
		if($row['post_code'] && $row['post_code'] != 'null'){
			$ld_json['jobLocation']['address']['postalCode'] = $row['post_code'];
		}

		if($row['lat'] && $row['lng']){
			$ld_json['geo'] = [
				"@type" => "GeoCoordinates",
				"longitude" => $row['lng'],
				"latitude" => $row['lat'],
			];
		}

		$ld_json['description'] = $row['start_description'].$row['description'];

		$ld_json['url'] = $this->get_job_url($job_id);


		if($row['employment_type']){
			$ld_json['employmentType'] = $row['employment_type'];
		}else{
			$ld_json['employmentType'] = 'FULL_TIME';
		}


		if(in_array($row['type_id_fk'], [3,4])){
			$ld_json['jobLocationType'] = 'TELECOMMUTE';
		}


		if($row['industry_id_fk']){
			$row2 = $this->database->get(
				'job_industries',
				['industry_title_'.$row['locale_code'].'(industry_title)'],
				['industry_id' => $row['industry_id_fk']]
			);
			$ld_json['industry'] = $row2['industry_title'];
		}

		if(
		    mb_stripos($ld_json['description'], 'ö') !== false ||
            mb_stripos($ld_json['description'], 'ä') !== false ||
            mb_stripos($ld_json['description'], 'ü') !== false ||
            mb_stripos($ld_json['description'], 'ß') !== false
        ){
			$benefit_title = 'Benefits';
			$qualifications_title = 'Qualifikationen';
		}else{
			$benefit_title = 'Benefits';
			$qualifications_title = 'Qualifications';
		}

		if($row['qualifications'] && is_array(json_decode(json_decode($row['qualifications'], true)))){
			$ld_json['qualifications'] = implode(', ', json_decode(json_decode($row['qualifications'], true)));
			$qualifications = implode('</li><li>', json_decode(json_decode($row['qualifications'], true)));
			$qualifications = '<p>'.$qualifications_title.':</p><ul><li>'.$qualifications.'</li></ul>';
			$ld_json['description'] .= $qualifications;
		}

		if($row['benefits'] && is_array(json_decode(json_decode($row['benefits'], true)))){
			$ld_json['jobBenefits'] = implode(', ', json_decode(json_decode($row['benefits'], true)));
			$benefits = implode('</li><li>', json_decode(json_decode($row['benefits'], true)));
			$benefits = '<p>'.$benefit_title.':</p><ul><li>'.$benefits.'</li></ul>';
			$ld_json['description'] .= $benefits;
		}

		$ld_json['description'] .= $row['final_description'];

		$p_array = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div'];
		$ul_array = ['ol'];
		$space_array = ['strong', 'b', 'i', 'u', 'span'];

		foreach ($p_array as $tag) {
			$ld_json['description'] = str_ireplace('<'.$tag.'>', '<p>', $ld_json['description']);
			$ld_json['description'] = str_ireplace('</'.$tag.'>', '</p>', $ld_json['description']);
		}

		foreach ($ul_array as $tag) {
			$ld_json['description'] = str_ireplace('<'.$tag.'>', '<ul>', $ld_json['description']);
			$ld_json['description'] = str_ireplace('</'.$tag.'>', '</ul>', $ld_json['description']);
		}

		foreach ($space_array as $tag) {
			$ld_json['description'] = str_ireplace('<'.$tag.'>', ' ', $ld_json['description']);
			$ld_json['description'] = str_ireplace('</'.$tag.'>', ' ', $ld_json['description']);
		}

		$data = '<script type="application/ld+json">'.
		json_encode($ld_json,  JSON_PRETTY_PRINT+JSON_UNESCAPED_SLASHES+JSON_UNESCAPED_UNICODE).
		'</script>';
		$this->set_data($data);
		return $data;
	}

	public function view() {
        /** @var array $data */
		$data = $this->get_input_data();
		if(!isset($data['job_id'])){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}
		$row = $this->database->get(
			'jobs',
			['job_view'],
			['job_id' => $data['job_id']]
		);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}
		$views = $row['job_view'] + 1;

		$this->database->update(
			'jobs',
			['job_view' => $views],
			['job_id' => $data['job_id']]
		);

		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}else{
			return true;
		}

	}

	public function converse() {
        /** @var array $data */
		$data = $this->get_input_data();
		if(!isset($data['job_id'])){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}
		$row = $this->database->get(
			'jobs',
			['job_conversion'],
			['job_id' => $data['job_id']]
		);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}
		$conversions = $row['job_conversion'] + 1;

		$this->database->update(
			'jobs',
			['job_conversion' => $conversions],
			['job_id' => $data['job_id']]
		);

		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}else{
			return true;
		}
	}

	public function share() {
        /** @var array $data */
		$data = $this->get_input_data();
		if(!isset($data['job_id'])){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}
		$row = $this->database->get(
			'jobs',
			['job_share'],
			['job_id' => $data['job_id']]
		);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}
		$shares = $row['job_share'] + 1;

		$this->database->update(
			'jobs',
			['job_share' => $shares],
			['job_id' => $data['job_id']]
		);

		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}else{
			return true;
		}
	}

	public function copy($job_id = null) {
		if(!$job_id){
		    /** @var array $data */
		    $data = $this->get_input_data();
			if(isset($data['job_id'])){
				$job_id = (int)$data['job_id'];
			}else{
				$this->set_error('No job_id');
				$this->set_data('');
				return false;
			}
		}

		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error('User is not logged');
			return false;
		}

		$user_id = $this->user()->get_logged_user_id();

		$sql = "
			INSERT INTO `jobs` (`user_id_fk`, `company_id_fk`, `title`, `start_description`, `description`, 
			                    `qualifications`, `benefits`, `final_description`, `category_id_fk`, 
			                    `industry_id_fk`, `employments`, `date_start`, `date_end`, `date_posted`, 
			                    `link`, `type_id_fk`, `street`, `post_code`, `city`, `lat`, `lng`, `state_code_fk`, 
			                    `country_code_fk`, `salary_gross`, `salary_from`, `salary_to`, `salary_mode`, 
			                    `salary_period_id_fk`, `currency_id_fk`, `locale_code`, `auto_active`, `location_id_fk`, 
			                    `deleted`, `lastmod`)
			SELECT `user_id_fk`, `company_id_fk`, `title`, `start_description`, `description`, `qualifications`, 
			       `benefits`, `final_description`, `category_id_fk`, `industry_id_fk`, `employments`, `date_start`, 
			       `date_end`, `date_posted`, `link`, `type_id_fk`, `street`, `post_code`, `city`, `lat`, `lng`, 
			       `state_code_fk`, `country_code_fk`, `salary_gross`, `salary_from`, `salary_to`, `salary_mode`, 
			       `salary_period_id_fk`, `currency_id_fk`, `locale_code`, `auto_active`, `location_id_fk`, `deleted`, 
			       `lastmod`
				FROM `jobs`
				WHERE `job_id` = $job_id AND `user_id_fk` = $user_id
			";

		$this->database->query($sql);

		if($this->database->error()[1]){
			$this->set_error($this->database->error());
			$this->set_data('');
			return false;
		}

		$new_job_id = $this->database->id();

		if($new_job_id){
			$this->set_error('');
			$this->set_data(['job_id' => $new_job_id]);
			return true;
		}else{
			$this->set_data('');
			$this->set_error('No new job id');
			return false;
		}

	}

	public function is_job_page() {
		if(isset($_GET['job_id']) && (int)$_GET['job_id']){
			$row = $this->database->get(
				'jobs',
				['job_id'],
				[
					'AND' => [
						'job_id' => $_GET['job_id'],
						'jobs.active' => 1,
						'date_start[<=]' => gmdate("Y-m-d", $GLOBALS['now']),
						'date_end[>=]' => gmdate("Y-m-d", $GLOBALS['now']),
					]
				]
			);
			if(!$this->database->error()[1] && isset($row['job_id'])){
				return [
					'job_id' => $row['job_id'],
				];
			}else{
				return false;
			}
		} else {
			return false;
		}
	}

	public function send_contacts() {
        /** @var array $data */
		$data = $this->get_input_data();
		$this->set_data('');
		if(!isset($data['email']) || !$data['email']){
			$this->set_data('');
			$this->set_error('email expected');
			return false;
		}elseif(!isset($data['first_name']) || !$data['first_name']){
			$this->set_data('');
			$this->set_error('first_name expected');
			return false;
		}elseif(!isset($data['last_name']) || !$data['last_name']) {
			$this->set_data('');
			$this->set_error('last_name expected');
			return false;
		}

		$email = $this->company()->get_company_data($data['company_id'])['company_email'];

		if(!$email){
			$this->set_data('');
			$this->set_error('Company email not found');
			return false;
		}

		$email_subject = 'New mail from smarketerjobs';
		$email_body = "<div><strong>e-mail:</strong> $data[email]</div>\n";
		$email_body .= "<div><strong>First Name:</strong> $data[first_name]</div>\n";
		$email_body .= "<div><strong>Last Name:</strong> $data[last_name]</div>\n";
		if($data['linkedin']){
			$email_body .= "<div><strong>Linkedin:</strong> $data[linkedin]</div>\n";
		}

		$attachments = [];
		if(isset($_FILES['document1']) && is_uploaded_file($_FILES['document1']['tmp_name'])){
			$attachments[$_FILES['document1']['tmp_name']] = $_FILES['document1']['name'];
		}else{
			$this->set_data('');
			$this->set_error('No document');
			return false;
		}

		if(isset($_FILES['document2']) && is_uploaded_file($_FILES['document2']['tmp_name'])){
			$attachments[$_FILES['document2']['tmp_name']] = $_FILES['document2']['name'];
		}

		if ($this->mail()->send_now($email, $email_subject, $email_body, $attachments)){
			$this->mail()->send_now($GLOBALS['mail_from_email'], $email_subject, $email_body, $attachments);
			$this->set_data("ok");
			$this->set_error('');
			return true;
		} else {
			$this->set_data("");
			$this->set_error('Mail error');
			return false;
		}
	}

    /**
     * @throws Exception
     */
    public function cron_deactivate() {
		$date_now = gmdate("Y-m-d", $GLOBALS['now']);

        /** @var array $rows */
        $rows = $this->database->select(
		    'jobs',
            ['job_id'],
            [
                'active' => 1,
                'date_start[<]' => $date_now,
                'date_end[<]' => $date_now,
            ]
        );

		if(count($rows)){
			foreach ($rows as $row) {
			    $this->cron_deactivate_job($row['job_id']);
			}
		}
	}

    /**
     * @param $job_id
     * @throws Google_Exception
     */
    public function cron_deactivate_job($job_id) {
        $job = $this->database->get(
            'jobs',
            ['job_id', 'active', 'auto_active'],
            ['job_id' => $job_id]
        );

        if (!$job['auto_active']) {
            // DEACTIVATE JOB
            $this->database->update(
                'jobs',
                ['active' => 0],
                ['job_id' => $job_id]
            );
            $this->google_delete($job_id);
        }
    }

	public function get_sitemap() {
		$date_now = gmdate("Y-m-d H:i:s", $GLOBALS['now']);
		$site_addr = SITE_ADDR;
		$rows = $this->database->query(
			"SELECT `job_id`, `user_id_fk`, `date_posted`, `lastmod` FROM `jobs`
				WHERE `active` = 1
					AND `date_start` <= '$date_now'
					AND `date_end` >= '$date_now'"
		)->fetchAll();

		if(count($rows)){
			echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			echo "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
			foreach ($rows as $row) {
				echo "\n\t<url>";
				echo "\n\t\t<loc>{$site_addr}/overview?job_id={$row['job_id']}</loc>";
				echo "\n\t\t<lastmod>".mb_substr($row['date_posted'], 0, 10)."</lastmod>";
				echo "\n\t</url>";
			}
			echo "\n</urlset>";
		}

	}

	public function auto_active_on($job_id = null){
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return false;
		}
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			$job_id = $data['job_id'];
			$row = $this->database->get(
				'jobs',
				['user_id_fk'],
				['job_id' => $job_id]
			);
			if($this->database->error()[1] || !is_array($row) || !count($row) || !isset($row['user_id_fk']) || $row['user_id_fk'] != $this->user()->get_logged_user_id()){
				$this->set_data('');
				$this->set_error('Not allowed');
				return false;
			}
		}

		if(!$job_id){
			return false;
		}

		$this->database->update(
			'jobs',
			['auto_active' => 1],
			['job_id' => $job_id]
		);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}else{
			$this->set_data('Success');
			$this->set_error('');
			return true;
		}

	}

	public function auto_active_off($job_id = null){
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return false;
		}
		if(!$job_id){
		    /** @var array $data */
			$data = $this->get_input_data();
			$job_id = $data['job_id'];
			$row = $this->database->get(
				'jobs',
				['user_id_fk'],
				['job_id' => $job_id]
			);
			if($this->database->error()[1] || !is_array($row) || !count($row) || !isset($row['user_id_fk']) || $row['user_id_fk'] != $this->user()->get_logged_user_id()){
				$this->set_data('');
				$this->set_error('Not allowed');
				return false;
			}
		}

		if(!$job_id){
			return false;
		}

		$this->database->update(
			'jobs',
			['auto_active' => 0],
			['job_id' => $job_id]
		);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}else{
			$this->set_data('Success');
			$this->set_error('');
			return true;
		}
	}

	public function get_jobs_by_company($company_id = null, $company_slug = null) {
        /** @var array $data */
		$data = $this->get_input_data();
		if (!isset($data['company_id']) && !isset($data['company_slug'])) {
			$this->set_error('Neither company_id or company_slug provided.');
			$this->set_data('');
			return false;
		}

		if (isset($data['company_id'])) {
			$jobs = $this->search_jobs();
			$this->set_error('');
			$this->set_data($jobs['result']);
			return false;
		}

		if (isset($data['company_slug'])) {
			$jobs = $this->search_jobs();
			$this->set_error('');
			$this->set_data($jobs['result']);
			return false;
		}
	}

	public function redirect_to_website($job_id = null) {
        /** @var array $data */
		$data = $this->get_input_data();
		if(!isset($data['job_id'])){
			$this->set_data('');
			$this->set_error('No job_id');
			return false;
		}
		$row = $this->database->get(
			'jobs',
			['job_redirect'],
			['job_id' => $data['job_id']]
		);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}

		if (!$row['job_redirect']) {
			$row['job_redirect'] = 0;
		}

		$redirects = $row['job_redirect'] + 1;

		$this->database->update(
			'jobs',
			['job_redirect' => $redirects],
			['job_id' => $data['job_id']]
		);

		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
			return false;
		}else{
			$this->set_data('success');
			return true;
		}
	}

	/**
	 * This method renews job for one month.
	 */
	public function renew_job_date() {
		$date_now = gmdate("Y-m-d", $GLOBALS['now']);
		$date_plus_month = gmdate("Y-m-d", strtotime("+1 month"));

		$rows = $this->database->query(
			"SELECT `job_id`, `company_id_fk` FROM `jobs`
				WHERE `active` = 1
					AND `auto_active` = 1
					AND `date_start` <= '$date_now'
					AND `date_end` >= '$date_now'"
		)->fetchAll();

		if(count($rows)){
			foreach ($rows as $row) {
				$this->database->update(
					'jobs',
					[
						'date_start' => $date_now,
						'date_end' => $date_plus_month
					],
					['job_id' => $row['job_id']]
				);
				$this->send_job_was_renewed_email($row);
			}
		}

		return;
	}

	/**
	 * This method will check if job is ends up soon and send email to user, that his job will be renewed soon.
	 */
	public function check_job_ending() {
		$date_now = gmdate("Y-m-d", $GLOBALS['now']);
		$date_plus_three_days = gmdate("Y-m-d", strtotime("+3 days"));

		$rows = $this->database->query(
			"SELECT `job_id`, `company_id_fk` FROM `jobs`
				WHERE `active` = 1
					AND `auto_active` = 1
					AND `date_start` <= '$date_now'
					AND `date_end` <= '$date_plus_three_days'"
		)->fetchAll();

		if(count($rows)){
			foreach ($rows as $row) {
				$this->send_job_will_be_renewed_email($row);
			}
		}

		return true;
	}

	public function send_job_will_be_renewed_email($job) {
		if ($this->locale()->get_locale() == "en") {
			$email_subject = 'Your job ad expires within 3 days';
			$email_body = "<p>Hello from smarketer.jobs.</p>";
			$email_body.= "<p>Your job ad expires within 3 days. You can extend the expiration date in your account or activate the automatic renewal of job ads for 30 days in the job ads overview in your account.</p>";
			$email_body.= "<p>Best,</p>";
			$email_body.= "<p>your smarketer.jobs team</p>";
			$email_body.= "------------------------------------------------------------------------------------";
			$email_body.= "<p style=\"color: #7f7f7f; font-size: 0.9em;\">Would you like to publish more job ads? <a href=\"https://smarketer.jobs/company-info\">Upgrade today</a> to a bigger plan.</p>";
			$email_body.= "<p style=\"color: #7f7f7f; font-size: 0.9em;\">Would you like to get more reach with your job ads? Contact our team at +49 (0) 30 577 008 136 or info@smarketer.jobs and we are happy to help you with individual google ads packages for more potential applicants.</p>";
		} else {
			$email_subject = 'Ihre Stellenanzeige läuft innerhalb von 3 Tagen ab';
			$email_body = "<p>Guten Tag.</p>";
			$email_body.= "<p>Ihre Stellenanzeige läuft innerhalb von 3 Tagen ab. Sie können das Enddatum der Stellenanzeige in Ihrem Account manuell verlängern oder die automatische Verlängerung der Stellenanzeigen um 30 Tage in Ihrem Account im Überblick der Stellenanzeigen aktivieren.</p>";
			$email_body.= "------------------------------------------------------------------------------------";
			$email_body.= "<p style=\"color: #7f7f7f; font-size: 0.9em;\">Sie wollen noch mehr Stellenanzeigen veröffentlichen? Sie können jederzeit auf das nächsthöhere Paket upgraden. <a href=\"https://smarketer.jobs/company-info\">Jetzt upgraden</a></p>";
			$email_body.= "<p style=\"color: #7f7f7f; font-size: 0.9em;\">Sie möchten mehr Reichweite mit Ihren Stellenanzeigen erzielen? Kontaktieren Sie unser Expertenteam unter +49 (0) 30 577 008 136 oder unter info@smarketer.jobs für maßgeschneiderte Google Ads Pakete, um noch mehr potenzielle Bewerber zu erreichen.</p>";
		}

		$company = $this->database->get(
			'companies',
			['company_id', 'company_email'],
			['company_id' => $job['company_id_fk']]
		);

		$email = $company['company_email'];

        if ($this->mail()->send_now($email, $email_subject, $email_body, null)){
			return true;
		} else {
			return false;
		}
	}

	public function send_job_was_renewed_email($job) {

		if ($this->locale()->get_locale() == "en") {
			$email_subject = 'Job has been automatically extended for 30 days';
			$email_body = "<p>Hello from smarketer.jobs,</p>";
			$email_body.= "<p>your job advertisement on smarketer.jobs has been automatically extended for 30 days according to your account settings. You can deactivate the automatic renewal at any time in your account under \"overview job ads\".</p>";
			$email_body.= "<p>Best,</p>";
			$email_body.= "<p>your smarketer.jobs team</p>";
			$email_body.= "------------------------------------------------------------------------------------";
			$email_body.= "<p style=\"color: #7f7f7f; font-size: 0.9em;\">Would you like to get more reach with your job ads? Contact our team at +49 (0) 30 577 008 136 or info@smarketer.jobs and we are happy to help you with individual google ads packages for more potential applicants.</p>";
		} else {
			$email_subject = 'Job has been automatically extended for 30 days';
			$email_body = "<p>Guten Tag,</p>";
			$email_body.= "<p>Ihre Stellenanzeige auf smarketer.jobs wurde Ihren Account Einstellungen entsprechend automatisch um 30 Tage verlängert. Sie können die automatische Verlängerung jederzeit in Ihrem Account deaktivieren unter der Ansicht „Überblick Stellenanzeigen“.</p>";
			$email_body.= "<p>Ihr Smarketer Jobs Team</p>";
			$email_body.= "------------------------------------------------------------------------------------";
			$email_body.= "<p style=\"color: #7f7f7f; font-size: 0.9em;\">Sie möchten mehr Reichweite mit Ihren Stellenanzeigen erzielen? Kontaktieren Sie unser Expertenteam unter +49 (0) 30 577 008 136 oder unter info@smarketer.jobs für maßgeschneiderte Google Ads Pakete, um noch mehr potenzielle Bewerber zu erreichen.</p>";
		}

		$company = $this->database->get(
			'companies',
			['company_id', 'email'],
			['company_id' => $job['company_id_fk']]
		);

		$email = $company['email'];

		if ($this->mail()->send_now($email, $email_subject, $email_body, null)){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method will reset post date each three days
	 */
	public function cron_reset_post_date() {
		$date = gmdate("Y-m-d H:i:s", strtotime("-3 days"));

		/** @var Medoo $database */
		$database = $this->mysql();

		$jobs = $database->select(
			'jobs',
			['job_id', 'date_posted'],
			[
				'date_posted[<=]' => $date,
				'active' => 1,
				'deleted' => 0
			]
		);

		if(count($jobs)){
			foreach ($jobs as $job) {
				$database->update(
					'jobs',
					['date_posted' => gmdate('Y-m-d H:i:s')],
					['job_id' => $job['job_id']]
				);
			}
		}
	}

	/**
	 * This method will save job address
	 */
	public function save_address() {
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return;
		}

		/** @var array $data */
		$data = $this->get_input_data();
		/** @var Medoo $database */
		$database = $this->mysql();
		$columns = [];

		$columns['user_id'] = $this->user()->get_logged_user_id();
		$columns['street'] = $data['street'];
		$columns['city'] = $data['city'];
		$columns['lat'] = $data['lat'];
		$columns['lng'] = $data['lng'];
		$columns['post_code'] = $data['post_code'];
		$columns['country_code_fk'] = $data['country_code'];
		$columns['state_code_fk'] = $data['state_code'];

		$address = $database->get(
			'job_address',
			['id', 'user_id'],
			['user_id' => $columns['user_id']]
		);

		if (!$address) {
			$database->insert('job_address', $columns);
		} else {
			$database->update(
				'job_address',
				$columns,
				['user_id' => $columns['user_id']]
			);
		}

		if($database->error()[1]){
			$this->set_data('');
			$this->set_error($database->error());
		}else{
			$this->set_data('success');
		}

		return;
	}

	/**
	 * This method returns predefined address for job
	 * @method POST
	 */
	public function get_address() {
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("User is not logged"));
			return;
		}

		/** @var Medoo $database */
		$database = $this->mysql();

		$address = $database->get(
			'job_address',
			['id', 'user_id', 'street', 'city', 'post_code', 'country_code_fk(country_code)', 'state_code_fk(state_code)', 'lat', 'lng'],
			['user_id' => $this->user()->get_logged_user_id()]
		);

		if (!$address) {
			$this->set_data("");
			$this->set_error("No address found");
			return;
		} else {
			$this->set_data($address);
			$this->set_error("");
			return;
		}
	}

	public function get_filters(&$data) {
	    unset($data['filters']);
	    $this->set_input_data($data);
	    $jobs = $this->search_jobs(true);
        return add_additional_data($this, $jobs);
    }

    /**
     * Method to search jobs.
     * @param bool $with_filters
     * @return array;
     */
	public function search_jobs($with_filters = null) {
		/** @var array $data */
		$data = $this->get_input_data();

		$filters = null;
		if (array_key_exists('filters', $data)) {
		    $filters = $this->get_filters($data);
        }

		$is_short_response = (array_key_exists('short_response', $data) && !empty($data['short_response']));
		$search_fields = get_job_search_fields($this, $is_short_response);

		$conditions = ['AND' => []];

		if (
		    array_key_exists('page', $data) && !empty($data['page']) &&
            array_key_exists('range', $data) && !empty($data['range']) &&
            !$with_filters
        ) {
			$page = intval($data['page']) - 1;
			$range = intval($data['range']);
			$offset = $page * $range;

			$conditions['LIMIT'] = [
				$offset,
				$range
			];
		}

		if (
			array_key_exists('address', $data) && !empty($data['address']) &&
			array_key_exists('country_code', $data) && !empty($data['country_code']) &&
			array_key_exists('distance', $data) && !empty($data['distance'])
		) {
		    $cities = $this->slow_geo_search($data['address'], $data['country_code'], $data['distance']);
			if ($cities && count($cities)) {
				$conditions['AND'] = array_merge($conditions['AND'], $cities);
			}
		}

		$jobs = $this->do_main_search($conditions, $search_fields);

		if ($with_filters) {
		    return $jobs;
        }

		if ($is_short_response) {
			$this->set_error("");
			$this->set_data($jobs);
            return $jobs;
		}

		$jobs = ['result' => $this->prepare_results($jobs, $data)];

		if (is_array($filters)) {
		    $jobs = array_merge($jobs, $filters);
        }

		$this->set_data($jobs);
		$this->set_error("");
        return $jobs;
	}

    /**
     * @param array $jobs
     * @param array $data
     * @return array
     * @throws Exception
     */
	private function prepare_results($jobs, $data) {

		if (array_key_exists('sort_by', $data) && !empty($data['sort_by'])) {
			$jobs = $this->sort_by($jobs, $data['sort_by'], $data['sort_method']);
		}

		foreach ($jobs as &$job) {
			if (isset($job['qualifications'])) {
				$qualifications = json_decode($job['qualifications']);
				if ($qualifications) {
					$job['qualifications'] = $qualifications;
				}
			}

			if (isset($job['benefits'])) {
				$benefits = json_decode($job['benefits']);
				if ($benefits) {
					$job['benefits'] = $benefits;
				}
			}

			if (isset($job['employments'])) {
				$employments = json_decode($job['employments']);
				if ($employments) {
					$job['employments'] = $employments;
				}
			}

            date_default_timezone_set('UTC');
            $job['date_start'] = intval(strtotime($job['date_start']));
            $job['date_end'] = intval(strtotime($job['date_end']));
            $job['date_posted'] = intval(strtotime($job['date_posted']));
		}

		return $jobs;
	}

	/**
	 * @param array $conditions
	 * @param array $search_fields
	 * @return array|bool
	 */
	public function do_main_search($conditions = [], $search_fields = []) {

		/** @var array $data */
		$data = $this->get_input_data();

		$joins = [
			'[><]users' => ['jobs.user_id_fk' => 'user_id'],
			'[><]companies' => ['jobs.company_id_fk' => 'company_id'],
			'[><]countries' => ['jobs.country_code_fk' => 'country_code'],
			'[>]job_categories' => ['jobs.category_id_fk'  => 'category_id']
		];

		if (array_key_exists('q', $data) && !empty($data['q'])) {
			$q = $data['q'];
			$words = get_words($q);
			if (count($words)) {
				$conditions['OR'] = [
					'title[~]' => $words,
					'company_name[~]' => $words,
					'start_description[~]' => $words,
					'description[~]' => $words,
					'final_description[~]' => $words,
				];
			}
		}

		if (array_key_exists('user_id', $data) && !empty($data['user_id'])) {
		    $conditions['AND']['jobs.user_id_fk'] = $data['user_id'];
        }

		if (array_key_exists('job_id', $data) && !empty($data['job_id'])) {
			$conditions['AND']['jobs.job_id'] = $data['job_id'];
		}

		if (array_key_exists('country_code', $data) && !empty($data['country_code'])) {
			$conditions['AND']['jobs.country_code_fk'] = $data['country_code'];
		}

        if (array_key_exists('state_code', $data) && !empty($data['state_code'])) {
            $conditions['AND']['jobs.state_code_fk'] = $data['state_code'];
        }

		if (array_key_exists('company_slug', $data) && !empty($data['company_slug'])) {
			$conditions['AND']['company_slug'] = $data['company_slug'];
		}

        if (array_key_exists('company_id', $data) && !empty($data['company_id'])) {
            $conditions['AND']['company_id'] = $data['company_id'];
        }

		if (array_key_exists('category_slug', $data) && !empty($data['category_slug'])) {
			$locales = $this->locale()->get_all_locales_list();
			foreach ($locales as $locale) {
				$conditions['AND']['OR']['job_categories.category_slug_'. $locale] = $data['category_slug'];
			}
		}

		if (array_key_exists('sort_method', $data) && !empty($data['sort_method'])) {
		    if (array_key_exists('sort_by', $data) && !empty($data['sort_by'])) {
                $conditions['ORDER'] = [$data['sort_by'] => strtoupper($data['sort_method'])];
            }
        }

		$is_active = [ 1 ];
		if (array_key_exists('active', $data) && !empty($data['active'])) {
		    $is_active = $data['active'];
        }

		$conditions['AND'] = array_merge($conditions['AND'], [
			'jobs.active' => $is_active,
			'jobs.deleted' => 0
		]);

		$jobs = $this->database->select('jobs', $joins, $search_fields, $conditions);
		if($this->database->error()[1]){
			$this->set_data('');
			$this->set_error($this->database->error());
		}

		if($jobs && count($jobs)){
			return $jobs;
		}

		return [];
	}

	public function slow_geo_search($address = null, $country_code = null, $distance = null) {

	    $location = [
	        'lat' => null,
            'lon' => null
        ];

	    $cities = [
	      'city' => [ strtolower($address) ],
        ];

	    /** @var array $data */
	    $data = $this->get_input_data();

	    if (!$address) {
	        $address = strtolower($data['address']);
        }

	    if (!$country_code) {
	        $country_code = $data['country_code'];
        }

	    if (!$distance) {
	        $distance = $data['distance'];
        }

        $jobs = $this->mysql()->select(
            'jobs',
            [
                'jobs.country_code_fk',
                'jobs.lat',
                'jobs.lng',
                'city',
            ],
            [
                'active' => 1,
                'country_code_fk' => $country_code,
            ]
        );

	    foreach($jobs as $job) {
	        if (strtolower($job['city']) === strtolower($address)) {
	            $location['lat'] = $job['lat'];
	            $location['lng'] = $job['lng'];
            }
        }

	    foreach($jobs as $key => $job) {
            $distance1 = $this->calculate_distance($location, ['lat' => $job['lat'], 'lng' => $job['lng']]);

            if ($distance1 <= ($distance * 1000) && $distance1 !== 0.0) {
                if (!in_array(strtolower($job['city']), $cities['city'])) {
                    $cities['city'][] = strtolower($job['city']);
                }
            }
        }

	    return $cities;
    }

    private function calculate_distance($location1, $location2, $earthRadius = 6371000) {
        $latFrom = deg2rad($location1['lat']);
        $lonFrom = deg2rad($location1['lng']);
        $latTo = deg2rad($location2['lat']);
        $lonTo = deg2rad($location2['lng']);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
}