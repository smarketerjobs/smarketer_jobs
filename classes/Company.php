<?php

class Company extends Main
{
    public function get_company_job_categories($company_id = null){
        if(!$company_id){
            if (isset($this->get_input_data()['company_id'])) {
                $company_id = $this->get_input_data()['company_id'];
            }else{
                $this->set_data('');
                $this->set_error('No company_id');
                return false;
            }
        }
        $locale = $this->locale()->get_locale();

        $category_title = 'category_title_'.$locale;

        $rows = $this->mysql()->select(
            'jobs',
            ['[><]job_categories' => ['category_id_fk'=>'category_id']],
            ['category_id', $category_title.'(category_title)'],
            [
                'company_id_fk' => $company_id,
                'active' => 1,
                'deleted' => 0,
                'ORDER' => [$category_title => 'ASC'],
                'GROUP' => ['category_id', $category_title]
            ]
        );
        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        $res = [];
        foreach ($rows as $key => $value) {
            $res[$value['category_id']] = $value['category_title'];
        }
        $this->set_data($res);
        $this->set_error('');
        return $res;
    }

    public function get_company_data($company_id = null){
        /** @var array $data */
        $data = $this->get_input_data();
        if(!$company_id && (!isset($data['company_id']) || !$data['company_id'])){
            $this->set_data('');
            $this->set_error('No company id');
            return false;
        } else if(!$company_id){
            $company_id = $data['company_id'];
        }

        $result = $this->mysql()->get(
            'companies',
            ['company_id', 'user_id_fk', 'company_name', 'company_email', 'company_website', 'company_logo', 'company_description', 'company_slug', 'active'],
            ['company_id' => $company_id, 'deleted' => 0]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        } else if(!$result){
            $this->set_data('');
            $this->set_error('No data');
            return false;
        }

        if (file_exists(DOCUMENT_ROOT . '/logotypes/' . $result['company_logo']) && is_file(DOCUMENT_ROOT . '/logotypes/' . $result['company_logo'])) {
            $result['company_logo'] = '/logotypes/' . $result['company_logo'];
        } else {
            $result['company_logo'] = null;
        }

        $result['company_job_categories'] = [];

        $company_job_categories = $this->get_company_job_categories($company_id);

        if(is_array($company_job_categories) && count($company_job_categories)){
            if (!in_array($result['company_job_categories'], $company_job_categories)) {
                $result['company_job_categories'][] = $company_job_categories;
            }
        }else{
            $result['company_job_categories'] = [];
        }

        $this->set_data($result);
        $this->set_error('');

        return $result;
    }

    public function get_companies_data($user_id = null){
        $data = $this->get_input_data();
        if(!$user_id && (!isset($data['user_id']) || !$data['user_id'])){
            $this->set_data('');
            $this->set_error('No user id');
            return false;
        }elseif(!$user_id){
            $user_id = $data['user_id'];
        }

        $res = $this->mysql()->select(
            'companies',
            ['company_id', 'company_name', 'company_email', 'company_website', 'company_logo', 'company_description', 'company_slug', 'active'],
            ['user_id_fk' => $user_id, 'deleted' => 0]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        $results = $res;
        foreach ($results as &$result) {
            if (file_exists(DOCUMENT_ROOT . '/logotypes/' . $result['company_logo']) && is_file(DOCUMENT_ROOT . '/logotypes/' . $result['company_logo'])) {
                $result['company_logo'] = '/logotypes/' . $result['company_logo'];
            } else {
                $result['company_logo'] = null;
            }

            $company_job_categories = $this->get_company_job_categories($user_id);
            if(is_array($company_job_categories) && count($company_job_categories)){
                $result['company_job_categories'][] = $company_job_categories;
            }else{
                $result['company_job_categories'] = [];
            }
        }

        $this->set_data($results);
        $this->set_error('');
        return $results;

    }

    public function get_company_data_by_slug($company_slug = null) {
        $data = $this->get_input_data();
        if (isset($data['company_slug'])) {
            $company_slug = $data['company_slug'];
        }

        if (!$company_slug) {
            $this->set_data('');
            $this->set_error('No company_slug found');
            return false;
        }

        $company = $this->mysql()->get(
            'companies',
            ['company_id'],
            ['company_slug' => $company_slug]
        );

        if ($company) {
            $company = $this->get_company_data($company['company_id']);
            $this->set_data($company);
            $this->set_error('');
            return false;
        }
        $this->set_data('');
        $this->set_error('No companies found!');
    }

    public function update_company_data() {

        $data = $this->get_input_data();

        $response = [
            'message' => '',
            'company_logo_link' => '',
        ];

        if(!$this->user()->is_logged_user()){
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        if(!$data['company_id']){
            $this->set_data('');
            $this->set_error('No company id');
            return false;
        }

        $company_slug = Main::slugify($data['company_name']);

        $duplicates = $this->mysql()->get(
            'companies',
            ['user_id_fk'],
            ['AND' =>
                [
                    'company_id[!]' => $data['company_id'],
                    'company_slug' => $company_slug,
                ]
            ]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        if(is_array($duplicates) && count($duplicates)){
            $this->set_data('');
            $this->set_error('Company with this name already exist.');
            return false;
        }

        $this->mysql()->update(
            'companies',
            [
                'company_name' => $data['company_name'],
                'company_email' => $data['company_email'],
                'company_slug' => $company_slug,
                'company_website' => $data['company_website'],
                'company_description' => $data['company_description'],
            ],
            [
                'company_id' => $data['company_id'],
                'user_id_fk' => $this->user()->get_logged_user_id(),
            ]
        );

        if(!$this->mysql()->error()[1]){

            if(isset($_FILES['company_logo']) && is_uploaded_file($_FILES['company_logo']['tmp_name'])){
                $tmp_file_name = $_FILES['company_logo']['tmp_name'];
                $file_type = exif_imagetype($tmp_file_name);
                $ext = '';
                if($file_type == 1){
                    $ext = '.gif';
                }elseif($file_type == 2){
                    $ext = '.jpg';
                }elseif($file_type == 3){
                    $ext = '.png';
                }else{
                    $this->set_data('');
                    $this->set_error("Wrong file format");
                    return false;
                }

                $imgsize = getimagesize($tmp_file_name);

                if($imgsize[0] != $imgsize[1]){
                    $this->set_data('');
                    $this->set_error('The logo should be square.');
                    return false;
                }

                if($imgsize[0] < 112 || $imgsize[1] < 112){
                    $this->set_data('');
                    $this->set_error('The logo file is too small (min size is 112x112px)');
                    return false;
                }

                $file_name = $this->user()->get_logged_user_id()."-".time().$ext;
                move_uploaded_file($tmp_file_name, DOCUMENT_ROOT.'/logotypes/'.$file_name);
                $this->mysql()->update(
                    'companies',
                    [
                        'company_logo' => $file_name,
                    ],
                    [
                        'company_id' => $data['company_id'],
                        'user_id_fk' => $this->user()->get_logged_user_id(),
                    ]
                );

                $response['company_logo_link'] = '/logotypes/' . $file_name;

                if($this->mysql()->error()[1]){
                    $this->set_data('');
                    $this->set_error('Update logo error');
                    return false;
                }

            }

            $response['message'] = 'Company successfully updated';
            $this->set_data($response);

            return true;
        }else{
            $this->set_data('');
            $this->set_error('Update error');
            return false;
        }
    }

    public function save_company_data($data = null){
        if(!$this->user()->is_logged_user()){
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        $response = [
            'message' => '',
            'company_id' => '',
            'company_logo_link' => ''
        ];

        if (!$this->can_create_company()) {
            $this->set_data('');
            $this->set_error('Can\'t create more companies on your plan.');
            return false;
        }

        if (!$data) {
            $data = $this->get_input_data();
        }

        if (!$data['company_name']) {
            $this->set_data('');
            $this->set_error("Company name cannot be empty");
            return false;
        }

        $company_slug = Main::slugify($data['company_name']);

        $row = $this->mysql()->get(
            'companies',
            ['user_id_fk'],
            ['AND' =>
                [
                    'deleted' => 0,
                    'company_slug' => $company_slug,
                ]
            ]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        if(is_array($row) && count($row)){
            $this->set_data('');
            $this->set_error('Duplicate company name');
            return false;
        }

        $this->mysql()->insert(
            'companies',
            [
                'company_name' => $data['company_name'],
                'company_email' => $data['company_email'],
                'company_slug' => $company_slug,
                'company_website' => $data['company_website'],
                'company_description' => $data['company_description'],
                'user_id_fk' => $this->user()->get_logged_user_id(),
            ]
        );

        $company_id = $this->mysql()->id();
        $response['company_id'] = $company_id;

        $this->activate($company_id);

        if(!$this->mysql()->error()[1]){
            if(isset($_FILES['company_logo']) && is_uploaded_file($_FILES['company_logo']['tmp_name'])){
                $tmp_file_name = $_FILES['company_logo']['tmp_name'];
                $file_type = exif_imagetype($tmp_file_name);
                $ext = '';
                if($file_type == 1){
                    $ext = '.gif';
                }elseif($file_type == 2){
                    $ext = '.jpg';
                }elseif($file_type == 3){
                    $ext = '.png';
                }else{
                    $this->set_data('');
                    $this->set_error("Wrong file format");
                    return false;
                }

                $imgsize = getimagesize($tmp_file_name);

                if($imgsize[0] != $imgsize[1]){
                    $this->set_data('');
                    $this->set_error('The logo should be square.');
                    return false;
                }

                if($imgsize[0] < 112 || $imgsize[1] < 112){
                    $this->set_data('');
                    $this->set_error('The logo file is too small (min size is 112x112px)');
                    return false;
                }

                $file_name = $this->user()->get_logged_user_id()."-".time().$ext;
                move_uploaded_file($tmp_file_name, DOCUMENT_ROOT.'/logotypes/'.$file_name);
                $this->mysql()->update(
                    'companies',
                    [
                        'company_logo' => $file_name,
                    ],
                    [
                        'company_id' => $company_id,
                        'user_id_fk' => $this->user()->get_logged_user_id(),
                    ]
                );

                $response['company_logo_link'] = '/logotypes/' . $file_name;

                if($this->mysql()->error()[1]){
                    $this->set_data('');
                    $this->set_error('Save logo error');
                    return false;
                }

            }
            $this->set_error('');
            $this->set_data($response);
            return true;
        }else{
            $this->set_data($this->mysql()->error());
            $this->set_error('Company save error');
            return false;
        }
    }

    /**
     * Require POST|GET company_id;
     */
    public function delete_company_logo(){
        if(!$this->user()->is_logged_user()){
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        $data = $this->get_input_data();

        $user_id = $this->user()->get_logged_user_id();

        $company_logo = $this->get_company_data($data['company_id'])['company_logo'];

        if(file_exists(DOCUMENT_ROOT.$company_logo) && is_file(DOCUMENT_ROOT.$company_logo)){
            $this->set_data('Successfully deleted');
            $this->mysql()->update(
                'companies',
                ['company_logo' => null],
                [
                    'user_id_fk' => $user_id,
                    'company_id' => $data['company_id'],
                ]
            );
            unlink(DOCUMENT_ROOT.$company_logo);
            return true;
        }else{
            $this->set_data('');
            $this->set_error('Logo does not exists');
            return false;
        }

    }

    /**
     * @param null $company_id
     * @return bool
     */
    public function delete_company($company_id = null) {
        if(!$this->user()->is_logged_user()){
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        $user_id = $this->user()->get_logged_user_id();
        $data = $this->get_input_data();

        if ($data['company_id']) {
            $company_id = $data['company_id'];
        }

        if(!$company_id){
            $this->set_data('');
            $this->set_error('Company ID required');
            return false;
        }

        $this->mysql()->update(
            'companies',
            ['deleted' => 1],
            [
                'user_id_fk' => $user_id,
                'company_id' => $company_id,
            ]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        $this->mysql()->update(
            'jobs',
            [
                'deleted' => 1,
                'active' => 0,
            ],
            [
                'company_id_fk' => $company_id,
                'user_id_fk' => $user_id,
            ]
        );

        $this->deactivate($company_id);

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        } else {
            $this->set_data(['id' => $company_id]);
            $this->set_error('');
            return false;
        }
    }

    public function activate($company_id = null) {
        if(!$this->user()->is_logged_user()){
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        $user_id = $this->user()->get_logged_user_id();
        $data = $this->get_input_data();

        if (!$this->can_activate_company()) {
            $this->set_data('');
            $this->set_error('You can\'t activate more companies on current plan.');
            return false;
        }

        if (isset($data['company_id'])) {
            $company_id = $data['company_id'];
        }

        if(!$company_id){
            $this->set_data('');
            $this->set_error('Company ID required');
            return false;
        }

        $this->mysql()->update(
            'companies',
            ['active' => 1],
            [
                'company_id' => $company_id,
                'user_id_fk' => $user_id,
            ]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        } else {
            $this->set_data('Company and activated successfully');
            $this->set_error('');
            return false;
        }
    }

    public function deactivate($company_id = null) {
        if(!$this->user()->is_logged_user()){
            $this->set_data('');
            $this->set_error('User is not logged');
            return false;
        }

        $user_id = $this->user()->get_logged_user_id();
        $data = $this->get_input_data();

        if (isset($data['company_id'])) {
            $company_id = $data['company_id'];
        }

        if(!$company_id){
            $this->set_data('');
            $this->set_error('Company ID required');
            return false;
        }

        $this->mysql()->update(
            'companies',
            ['active' => 0],
            [
                'company_id' => $company_id,
                'user_id_fk' => $user_id,
            ]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }

        $this->mysql()->update(
            'jobs',
            ['active' => 0],
            [
                'user_id_fk' => $this->user()->get_logged_user_id(),
                'company_id_fk' => $company_id,
            ]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        } else {
            $this->set_data('Company and related jobs deactivated successfully.');
            $this->set_error('');
            return false;
        }
    }

    private function set_company_slug($company_id = null) {
        $data = $this->get_input_data();
        if(!$company_id && (!isset($data['company_id']) || !$data['company_id'])){
            $this->set_data('');
            $this->set_error('No company id');
            return false;
        }elseif(!$company_id){
            $company_id = $data['company_id'];
        }

        $companies = $this->mysql()->select(
            'companies',
            ['company_id', 'company_name', 'company_slug'],
            ['company_id' => $company_id]
        );

        if($this->mysql()->error()[1]){
            $this->set_data('');
            $this->set_error($this->mysql()->error());
            return false;
        }else if(!is_array($companies) || !count($companies)){
            $this->set_data('');
            $this->set_error('No data');
            return false;
        }

        foreach($companies as $company) {
            $slug = Main::slugify($company['company_name']);
            $this->mysql()->update(
                'companies',
                ['company_slug' => $slug],
                ['company_id' => $company_id]
            );
        }

        $this->set_data('OK');
        $this->set_error('');
    }

    public function can_create_company() {
        $user_id = $this->user()->get_logged_user_id();
        $current_plan = $this->user()->get_current_plan($user_id);
        $companies = $this->mysql()->select(
            'companies',
            ['company_id'],
            [
                'deleted' => 0,          // company is not deleted
                'user_id_fk' => $user_id // user is current user
            ]
        );

        if (!$companies) {
            return true; // any user can create at least one company
        } else {         // if companies exist
            /** @var array $companies */
            if ($current_plan['max_companies'] > count($companies)) { // check if current plan can have more companies
                return true;
            }
        }
        return false;
    }

    public function can_activate_company() {
        $user_id = $this->user()->get_logged_user_id();
        $current_plan = $this->user()->get_current_plan($user_id);
        $companies = $this->mysql()->select(
            'companies',
            ['company_id'],
            [
                'active' => 1,
                'deleted' => 0,          // company is not deleted
                'user_id_fk' => $user_id // user is current user
            ]
        );

        if (!$companies) {
            return true; // any user can create at least one company
        } else {         // if companies exist
            /** @var array $companies */
            if ($current_plan['max_companies'] > count($companies)) { // check if current plan can have more companies
                return true;
            }
        }
        return false;
    }

    public function deactivate_all() {
        $user_id = $this->user()->get_logged_user_id();
        $companies = $this->mysql()->select(
            'companies',
            [
                'company_id'
            ],
            [
                'deleted' => 0,
                'user_id_fk' => $user_id
            ]
        );

        if ($companies && count($companies) > 1) {
            $this->mysql()->update(
                'companies',
                [
                    'active' => 0,
                ],
                [
                    'user_id_fk' => $user_id
                ]
            );
        }
    }

}