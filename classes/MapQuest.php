<?php

class MapQuest {
    public function get_cities_in_range($city, $country_code, $range) {
        $range = intval($range) * 1000;
        $coords = $this->get_overpass_coordinates($city . " " . $country_code);
        $query = '[timeout:25][out:json];node["place"="town"](around:' . $range . ',' . $coords['lat'] . ',' . $coords['lon'] . ');out;';
        $url = "http://overpass-api.de/api/interpreter?data=" . urlencode($query);

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
        ]);

        $result = json_decode(curl_exec($ch), true);

        $cities = [];
        foreach($result['elements'] as $element) {
            $cities[] = $element['tags']['name'];
        }

        return $cities;
    }

    public function get_overpass_coordinates($address) {
        $url = "https://nominatim.openstreetmap.org/search?q=".$address."&format=json";

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
        ]);

        $result = json_decode(curl_exec($ch), true);

        $data['lat'] = $result[0]['lat'];
        $data['lon'] = $result[0]['lon'];

        return $data;
    }

    public function get_coordinates($address){
        $address = urlencode($address);

        $url = "http://www.mapquestapi.com/geocoding/v1/address?key=".$GLOBALS['mapquest_geocoding_key']."&location=" . $address;

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
        ]);

        $result = json_decode(curl_exec($ch), true);

        if(isset($result['results'][0]['locations'][0]['latLng']['lat']) && isset($result['results'][0]['locations'][0]['latLng']['lng'])){
            return [
                'lat' => $result['results'][0]['locations'][0]['latLng']['lat'],
                'lng' => $result['results'][0]['locations'][0]['latLng']['lng'],
            ];
        }else{
            return false;
        }
    }
}