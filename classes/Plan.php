<?php
class Plan extends Main{
	public function get_plans(){
		$rows = $this->mysql()->select(
			'plans',
			['plan_id', 'plan_name', 'max_jobs', 'max_companies'],
			['ORDER' => ['plan_id' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
			$this->set_data('');
			$this->set_error("Database error");
			return false;
		}elseif(!count($rows)){
			$this->set_data('');
			$this->set_error("No Data");
			return false;
		}else{
			foreach ($rows as $key => $row) {
				$plans[] = $row['plan_name'];
			}
			$this->set_data($plans);
			return $plans;
		}
	}

	public function get_plan_id($plan_code){
		$row = $this->mysql()->get(
			'plans',
			['plan_id'],
			['OR'=>['plan_name'=>$plan_code, 'plan_code'=>$plan_code]]
		);
		if(count($row)){
			return $row['plan_id'];
		}else{
			return 0;
		}
	}

	public function get_plan_code_by_plan_name($plan_name){
		$row = $this->mysql()->get(
			'plans',
			['plan_code'],
			['plan_name' => $plan_name]
		);
		if($this->mysql()->error()[1] || !is_array($row) || !count($row)){
			return false;
		}else{
			return $row['plan_code'];
		}
	}

	public function get_current_plan(){
		if($this->user()->is_logged_user()){
			$subscription = $this->subscription()->get_stored_subscription();
			if($subscription){
				$plan_code = $subscription->items->data[0]->plan->id;
				if($plan_code){
					$row = $this->mysql()->get(
						'plans',
						['plan_id', 'plan_name', 'max_jobs', 'max_companies'],
						['plan_code' => $plan_code]
					);
					$plan = $row['plan_name'];
					$max_jobs = $row['max_jobs'];
                    $max_companies = $row['max_companies'];

					$data = [
					    'plan_id' => $row['plan_id'],
						'plan_name' => $plan,
						'plan_price' => $subscription->items->data[0]->plan->amount / 100,
						'plan_currency' => mb_strtoupper($subscription->items->data[0]->plan->currency),
						'plan_interval' => $subscription->items->data[0]->plan->interval,
						'next_billing_at' => gmdate("Y-m-d", $subscription->current_period_end),
						'current_billing_at' => gmdate("Y-m-d", $subscription->current_period_start),
						'max_active_jobs' => $max_jobs,
                        'max_active_companies' => $max_companies,
					];
				}else{
					$this->set_data('');
					$this->set_error('Get plan error');
					return false;
				}
			}else{
				$this->set_data('');
				$this->set_error($this->user()->get_logged_user_id());

				$plan = 'starter';
				$max_jobs = 1;
                $max_companies = 1;
				$data = [
				    'plan_id' => 1,
					'plan_name' => $plan,
					'plan_price' => 0,
					'plan_currency' => 'EUR',
					'plan_interval' => 'month',
					'next_billing_at' => null,
					'current_billing_at' => null,
					'max_active_jobs' => $max_jobs,
                    'max_active_companies' => $max_companies,
				];
			}
			$row = $this->mysql()->get(
				'users',
				['register_date'],
				['user_id' => $this->user()->get_logged_user_id()]
			);

			$data['user_registered_at'] = mb_substr($row['register_date'], 8, 2).'-'.mb_substr($row['register_date'], 5, 2).'-'.mb_substr($row['register_date'], 0, 4);

			$this->set_data($data);
			$this->set_error('');
			return $data;
		}else{
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key('User is not logged'));
			return false;
		}
	}

	public function retrieve_plan($plan_code){
		$plan = \Stripe\Price::retrieve($plan_code);
		return $plan;
	}

}
?>