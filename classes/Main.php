<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Medoo.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/PHPMailer/Exception.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/PHPMailer/PHPMailer.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/PHPMailer/SMTP.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Locale.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Subscription.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Plan.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Job.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Mail.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Company.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Stats.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/MapQuest.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Stripe/vendor/autoload.php';

use Medoo\Medoo;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use SmarketerProject\Locale;
use Stripe\StripeClient;

class Main {
	private $medoo;
	private $user;
	private $company;
	private $locale;
	private $mail;
	private $PHPMailer;
	private $job;
	private $catalog;
	private $subscription;
	private $plan;
	private $contact;
	private $stats;
	private $mapquest;


	public static $stripe = null;
	public static $input_data = '';
	public static $ajax_error = '';
	public static $ajax_data = '';


	public function __construct() {

	}

	public function init_mysql(){
		if(empty($GLOBALS['medoo'])){
			$GLOBALS['medoo'] = new Medoo([
				'database_type' => 'mysql',
				'database_name' => $GLOBALS['mysql_db'],
				'server' => $GLOBALS['mysql_host'],
				'username' => $GLOBALS['mysql_user'],
				'password' => $GLOBALS['mysql_pass'],
				'charset' => 'utf8',
				'prefix' => '',
			]);
		}
		$this->medoo = &$GLOBALS['medoo'];
	}

    /**
     * @return Medoo
     */
    public function mysql() {
		$this->init_mysql();
		return $this->medoo;
	}

	public function user(){
		if(empty($this->user)){
			$this->user = new User();
		}
		return $this->user;
	}

	public function company() {
	    if (empty($this->company)) {
	        $this->company = new Company();
        }
	    return $this->company;
    }

	public function job(){
		if(empty($this->job)){
			$this->job = new Job();
		}
		return $this->job;
	}

	public function get_plan_by_id($plan_id) {
	    $plan = $this->mysql()->get(
	        'plans',
            ['plan_id', 'plan_name', 'plan_code', 'max_jobs', 'max_companies'],
            ['plan_id' => $plan_id]
        );

	    return $plan;
    }

	public function plan(){
		if(empty($this->plan)){
			$this->plan = new Plan();
		}
		return $this->plan;
	}

	public function subscription(){
		if(empty($this->subscription)){
			$this->subscription = new Subscription();
		}
		return $this->subscription;
	}

    public function contact(){
        if(empty($this->contact)){
            $this->contact = new Contact();
        }
        return $this->contact;
    }

	public function catalog(){
		if(empty($this->catalog)){
			$this->catalog = new Catalog();
		}
		return $this->catalog;
	}

	public function locale(){
		if(empty($this->locale)){
			$this->locale = new Locale();
		}
		return $this->locale;
	}

	public function stats() {
	    if (empty($this->stats)) {
	        $this->stats = new Stats();
        }
	    return $this->stats;
    }

    public function mapquest() {
        if (empty($this->mapquest)) {
            $this->mapquest = new MapQuest();
        }
        return $this->mapquest;
    }

	public function mail(){
		if(empty($this->mail)){
			$this->mail = new Mail();
		}
		return $this->mail;
	}

	public function PHPMailer(){
		if(empty($this->PHPMailer)){
			$this->PHPMailer = new PHPMailer(true);
		}
		return $this->PHPMailer;
	}


	public function send_email($to, $subject, $message){
		$mail = new PHPMailer(true);
		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = $GLOBALS['mail_smtp_host'];                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = $GLOBALS['mail_smtp_user_name'];                     // SMTP username
			$mail->Password   = $GLOBALS['mail_smtp_password'];                               // SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = $GLOBALS['mail_smtp_port'];                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

			//Recipients
            $mail->setFrom($GLOBALS['mail_from_email'], $GLOBALS['mail_from_name']);
            $mail->addAddress($to);     // Add a recipient

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->AltBody = '';

            $mail->send();
            $this->set_data("Message has been sent");
            $this->set_error("");
            return true;
        } catch (Exception $e) {
            $this->set_data("");
            $this->set_error("Message could not be sent.");
            return false;
        }
	}

	public function set_data($data){
		self::$ajax_data = $data;
	}

	public function set_error($msg){
		self::$ajax_error = $msg;
	}

	public function set_input_data($data){
		self::$input_data = $data;
	}
	public function get_input_data(){
		return self::$input_data;
	}

    /**
     * @return StripeClient
     */
    public function stripe(){
		return self::$stripe;
	}

	public function set_stripe($stripe){
		self::$stripe = $stripe;
	}

	public function get_data(){
		return self::$ajax_data;
	}
	public function get_error(){
		if(mb_substr(self::$ajax_error, 0, 2) == ', '){
			return mb_substr(self::$ajax_error, 2);
		}else{
			return self::$ajax_error;
		}
	}
	public function log($data, $log_file = 'general.log'){
		$data = print_r($data, true);
		$f = fopen($_SERVER['DOCUMENT_ROOT'].'/logs/'.$log_file, "a");
		fwrite($f, date("Y-m-d H:i:s : \t").$data."\n================================\n");
		fclose($f);
	}

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return null;
        }

        return $text;
    }
}
?>