<?php

use Stripe\Checkout\Session;
use Stripe\Coupon;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\PromotionCode;
use Stripe\Stripe;
use Stripe\StripeClient;
use Stripe\TaxRate;
use Stripe\Webhook;

class Subscription extends Main {

	private $stored_subscription = false;

	public function __construct(){
		$this->stripe_setup();
		parent::__construct();
	}

    /**
     * @throws ApiErrorException
     * @throws Google_Exception
     */
    public function input(){
		$data = json_decode(file_get_contents("php://input"), true);
		if(isset($data['mode'])){
			if($data['mode'] == 'setup'){
				echo $this->get_public_key();
			}elseif($data['mode'] == 'subscribe'){
				$this->subscribe($data);
			}
		}else{
			echo json_encode(['error'=>'no mode']);
		}
	}

	public function stripe_setup(){
		Stripe::setApiKey($GLOBALS['stripe_secret_key']);
		$this->set_stripe(new StripeClient($GLOBALS['stripe_secret_key']));
	}

	public function get_public_key(){
		return json_encode([
			'public_key'=>$GLOBALS['stripe_public_key'],
		]);
	}

    /**
     * @return bool
     * @throws ApiErrorException
     */
    public function payment_success(){
		$this->get_stored_subscription();
		return true;
	}

	public function store_subscription($subscription){
		if(!$this->user()->is_logged_user()){
			return false;
		}
		$plan_code = $subscription->items->data[0]->plan->id;
		$row = $this->mysql()->get(
			'plans',
			['plan_id', 'plan_name'],
			['plan_code' => $plan_code]
		);
		$plan_id = $row['plan_id'];


		$this->mysql()->update(
			'users',
			[
				'plan_id_fk' => $plan_id,
				'stripe_user_id' => $subscription->customer,
				'stripe_subscription' => json_encode($subscription),
			],
			['user_id' => $this->user()->get_logged_user_id()]
		);
		if($this->mysql()->error()[1]){
			return false;
		}else{
			return true;
		}

	}

    /**
     * @param null $stripe_customer_id
     * @return bool
     * @throws ApiErrorException
     */
    public function get_stored_subscription($stripe_customer_id = null){
		if(!$stripe_customer_id){
			$row = $this->mysql()->get(
				'users',
				['stripe_user_id'],
				['user_id' => $this->user()->get_logged_user_id()]
			);
			$stripe_customer_id = $row['stripe_user_id'];
		}

		if(!$stripe_customer_id){
			$stripe_customer_id = $this->get_stripe_customer_id();
		}
		if(!$stripe_customer_id){
			return false;
		}
		if($this->stored_subscription && $this->stored_subscription != ''){
			return $this->stored_subscription;
		}
		$res = \Stripe\Subscription::all([
			'customer' => $stripe_customer_id,
			'status' => 'active'
		]);
		if(count($res['data'])){
			$this->stored_subscription = $res['data'][count($res['data']) - 1];
			$this->store_subscription($this->stored_subscription);
			return $this->stored_subscription;
		}else{
			return false;
		}
	}

    /**
     * @param $subscription_id
     * @param $plan_code
     * @param $coupon_id
     * @return bool
     * @throws ApiErrorException
     */
    private function update_subscription($subscription_id, $plan_code, $coupon_id){
        /** @var stdClass $subscription */
		$subscription = $this->stored_subscription;

		$parameters = [
			'cancel_at_period_end' => false,
			'billing_cycle_anchor' => 'now',
			'proration_behavior' => 'create_prorations',
			'items' => [
				[
					'id' => $subscription->items->data[0]->id,
					'price' => $plan_code,
				],
			],
			'default_tax_rates' => [
				$GLOBALS['stripe_tax_rate_id'],
			],
		];
		if($coupon_id){
			$parameters['coupon'] = $coupon_id;
		}

		$res = \Stripe\Subscription::update($subscription->id, $parameters);

		if(isset($res->id) && $res->id == $subscription->id){
			$this->store_subscription($res);
			return true;
		}else{
			return false;
		}

	}

    /**
     * @param array $data
     * @return bool
     * @throws ApiErrorException
     * @throws Google_Exception
     */
    private function subscribe($data){
		if(!$this->user()->is_logged_user()){
			return false;
		}

        $old_plan_id = $this->plan()->get_current_plan();
		$old_plan_id = $old_plan_id['plan_id'];

        if($data['plan_name'] == 'change_payment_method'){
			$stripe_customer_id = $this->get_stripe_customer_id();

			$parameters = [
				'payment_method_types' => ['card'],
				'mode' => 'setup',
				'success_url' => $GLOBALS['stripe_payment_success'],
				'cancel_url' => $GLOBALS['stripe_payment_cancel'],
			];

			if($stripe_customer_id){
				$parameters['customer'] = $stripe_customer_id;
			}else{
				$parameters['customer_email'] = $this->user()->get_logged_email();
			}
			$session = $this->stripe()->checkout->sessions->create($parameters);
			$session_id = $session->id;// json_decode($session, true);

            $this->mysql()->update(
                'users',
                ['stripe_payment_method_id' => $session_id],
                ['email' => $this->user()->get_logged_email()]
            );

			$this->log('session_id='.$session_id);
			echo json_encode([
				'session_id'=>$session_id,
				'mode'=>'redirectToCheckout'
			]);
		}else{
			$plan_code = $this->plan()->get_plan_code_by_plan_name($data['plan_name']);

			if(isset($data['promocode']) && $data['promocode']){
				$coupon_id = $data['promocode'];
			}else{
				$coupon_id = 0;
			}
			if($plan_code){
				$parameters = [
					'payment_method_types' => ['card'],
					'line_items' => [[
						'price' => $plan_code,
						'quantity' => 1,
					]],
					'subscription_data' => [
						'default_tax_rates' => [$GLOBALS['stripe_tax_rate_id']],
					],
					'mode' => 'subscription',
					'success_url' => $GLOBALS['stripe_payment_success'],
					'cancel_url' => $GLOBALS['stripe_payment_cancel'],
				];

				if($coupon_id){
					$parameters['subscription_data']['coupon'] = $coupon_id;
				}

				$stripe_customer_id = $this->get_stripe_customer_id();
				$subscription_id = $this->get_stored_subscription($stripe_customer_id) ? $this->get_stored_subscription($stripe_customer_id)['id'] : false;

				if($subscription_id){
					$res = $this->update_subscription($subscription_id, $plan_code, $coupon_id);
					if($res){
						echo json_encode([
							'mode'=>'subscription ok',
							'url'=>$GLOBALS['stripe_payment_success'],
						]);
						$current_plan_id = $this->plan()->get_plan_id($plan_code);
                        $this->check_plan_capacity($current_plan_id, $old_plan_id);
					}else{
						echo json_encode(['mode'=>'error']);
					}
					exit();
				}else{
					$stripe_user_id = $this->user()->get_stripe_user_id($this->user()->get_logged_user_id());
					if(isset($this->user()->get_stripe_payment_method($stripe_user_id)->id)){
						$payment_method_id = $this->user()->get_stripe_payment_method($stripe_user_id)->id;
					}else{
						$payment_method_id = 0;
					}

					$items = [
						['price' => $plan_code],
					];

					if($stripe_user_id && $payment_method_id){

                        $params = [
                            'customer' => $stripe_user_id,
                            'items' => $items,
                            'default_tax_rates' => [
                                $GLOBALS['stripe_tax_rate_id'],
                            ],
                        ];

                        if($coupon_id){
                            $params['coupon'] = $coupon_id;
                        }

						$res = $this->stripe()->subscriptions->create($params);
						if($res) {
							echo json_encode([
								'mode'=>'subscription ok',
								'url'=>$GLOBALS['stripe_payment_success'],
							]);
                            $current_plan_id = $this->plan()->get_plan_id($plan_code);
                            $this->check_plan_capacity($current_plan_id, $old_plan_id);
						}else{
							echo json_encode(['mode'=>'error']);
						}
						exit();
					}else{
						if($stripe_user_id){
							$parameters['customer'] = $stripe_user_id;
						}else{
							$parameters['customer_email'] = $this->user()->get_logged_email();
						}

						if($data['plan_name'] == 'starter'){
							$data = json_encode([
								'mode' => 'subscription ok',
								'url' => $GLOBALS['stripe_payment_success'],
							]);
                            $current_plan_id = $this->plan()->get_plan_id($plan_code);
                            $this->check_plan_capacity($current_plan_id, $old_plan_id);
						}else{
							$session = Session::create($parameters);
							$session_id = $session->id;
							$data = json_encode([
								"session_id" => $session_id,
								"mode" => "redirectToCheckout"
							]);
						}

						echo $data;
						exit();
					}
				}
			}else{
				echo json_encode(['error'=>'plan not exists']);
				exit();
			}

		}
        return false;
	}

    /**
     * @param $current_plan_id
     * @param $old_plan_id
     * @throws Google_Exception
     */
    private function check_plan_capacity($current_plan_id, $old_plan_id) {
	    $current_plan = $this->plan()->get_plan_by_id($current_plan_id);
        $old_plan = $this->plan()->get_plan_by_id($old_plan_id);

        if ($current_plan['max_jobs'] < $old_plan['max_jobs']) {
            $this->company()->deactivate_all();
            $this->job()->deactivate_all();
        }
    }

	public function check_stripe_customer_id($data = null){
		if(!$data){
		    /** @var array $data */
			$data = $this->get_input_data();
			$stripe_customer_id = $data['stripe_customer_id'];
		}else{
			$stripe_customer_id = $data;
		}
		try{
			$customer = Customer::retrieve($stripe_customer_id);
			$this->set_data($customer);
			if(!isset($customer['deleted']) || !$customer['deleted']){
				return true;
			}else{
				return false;
			}
		}catch(Exception $e){
			$this->set_data('');
			$this->set_error("Customer not found");
			return false;
		}
	}

    /**
     * @return mixed
     * @throws ApiErrorException
     */
    private function create_stripe_customer(){
		$res = Customer::create([
			'email' => $this->user()->get_logged_email(),
            'preferred_locales' => $this->get_preferred_locale(),
		]);

		$this->mysql()->update(
			'users',
			['stripe_user_id' => $res['id']],
			['email' => $this->user()->get_logged_email()]
		);
		return $res['id'];
	}

    /**
     * @return mixed|null
     * @throws ApiErrorException
     */
    private function get_stripe_customer_id(){
		if($this->user()->is_logged_user()){
			$email = $this->user()->get_logged_email();
			$row = $this->mysql()->get(
				'users',
				['stripe_user_id'],
				['email' => $email]
			);
			if(count($row) && $row['stripe_user_id'] && $this->check_stripe_customer_id($row['stripe_user_id'])){
				return $row['stripe_user_id'];
			} else {
				$res = Customer::all([
					'email' => $email
				]);
				if(isset($res->data[0])){
				    /** @var Customer $customer */
				    $customer = $res->data[0];
				    Customer::update($customer->id, [
				        'preferred_locales' => $this->get_preferred_locale(),
                    ]);
					return $res->data[0]->id;
				}else{
					return $this->create_stripe_customer();
				}
			}
		}
		return false;
	}

	private function cancel_subscription_at_term_end($subscription){
		try{
			return $this->stripe()->subscriptions->update(
				$subscription->id,
				['cancel_at_period_end' => true]
			);
		}catch(Exception $e){
			return false;
		}
	}

    /**
     * @return bool
     * @throws ApiErrorException
     */
    public function cancel(){
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key('User is not logged'));
			return false;
		}
		$subscription = $this->get_stored_subscription();
		if($this->cancel_subscription_at_term_end($subscription)){
			$this->set_data('success');
		}else{
			$this->set_data('fail');
		}
	}

    /**
     * @return bool
     * @throws ApiErrorException
     */
    public function restore(){
		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key('User is not logged'));
			return false;
		}
		$subscription = $this->get_stored_subscription();
		try{
			$this->stripe()->subscriptions->update(
				$subscription->id,
				['cancel_at_period_end' => false]
			);
			$this->set_data('success');
		}catch(Exception $e){
			$this->set_data('fail');
		}

	}

    /**
     * @throws ApiErrorException
     */
    public function retrieve(){
		$this->set_data($this->get_stored_subscription());
	}

    /**
     * @throws ApiErrorException
     */
    public function cancel_1(){
		$subscription = $this->get_stored_subscription();
		try{
			$this->stripe()->subscriptions->update(
				$subscription->id,
				['cancel_at' => time()+60]
			);
			$this->set_data('success');
		}catch(Exception $e){
			$this->set_data('fail');
		}
	}

    /**
     * @throws ApiErrorException
     */
    public function cancel_2(){
		$subscription = $this->get_stored_subscription();
		try{
			$this->stripe()->subscriptions->update(
				$subscription->id,
				['cancel_at' => time()+120]
			);
			$this->set_data('success');
		}catch(Exception $e){
			$this->set_data('fail');
		}
	}

    /**
     * @throws ApiErrorException
     */
    public function get_details(){
		$subscription = $this->get_stored_subscription();
		if($subscription){
			$data = [
				'subscription_id' => $subscription->id,
				'current_period_end' => gmdate("Y-m-d H:i:s", $subscription->current_period_end),
			];

			if($subscription->canceled_at){
				$data['cancel_at'] = gmdate("Y-m-d H:i:s", $subscription->canceled_at);
			}else{
				$data['cancel_at'] = null;
			}

		}

        $data = array_merge($data, $this->plan()->get_current_plan());
        $this->set_data($data);
	}

	private function subscribe_to_starter(){
		if(!$this->user()->is_logged_user()){
			return false;
		}

		$this->stripe()->subscriptions->create([
			'customer' => $this->user()->get_data()['stripe_user_id'],
			'items' => [
				['price' => $this->plan()->get_plan_code_by_plan_name('starter')],
			]
		]);
		return true;
	}

	public function all_payment_methods(){
		$res = $this->stripe()->paymentMethods->all([
			'customer' => $this->user()->get_data()['stripe_user_id'],
			'type' => 'card',
		]);

		$this->set_data($res);
	}

    /**
     * @return bool
     * @throws ApiErrorException
     * @throws Google_Exception
     */
    public function webhooks(){
		$input = file_get_contents("php://input");

		try{
			$event = Webhook::constructEvent(
				$input, $_SERVER['HTTP_STRIPE_SIGNATURE'], $GLOBALS['stripe_webhook_secret_key']
			);
		}catch(Exception $e){
			http_response_code(400);
			$this->log("webhook error", 'webhooks.log');
			exit();
		}

		switch ($event->type){
			case 'customer.subscription.updated':
			case 'customer.subscription.created':
			case 'customer.subscription.deleted':
			// case 'customer.subscription.pending_update_applied':
			// case 'customer.subscription.pending_update_expired':
				$subscription = $event->data->object;
				$this->log($subscription, 'webhooks.log');
				$stripe_user_id = $subscription->customer;

				if(!$this->user()->get_user_id_for_stripe_user_id($stripe_user_id)){
					$stripe_customer = Customer::retrieve(
						$stripe_user_id,
						[]
					);
					$email = $stripe_customer->email;
					$this->mysql()->update(
						'users',
						['stripe_user_id' => $stripe_user_id],
						['email' => $email]
					);
				}

				if($subscription->status == 'unpaid' || $subscription->status == 'canceled'){
					$this->mysql()->update(
						'users',
						['plan_id_fk' => 1, 'stripe_subscription' => null],
						['stripe_user_id' => $stripe_user_id]
					);
					$this->job()->deactivate_all();
					$this->company()->deactivate_all();
				} else {
					$current_plan_id = $this->user()->get_current_plan($this->user()->get_logged_user_id());
                    $plan_id = $this->plan()->get_plan_id($subscription->plan->id);

					$this->mysql()->update(
						'users',
						['plan_id_fk'=>$plan_id, 'stripe_subscription' => json_encode($subscription)],
						['stripe_user_id' => $stripe_user_id]
					);

					if ($current_plan_id !== $plan_id) {
                        $this->job()->deactivate_all();
                        $this->company()->deactivate_all();
                    }
				}

				if($event->type !== 'customer.subscription.deleted'){
					$payment_method_id = $this->user()->get_stripe_payment_method($stripe_user_id)->id;
					$this->stripe()->customers->update(
						$stripe_user_id,
						[
							'invoice_settings' => [
								'default_payment_method' => $payment_method_id,
							],
						]
					);
				}

				break;
			case 'checkout.session.completed':
				$obj = $event->data->object;

				$setup_intent_id = $obj->setup_intent;
				$customer_id = $obj->customer;

				$setup_intent = $this->stripe()->setupIntents->retrieve(
					$setup_intent_id,
					[]
				);
				$payment_method_id = $setup_intent->payment_method;
				$this->log($payment_method_id, 'checkout.log');
				$this->log($customer_id, 'checkout.log');

				$res = $this->stripe()->customers->update(
					$customer_id,
					[
						'invoice_settings' => [
							'default_payment_method' => $payment_method_id,
						]
					]
				);

				$payment_methods = $this->stripe()->paymentMethods->all([
					'customer' => $customer_id,
					'type' => 'card',
				]);
				foreach ($payment_methods['data'] as $row) {
					if($row->id != $payment_method_id){
						$this->stripe()->paymentMethods->detach(
							$row->id,
							[]
						);
					}
				}

				$this->log($res, 'checkout.log');
				$this->log('end', 'checkout.log');
				break;
			default:
				http_response_code(400);
				exit();
		}

		http_response_code(200);

		return true;
	}

	public function recalculate($plan_name = null, $coupon_id = null){
		$data = $this->get_input_data();

		if(!$this->user()->is_logged_user()){
			$this->set_data('');
			$this->set_error('User is not logged');
			return false;
		}


		if(!$plan_name && isset($data['plan_name'])){
			$plan_name = $data['plan_name'];
		}
		if(!$coupon_id && isset($data['coupon'])){
			$coupon_id = $data['coupon'];
		}

		$user_id = $this->user()->get_logged_user_id();

		$stripe_customer_id = $this->user()->get_stripe_user_id($user_id);
		$stripe_balance = $this->get_stripe_balance($stripe_customer_id) / 100;
		// $this->set_data('sdf');
		// return true;
		$stripe_plan_price = $this->plan()->retrieve_plan($this->plan()->get_plan_code_by_plan_name($plan_name))->unit_amount / 100;
		$tax_rate = $this->get_tax_rate()->percentage;

		if($coupon_id){
			// For use promocodes
			/*$promocode = $this->retrieve_promocode($coupon_id);
			if($promocode){
				if($promocode->active){
					$coupon_id = $promocode->coupon->id;
					try{
						$coupon = $this->retrieve_coupon($coupon_id);
						if($coupon->valid){
							$coupon_amount = $coupon->amount_off / 100;
							$coupon_percent = $coupon->percent_off;
							$coupon_valid = 1;
						}else{
							$coupon_amount = 0;
							$coupon_percent = 0;
							$coupon_valid = 0;
						}
					}catch(Exception $e){
						$coupon_amount = 0;
						$coupon_percent = 0;
						$coupon_valid = 0;
					}
				}else{
					$coupon_amount = 0;
					$coupon_percent = 0;
					$coupon_valid = 0;
				}
			}else*/
			{
				try{
					$coupon = $this->retrieve_coupon($coupon_id);
					if($coupon->valid){
						$coupon_amount = $coupon->amount_off / 100;
						$coupon_percent = $coupon->percent_off;
						$coupon_valid = 1;
					}else{
						$coupon_amount = 0;
						$coupon_percent = 0;
						$coupon_valid = 0;
					}
				}catch(Exception $e){
					$coupon_amount = 0;
					$coupon_percent = 0;
					$coupon_valid = 0;
				}
			}

		}else{
			$coupon_amount = 0;
			$coupon_percent = 0;
			$coupon_valid = 0;
		}

		if($coupon_amount){
			$plan_price_with_coupon = $stripe_plan_price - $coupon_amount;
		}elseif($coupon_percent){
			$coupon_val = round($stripe_plan_price / 100 * $coupon_percent, 2);
			$plan_price_with_coupon = $stripe_plan_price - $coupon_val;
		}else{
			$plan_price_with_coupon = $stripe_plan_price;
		}

		$tax_value = round($plan_price_with_coupon / 100 * $tax_rate, 2);
		$plan_price_with_taxes = $plan_price_with_coupon + $tax_value;


		$total_price = $plan_price_with_taxes + $stripe_balance;
		if($total_price < 0){
			$total_price = 0;
		}

		$this->set_data([
			'balance' => -$stripe_balance,
			'plan_price' => $stripe_plan_price,
			'coupon_valid' => $coupon_valid,
			'coupon_amount' => $coupon_amount,
			'coupon_percent' => $coupon_percent,
			'tax_rate' => $tax_rate,
			'tax_value' => $tax_value,
			'plan_with_taxes' => $plan_price_with_taxes,
			'total_price' => $total_price,
		]);
		$this->set_error('');
		return true;
	}

    /**
     * @param $coupon
     * @return Coupon
     * @throws ApiErrorException
     */
    public function retrieve_coupon($coupon){
		$coupon = Coupon::retrieve($coupon);
		return $coupon;
	}

    /**
     * @return TaxRate
     * @throws ApiErrorException
     */
    public function get_tax_rate(){
		$tax_rate = TaxRate::retrieve($GLOBALS['stripe_tax_rate_id']);
		return $tax_rate;
	}

    /**
     * @param null $stripe_customer_id
     * @return int
     * @throws ApiErrorException
     */
    public function get_stripe_balance($stripe_customer_id = null){
		$customer = Customer::retrieve($stripe_customer_id);
		$balance = $customer->balance;
		return $balance;
	}

	public function retrieve_promocode($promo_name = false){
        /** @var array $data */
        $data = $this->get_input_data();
		if(!$promo_name && isset($data['promo_name'])){
			$promo_name = $data['promo_name'];
		}

		try{
			$promocode = PromotionCode::all(
				[
					'code' => $promo_name,
				]
			);

			if(!count($promocode->data)){
				$this->set_error('No promocode');
				$this->set_data('');
				return false;
			}else{
				$this->set_error('');
				$this->set_data($promocode->data[0]);
				return $promocode->data[0];
			}


		}catch(Exception $e){
			return false;
		}
	}

	public function get_promo_id_by_promo_name($promo_name = null){
        /** @var array $data */
        $data = $this->get_input_data();
		if(!$promo_name && isset($data['promo_name'])){
			$promo_name = $data['promo_name'];
		}

		$promocode = $this->retrieve_promocode($promo_name);

		if($promocode){
			$this->set_data($promocode);
			$this->set_error('');
			return true;
		}else{
			$this->set_data('');
			$this->set_error('No promocode');
			return false;
		}

	}

	public function update_payment_method(){

	}

	public function update_billing_info(){

	}

    /**
     * @throws ApiErrorException
     */
    public function user_invoices() {
	    if ($this->user()->is_logged_user()) {
	        $billing_info = $this->user()->get_billing_info();
	        $billing_info_exists = true;
	        $required_fields = [
                'company_name',
                'first_name',
                'last_name',
                'email',
                'country_code'
            ];
	        foreach($required_fields as $field) {
	            if ($billing_info[$field] === "" || $billing_info[$field] === null) {
                    $billing_info_exists = false;
                }
            }

	        if (!$billing_info_exists) {
	            $this->set_data([]);
	            $this->set_error("");
	            return false;
            }

            $data = [];
            $invoices = $this->stripe()->invoices->all([
                'customer' => $this->user()->get_stripe_user_id(
                    $this->user()->get_logged_user_id()
                )
            ]);

            foreach ($invoices->data as $invoice) {
                $data[] = [
                    'id' => $invoice->id,
                    'date' => ($invoice->created) * 1000,
                    'amount' => ($invoice->amount_paid) / 100,
                    'payment_method' => 'card',
                    'link' => $invoice->invoice_pdf,
                    'currency' => $invoice->currency,
                    'status' => $invoice->status,
                ];
            }
            $this->set_data($data);
            $this->set_error("");
        }
    }

    public function get_preferred_locale() {
        $locale = $this->locale()->get_locale();

        $preferred_locale = null;
        if ($locale === "en") {
            $preferred_locale = ['en-US'];
        } else if ($locale === "de") {
            $preferred_locale = ['de-DE'];
        }
        return $preferred_locale;
    }

}

?>