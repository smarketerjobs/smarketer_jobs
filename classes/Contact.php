<?php

class Contact extends Main {
    public function send(){
        /** @var array $form_data */
        $form_data = $this->get_input_data();
        $invalid_fields = array();

        $required = [
            'firstName',
            'lastName',
            'companyName',
            'phoneNumber',
            'email',
            'webSite',
            'message',
        ];

        foreach($required as $field) {
            if (!isset($form_data[$field])) {
                $invalid_fields[$field] = "The field is required";
            }
        }

        if ($invalid_fields) {
            $this->set_data($invalid_fields);
            $this->set_error(1);
            return false;
        } else {
            $to = $GLOBALS['mail_from_email'];
            $subject = "Contact form Submission";

            $p = "<p>";
            $endp = "</p>";

            $message = "";
            $message .= $p . "Name: " . $form_data['firstName'] . " " . $form_data['lastName'] . $endp;
            $message .= $p . "Company Name: " . $form_data['companyName'] . $endp;
            $message .= $p . "Phone Number: " . $form_data['phoneNumber'] . $endp;
            $message .= $p . "Email: " . $form_data['email'] . $endp;
            $message .= $p . "Website: " . $form_data['webSite'] . $endp;
            $message .= $p . "Message: " . $endp;
            $message .= $form_data['phoneNumber'];

            $this->send_email($to, $subject, $message);
            return true;
        }
    }
}