<?php

/**
 * @param string $str
 * @return array
 */
function get_words($str) {
    $words = explode(' ', $str);
    foreach ($words as $key => $value) {
        $value = trim($value);
        if($value) {
            $words[$key] = $value;
        } else {
            unset($words[$key]);
        }
    }
    return $words;
}

/**
 * @param Main $context
 * @param $is_short
 * @return array
 */
function get_job_search_fields($context, $is_short) {
    if ($is_short) {
        $search_columns = [
            'jobs.job_id',
            'jobs.title',
            'jobs.date_start',
            'jobs.date_end',
            'jobs.date_posted',
            'jobs.type_id_fk(type_id)',
            'companies.company_id',
            'companies.company_logo',
            'companies.company_name',
            'companies.company_slug',
        ];
    } else {
        $search_columns = [
            'jobs.job_id',
            'jobs.company_id_fk',
            'jobs.title',
            'jobs.start_description',
            'jobs.description',
            'jobs.qualifications',
            'jobs.benefits',
            'jobs.final_description',
            'jobs.category_id_fk(category_id)',
            'jobs.industry_id_fk(industry_id)',
            'jobs.employments',
            'jobs.date_start',
            'jobs.date_end',
            'jobs.date_posted',
            'jobs.link',
            'jobs.type_id_fk(type_id)',
            'jobs.location_id_fk(location_id)',
            'jobs.street',
            'jobs.post_code',
            'jobs.city',
            'jobs.lat',
            'jobs.lng',
            'jobs.state_code_fk(state_code)',
            'jobs.country_code_fk(country_code)',
            'jobs.salary_gross',
            'jobs.salary_from',
            'jobs.salary_to',
            'jobs.salary_mode',
            'jobs.salary_period_id_fk(salary_period_id)',
            'jobs.currency_id_fk(currency_id)',
            'jobs.active',
            'jobs.auto_active',
            'jobs.job_view',
            'jobs.job_conversion',
            'jobs.job_share',
            'jobs.job_redirect',
            'users.user_id',
            'users.email',
            'companies.company_id',
            'companies.company_name',
            'companies.company_logo',
            'companies.company_website',
            'companies.company_description',
            'companies.company_slug',
            'countries.country',
            'job_categories.category_title_' . $context->locale()->get_locale() . '(category_title)',
        ];
    }

    $locales = $context->locale()->get_all_locales_list();
    foreach ($locales as $locale) {
        $search_columns[] = 'job_categories.category_slug_'.$locale.'(category_slug_'.$locale.')';
    }

    return $search_columns;
}

/**
 * @param Main $context
 * @param array $jobs
 * @return array
 */
function add_additional_data($context, $jobs) {
    $cities         = [];
    $categories     = [];
    $tmp_categories = [];
    $companies      = [];
    foreach ($jobs as $key => $job) {
        if(array_key_exists('city', $job) && !empty($job['city'])) {
            $cities[] = ucwords(trim($job['city']));
        }

        if(array_key_exists('category_id', $job) && !empty($job['category_id'])){
            $category = [
                'category_id'    => $job['category_id'],
                'category_title' => $job['category_title'],
                'category_slug'  => $job['category_slug_' . $context->locale()->get_locale()],
                'jobs' => 1,
            ];

            if (isset($tmp_categories[$job['category_id']])) {
                $tmp_categories[$job['category_id']]['jobs'] += 1;
            } else {
                $tmp_categories[$job['category_id']] = $category;
            }
        }

        if(array_key_exists('company_name', $job) && !empty($job['company_name'])){
            $company = [
                'company_id'   => $job['company_id'],
                'company_name' => $job['company_name'],
                'company_slug' => $job['company_slug'],
            ];

            if (!in_array($company, $companies)) {
                $companies[] = $company;
            }
        }
    }

    $cities = array_unique($cities);
    sort($cities);

    $countries = $context->catalog()->get_countries(true);
    usort($tmp_categories, function ($cat1, $cat2) {
        if ($cat1['jobs'] == $cat2['jobs']) return 0;
        return $cat1['jobs'] > $cat2['jobs'] ? -1 : 1;
    });

    foreach($tmp_categories as $category_id => $category) {
        $categories[] = $category;
    }

    return [
        'cities' => $cities,
        'categories' => $categories,
        'companies' => $companies,
        'countries' => $countries,
        'length' => count($jobs),
    ];
}