<?php
class Catalog extends Main{
	public function get_currencies($returnable = false){
		$rows = $this->mysql()->select(
			'job_currencies',
			['currency_id', 'currency_code'],
			[
				'ORDER'=>['currency_order'=>'ASC']
			]
		);
		if(count($rows)){
            if ($returnable) {
                return $rows;
            }
			$this->set_data($rows);
		}else{
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("No data"));
		}
		return $rows;
	}

    /**
     * @param $country_code
     * @return false|mixed
     */
    public function get_country_by_country_code($country_code){
		$row = $this->mysql()->get(
			'countries',
			['country_code', 'country'],
			['country_code' => $country_code]
		);

		if(!$this->mysql()->error()[1] && count($row)){
			return $row['country'];
		}else{
			return false;
		}
	}

    /**
     * @param $state_code
     * @return false|mixed
     */
    public function get_state_by_state_code($state_code)
    {
		$row = $this->mysql()->get(
			'states',
			['state_code', 'state'],
			['state_code' => $state_code]
		);
		if(!$this->mysql()->error()[1] && count($row)){
			return $row['state'];
		}else{
			return false;
		}
	}

    /**
     * @param false $with_jobs_only
     * @return array|false
     */
    public function get_countries($with_jobs_only = false){

		if($with_jobs_only){
			$rows = $this->mysql()->select(
				'countries',
				['[><]jobs' => ['country_code' => 'country_code_fk']],
				['country_code', 'country'],
				[
					'jobs.active' => 1,
					'jobs.deleted' => 0,
					'GROUP' => ['country_code', 'country'],
					'ORDER' => ['country' => 'ASC'],
				]
			);
		}else{
			$rows = $this->mysql()->select(
				'countries',
				['country_code', 'country'],
				[
					'ORDER' => ['country' => 'ASC']
				]
			);
		}


		foreach ($rows as $key => $row) {
			if(file_exists(DOCUMENT_ROOT.'/flags/'.strtolower($row['country_code']).'.png')){
				$rows[$key]['flag'] = '/flags/'.strtolower($row['country_code']).'.png';
			}else{
				$rows[$key]['flag'] = null;
			}
		}

		if(count($rows)){
			$this->set_data($rows);
		}else{
			$this->set_data('');
			$this->set_error("No countries");
		}
		return $rows;
	}

	public function get_states(){
        /** @var array $data */
		$data = $this->get_input_data();
		if(isset($data['country_code'])){
			$rows = $this->mysql()->select(
				'states',
				['state_code', 'state'],
				[
					'country_code_fk' => $data['country_code'],
					'ORDER' => ['state'=>'ASC']
				]
			);
			if(count($rows)){
				$this->set_data($rows);
				return $rows;
			}else{
				$this->set_data('');
				$this->set_error("error");
				return false;
			}
		}else{
			$this->set_data('');
			$this->set_error("No country selected");
			return false;
		}
	}

    /**
     * @param false $returnable
     * @return array|false
     */
    public function get_job_categories($returnable = false) {
		$locale = $this->locale()->get_locale();
		$rows = $this->mysql()->select(
			'job_categories',
			['category_id', 'category_title_'.$locale.'(category_title)'],
			['ORDER' => ['category_order' => 'ASC', 'category_title' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->mysql()->error()[1]);
			return false;
		}else{
			if(count($rows)){
                usort($rows, function ($a, $b) {
			       return $a['category_title'] > $b['category_title'];
                });
                if ($returnable) {
                    return $rows;
                }
				$this->set_data($rows);
			}else{
                if ($returnable) {
                    return [];
                }
				$this->set_data('');
				$this->set_error('No data');
				return false;
			}
		}
		return false;
	}

    /**
     * @param false $returnable
     * @return array|false
     */
    public function get_job_employments($returnable = false){
		$locale = $this->locale()->get_locale();
		$rows = $this->mysql()->select(
			'job_employments',
			['employment_id', 'employment_title_'.$locale.'(employment_title)'],
			['ORDER' => ['employment_order' => 'ASC', 'employment_title' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->mysql()->error()[1]);
			return false;
		}else{
			if(count($rows)){
                usort($rows, function ($a, $b) {
                    return $a['employment_title'] > $b['employment_title'];
                });
                if ($returnable) {
                    return $rows;
                }
				$this->set_data($rows);
			}else{
                if ($returnable) {
                    return [];
                }
				$this->set_data('');
				$this->set_error('No data');
				return false;
			}
		}
	}

    /**
     * @param false $returnable
     * @return array|false
     */
    public function get_job_industries($returnable = false){
		$locale = $this->locale()->get_locale();
		$rows = $this->mysql()->select(
			'job_industries',
			['industry_id', 'industry_title_'.$locale.'(industry_title)'],
			['ORDER' => ['industry_order' => 'ASC', 'industry_title' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->mysql()->error()[1]);
			return false;
		}else{
			if(count($rows)){
                if ($returnable) {
                    return $rows;
                }
				$this->set_data($rows);
			}else{
                if ($returnable) {
                    return [];
                }
				$this->set_data('');
				$this->set_error('No data');
				return false;
			}
		}
	}

    /**
     * @param false $returnable
     * @return array|false
     */
    public function get_job_types($returnable = false){
		$locale = $this->locale()->get_locale();
		$rows = $this->mysql()->select(
			'job_types',
			['type_id', 'type_title_'.$locale.'(type_title)'],
			['ORDER' => ['type_order' => 'ASC', 'type_title' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->mysql()->error()[1]);
			return false;
		}else{
			if(count($rows)){
                if ($returnable) {
                    return $rows;
                }
				$this->set_data($rows);
			}else{
                if ($returnable) {
                    return [];
                }
				$this->set_data('');
				$this->set_error('No data');
				return false;
			}
		}
	}

    /**
     * @param false $returnable
     * @return array|false
     */
    public function get_salary_periods($returnable = false){
		$locale = $this->locale()->get_locale();
		$rows = $this->mysql()->select(
			'job_salary_periods',
			['period_id', 'period_title_'.$locale.'(period_title)'],
			['ORDER' => ['period_order' => 'ASC', 'period_title' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->mysql()->error()[1]);
			return false;
		}else{
			if(count($rows)){
                if ($returnable) {
                    return $rows;
                }
				$this->set_data($rows);
			}else{
                if ($returnable) {
                    return [];
                }
				$this->set_data('');
				$this->set_error('No data');
				return false;
			}
		}
	}

    /**
     * @param false $returnable
     * @return array|false
     */
    public function get_job_locations($returnable = false){
		$locale = $this->locale()->get_locale();
		$rows = $this->mysql()->select(
			'job_locations',
			['location_id', 'location_title_'.$locale.'(location_title)'],
			['ORDER' => ['location_order' => 'ASC', 'location_title' => 'ASC']]
		);
		if($this->mysql()->error()[1]){
            if ($returnable) {
                return [];
            }
			$this->set_data('');
			$this->set_error($this->mysql()->error()[1]);
			return false;
		}else{
			if(count($rows)){
                if ($returnable) {
                    return $rows;
                }
				$this->set_data($rows);
			}else{
                if ($returnable) {
                    return [];
                }
				$this->set_data('');
				$this->set_error('No data');
				return false;
			}
		}
	}

    public function get_catalog() {
        $categories = $this->get_job_categories(true);
        $employments = $this->get_job_employments(true);
        $salary_periods = $this->get_salary_periods(true);
        $industries = $this->get_job_industries(true);
        $currencies = $this->get_currencies(true);
        $locations = $this->get_job_locations(true);
        $types = $this->get_job_types(true);

        $this->set_data([
            'categories' => $categories,
            'employments' => $employments,
            'salary_periods' => $salary_periods,
            'industries' => $industries,
            'currencies' => $currencies,
            'locations' => $locations,
            'types' => $types
        ]);
    }
}
?>