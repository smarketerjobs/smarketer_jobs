<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Main.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/User.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Job.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Catalog.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Contact.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Stats.php';

class Ajax extends Main{
	// private $ajax_error = '';
	// private $ajax_res = '';

	public function __construct($data){
		$this->set_input_data($data);
		if(!isset($data['model'])){
			$this->set_data('');
			$this->set_error("no model");
		}elseif(!isset($data['mode'])){
			$this->set_data('');
			$this->set_error("no mode");
		}elseif(!method_exists($this, $data['model'].'_model')){
			$this->set_data('');
			$this->set_error($data['model']." model does not exist");
		}else{
			$model_name = $data['model'].'_model';
			$model = $this->$model_name();
			if(!method_exists($model, $data['mode'])){
				$this->set_data('');
				$this->set_error($data['mode']." mode does not exist");
			}else{
				if(!isset($data['locale'])){
					$data['locale'] = $GLOBALS['default_locale'];
				}
				$mode = $data['mode'];
				$model->$mode();
				// $this->set_error($model->get_error());
				// $this->ajax_res($model->get_res());
			}
		}
	}


	private function user_model(){
		return $this->user();
	}
	private function company_model() {
	    return $this->company();
    }
	private function locale_model(){
		return $this->locale();
	}
	private function job_model(){
		return $this->job();
	}
	private function catalog_model(){
		return $this->catalog();
	}
	private function plan_model(){
		return $this->plan();
	}
	private function subscription_model(){
		return $this->subscription();
	}
    private function contact_model(){
        return $this->contact();
    }
    private function stats_model() {
	    return $this->stats();
    }

	public function get_response(){
		if(mb_strlen(self::$ajax_error) > 2 && mb_substr(self::$ajax_error, 0, 2) == ', '){
			self::$ajax_error = mb_substr(self::$ajax_error, 2);
		}
		$data = array(
			'error' => self::$ajax_error,
			'data' => self::$ajax_data,
		);
		$res = json_encode($data, JSON_UNESCAPED_UNICODE + JSON_UNESCAPED_SLASHES);
		return $res;
	}
}
?>