<?php


use Medoo\Medoo;

class Stats extends Main
{
    public function register($type = null, $user_id = null) {

        $data = $this->get_input_data();

        if (isset($data['type'])) {
            $type = $data['type'];
        }

        if (isset($data['user_id'])) {
            $user_id = $data['user_id'];
        }

        if ($type && $user_id) {

            $this->mysql()->insert(
                'stats_register',
                ['type' => $type, 'user_id' => $user_id, 'date' => gmdate('Y-m-d H:i:s')]
            );

        }

        return;
    }

    public function plans_active() {
        $plans = $this->mysql()->select(
            'plans',
            ['plan_id', 'plan_name']
        );

        if (!$plans) return;

        $plan_row = [];
        foreach($plans as &$plan) {
            $users = $this->mysql()->select(
                'users',
                ['user_id', 'plan_id_fk'],
                ['plan_id_fk' => $plan['plan_id'], 'user_active' => 1]
            );

            $plan['amount_of_users'] = 0;

            if ($users) {
                $plan['amount_of_users'] = count($users);
            }

            $plan_row[$plan['plan_name']] = $plan['amount_of_users'];
        }

        $plan_row['date'] = gmdate('Y-m-d H:i:s');

        $this->mysql()->insert(
            'stats_plan',
            $plan_row
        );

        return;
    }

    public function job_published() {

        $counter = $this->mysql()->get(
            'stats_job_publish',
            ['id', 'jobs']
        );

        if ($counter) {
            $counter_id = $counter['id'];
        } else {

            $jobs = $this->mysql()->select(
                'jobs',
                ['job_id'],
                ['deleted' => 0]
            );

            $this->mysql()->insert(
                'stats_job_publish',
                ['jobs' => count($jobs)]
            );

            $counter_id = $this->mysql()->id();
        }


        $this->mysql()->update(
            'stats_job_publish',
            ['jobs' => $counter['jobs'] + 1],
            ['id' => $counter_id]
        );

        return;
    }

    public function users_with_active_jobs() {
        $users = $this->mysql()->select(
            'users',
            ['[><]jobs' => ['user_id' => 'user_id_fk']],
            ['user_id'],
            [ 'users.user_active' => 1, 'jobs.active' => 1]
        );

        $unique_users = [];

        if ($users) {
            foreach ($users as $user) {
                $unique_users[$user['user_id']] = $user['user_id'];
            }
        }

        $this->mysql()->insert(
            'stats_users_with_jobs',
            ['users' => count($unique_users), 'date' => gmdate('Y-m-d H:i:s')]
        );

        return;
    }

    public function avg_job_duration() {
        $jobs = $this->mysql()->select(
            'jobs',
            ['job_id', 'date_start'],
            ['deleted' => 0, 'active' => 1]
        );

        if (!$jobs) return;

        $jobs_count = count($jobs);
        $now = time();
        $total_duration = 0;

        foreach($jobs as $job) {
            date_default_timezone_set('UTC');
            $date_start = strtotime($job['date_start']);
            if (!$date_start) {
                $jobs_count--;
                continue;
            }
            $duration = $now - $date_start;
            $total_duration += $duration;
        }

        $avg = $total_duration / $jobs_count;

        $this->mysql()->insert(
            'stats_avg_duration',
            ['avg_duration' => $avg, 'date' => gmdate('Y-m-d H:i:s')]
        );

        return;
    }


    public function cron_stats() {
        /** update plans data */
        $this->plans_active();

        /** update users with active jobs */
        $this->users_with_active_jobs();

        /** calculate avg duration of active jobs */
        $this->avg_job_duration();


        $plans = $this->mysql()->select(
            'plans',
            ['plan_name']
        );

        foreach($plans as $key => $plan) {
            $plans[$key] = $plan['plan_name'];
        }

        $rows = [];
        $plans_active = $this->mysql()->select(
            'stats_plan',
            array_merge($plans, ['date'])
        );

        foreach($plans_active as $active) {
            $date = $active['date'];

            $row = [gmdate('Y-m-d', strtotime($date))];
            unset($active['date']);
            $row = array_merge($row, $active);

            $registrations = $this->mysql()->select(
                'stats_register',
                ['type'],
                ['date[~]' => gmdate('Y-m-d', strtotime($date))]
            );

            $row[] = "";
            $row[] = "";

            $types = ['email' => 0, 'google' => 0, 'linkedin' => 0, 'total' => 0];
            foreach($registrations as $registration) {
                $type = $registration['type'];
                $types[$type]++;
                $types['total'] += 1;
            }

            $row = array_merge($row, $types);

            $row[] = "";
            $row[] = "";

            $job_publish = $this->mysql()->get(
                'stats_job_publish',
                'jobs'
            );

            if (!$job_publish) {
                $job_publish = 0;
            }

            $row[] = $job_publish;

            $users_with_jobs = $this->mysql()->get(
                'stats_users_with_jobs',
                'users',
                ['date[~]' => gmdate('Y-m-d', strtotime($date))]
            );

            $row[] = $users_with_jobs;


            $avg_job_duration = $this->mysql()->get(
                'stats_avg_duration',
                'avg_duration',
                ['date[~]' => gmdate('Y-m-d', strtotime($date))]
            );

            $row[] = $this->seconds_to_days($avg_job_duration);

            $rows[] = $row;
        }

        $csv = [
            ['Date', 'Plans', '', '', '', '', '', '', '', '', '', 'Registrations', '', '', '', '', '', 'Jobs'],
            array_merge(
                array_merge([''], array_values($plans)),
                array_merge(['', '', 'email', 'google', 'linkedin', 'total'], ['', '', 'New Jobs Added', 'Users with active jobs', 'Avg Job Duration'])
            ),
        ];

        foreach($rows as $row) {
            $csv[] = $row;
        }

        $out = fopen('media/stats.csv', 'w');
        foreach ($csv as $fields) {
            fputcsv($out, $fields);
        }
        fclose($out);
        exit;
    }

    private function seconds_to_days($inputSeconds): string
    {
        return number_format($inputSeconds / 60 / 60 / 24, 1, ".", "");
    }

    public function upload_stats() {
        $data = $this->get_input_data();

        if (is_array($data) && array_key_exists('p', $data) && !empty($data['p']) && $data['p'] === "8Gn9kUyrC2dA7EHC") {
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename="stats.csv"');
            header("Content-Length: " . filesize("media/stats.csv"));

            $fp = fopen('media/stats.csv', 'r');
            fpassthru($fp);
            fclose($fp);
            exit;
        } else {
            echo "Incorrect password.";
        }
    }
}