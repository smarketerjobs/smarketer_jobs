<?php
namespace SmarketerProject;

class Locale extends \Main{

	public function set($data=null){
		if(!$data){
			$data = $this->get_input_data();
		}
		if(isset($data['locale'])){
			$_COOKIE['locale'] = $data['locale'];
		}else{
			$_COOKIE['locale'] = $GLOBALS['default_locale'];
		}
		$this->set_data($_COOKIE['locale']);
	}

	public function get_locale(){
		if(isset($_COOKIE['locale'])){
			return $_COOKIE['locale'];
		}else{
			return $GLOBALS['default_locale'];
		}
	}

	public function get(){
		self::set_data($this->get_locale());
	}



	public function get_by_key($keyIn=null){
		if(!$keyIn){
		    /** @var array $data */
			$data = $this->get_input_data();
			$key = $data['key'];
		}else{
			$key = $keyIn;
		}
		$val = '';
		if(file_exists(DOCUMENT_ROOT.'/locales/'.$this->get_locale().'.php')){
			include DOCUMENT_ROOT.'/locales/'.$this->get_locale().'.php';
			if(isset($locale[$key])){
				$val = $locale[$key];
			}
		}
		if(!$val && file_exists(DOCUMENT_ROOT.'/locales/'.$GLOBALS['default_locale'].'.php')){
			include DOCUMENT_ROOT.'/locales/'.$GLOBALS['default_locale'].'.php';
			if(isset($locale[$key])){
				$val = $locale[$key];
			}else{
				$val = $key;
			}
		}
		if(!$keyIn){
			$this->set_data($val);
		}else{
			return $val;
		}
	}

	public function get_all(){
		$data = $this->get_input_data();
		if(isset($data['locale'])){
			$locale = $data['locale'];
		}else{
			$locale = $this->get_locale();
		}

		if(file_exists(DOCUMENT_ROOT.'/locales/'.$locale.'.php')){
			include DOCUMENT_ROOT.'/locales/'.$locale.'.php';
			$this->set_data($locale);
			$this->set_error('');
		}else{
			$this->set_data('');
			$this->set_error($this->locale()->get_by_key("Locale does not exist"));
		}
	}

	public function get_all_locales_list() {
        $locales = glob(DOCUMENT_ROOT.'/locales/*.php');
        foreach($locales as $key => $locale) {
            $locales[$key] = basename($locale, '.php');
        }
        return $locales;
	}

}
?>