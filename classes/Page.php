<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Main.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/User.php';
class Page extends Main{
	private $page_template;
	private $page_data;
	private $page_url;

	public $job_id;
	public $user_id;

	public function __construct(){
		$this->init_page();
	}

	public function init_page(){
		$this->init_mysql();
		$full_url=$_SERVER['REQUEST_URI'];
		if(strpos($full_url,'?')){
			$page_url=mb_substr($full_url,0,strpos($full_url,'?'));
		}else{
			$page_url=$full_url;
		}
		$this->page_url = $page_url;

		if(mb_substr($page_url, -3, 1) == '/'){
			$locale = mb_substr($page_url, -2);
			if(file_exists(DOCUMENT_ROOT.'/locales/'.$locale.'.php')){
				$this->locale()->set(['locale'=>$locale]);
				$page_url = mb_substr($page_url, 0, -3);
			}else{
				// use geo location for locale ?
			}
		}

		if(isset($_POST) && count($_POST)){
			$data = $_POST;
		}else if(isset($_GET) && count($_GET)){
			$data = $_GET;
		}else{
			$data = array();
		}
		if(count($data)){
			$this->set_input_data($data);
		}

		$this->page_template = 'indexApp.php';
		switch ($page_url) {
			case '/':
			// 	$this->page_template = 'main.php';
				break;
			case '/job':
				// 	$this->page_template = 'blocks/job.php';
				break;
			case '/job-add':
				if (!$this->user()->is_logged_user()) {
					header("Location:/");
					exit();
				}
				// $this->page_template = 'blocks/job_ad.php';
				break;
			case '/search':
				//     $this->page_template = 'blocks/search.php';
				break;
            case '/job-search':
            //     $this->page_template = 'blocks/job-search.php';
                break;
            case '/company-info':
            //     $this->page_template = 'blocks/company_info.php';
                break;
			case '/google_login':
				$this->user()->google_login();
				header("Location:/");
				exit();
			case '/linkedin_login':
				$this->user()->linkedin_login();
				header("Location:/");
				exit();
			case '/send_email_queue':
				$this->mail()->send_queue();
				break;
			case '/subscription-test':
//				$this->page_template = 'subscription-test.php';
				break;
			case '/subscription':
				$this->subscription()->input();
				exit();
			case '/payment-success':
				$this->subscription()->payment_success();
				// $this->page_template = 'payment-success.php';
				break;
			case '/payment-cancel':
				// $this->page_template = 'payment-cancel.php';
				break;
			case '/create-checkout-session':
				$this->subscription()->create_checkout_session();
				exit();
			case '/webhooks':
				$this->subscription()->webhooks();
				exit();
			case '/coord':
				$this->mapquest()->get_coordinates($_GET['address']);
				exit();
			case '/send_contacts':
			// 	$this->page_template = 'send_contacts.php';
				break;
			case '/privacy-en':
			// 	$this->page_template = 'privacyEn.php';
				break;
			case '/privacy-de':
			// 	$this->page_template = 'privacyDe.php';
				break;
			case '/imprint-en':
			// 	$this->page_template = 'imprintEn.php';
				break;
			case '/imprint-de':
			// 	$this->page_template = 'imprintDe.php';
				break;
			case '/sitemap.xml':
				$this->job()->get_sitemap();
				exit();
			case '/get_stats':
				$this->stats()->upload_stats();
				exit();
			default:
				$user_page = $this->user()->is_company_page($page_url);
				$job_page = $this->job()->is_job_page($page_url);
				if($page_url == '/overview' && isset($_GET['company_id']) && isset($_GET['job_id'])){
					header("HTTP/1.1 301 Moved Permanently");
					header("location: /overview?job_id=".$_GET['job_id']);
					exit();
				}
				if(0 && $page_url == '/overview' && $user_page){
					//show company page
					// echo 'Is company page. user_id is '. $user_page;
					// exit();
				}elseif($page_url == '/overview' && $job_page){
					//show job page
					$this->job_id = $job_page['job_id'];
					// $this->user_id = $job_page['user_id'];
					// $this->page_template = 'job-for-google.php';

					// echo 'Is job page. user_id is '. $job_page['user_id'].', job_id is '.$job_page['job_id'];
				}else{
					if($this->page_url == '/login'){
						if(isset($_GET['ah'])){
							$this->user()->activate_user_via_hash();
						}
					}

					$method_name = 'get_'.mb_substr($page_url, 1).'_page';
					if(method_exists($this, $method_name)){
						$this->page_data = $this->get_page($method_name);
					}else{
						$this->page_data = $this->get_404_page();
						// $this->page_template = 'main.php';
					}
				}
				break;
		}
	}


	public function show_page(){
		$page_data = $this->page_data;
		// echo TEMPLATE_DIR.$this->page_template;
		include TEMPLATE_DIR.$this->page_template;
	}

	private function get_page($method_name){
		ob_start();
		$this->$method_name();
		$data['page_content'] = ob_get_contents();
		ob_end_clean();
		return $data;
	}

	private function get_register_page(){
		if($this->user()->register()) {
			header("Location:/job-add");
		}else{
			//user register fail
		}
		// echo $this->get_error();
		// echo $this->get_data();
		include TEMPLATE_DIR.'blocks/register.php';
	}

	// private function get_password_reminder_page(){
	// 	if(isset($_POST['remind'])){
	// 		// $user = new User();
	// 		$this->user()->remind_password();
	// 	}
	// 	include TEMPLATE_DIR.'blocks/remind_password.php';
	// }

	// private function get_flush_password_page(){
	// 	if(isset($_POST['flush'])){
	// 		// $user = new User();
	// 		$data = $this->user()->flush_password();
	// 	}
	// 	include TEMPLATE_DIR.'blocks/flush_password.php';
	// }

	private function get_login_page(){
		if(isset($_GET['google'])){
			header('location: /profile');
			exit();
		}

		if(isset($_POST['login'])){
			// $user = new User();
			$data = $this->user()->login();

			if($this->user()->is_logged_user()) {
				header('location: /job-add');
				exit();
			}
		}
		include TEMPLATE_DIR.'blocks/login_form.php';
	}

	private function get_profile_page(){
		// $user = new User();
		if($this->user()->is_logged_user()){
			include TEMPLATE_DIR.'blocks/profile.php';
		}else{
			header('location:/');
			exit();
		}
	}

	private function get_logout_page(){
		unset($_SESSION['user_email']);
		unset($_SESSION['user_pass']);
		session_destroy();
		header('location: /');
		exit();
	}

	public function get_main_page(){

	}

	public function get_404_page(){
		header('HTTP/1.0 404 not found');
	}
}


?>
