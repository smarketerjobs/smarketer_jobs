<?php
class Mail extends \Main{
	public function create($from, $to, $subject, $body){
		$this->mysql()->insert(
			'mails',
			['from'=>$from, 'to'=>$to, 'subject'=>$subject, 'body'=>$body, 'status'=>0]
		);

		if($this->mysql()->error()[1] || !$this->mysql()->id()){
			return false;
		}else{
			return $this->mysql()->id();
		}
	}

	public function send_queue($email_id = null){
		if($email_id){
			$rows = $this->mysql()->select(
				'mails',
				['mail_id', 'from', 'to', 'subject', 'body', 'status'],
				[
					'mail_id' => $mail_id,
					'ORDER' => ['mail_id'=>'ASC'],
					'LIMIT' => 2
				]
			);
		}else{
			$rows = $this->mysql()->select(
				'mails',
				['mail_id', 'from', 'to', 'subject', 'body', 'status'],
				[
					'AND' => ['status[<]'=>1, 'status[>]'=>-6],
					'ORDER' => ['mail_id'=>'ASC'],
					'LIMIT' => 2
				]
			);
		}

		if(count($rows)){
			$mail = $this->PHPMailer();

			foreach ($rows as $key => $row) {
				try {
					//Server settings
					// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
					$mail->isSMTP();                                            // Send using SMTP
					$mail->Host       = $GLOBALS['mail_smtp_host'];                    // Set the SMTP server to send through
					$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
					$mail->Username   = $GLOBALS['mail_smtp_user_name'];                     // SMTP username
					$mail->Password   = $GLOBALS['mail_smtp_password'];                               // SMTP password
					$mail->SMTPSecure = $this->PHPMailer()::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
					$mail->Port       = $GLOBALS['mail_smtp_port'];                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

					//Recipients
					$mail->setFrom($GLOBALS['mail_from_email'], $GLOBALS['mail_from_name']);
					$mail->addAddress($row['to']);     // Add a recipient

					// Content
					$mail->isHTML(true);                                  // Set email format to HTML
					$mail->Subject = $row['subject'];
					$mail->Body    = $row['body'];
					$mail->AltBody = '';

					$mail->send();
					$this->mysql()->update(
						'mails',
						['status'=>1, 'error_message'=>''],
						['mail_id'=>$row['mail_id']]
					);
					// echo 'Message has been sent';
					// return true;
				} catch (Exception $e) {
					// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
					// return false;
					$this->mysql()->update(
						'mails',
						['status'=>$row['status']--, 'error_message'=>$mail->ErrorInfo],
						['mail_id'=>$row['mail_id']]
					);
				}

			}
		}
	}

	public function send_now($to, $subject, $body, $attachments = null){
		$mail = $this->PHPMailer();
		$mail->clearAllRecipients();

		try {
			//Server settings
			// $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
			$mail->isSMTP();                                            // Send using SMTP
			$mail->Host       = $GLOBALS['mail_smtp_host'];                    // Set the SMTP server to send through
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = $GLOBALS['mail_smtp_user_name'];                     // SMTP username
			$mail->Password   = $GLOBALS['mail_smtp_password'];                               // SMTP password
			$mail->SMTPSecure = $this->PHPMailer()::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = $GLOBALS['mail_smtp_port'];                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
			$mail->CharSet    = 'UTF-8';

			if($attachments){
				foreach ($attachments as $file => $name) {
					$mail->addAttachment($file, $name);
				}
			}

			//Recipients
			$mail->setFrom($GLOBALS['mail_from_email'], $GLOBALS['mail_from_name']);
			$mail->addAddress($to);     // Add a recipient

			// Content
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
			$mail->Body    = $body;
			$mail->AltBody = '';

			$mail->send();

			// echo 'Message has been sent';
			return true;
		} catch (Exception $e) {
			// echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return false;
			// $this->mysql()->update(
			// 	'mails',
			// 	['status'=>$row['status']--, 'error_message'=>$mail->ErrorInfo],
			// 	['mail_id'=>$row['mail_id']]
			// );
		}
	}
}
?>