const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        indexApp: "./client/index.js"
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 200,
        poll: 1000
    },
    resolve: {
        alias: {
            '~flags': path.join(__dirname, './flags'),
        },
    },
    output: {
        filename: `[name].js?ver=${new Date().getTime()}`,
        publicPath: '/dist/',
        path: path.resolve(__dirname, './dist'),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: {
                                localIdentName: '[name]__[local]___[hash:base64:5]',
                            },
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                loader: 'style-loader!autoprefixer-loader?browsers=["last 2 version", "IE 10"]!',
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg|mp4|jpg)$/,
                use: [
                    {
                        // loader: "file-loader",
                        // options: {
                        //     name: "[path][name].[ext]"
                        // }
                        loader: 'url-loader?limit=1000000',
                        options: {
                            name: '[name].[contenthash].[ext]',
                            outputPath: '/dist',
                            esModule: false // <- here
                        }
                    }
                ]

            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'templates/indexAppTemplate.php',
            filename: '../templates/indexApp.php',
        })
    ]
};
