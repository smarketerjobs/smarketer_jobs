# SMARKETER.JOBS

Server requirements:
- PHP >= 7
- MySQL 8

## Installation

### .htaccess file for apache

```
RewriteEngine On
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
```

Rename ```config-sample.php``` to ```config.php``` and set DB credentials

Import table structures from https://smarketer.fox.ck.ua/tools/phpmyadmin or https://smarketer.jobs/tools/phpmyadmin/index.php.
It may be advantageous to export static table data from these tables to allow full usage of the installation:
- countries
- geo_cities
- job_categories
- job_currencies
- job_employments
- job_industries
- job_locations
- job_salary_periods
- job_types
- plans
- states


## Available Scripts

#### Install Node.js 12+
### `brew install node`

In the project directory, you can run:

### `npm install`
### `npm start dev`
### `npm start build`

Builds the app for production to the `dist` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

#### Install MAMP PRO 5.7+
### `brew install mamp`

Open Mamp Pro, open sidebar menu, switch on Apache and MeSQL. <br/>
Open Settings, Hosts.
Create a new host with name `smarketer`.<br/>

In the right bar menu select `Databases` and `Create new database` with name `smarketer`.<br/>
Click to phpMyAdmin for open phpMyAdmin in the default browser. <br/>
Select `smarketer` folder and import damp from staging app.

Back to MAMP PRO and run servers. <br/>

Open `http://smarketer:8888/` in your browser.

To see the changes in the browser you need to run the build with command 
### `npm start dev` or `npm start build`
after making changes to the project 

### Deploy to firebase

##### Stage

Open terminal, put
 ### `ssh root@smarketer.fox.ck.ua`
end enter password.<br/>

Change folder using
 ### `cd /var/www/smarketer/www/`

Run git command for pull changes
 ### `git pull origin develop`
 
##### Production
