<?php
	include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Ajax.php';
	if(isset($_POST) && count($_POST)){
		$data = $_POST;
	}else if(isset($_GET) && count($_GET)){
		$data = $_GET;
	}else if(file_get_contents("php://input")){
		$data = json_decode(file_get_contents("php://input"), true);
	}else{
		$data = array();
	}

	if(count($data)){
		$ajax = new Ajax($data);
		$res = $ajax->get_response();
		echo $res;
	}
?>