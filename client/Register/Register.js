import React, {useEffect} from 'react';

import axios from "axios";
import ReactGA from "react-ga";
import {NavLink, useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {faAngleRight} from '@fortawesome/fontawesome-free-solid'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Field, Form, Formik} from "formik";
import * as Yup from "yup";

import {fetchUserAccountData, fetchUserId} from "../store/ducks/user";
import {closeModals} from "../store/ducks/modals";
import {showAlert} from "../store/ducks/alert";

import styles from "./Register.module.scss";


const Register = () => {
    const history = useHistory();
    const dispatch = useDispatch();

    const {locale} = useSelector(state => state.localeState);
    const {userId} = useSelector(state => state.userIdState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);
    const {userAccountData, fetching} = useSelector(state => state.userAccountDataState);

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .required(locale['Please enter a correct email format'])
            .email(locale['Please enter a correct email format']),
        password: Yup.string().required(locale['The passwords must be identical']),
        confirm: Yup.string()
            .required(locale['The passwords must be identical'])
            .oneOf([Yup.ref('password')], locale['The passwords must be identical']),
        isAgree: Yup.bool().oneOf([true], locale['Please agree to the terms and conditions and the privacy policy']),
    });

    const initialValues = {
        email: '',
        password: '',
        confirm: '',
        isAgree: false,
    }

    const userRegister = ({email, password, isAgree}) => {
        if (isAgree && email && password) {
            axios.get('/ajax.php', {
                params: {model: 'user', mode: 'register', email: email, password: password},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data?.data) {
                        dispatch(fetchUserId());
                        dispatch(fetchUserAccountData());
                        ReactGA.event({
                            label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                            action: 'registration_e-mail',
                            category: 'Registration'
                        });
                    } else {
                        dispatch(showAlert('failure', 'E-mail already exists'));
                        console.error(data);
                    }
                })
                .catch(err => console.error(err));
        }
    };

    useEffect(() => {
        if (userId) {
            if (!fetching && userAccountData && !userAccountData.first_name) {
                history.push('/required');
                dispatch(closeModals());
            }
            if (userAccountData?.first_name) {
                history.push('/job-add?active=create');
                dispatch(closeModals());
            }
        }
    }, [userAccountData, userId, fetching]);

    return (
        <div className={styles.register}>
            <p className={styles.description}>
                {locale ? locale['Create an account to create or edit job ads.'] : ''}
            </p>
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={values => userRegister(values)}
            >
                {({errors, touched}) => (
                    <Form className={styles.authForm}>
                        <label className={styles.inputLabel}>
                            <div className={styles.inputLabelText}>
                                <span className={styles.inputLabelText}>
                                    {locale ? locale['E-Mail Address'] : ''}
                                </span>
                            </div>
                            <Field
                                name="email"
                                className={`${styles.input} ${errors.email && touched.email ? styles.invalidInput : ''}`}
                            />
                            {errors.email && touched.email ? (
                                <div className={styles.errorInput}>{errors.email}</div>
                            ) : null}
                        </label>
                        <label className={styles.inputLabel}>
                            <div className={styles.inputLabelText}>
                                {locale ? locale['Password'] : ''}
                            </div>
                            <Field
                                name="password"
                                type="password"
                                className={`${styles.input} ${errors.password && touched.password ? styles.invalidInput : ''}`}
                            />
                            {errors.password && touched.password ? (
                                <div className={styles.errorInput}>{errors.password}</div>
                            ) : null}
                        </label>
                        <label className={styles.inputLabel}>
                            <div className={styles.inputLabelText}>
                                {locale ? locale['Confirm Password'] : ''}
                            </div>
                            <Field
                                name="confirm"
                                type="password"
                                className={`${styles.input} ${errors.confirm && touched.confirm ? styles.invalidInput : ''}`}
                            />
                            {errors.confirm && touched.confirm ? (
                                <div className={styles.errorInput}>
                                    {errors.confirm}
                                </div>
                            ) : null}
                        </label>
                        <label className={styles.flexCenter}>
                            <Field
                                name="isAgree"
                                type="checkbox"
                                className={`${styles.checkbox} ${errors.isAgree && touched.isAgree ? styles.invalidInput : ''}`}
                            />
                            <span>
                                {locale ? locale['I agree to the'] : ''}
                                {' '}
                                <a href={locale && locale.lang === 'de'
                                    ? 'https://www.smarketer.de/agb-smarketer-jobs/'
                                    : 'https://www.smarketer.de/general-terms-and-conditions-smarketer-jobs/'}
                                   target="_blank"
                                   className={styles.link}>
                                    {locale ? locale['terms and conditions'] : ''}
                                </a>
                                {' '}
                                {locale ? locale['and the'] : ''}
                                {' '}
                                <NavLink
                                    to={`/privacy-${locale ? locale['lang'] : 'en'}`}
                                    className={styles.link} target="_blank"
                                >
                                    {locale ? locale['privacy policy'] : ''}
                                </NavLink>
                                {locale ? locale['lang'] === 'de' ? ' zu' : '' : ''}
                            </span>
                        </label>
                        {errors.isAgree && touched.isAgree ? (
                            <div className={styles.errorCheckbox}>{errors.isAgree}</div>
                        ) : null}

                        <button type='submit' id='registration_by_email_btn'
                                className={styles.authBtnSubmit}>
                            {locale ? locale['Register now'] : ''}
                            <FontAwesomeIcon icon={faAngleRight} className={styles.submitIcon}/>
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    )
};

export default Register;
