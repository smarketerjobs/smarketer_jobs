import React from 'react';

import styles from './StateicPages.module.scss';

import OverviewPage from "../blocks/OverviewPage/OverviewPage";

const PrivacyEn = () => {
    return <OverviewPage hiddenLangSwitcher={true}>
        <div className={styles.staticPageWrapper}>
            <div>
                <h1 className={styles.mainTitle}>Privacy policy - SMARKETER</h1>

                <p className={styles.text}>Thank you for your interest in our homepage and our company. The protection
                    of your personal data has the highest priority for us, therefore we would like to inform you below
                    how Smarketer GmbH (hereinafter Smarketer) collects, uses, stores, transmits and protects your
                    data.</p>
                <p className={styles.text}>This Privacy Policy was last updated on: 24.05.2018</p>
                <p className={styles.text}>In the event of any changes to this Privacy Policy, we will post the amended
                    policy and the effective date of the amended policy on this website. We encourage you to review this
                    policy periodically. We will only make changes that affect the consent you have given us by
                    obtaining consent again. This data protection declaration is valid from 25.05.2018.</p>

                <h2 className={styles.secondaryTitle}>1. Responsible in the sense of DSGVO</h2>
                <p className={styles.text}>Responsible in the sense of the data protection basic regulation is:</p>
                <p className={styles.text}>Smarketer GmbH</p>
                <p className={styles.text}>Alte Jakobstrasse 83/84</p>
                <p className={styles.text}>10179 Berlin - Germany</p>
                <p className={styles.text}>Phone: +49 30 96 53 51 98 8</p>
                <p className={styles.text}>E-mail: info@smarketer.jobs</p>
                <p className={styles.text}>If you have any questions about data protection issues that are not answered
                    in this privacy statement or if you would like further information, please feel free to write to us
                    at datenschutz@smarketer.de.</p>

                <h2 className={styles.secondaryTitle}>2. General information on data processing</h2>
                <p className={styles.text}>We take the protection of your personal data very seriously. For this reason,
                    we process your data exclusively in accordance with the Basic Data Protection Regulation (DSGVO) and
                    other national data protection laws. This is necessary to provide a functional website. It also
                    enables us to provide you with our content and services.</p>
                <p className={styles.text}>Personal data is always processed with your consent. An exception to this
                    only takes place if consent cannot be obtained for actual reasons and statutory regulations permit
                    data processing.</p>
                <p className={styles.text}>Data processing only takes place to the extent that it is legal. Lawfulness
                    exists if at least one condition specified in Art. 6 para. 1 DSGVO is fulfilled. We delete your data
                    as soon as the purpose of storage no longer applies or a period prescribed by law (usually 7 days)
                    has expired. You have the right at any time to object to data processing with effect for the future.
                    For this purpose, please send us a written declaration of revocation in writing by post or
                    e-mail.</p>
                <p className={styles.text}>Our offer is not aimed at children under the age of 13 and we do not
                    knowingly collect data from children under the age of 13. If, when collecting the data, we discover
                    that this is the data of a child under 13 years of age, we will not process or retain it without the
                    consent of a parent or guardian. However, if we have ever processed a child's information, we will
                    make every effort to remove that information from our systems.</p>

                <h2 className={styles.secondaryTitle}>3.Logfiles</h2>
                <p className={styles.text}>When you visit our website, our system automatically collects data and
                    information from the computer system of your computer. The following data is collected:</p>
                <p className={styles.text}>a. browser types and versions used,</p>
                <p className={styles.text}>b. the operating system used by the accessing system,</p>
                <p className={styles.text}>c. the website from which an accessing system accesses our website (so-called
                    referrer),</p>
                <p className={styles.text}>d. the subwebsites which are accessed via an accessing system on our
                    website,</p>
                <p className={styles.text}>e. the date and time of access to the website,</p>
                <p className={styles.text}>f. a shortened Internet protocol address (anonymous IP address),</p>
                <p className={styles.text}>g. the Internet service provider of the accessing system.</p>
                <p className={styles.text}>This data is stored in the log files of our system. These data are not stored
                    together with other personal data.</p>
                <p className={styles.text}>The data is stored in the log files in order to ensure the functionality of
                    our website. We can also use the data to optimise our website and ensure the security of our
                    information technology systems. An evaluation of the data for marketing purposes does not take place
                    in this context.</p>
                <p className={styles.text}>These purposes also include our legitimate interest in data processing
                    pursuant to Art. 6 Para. 1 lit. f) DSGVO. The stored data will be deleted as soon as their purpose
                    has been fulfilled. If the data is collected to provide the website, its purpose is achieved when
                    the session has ended. Since the collection of the data for the provision of the website and the
                    storage of the data in log files is absolutely necessary for the operation of the website, there is
                    no possibility for you to object.</p>

                <h2 className={styles.secondaryTitle}>4.Cookies</h2>
                <p className={styles.text}>We use so-called "cookies" on our website. These are text files that are
                    stored on your end device. Some of the cookies we use, so-called session cookies, are deleted
                    directly when you close your browser, i.e. end the session. Other cookies continue to be stored on
                    your end device and enable us and our third-party providers to recognize your browser the next time
                    you visit our website (persistent cookies). Cookies are processed to an individual extent and
                    collect data such as browser and location data as well as IP address values. Persistent cookies are
                    automatically deleted after a prescribed period.</p>
                <p className={styles.text}>Cookies are used to make your visit to our website attractive and to enable
                    the use of certain functions. Insofar as the cookies set by us process personal data, processing is
                    lawful pursuant to Art. 6 Para. 1 lit. b) DSGVO either for contractual purposes or pursuant to Art.
                    6 Para. 1 lit. f) DSGVO to safeguard our legitimate interests in a user-friendly website.</p>
                <p className={styles.text}>You will learn more about the cookies collected by third-party providers and
                    the respective type and scope of data processing below (see 5, 6 and 7).</p>
                <p className={styles.text}>In order to prevent the setting of cookies, you can set your browser so that
                    you are informed about the setting of cookies and can decide individually on their acceptance or
                    generally exclude the acceptance of cookies in certain cases. The type of browser setting is
                    different for each browser. You will find a description of the cookie settings for the respective
                    browser under the following links:</p>
                <p className={styles.text}>Internet Explorer:
                    http://windows.microsoft.com/de-DE/windows-vista/Block-or-allow-cookies</p>
                <p className={styles.text}>Firefox: https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen</p>
                <p className={styles.text}>Chrome:
                    http://support.google.com/chrome/bin/answer.py?hl=de&hlrm=en&answer=95647</p>
                <p className={styles.text}>Safari: https://support.apple.com/kb/ph21411?locale=de_DE</p>
                <p className={styles.text}>Opera: http://help.opera.com/Windows/10.20/de/cookies.html</p>
                <p className={styles.text}>Please note that if cookies are not accepted, the functionality of our
                    website may be restricted.</p>

                <h2 className={styles.secondaryTitle}>5. Tracking and advertising</h2>
                <h2 className={styles.secondaryTitle}>5.1 Google Analytics</h2>
                <p className={styles.text}>We use Google Analytics, a web analysis service provided by Google Inc.
                    (https://www.google.de/intl/de/about/) (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA;
                    hereinafter "Google"). Google Analytics uses "cookies", which are text files placed on your device,
                    to help the website analyze how users use the site. The information generated by the cookie such
                    as:</p>
                <p className={styles.text}>a. Browser type/version,</p>
                <p className={styles.text}>b. Operating system used,</p>
                <p className={styles.text}>c. Referrer URL (the previously visited page),</p>
                <p className={styles.text}>d. Host name of the accessing computer (IP address),</p>
                <p className={styles.text}>e. Time of the server request</p>
                <p className={styles.text}>are usually transferred to a Google server in the USA and stored there. IP
                    anonymisation is activated on this website, which means that the IP address of Google users within
                    Member States of the European Union or in other Contracting States to the Agreement on the European
                    Economic Area is shortened beforehand. Only in exceptional cases will the full IP address be
                    transmitted to a Google server in the USA where it will be shortened. On behalf of the operator of
                    this website, Google will use this information for the purpose of evaluating the use of the website
                    by users, compiling reports on website activity and providing other services to website operators
                    relating to website activity and internet usage. The IP address transmitted by your browser as part
                    of Google Analytics is not combined with other data from Google. You can prevent the storage of
                    cookies by setting your browser software accordingly. However, we would like to point out that in
                    this case you may not be able to use all the functions of this website to their full extent. You can
                    also prevent Google from collecting the data generated by the cookie and related to your use of the
                    website (including your IP address) and Google from processing this data by downloading and
                    installing the browser plugin available under the following link:
                    http://tools.google.com/dlpage/gaoptout?hl=de.</p>

                <h2 className={styles.secondaryTitle}>5.2 Google Analytics Remarketing</h2>
                <p className={styles.text}>We have integrated Google Remarketing services on this website. Google
                    Remarketing is a Google AdWords feature that enables a company to display advertisements to Internet
                    users who have previously been on the company's website. The integration of Google Remarketing
                    therefore allows a company to create user-related advertisements and consequently to display
                    advertisements of interest to the Internet user.</p>
                <p className={styles.text}>Google Remarketing services are operated by Google Inc, 1600 Amphitheatre
                    Pkwy, Mountain View, CA 94043-1351, USA.</p>
                <p className={styles.text}>The purpose of Google Remarketing is the display of interest-relevant
                    advertising. Google Remarketing enables us to display advertisements via the Google advertising
                    network or on other websites that are tailored to the individual needs and interests of Internet
                    users.</p>
                <p className={styles.text}>Google Remarketing places a cookie on the IT system of the person concerned.
                    By setting the cookie, Google is able to recognize the visitor to our website who subsequently
                    visits websites that are also members of the Google advertising network. Each time you visit a
                    website on which the Google Remarketing service has been integrated, your Internet browser
                    automatically identifies itself to Google. As part of this technical process, Google obtains
                    knowledge of personal data such as your IP address or surfing behaviour, which Google uses, among
                    other things, to display advertisements of interest.</p>
                <p className={styles.text}>The cookie is used to store personal information, such as the Internet pages
                    visited by you. Accordingly, each time you visit our website, personal data, including your IP
                    address, is transmitted to Google in the United States of America. This personal data is stored by
                    Google in the United States of America. Google may disclose personal data collected through this
                    technical process to third parties.</p>
                <p className={styles.text}>You may refuse the use of cookies by selecting the appropriate settings on
                    your browser, however please note that if you do this you may not be able to use the full
                    functionality of this website. Such a setting of the Internet browser used would also prevent Google
                    from setting a cookie on your IT system. In addition, a cookie already set by Google Analytics can
                    be deleted at any time via the Internet browser or other software programs.</p>
                <p className={styles.text}>Furthermore, you have the possibility to object to the interest-related
                    advertising by Google. To do this, you must call up the link www.google.de/settings/ads from every
                    Internet browser you use and make the desired settings there.</p>
                <p className={styles.text}>Such an evaluation is carried out in particular in accordance with Art. 6
                    para. 1 lit.f DS-GVO on the basis of our legitimate interest in the display of personalised
                    advertising, market research and/or the design of its website to meet requirements.</p>
                <p className={styles.text}>Further information and the valid data protection regulations of Google can
                    be called up under https://www.google.de/intl/de/policies/privacy/</p>

                <h2 className={styles.secondaryTitle}>5.3 Google (AdWords) Remarketing</h2>
                <p className={styles.text}>Our website uses the functions of Google AdWords Remarketing, hereby we
                    advertise this website in the Google search results, as well as on third party websites. The
                    provider is Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA ("Google"). To this
                    end, Google places a cookie in the browser of your terminal device, which automatically enables
                    interest-based advertising using a pseudonymous cookie ID and based on the pages you visit.</p>
                <p className={styles.text}>Processing is based on our legitimate interest in the optimal marketing of
                    our website in accordance with Art. 6 Para. 1 lit. f DS-GVO.</p>
                <p className={styles.text}>Further data processing will only take place if you have agreed to Google
                    linking your internet and app browser history to your Google account and using information from your
                    Google account to personalise advertisements you view on the web. In this case, if you are logged
                    into Google during your visit to our website, Google will use your information in conjunction with
                    Google Analytics data to create and define cross-device remarketing target audience lists. Google
                    will temporarily link your personal data to Google Analytics data in order to create target
                    groups.</p>
                <p className={styles.text}>You can permanently disable the setting of cookies for ad preferences by
                    downloading and installing the browser plug-in available from the following link:
                    https://www.google.com/settings/ads/onweb/</p>
                <p className={styles.text}>Alternatively, you can contact the Digital Advertising Alliance at the
                    Internet address www.aboutads.info to find out about the setting of cookies and configure your
                    settings. Finally, you can set your browser so that you are informed about the setting of cookies
                    and decide individually whether to accept them or whether to exclude the acceptance of cookies in
                    certain cases or in general. If cookies are not accepted, the functionality of our website may be
                    restricted.</p>
                <p className={styles.text}>Google LLC, headquartered in the USA, is certified for the us European
                    Privacy Shield Agreement, which ensures compliance with the data protection level applicable in the
                    EU.</p>
                <p className={styles.text}>Further information and the data protection regulations regarding advertising
                    and Google can be found here: https://www.google.com/policies/technologies/ads/</p>

                <h2 className={styles.secondaryTitle}>5.4 Google AdWords with Conversion Tracking</h2>
                <p className={styles.text}>We have integrated Google AdWords on this website. Google AdWords is an
                    Internet advertising service that allows advertisers to serve ads in both Google's search engine
                    results and the Google advertising network. Google AdWords allows an advertiser to pre-define
                    keywords that will be used to display an ad in Google's search engine results only when the user
                    uses the search engine to retrieve a keyword relevant search result. In the Google advertising
                    network, the ads are distributed to topic-relevant Internet pages using an automatic algorithm and
                    taking into account the previously defined keywords.</p>
                <p className={styles.text}>The operating company of the Google AdWords services is Google Inc, 1600
                    Amphitheatre Pkwy, Mountain View, CA 94043-1351, USA.</p>
                <p className={styles.text}>The purpose of Google AdWords is to promote our website by displaying
                    advertisements of interest on third-party websites and in the search engine results of Google and by
                    displaying third-party advertisements on our website.</p>
                <p className={styles.text}>If you access our website via a Google advertisement, a so-called conversion
                    cookie is stored on your IT system by Google. A conversion cookie loses its validity after thirty
                    days and does not serve to identify you. If the cookie has not yet expired, the conversion cookie is
                    used to track whether certain subpages, such as the shopping cart of an online shop system, have
                    been accessed on our website. The conversion cookie enables both we and Google to track whether a
                    user who came to our website via an AdWords ad generated a turnover, i.e. completed or cancelled a
                    purchase.</p>
                <p className={styles.text}>The data and information collected through the use of the conversion cookie
                    is used by Google to generate visit statistics for our website. These visit statistics are in turn
                    used by us to determine the total number of users who were referred to us via AdWords ads, i.e. to
                    determine the success or failure of the respective AdWords ad and to optimise our AdWords ads for
                    the future. Neither our company nor other Google AdWords advertisers receive any information from
                    Google that could be used to identify you.</p>
                <p className={styles.text}>The conversion cookie is used to store personal information, such as the
                    Internet pages you visit. Accordingly, each time you visit our website, personal data, including the
                    IP address of the Internet connection you use, is transmitted to Google in the United States of
                    America. This personal data is stored by Google in the United States of America. Google may disclose
                    personal data collected through this technical process to third parties.</p>
                <p className={styles.text}>You may refuse the use of cookies by selecting the appropriate settings on
                    your browser, however please note that if you do this you may not be able to use the full
                    functionality of this website. Such a setting of the Internet browser used would also prevent Google
                    from setting a conversion cookie on your IT system. In addition, a cookie already set by Google
                    AdWords can be deleted at any time via the Internet browser or other software programs.</p>
                <p className={styles.text}>You also have the option of opting out of receiving interest-based
                    advertising from Google. To do this, you must call up the link www.google.de/settings/ads from your
                    Internet browser and make the desired settings there.</p>
                <p className={styles.text}>Such evaluation is carried out in particular in accordance with Art. 6 Para.
                    1 lit.f DS-GVO on the basis of our legitimate interests in the display of personalised advertising,
                    market research and/or the design of its website to meet requirements.</p>
                <p className={styles.text}>Further information and the valid data protection regulations of Google can
                    be called up under https://www.google.de/intl/de/policies/privacy/</p>

                <h2 className={styles.secondaryTitle}>5.5 VWO</h2>
                <p className={styles.text}>On our website we use Visual Website Optimizer ("VWO"), a web analysis
                    service from Wingify, 14th Floor, KLJ Tower North, Netaji Subhash Place, Pitam Pura, Delhi 110034,
                    India ("Wingify").</p>
                <p className={styles.text}>Wingify uses cookies that enable us to evaluate the use of our website. These
                    cookies generate information about the usage behavior on this website and store your anonymized IP
                    address. The data is then transferred to Wingify's server in India and stored there. On our behalf,
                    Wingify will use this information to analyze your use of the website and to improve our website
                    based on this. If the cookies do not expire at the end of the session, they will be available for a
                    maximum of 100 days (for more information, please visit
                    https://vwo.com/knowledge/cookies-used-by-vwo/).</p>
                <p className={styles.text}>You can prevent the cookies from being saved by making the appropriate
                    adjustments in your browser as described above or delete cookies that have already been saved. You
                    can also object at any time to the collection by Wingify of the data generated by the cookie and
                    related to your use of the website (including your anonymous IP address) as well as the processing
                    of this data for the future under this link http://www.polyas.de?vwo_opt_out=1.</p>
                <p className={styles.text}>Further information on data protection can be found here:
                    https://vwo.com/terms-conditions/.</p>

                <h2 className={styles.secondaryTitle}>6. SSL/TLS encryption</h2>
                <p className={styles.text}>This site uses SSL or TLS encryption to ensure the security of data
                    processing and to protect the transmission of confidential content, such as orders, login data or
                    contact requests that you send to us as the operator. You can recognize an encrypted connection by
                    the fact that in the address line of the browser there is a "https://" instead of a "http://" and by
                    the lock symbol in your browser line.</p>
                <p className={styles.text}>If SSL or TLS encryption is activated, the data you transmit to us cannot be
                    read by third parties.</p>

                <h2 className={styles.secondaryTitle}>7. services</h2>
                <h2 className={styles.secondaryTitle}>7.1 Google Maps</h2>
                <p className={styles.text}>On our website we use Google Maps (API) from Google LLC, 1600 Amphitheatre
                    Parkway, Mountain View, CA 94043, USA. Google Maps is a web service that displays interactive (land)
                    maps to visually represent geographic information. By using this service, you can, for example, view
                    our location and make it easier for you to get to us.</p>
                <p className={styles.text}>Already when you call up the subpages in which the Google Maps map is
                    integrated, information about your use of our website (e.g. your IP address) is transferred to
                    Google's servers in the USA and stored there. This takes place regardless of whether Google provides
                    a user account that you are logged in to or whether there is no user account. If you are logged in
                    at Google, your data will be directly assigned to your account. If you do not want your profile to
                    be associated with Google, you will need to log out of your Google Account. Google stores your data
                    (even for users who are not logged in) as usage profiles and evaluates them. In particular, such
                    evaluation is carried out in accordance with Art. 6 Para. 1 lit.f DS-GVO on the basis of Google's
                    legitimate interests in the display of personalised advertising, market research and/or the design
                    of its website to meet requirements. You have the right to object to the creation of these user
                    profiles, and you must contact Google to exercise this right.</p>
                <p className={styles.text}>Google LLC, headquartered in the USA, is certified for the us-European data
                    protection agreement "Privacy Shield", which guarantees compliance with the data protection level
                    applicable in the EU.</p>
                <p className={styles.text}>If you do not agree with the future transmission of your data to Google
                    within the framework of the use of Google Maps, you also have the option of completely deactivating
                    the Google Maps web service by deactivating the JavaScript application in your browser. Google Maps
                    and thus also the map display on this website can then not be used.</p>
                <p className={styles.text}>The use of Google Maps is in the interest of an appealing presentation of our
                    online offers and an easy retrievability of the places indicated by us on the website. This
                    constitutes a legitimate interest within the meaning of Art. 6 para. 1 lit. f DS-GVO.</p>
                <p className={styles.text}>You can view Google's terms of use at
                    https://www.google.de/intl/de/policies/terms/regional.html, the additional terms of use for Google
                    Maps can be found at https://www.google.com/intl/de_US/help/terms_maps.html</p>
                <p className={styles.text}>Detailed information on data protection in connection with the use of Google
                    Maps can be found on Google's website ("Google Privacy Policy"):
                    https://www.google.de/intl/de/policies/privacy/</p>

                <h2 className={styles.secondaryTitle}>7.2 Google Tag Manager</h2>
                <p className={styles.text}>This website uses Google Tag Manager, a cookie-free domain that does not
                    collect personally identifiable information.</p>
                <p className={styles.text}>This tool allows "website tags" (i.e. keywords embedded in HTML elements) to
                    be implemented and managed through an interface. By using the Google Tag Manager, we can
                    automatically track which button, link or personalized image you have actively clicked on and then
                    record which content on our website is of particular interest to you.</p>
                <p className={styles.text}>The tool also triggers other tags, which in turn may collect data. Google Tag
                    Manager does not access this data. If you have deactivated it at the domain or cookie level, it will
                    remain active for all tracking tags implemented with Google Tag Manager.</p>
                <p className={styles.text}>Google Tag Manager is used for the convenience and ease of use of our
                    website. This constitutes a legitimate interest within the meaning of Art. 6 para. 1 lit. f
                    DS-GVO.</p>

                <h2 className={styles.secondaryTitle}>7.3 Google WebFonts</h2>
                <p className={styles.text}>Our website uses so-called web fonts provided by Google LLC, 1600
                    Amphitheatre Parkway, Mountain View, CA 94043, USA for the uniform display of fonts. When you access
                    a page, your browser loads the web fonts you need into its browser cache to display text and fonts
                    correctly.</p>
                <p className={styles.text}>To do this, the browser you are using must connect to Google's servers. This
                    enables Google to know that your IP address has been used to access our website. The use of Google
                    Web Fonts is in the interest of a uniform and appealing presentation of our website.</p>
                <p className={styles.text}>This constitutes a legitimate interest within the meaning of Art. 6 para. 1
                    lit. f DS-GVO.</p>
                <p className={styles.text}>Google LLC, headquartered in the USA, is certified for the us-European data
                    protection agreement "Privacy Shield", which guarantees compliance with the data protection level
                    applicable in the EU.</p>
                <p className={styles.text}>Further information on Google Web Fonts can be found at
                    https://developers.google.com/fonts/faq and in Google's privacy policy:
                    https://www.google.com/policies/privacy/</p>

                <h2 className={styles.secondaryTitle}>7.4 YouTube (Videos)</h2>
                <p className={styles.text}>We have integrated components from YouTube on this website. YouTube is an
                    internet video portal that allows video publishers to post video clips and other users to view, rate
                    and comment on them free of charge. YouTube allows the publication of all types of videos, which is
                    why complete film and television programmes, but also music videos, trailers or videos made by users
                    themselves can be accessed via the Internet portal.</p>
                <p className={styles.text}>YouTube is operated by YouTube, LLC, 901 Cherry Ave, San Bruno, CA 94066,
                    USA. YouTube, LLC is a subsidiary of Google Inc, 1600 Amphitheatre Pkwy, Mountain View, CA
                    94043-1351, USA.</p>
                <p className={styles.text}>Each time you access one of the individual pages of this website, which is
                    operated by us and on which a YouTube component (YouTube video) has been integrated, the Internet
                    browser on your IT system is automatically prompted by the respective YouTube component to download
                    a display of the corresponding YouTube component from YouTube. More information about YouTube can be
                    found at https://www.youtube.com/yt/about/de/abgerufen . As part of this technical process, YouTube
                    and Google will know which specific page on our website you are visiting.</p>
                <p className={styles.text}>If the person concerned is logged in to YouTube at the same time, YouTube
                    recognizes which specific subpage of our website you are visiting by calling up a subpage containing
                    a YouTube video. This information is collected by YouTube and Google and assigned to your YouTube
                    account.</p>
                <p className={styles.text}>YouTube and Google always receive information via the YouTube component that
                    you have visited our website when you are logged in to YouTube at the same time as you visit our
                    website; this takes place regardless of whether you click on a YouTube video or not. If you do not
                    want this information to be transmitted to YouTube and Google in this way, you can prevent it from
                    being transmitted by logging out of your YouTube account before you visit our website.</p>
                <p className={styles.text}>YouTube is used in the interest of comfortable and easy use of our website.
                    This constitutes a legitimate interest within the meaning of Art. 6 para. 1 lit. f DS-GVO.</p>
                <p className={styles.text}>The data protection provisions published by YouTube, which can be accessed at
                    https://www.google.de/intl/de/policies/privacy/, provide information on the collection, processing
                    and use of personal data by YouTube and Google.</p>

                <h2 className={styles.secondaryTitle}>7.5 Optmyzer</h2>
                <h2 className={styles.secondaryTitle}>7.6 BING</h2>
                <h2 className={styles.secondaryTitle}> 8. e-mail contact</h2>
                <p className={styles.text}>On our website there is a contact form which you can use to contact us
                    electronically. If you make use of this possibility, the data entered in the input mask will be
                    transmitted to us and processed. These data are:</p>
                <p className={styles.text}>- Your name</p>
                <p className={styles.text}>- email address</p>
                <p className={styles.text}>- telephone number</p>
                <p className={styles.text}>- http:// - Website Address</p>
                <p className={styles.text}>- Other personal data entered in the message text field</p>
                <p className={styles.text}>In the process of sending this message the following data will be stored:</p>
                <p className={styles.text}>- IP address</p>
                <p className={styles.text}>- Date and time of registration</p>
                <p className={styles.text}>- Add further data</p>
                <p className={styles.text}>Your consent will be obtained for the processing of the data as part of the
                    sending process and reference will be made to this data protection declaration.</p>
                <p className={styles.text}>Alternatively you have the possibility to contact us via the following e-mail
                    addresses:</p>
                <p className={styles.text}>- datenschutz@smarketer.de</p>
                <p className={styles.text}>- Further e-mail addresses with which the website visitor can get in touch
                    can be completed</p>
                <p className={styles.text}>In this case, we process and store the personal data transmitted by e-mail.
                    This will basically be your email address and any other information you disclose. Such data
                    processing is necessary for the processing of the conversation. Data processing is based on our
                    legitimate interest within the meaning of Art. 6 Para. 1 lit. f) DSGVO and is therefore lawful,
                    unless your rights are unreasonably restricted as a result. If the purpose of establishing contact
                    is to conclude a contract, the processing is lawful pursuant to Art. 6 para. 1 lit. b) DSGVO. If you
                    provide further data without being asked, the processing is based on your consent within the meaning
                    of Art. 6 para. 1 lit. a) DSGVO. We delete this data as soon as the conversation has ended and there
                    is no longer any purpose for storage. The conversation is terminated when it can be inferred from
                    the circumstances that the relevant facts have been conclusively clarified. All further data
                    transmitted by the sending process will be deleted after a period of 7 days at the latest. The data
                    processing can be contradicted by a written explanation of the revocation of the data processing to
                    datenschutz@smarketer.de.</p>

                <h2 className={styles.secondaryTitle}>8. disclosure of the data by us</h2>
                <p className={styles.text}>We disclose personal information to comply with legal obligations,
                    investigate complaints about offerings or content that violate the rights of others, and protect the
                    rights, property, or safety of others. Such information will be disclosed only in accordance with
                    applicable law. As stated above, we will not disclose your personal information to third parties for
                    their marketing purposes without your express consent.</p>
                <p className={styles.text}>We may also disclose your personal information to law enforcement or
                    regulatory authorities or authorized third parties in response to a request for information in
                    connection with an investigation or suspicion of a criminal offence, illegal activity or other
                    activity that may give rise to legal liability for us, you or another user. In such cases, we will
                    disclose information necessary for the investigation, such as name, city, zip code, telephone
                    number, email address, previous user names, IP address, fraud complaints, and listing of offers.</p>

                <h2 className={styles.secondaryTitle}>9. rights of data subjects</h2>
                <p className={styles.text}>The applicable data protection law grants you, as the data subject,
                    comprehensive data protection rights (information and intervention rights) vis-à-vis the person
                    responsible (playing field) with regard to the processing of your personal data, about which we will
                    inform you below:</p>

                <h2 className={styles.secondaryTitle}>9.1 Right to confirmation, Art. 15 para. 1 sentence 1 DSGVO:</h2>
                <p className={styles.text}>The data subject has the right to request confirmation from the person
                    responsible as to whether personal data relating to him or her will be processed.</p>

                <h2 className={styles.secondaryTitle}>9.2 Right of access, Art. 15 para. 1 sentence 2 DSGVO:</h2>
                <p className={styles.text}>If personal data of the data subject are processed, the data subject has the
                    right of access to this personal data and to the following information:</p>
                <p className={styles.text}>a. the purposes of the processing;</p>
                <p className={styles.text}>b. the categories of personal data to be processed;</p>
                <p className={styles.text}>c. the recipients or categories of recipients to whom the personal data have
                    been or will be disclosed, in particular recipients in third countries or international
                    organisations;</p>
                <p className={styles.text}>d. if possible, the planned duration for which the personal data will be
                    stored or, if that is not possible, the criteria for determining that duration;</p>
                <p className={styles.text}>e. the existence of a right of rectification or erasure of personal data
                    relating to them or of a right of limitation or of opposition to the processing by the
                    controller;</p>
                <p className={styles.text}>f. the existence of a right of appeal to a supervisory authority;</p>
                <p className={styles.text}>g. where the personal data are not collected from the data subject, any
                    available information as to the source of the data;</p>
                <p className={styles.text}>h. the existence of automated decision-making, including profiling, in
                    accordance with Article 22(1) and (4) and, at least in these cases, meaningful information on the
                    logic involved and the scope and intended impact of such processing on the data subject.</p>

                <h2 className={styles.secondaryTitle}>9.3 Right to rectification and completion, Art. 16 DSGVO:</h2>
                <p className={styles.text}>The data subject has the right to request the controller to rectify without
                    delay any inaccurate personal data concerning him or her.</p>
                <p className={styles.text}>Taking into account the purposes of the processing, the data subject shall
                    have the right to request the completion of incomplete personal data, including by means of a
                    supplementary statement.</p>

                <h2 className={styles.secondaryTitle}>9.4 Right to cancellation (right to be forgotten), Art. 17
                    DSGVO</h2>
                <p className={styles.text}>The data subject shall have the right to obtain from the controller the
                    erasure without delay of personal data relating to him or her and the controller shall be obliged to
                    erase without delay personal data for any of the following reasons:</p>
                <p className={styles.text}>a. Personal data are no longer necessary for the purposes for which they were
                    collected or otherwise processed.</p>
                <p className={styles.text}>b. The data subject shall revoke the consent on which the processing was
                    based pursuant to Article 6(1)(a) or Article 9(2)(a) and there shall be no other legal basis for the
                    processing.</p>
                <p className={styles.text}>c. The data subject objects to the processing pursuant to Article 21(1) and
                    there are no overriding legitimate reasons for the processing, or the data subject submits the
                    following pursuant to Article 21(2)</p>
                <p className={styles.text}>d. objection to the processing.</p>
                <p className={styles.text}>e. Personal data have been processed unlawfully.</p>
                <p className={styles.text}>f. The deletion of personal data is necessary to fulfil a legal obligation
                    under Union law or the law of the Member States to which the controller is subject.</p>
                <p className={styles.text}>g. Personal data have been collected in relation to information society
                    services offered in accordance with Article 8(1).</p>
                <p className={styles.text}>However, the above does not apply where the processing is carried out for
                    legal purposes (see 8.).</p>

                <h2 className={styles.secondaryTitle}>9.5 Right to limitation of processing, Art. 18 DSGVO:</h2>
                <p className={styles.text}>The data subject has the right to request the controller to restrict the
                    processing if one of the following conditions is met:</p>
                <p className={styles.text}>a. the accuracy of the personal data is contested by the data subject for a
                    period of time which allows the controller to verify the accuracy of the personal data,</p>
                <p className={styles.text}>b. the processing is unlawful and the data subject refuses to erase the
                    personal data and instead requests that the use of the personal data be restricted;</p>
                <p className={styles.text}>c. the controller no longer needs the personal data for the purposes of the
                    processing, but the data subject needs them for the assertion, exercise or defence of legal rights;
                    or</p>
                <p className={styles.text}>d. the data subject has objected to the processing referred to in Article
                    21(1) before it has been established whether the controller's legitimate reasons outweigh those of
                    the data subject.</p>
                <p className={styles.text}>Where processing has therefore been restricted, such personal data, apart
                    from being stored, may not be processed without the consent of the data subject or in order to
                    establish, exercise or defend a right or the rights of another natural or legal person or for
                    reasons of an important public interest of the Union or of a Member State.</p>
                <p className={styles.text}>A data subject who has obtained a restriction on processing shall be informed
                    by the controller before the restriction is lifted.</p>

                <h2 className={styles.secondaryTitle}>9.6 Right to data transferability, Art. 20 DSGVO</h2>
                <p className={styles.text}>The data subject shall have the right to obtain the personal data concerning
                    him which he has provided to a data controller in a structured, common and machine-readable format
                    and shall have the right to communicate such data to another data controller without being impeded
                    by the data controller to whom the personal data have been provided, if:</p>
                <p className={styles.text}>a. the processing is based on a consent pursuant to Article 6(1)(a) or
                    Article 9(2)(a) or on a contract pursuant to Article 6(1)(b), and</p>
                <p className={styles.text}>b. the processing is carried out using automated procedures.</p>
                <p className={styles.text}>In exercising his right to data transfer, the data subject shall have the
                    right to obtain that the personal data be transferred directly from one controller to another
                    controller, to the extent that this is technically feasible.</p>

                <h2 className={styles.secondaryTitle}>9.7 Right to object, Art. 21 DSGVO:</h2>
                <p className={styles.text}>The data subject has the right to object at any time, for reasons related to
                    his/her particular situation, to the processing of personal data concerning him/her carried out
                    pursuant to Art. 6 para. 1 lit. e) and f), including profiling based on these provisions. The
                    controller shall no longer process personal data unless he can prove compelling legitimate reasons
                    for the processing outweighing the interests, rights and freedoms of the data subject or the
                    processing is for the exercise, exercise or defence of legal rights.</p>

                <h2 className={styles.secondaryTitle}>9.8 Right to revoke consent, Art. 7 para. 3 DSGVO:</h2>
                <p className={styles.text}>The data subject has the right to revoke his/her consent at any time. The
                    revocation of consent shall not affect the lawfulness of the processing carried out on the basis of
                    consent until revoked. The data subject shall be informed before consent is given. Revocation of
                    consent must be as simple as giving consent.</p>

                <h2 className={styles.secondaryTitle}>9.9 Right to appeal, Art. 77 para. 1 DSGVO</h2>
                <p className={styles.text}>Without prejudice to any other administrative or judicial remedy, any data
                    subject shall have the right to complain to a supervisory authority, in particular in the Member
                    State of his or her habitual residence, place of work or place of presumed infringement, if he or
                    she considers that the processing of his or her personal data is contrary to this Regulation.</p>

                <h2 className={styles.secondaryTitle}>10 Newsletter dispatch</h2>
                <h2 className={styles.secondaryTitle}>10.1 Newsletter distribution to existing customers</h2>
                <p className={styles.text}>If you have provided us with your e-mail address when purchasing goods or
                    services, we reserve the right to send you regular offers of similar goods or services to those
                    already purchased from our range by e-mail. According to § 7 Abs. 3 UWG, we do not have to obtain
                    your separate consent for this. Data processing in this respect takes place solely on the basis of
                    our justified interest in personalised direct advertising pursuant to Art. 6 Para. 1 lit. f DS-GVO.
                    If you have initially objected to the use of your e-mail address for this purpose, we will not send
                    you an e-mail. You are entitled to object to the use of your e-mail address for the aforementioned
                    advertising purpose at any time with effect for the future by notifying the person responsible named
                    at the beginning. For this you only incur transmission costs according to the basic tariffs. Upon
                    receipt of your objection, the use of your e-mail address for advertising purposes will be
                    discontinued immediately.</p>

                <h2 className={styles.secondaryTitle}>10.2 Advertising newsletter</h2>
                <p className={styles.text}>On our website you have the possibility to subscribe to the newsletter of our
                    company. Which personal data is transmitted to us when ordering the newsletter results from the
                    input mask used for this purpose.</p>
                <p className={styles.text}>We inform our customers and business partners about our offers at regular
                    intervals by means of a newsletter. The newsletter of our company can only be received by you if</p>
                <p className={styles.text}>1. you have a valid e-mail address, and</p>
                <p className={styles.text}>2. you have registered for the newsletter.</p>
                <p className={styles.text}>For legal reasons, a confirmation e-mail will be sent to the e-mail address
                    you entered for the first time for the newsletter mailing using the double opt-in procedure. This
                    confirmation e-mail is used to check whether you as the owner of the e-mail address have authorised
                    receipt of the newsletter.</p>
                <p className={styles.text}>When you register for the newsletter, we also store the IP address assigned
                    by your Internet Service Provider (ISP) to the IT system you are using at the time of registration
                    as well as the date and time of registration. The collection of this data is necessary in order to
                    trace the (possible) misuse of your e-mail address at a later point in time and therefore serves our
                    legal protection.</p>
                <p className={styles.text}>The personal data collected during registration for the newsletter will only
                    be used to send you our newsletter. Furthermore, subscribers to the newsletter could be informed by
                    e-mail if this is necessary for the operation of the newsletter service or registration in this
                    respect, as might be the case in the event of changes to the newsletter offering or changes to the
                    technical conditions. The personal data collected as part of the newsletter service will not be
                    passed on to third parties. You may cancel your subscription to our newsletter at any time. The
                    consent to the storage of personal data that you have given us for the newsletter can be revoked at
                    any time. For the purpose of revoking your consent, you will find a corresponding link in every
                    newsletter. You also have the option of unsubscribing from the newsletter at any time directly on
                    our website or informing us of this in any other way.</p>
                <p className={styles.text}>The legal basis for data processing for the purpose of sending newsletters is
                    Art. 6 Para. 1 lit. a DS-GVO.</p>

                <h2 className={styles.secondaryTitle}>10.3 Newsletter tracking</h2>
                <p className={styles.text}>Our newsletters contain so-called tracking pixels. A pixel-code is a
                    miniature graphic embedded in emails sent in HTML format for log file recording and analysis. This
                    allows a statistical evaluation of the success or failure of online marketing campaigns. The
                    embedded pixel-code allows the company to see if and when an email was opened by you and which links
                    in the email you viewed.</p>
                <p className={styles.text}>Such personal data collected via the pixel-code contained in the newsletters
                    are stored and evaluated by us in order to optimise the newsletter dispatch and to adapt the content
                    of future newsletters even better to your interests. This personal data will not be passed on to
                    third parties. Affected persons are entitled at any time to revoke the relevant separate declaration
                    of consent given via the double opt-in procedure. After a revocation these personal data are deleted
                    by us. A deregistration from the receipt of the newsletter we automatically interpret as a
                    revocation.</p>
                <p className={styles.text}>Such an evaluation takes place in particular in accordance with Art. 6 Para.
                    1 lit.f DS-GVO on the basis of our legitimate interests in the display of personalised advertising,
                    market research and/or the design of our website in line with requirements.</p>

                <h2 className={styles.secondaryTitle}>10.4 Mailchimp</h2>
                <p className={styles.text}>Our e-mail newsletters are sent via the technical service provider The Rocket
                    Science Group, LLC d/b/a MailChimp, 675 Ponce de Leon Ave NE, Suite 5000, Atlanta, GA 30308, USA
                    (https://www.mailchimp.com/), to whom we forward the data you provided when registering for the
                    newsletter. This disclosure is made in accordance with Art. 6 Para. 1 lit. f DS-GVO and serves our
                    legitimate interest in the use of an effective, secure and user-friendly newsletter system. Please
                    note that your data is usually transferred to a MailChimp server in the USA and stored there.</p>
                <p className={styles.text}>MailChimp uses this information for the dispatch and statistical evaluation
                    of the newsletter on our behalf. For the evaluation, the e-mails sent contain so-called web beacons
                    or tracking pixels, which represent one-pixel image files stored on our website. This enables us to
                    determine whether a newsletter message has been opened and which links have been clicked. Technical
                    information is also recorded (e.g. time of access, IP address, browser type and operating system).
                    The data is collected exclusively under a pseudonym and is not linked to your other personal data, a
                    direct personal reference is excluded. This data is used exclusively for statistical analysis of
                    newsletter campaigns. The results of these analyses can be used to better adapt future newsletters
                    to the interests of the recipients.</p>
                <p className={styles.text}>If you wish to object to the data analysis for statistical evaluation
                    purposes, you must unsubscribe from the newsletter.</p>
                <p className={styles.text}>Furthermore, MailChimp can use this data according to Art. 6 para. 1 lit. f
                    DS-GVO itself due to its own legitimate interest in the need-based design and optimization of the
                    service as well as for market research purposes in order to determine, for example, from which
                    countries the recipients come. However, MailChimp does not use the data of our newsletter recipients
                    to write to them itself or to pass them on to third parties.</p>
                <p className={styles.text}>In order to protect your data in the USA, we have concluded a data processing
                    agreement with MailChimp based on the standard contractual clauses of the European Commission to
                    enable the transfer of your personal data to MailChimp. If you are interested, this data processing
                    agreement can be viewed at the following Internet address:
                    https://mailchimp.com/legal/forms/data-processing-agreement/</p>
                <p className={styles.text}>In addition, MailChimp is certified under the us European Privacy Shield and
                    is thus committed to complying with EU data protection regulations.</p>
                <p className={styles.text}>You can view MailChimp's privacy policy here:
                    https://mailchimp.com/legal/privacy/</p>

                <h2 className={styles.secondaryTitle}>10.5 Newsletter unsubscription</h2>
                <p className={styles.text}>You can unsubscribe from the newsletter at any time. This can be done by
                    contacting us directly. Write us an email with the following data:</p>
                <p className={styles.text}>- First name, last name (personal address in the newsletter)</p>
                <p className={styles.text}>- E-mail address (recipient of the newsletter)</p>
                <p className={styles.text}>to the address Newsletterabmeldung@smarketer.de</p>

                <h2 className={styles.secondaryTitle}>11 Contents of the website</h2>
                <h2 className={styles.secondaryTitle}>11.1 Data processing for order processing</h2>
                <p className={styles.text}>The personal data collected by us are passed on to the transport company
                    commissioned with the delivery within the framework of contract processing, insofar as this is
                    necessary for the delivery of the goods. We pass on your payment data to the commissioned credit
                    institution within the framework of payment processing, insofar as this is necessary for payment
                    processing. If payment service providers are used, we inform about this explicitly below. The legal
                    basis for the passing on of the data is here art. 6 Abs. 1 lit. b DS-GVO.</p>

                <h2 className={styles.secondaryTitle}>11.2 Making contact / contact form</h2>
                <p className={styles.text}>Within the scope of contacting us (e.g. via contact form or e-mail), personal
                    data is collected. Which data is collected in the case of a contact form, is apparent from the
                    respective contact form. This data is stored and used to respond to your request or for contacting
                    you and the associated technical administration, and data is also stored for statistical purposes.
                    The legal basis for processing the data is our legitimate interest in responding to your request
                    pursuant to Art. 6 para. 1 lit. f DS-GVO. If the purpose of your contact is to conclude a contract,
                    the additional legal basis for the processing is Art. 6 para. 1 lit. b DS-GVO. Your data will be
                    deleted after final processing of your enquiry; this is the case if it can be inferred from the
                    circumstances that the facts in question have been conclusively clarified and provided that there
                    are no legal storage obligations to the contrary.</p>

                <h2 className={styles.secondaryTitle}>11.3 Services / Digital Goods</h2>
                <p className={styles.text}>We transmit personal data to third parties only if this is necessary in the
                    context of contract processing, for example to the bank commissioned with payment processing.</p>
                <p className={styles.text}>A further transmission of the data does not take place or only if you have
                    expressly agreed to the transmission. Your data will not be passed on to third parties without your
                    express consent, for example for advertising purposes.</p>
                <p className={styles.text}>The basis for data processing is Art. 6 Para. 1 lit. b DS-GVO, which permits
                    the processing of data for the fulfilment of a contract or pre-contractual measures.</p>

                <h2 className={styles.secondaryTitle}>11.4 Comment function Blog</h2>
                <p className={styles.text}>We offer users the opportunity to leave individual comments on individual
                    blog posts on a blog located on our website. A blog is a publicly accessible portal that is
                    maintained on a website and in which one or more people, called bloggers or web bloggers, can post
                    articles or write thoughts in so-called blog posts. Blog posts can usually be commented on by third
                    parties.</p>
                <p className={styles.text}>If you leave a comment in the blog published on this website, the information
                    you leave will be stored and published along with your comments, as well as the time at which you
                    entered your comment and the user name (pseudonym) you chose. The IP address assigned by your
                    Internet Service Provider (ISP) is also logged. This IP address is stored for security reasons and
                    in the event that you have violated the rights of third parties or posted illegal content by posting
                    a comment. The storage of this personal data is therefore in our own interest so that we can
                    exculpate ourselves in the event of a violation of the law. This constitutes a legitimate interest
                    within the meaning of Art. 6 Para. 1 lit. f DS-GVO. Personal data collected will not be disclosed to
                    third parties unless such disclosure is required by law or serves our legal defence.</p>

                <h2 className={styles.secondaryTitle}>11.5 Application management / job exchange</h2>
                <p className={styles.text}>We collect and process the personal data of applicants for the purpose of
                    processing the application process. The processing can also be carried out electronically. This is
                    particularly the case if an applicant submits corresponding application documents to us
                    electronically, for example by e-mail or via a web form on the website. If we conclude an employment
                    contract with an applicant, the data transmitted will be stored for the purpose of processing the
                    employment relationship in compliance with the statutory provisions. If we do not conclude an
                    employment contract with the applicant, the application documents will be automatically deleted six
                    months after notification of the rejection decision, provided that no other legitimate interests on
                    our part are opposed to deletion. Other legitimate interests in this sense include, for example, a
                    duty to provide evidence in proceedings under the General Equal Treatment Act (Allgemeines
                    Gleichbehandlungsgesetz - AGG).</p>
                <p className={styles.text}>Data processing in this respect takes place solely on the basis of our
                    legitimate interest pursuant to Art. 6 Para. 1 lit. f DS-GVO.</p>
            </div>
        </div>
    </OverviewPage>;
};

export default PrivacyEn;
