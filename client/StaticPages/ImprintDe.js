import React from 'react';

import OverviewPage from "../blocks/OverviewPage/OverviewPage";

import styles from './StateicPages.module.scss';

const ImprintDe = () => {
    return <OverviewPage hiddenLangSwitcher={true}>
        <div className={styles.staticPageWrapper}>
            <div>
                <h1 className={styles.mainTitle}>Impressum</h1>

                <h2 className={styles.secondaryTitle}>Smarketer GmbH</h2>
                <p className={styles.text}>Alte Jakobstraße 83/84, 10179 Berlin</p>
                <p className={styles.text}>Vertreten durch Herr David Gabriel</p>
                <p className={styles.text}>info@smarketer.jobs</p>
                <p className={styles.text}>+49 30 96 53 66 56 17</p>
                <p className={styles.text}>Eintragung im Handelsregister, Amtsgericht Charlottenburg, HRB 169962 B</p>
                <p className={styles.text}>Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz: DE
                    301407325</p>
                <p className={styles.text}>Plattform der EU-Kommission zur Online-Streitbeilegung:
                    https://ec.europa.eu/consumers/odr</p>
                <p className={styles.text}>Wir sind zur Teilnahme an einem Streitbeilegungsverfahren vor einer
                    Verbraucherschlichtungsstelle weder verpflichtet noch bereit.</p>

                <h2 className={styles.secondaryTitle}>Haftungsausschluss</h2>
                <h2 className={styles.secondaryTitle}>Haftung für Inhalte</h2>
                <p className={styles.text}>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die
                    Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen.
                    Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den
                    allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht
                    verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen
                    zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder
                    Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine
                    diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten
                    Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese
                    Inhalte umgehend entfernen.</p>

                <h2 className={styles.secondaryTitle}>Haftung für Links</h2>
                <p className={styles.text}>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte
                    wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr
                    übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber
                    der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche
                    Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.
                    Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte
                    einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige
                    Links umgehend entfernen.</p>

                <h2 className={styles.secondaryTitle}>Urheberrecht</h2>
                <p className={styles.text}>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten
                    unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art
                    der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des
                    jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten,
                    nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber
                    erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als
                    solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden,
                    bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir
                    derartige Inhalte umgehend entfernen.</p>

            </div>
        </div>
    </OverviewPage>;
};

export default ImprintDe;
