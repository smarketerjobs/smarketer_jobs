import React from 'react';

import OverviewPage from "../blocks/OverviewPage/OverviewPage";

import styles from './StateicPages.module.scss';

const ImprintEn = () => {
    return <OverviewPage hiddenLangSwitcher={true}>
        <div className={styles.staticPageWrapper}>
            <div>
                <h1 className={styles.mainTitle}>Imprint</h1>

                <h2 className={styles.secondaryTitle}>Smarketer GmbH</h2>
                <p className={styles.text}>Alte Jakobstrasse 83/84, 10179 Berlin</p>
                <p className={styles.text}>Represented by Mr David Gabriel</p>
                <p className={styles.text}>info@smarketer.jobs</p>
                <p className={styles.text}>+49 30 96 53 66 56 17</p>
                <p className={styles.text}>Entry in the commercial register</p>
                <p className={styles.text}>Local Court Charlottenburg</p>
                <p className={styles.text}>HRB 169962 B</p>
                <p className={styles.text}>Value added tax identification number according to §27 a Umsatzsteuergesetz:
                    DE 301407325</p>

                <h2 className={styles.secondaryTitle}>Disclaimer</h2>
                <h2 className={styles.secondaryTitle}>Liability for contents</h2>
                <p className={styles.text}>The contents of our pages were created with the greatest care. For the
                    correctness, completeness and topicality of the contents we can take over however no guarantee. As a
                    service provider we are responsible according to § 7 Abs.1 TMG for our own contents on these pages
                    according to the general laws. According to §§ 8 to 10 TMG we are not obliged as a service provider
                    to monitor transmitted or stored third-party information or to investigate circumstances that
                    indicate illegal activity. Obligations to remove or block the use of information in accordance with
                    general laws remain unaffected by this. However, liability in this respect is only possible from the
                    time of knowledge of a concrete violation of the law. As soon as we become aware of such
                    infringements, we will remove the content immediately.</p>

                <h2 className={styles.secondaryTitle}>Liability for links</h2>
                <p className={styles.text}>Our offer contains links to external websites of third parties, on whose
                    contents we have no influence. Therefore, we cannot assume any liability for these external
                    contents. The respective provider or operator of the pages is always responsible for the contents of
                    the linked pages. The linked pages were checked for possible legal infringements at the time of
                    linking. Illegal contents were not recognisable at the time of linking. A permanent control of the
                    contents of the linked pages is not reasonable without concrete evidence of an infringement. As soon
                    as we become aware of any legal infringements, we will remove such links immediately.</p>

                <h2 className={styles.secondaryTitle}>Copyright</h2>
                <p className={styles.text}>The contents and works on these pages created by the site operators are
                    subject to German copyright law. Duplication, processing, distribution and any form of
                    commercialization of such material beyond the scope of the copyright law shall require the prior
                    written consent of its respective author or creator. Downloads and copies of these pages are only
                    permitted for private, non-commercial use. Insofar as the content on this site was not created by
                    the operator, the copyrights of third parties are respected. In particular, contents of third
                    parties are marked as such. Should you nevertheless become aware of a copyright infringement, please
                    inform us accordingly. As soon as we become aware of any infringements, we will remove such content
                    immediately.</p>

            </div>
        </div>
    </OverviewPage>;
};

export default ImprintEn;
