import React from 'react';

import styles from './StateicPages.module.scss';

import OverviewPage from "../blocks/OverviewPage/OverviewPage";

const PrivacyDe = () => {
    return <OverviewPage hiddenLangSwitcher={true}>
        <div className={styles.staticPageWrapper}>
            <div>
                <h1 className={styles.mainTitle}>Datenschutzerklärung</h1>

                <p className={styles.text}>Wir freuen uns über Ihr Interesse an unserer Homepage und unserem
                    Unternehmen. Der Schutz Ihrer personenbezogenen Daten hat für uns höchste Priorität, darum möchten
                    wir Sie nachstehend darüber informieren, wie die Smarketer GmbH (im Folgenden Smarketer) ihre Daten
                    erhebt, nutzt, speichert, weitergibt und schützt.</p>
                <p className={styles.text}>Diese Datenschutzerklärung wurde zuletzt aktualisiert am: 24.05.2018</p>
                <p className={styles.text}>Im Falle von Änderungen dieser Datenschutzerklärung werden wir die geänderte
                    Erklärung sowie das Datum des Inkrafttretens der geänderten Erklärung auf dieser Website
                    veröffentlichen. Wir empfehlen Ihnen daher, diese in regelmäßigen Abständen durchzulesen.
                    Änderungen, die eine von Ihnen erteilte Einwilligung betreffen, werden wir nur durch erneute
                    Einholung der Einwilligung durchführen. Die vorliegende Datenschutzerklärung ist gültig ab dem
                    25.05.2018.</p>

                <h2 className={styles.secondaryTitle}>1. Verantwortlich i.S.d. DSGVO</h2>
                <p className={styles.text}>Verantwortlich im Sinne der Datenschutzgrundverordnung ist:</p>
                <p className={styles.text}>Smarketer GmbH, Alte Jakobstraße 83/84, 10179 Berlin – Deutschland</p>
                <p className={styles.text}>Tel.: +49 30 96 53 51 98 8</p>
                <p className={styles.text}>E-Mail: info@smarketer.jobs</p>
                <p className={styles.text}>Sollten Sie Fragen zu datenschutzrechtlichen Angelegenheiten haben, die sich
                    in dieser Datenschutzerklärung nicht beantworten oder Sie weitere Informationen wünschen, dann
                    schreiben Sie uns gerne über datenschutz@smarketer.de.</p>

                <h2 className={styles.secondaryTitle}>2.Allgemeines zur Datenverarbeitung</h2>
                <p className={styles.text}>Wir nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Darum verarbeiten
                    wir diese ausschließlich im Sinne der Datenschutz-Grundverordnung (kurz DSGVO) und anderen
                    nationalen Datenschutzgesetzen. Dies ist zur Bereitstellung einer funktionsfähigen Website
                    erforderlich. Außerdem können wir Ihnen so unsere Inhalte und Leistungen ermöglichen.</p>
                <p className={styles.text}>Eine Verarbeitung von personenbezogenen Daten geschieht grundsätzlich, soweit
                    Sie dazu Ihre Einwilligung gegeben haben. Eine Ausnahme dazu findet nur statt, wenn aus
                    tatsächlichen Gründen keine Einwilligung eingeholt werden kann und gesetzliche Vorschriften eine
                    Datenverarbeitung gestatten.</p>
                <p className={styles.text}>Die Datenverarbeitung erfolgt nur, soweit sie rechtmäßig ist. Rechtmäßigkeit
                    liegt vor, wenn mindestens eine in Art. 6 Abs. 1 DSGVO genannte Bedingung erfüllt ist. Wir löschen
                    Ihre Daten, sobald der Zweck der Speicherung entfällt oder eine durch gesetzliche Vorschriften
                    vorgeschriebene Frist (in der Regel 7 Tage) abgelaufen ist. Sie haben jederzeit die Möglichkeit
                    einer Datenverarbeitung mit Wirkung für die Zukunft zu widersprechen. Dazu senden Sie uns bitte eine
                    schriftliche Erklärung des Widerrufs schriftlich über den Postweg oder als Email zu.</p>
                <p className={styles.text}>Unser Angebot richtet sich nicht an Kinder unter 13 Jahren und wir erfassen
                    wissentlich keine Daten von unter 13-jährigen. Wenn wir bei der Erfassung der Daten feststellen,
                    dass dies die Daten von einem Kind unter 13 Jahren sind, werden wir diese ohne die Zustimmung der
                    Erziehungsberechtigten nicht weiterverarbeiten oder beibehalten. Falls wir dennoch einmal die Daten
                    eines Kindes verarbeitet haben, unternehmen wir alle Anstrengungen, um diese Informationen aus
                    unseren Systemen zu entfernen.</p>

                <h2 className={styles.secondaryTitle}>3.Logfiles</h2>
                <p className={styles.text}>Bei Aufruf unserer Website erfasst unser System automatisch Daten und
                    Informationen des Computersystems Ihres Rechners. Dabei werden folgende Daten erhoben:</p>
                <p className={styles.text}>a. verwendeten Browsertypen und Versionen,</p>
                <p className={styles.text}>b. das vom zugreifenden System verwendete Betriebssystem,</p>
                <p className={styles.text}>c. die Internetseite, von welcher ein zugreifendes System auf unsere
                    Internetseite gelangt (sogenannte Referrer),</p>
                <p className={styles.text}>d. die Unterwebseiten, welche über ein zugreifendes System auf unserer
                    Internetseite angesteuert werden,</p>
                <p className={styles.text}>e. das Datum und die Uhrzeit eines Zugriffs auf die Internetseite,</p>
                <p className={styles.text}>f. eine gekürzte Internet-Protokoll-Adresse (anonymisierte IP-Adresse),</p>
                <p className={styles.text}>g. der Internet-Service-Provider des zugreifenden Systems.</p>
                <p className={styles.text}>Diese Daten werden in den Logfiles unseres Systems gespeichert. Eine
                    Speicherung dieser Daten zusammen mit anderen personenbezogenen Daten findet nicht statt.</p>
                <p className={styles.text}>Die Speicherung in den Logfiles erfolgt, um die Funktionsfähigkeit unserer
                    Website sicherzustellen. Außerdem können wir mit den Daten unsere Website optimieren und die
                    Sicherheit unserer informationstechnischen Systeme sicherstellen. Eine Auswertung der Daten zu
                    Marketingzwecken erfolgt in diesem Zusammenhang nicht.</p>
                <p className={styles.text}>In diesen Zwecken liegt auch unser berechtigtes Interesse an der
                    Datenverarbeitung nach Art. 6 Abs. 1 lit. f) DSGVO. Die gespeicherten Daten werden gelöscht, sobald
                    ihr Zweck erfüllt ist. Werden die Daten zur Bereitstellung der Website erfasst, so ist ihr Zweck
                    erreicht, wenn die Sitzung beendet ist. Da die Erfassung der Daten zur Bereitstellung der Website
                    und die Speicherung der Daten in Logfiles für den Betrieb der Website zwingend erforderlich ist,
                    besteht für Sie keine Widerspruchsmöglichkeit.</p>

                <h2 className={styles.secondaryTitle}>4.Cookies</h2>
                <p className={styles.text}>Wir verwenden auf unserer Website so genannte „Cookies“. Dabei handelt es
                    sich um Textdateien, die auf ihrem Endgerät abgelegt werden. Einige der von uns verwendeten Cookies,
                    sogenannte Sitzungs-Cookies, werden direkt wieder gelöscht, wenn Sie Ihren Browser schließen, also
                    die Sitzung beenden. Andere Cookies werden weiterhin auf Ihrem Endgerät gespeichert und ermöglichen
                    uns und unseren Drittanbietern Ihren Browser beim nächsten Besuch unserer Website wieder zu erkennen
                    (persistente Cookies). Gesetzte Cookies werden im individuellen Umfang verarbeitet und erheben Daten
                    wie Browser- und Standortdaten sowie IP-Adresswerte. Persistente Cookies werden automatisch nach
                    einer vorgeschriebenen Dauer gelöscht.</p>
                <p className={styles.text}>Cookies dienen dazu, Ihren Besuch auf unserer Website attraktiv zu gestalten
                    und um die Nutzung bestimmter Funktionen zu ermöglichen. Sofern die von uns gesetzten Cookies
                    personenbezogene Daten verarbeiten, erfolgt die Verarbeitung gemäß Art. 6 Abs. 1 lit. b) DSGVO
                    entweder zu Vertragszwecken oder gemäß Art. 6 Abs. 1 lit. f) DSGVO zur Wahrung unserer berechtigten
                    Interessen einer benutzerfreundlichen Website rechtmäßig.</p>
                <p className={styles.text}>Über die von Drittanbietern erhobenen Cookies und die jeweilige Art und den
                    Umfang der Datenverarbeitung erfahren Sie im Folgenden mehr (siehe 5, 6 und 7).</p>
                <p className={styles.text}>Um die Cookie-Setzung zu verhindern, können Sie Ihren Browser so einstellen,
                    dass Sie über das Setzen von Cookies informiert werden und einzeln über deren Annahme entscheiden
                    oder die Annahme von Cookies für bestimmte Fälle generell ausschließen können. Die Art der
                    Browsereinstellung ist bei jedem Browser unterschiedlich. Eine Beschreibung der Cookie-Einstellungen
                    finden Sie für den jeweiligen Browser unter den folgenden Links:</p>
                <p className={styles.text}>Internet Explorer:
                    http://windows.microsoft.com/de-DE/windows-vista/Block-or-allow-cookies</p>
                <p className={styles.text}>Firefox: https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen</p>
                <p className={styles.text}>Chrome:
                    http://support.google.com/chrome/bin/answer.py?hl=de&hlrm=en&answer=95647</p>
                <p className={styles.text}>Safari: https://support.apple.com/de-de/guide/safari/sfri11471/mac</p>
                <p className={styles.text}>Opera: https://help.opera.com/de/latest/web-preferences/</p>
                <p className={styles.text}>Bitte beachten Sie, dass bei Nichtannahme von Cookies die Funktionalität
                    unserer Website eingeschränkt sein kann.</p>

                <h2 className={styles.secondaryTitle}>5. Tracking und Werbung</h2>
                <h2 className={styles.secondaryTitle}>5.1 Google Analytics</h2>
                <p className={styles.text}>Wir benutzen Google Analytics, einen Webanalysedienst der Google Inc.
                    (https://www.google.de/intl/de/about/) (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; im
                    Folgenden “Google”). Google Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Endgerät
                    gespeichert werden und die eine Analyse der Benutzung der Website ermöglichen. Die durch den Cookie
                    erzeugten Informationen wie:</p>
                <p className={styles.text}>a. Browser-Typ/-Version,</p>
                <p className={styles.text}>b. Verwendetes Betriebssystem,</p>
                <p className={styles.text}>c. Referrer-URL (die zuvor besuchte Seite),</p>
                <p className={styles.text}>d. Hostname des zugreifenden Rechners (IP-Adresse),</p>
                <p className={styles.text}>e. Uhrzeit der Serveranfrage</p>
                <p className={styles.text}>werden in der Regel an einen Server von Google in den USA übertragen und dort
                    gespeichert. Auf dieser Website ist die IP-Anonymisierung aktiviert, was bedeutet, dass die
                    IP-Adresse der Nutzer von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in
                    anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt wird. Nur
                    in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und
                    dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um
                    die Nutzung der Website durch die Nutzer auszuwerten, um Reports über die Websiteaktivitäten
                    zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene
                    Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von
                    Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. Sie
                    können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software
                    verhindern. Allerdings weisen wir Sie darauf hin, dass Sie in diesem Fall gegebenenfalls nicht
                    sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus
                    die Erfassung der durch das Cookie erzeugten und auf ihre Nutzung der Website bezogenen Daten (inkl.
                    Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie
                    das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren:
                    http://tools.google.com/dlpage/gaoptout?hl=de.</p>

                <h2 className={styles.secondaryTitle}>Google Analytics deaktivieren</h2>
                <p className={styles.text}>Alternativ zum Browser-Add-On oder innerhalb von Browsern auf mobilen
                    Geräten, klicken Sie bitte diesen Link, um die Erfassung durch Google Analytics innerhalb dieser
                    Website zukünftig zu verhindern (das Opt Out funktioniert nur in diesem Browser und nur für diese
                    Domain). Dabei wird ein Opt-Out-Cookie auf Ihrem Gerät abgelegt. Löschen Sie Ihre Cookies in diesem
                    Browser, müssen Sie diesen Link erneut klicken.</p>
                <p className={styles.text}>Nähere Informationen zu Nutzungsbedingungen und Datenschutz sind unter
                    http://www.google.com/analytics/terms/de.html bzw. https://www.google.de/intl/de/policies/ zu
                    finden. Dieses Angebot benutzt Google Analytics zudem dazu, Daten aus AdWords und dem
                    Double-Click-Cookie zu statistischen Zwecken auszuwerten. Sollten Sie dies nicht wünschen, können
                    Sie dies über den Anzeigenvorgaben-Manager (http://www.google.com/settings/ads/onweb/?hl=de)
                    deaktivieren. Die verarbeiteten Daten werden spätestens nach einer Frist von 14 Monaten
                    gelöscht.</p>

                <h2 className={styles.secondaryTitle}>5.2 Google Analytics Remarketing</h2>
                <p className={styles.text}>Wir haben auf dieser Internetseite Dienste von Google Remarketing integriert.
                    Google Remarketing ist eine Funktion von Google-AdWords, die es einem Unternehmen ermöglicht, bei
                    solchen Internetnutzern Werbung einblenden zu lassen, die sich zuvor auf der Internetseite des
                    Unternehmens aufgehalten haben. Die Integration von Google Remarketing gestattet es einem
                    Unternehmen demnach, nutzerbezogene Werbung zu erstellen und dem Internetnutzer folglich
                    interessenrelevante Werbeanzeigen anzeigen zu lassen.</p>
                <p className={styles.text}>Betreibergesellschaft der Dienste von Google Remarketing ist die Google Inc.,
                    1600 Amphitheatre Pkwy, Mountain View, CA 94043-1351, USA.</p>
                <p className={styles.text}>Zweck von Google Remarketing ist die Einblendung von interessenrelevanter
                    Werbung. Google Remarketing ermöglicht es uns, Werbeanzeigen über das Google-Werbenetzwerk
                    anzuzeigen oder auf anderen Internetseiten anzeigen zu lassen, welche auf die individuellen
                    Bedürfnisse und Interessen von Internetnutzern abgestimmt sind.</p>
                <p className={styles.text}>Google Remarketing setzt ein Cookie auf Ihrem IT-System der betroffenen
                    Person. Mit der Setzung des Cookies wird Google eine Wiedererkennung des Besuchers unserer
                    Internetseite ermöglicht, wenn dieser in der Folge Internetseiten aufruft, die ebenfalls Mitglied
                    des Google-Werbenetzwerks sind. Mit jedem Aufruf einer Internetseite, auf welcher der Dienst von
                    Google Remarketing integriert wurde, identifiziert sich Ihr Internetbrowser der automatisch bei
                    Google. Im Rahmen dieses technischen Verfahrens erhält Google Kenntnis über personenbezogene Daten,
                    wie Ihre IP-Adresse oder das Surfverhalten, welche Google unter anderem zur Einblendung
                    interessenrelevanter Werbung verwendet.</p>
                <p className={styles.text}>Mittels des Cookies werden personenbezogene Informationen, beispielsweise die
                    durch Sie besuchten Internetseiten, gespeichert. Bei jedem Besuch unserer Internetseiten werden
                    demnach personenbezogene Daten, einschließlich Ihrer IP-Adresse, an Google in den Vereinigten
                    Staaten von Amerika übertragen. Diese personenbezogenen Daten werden durch Google in den Vereinigten
                    Staaten von Amerika gespeichert. Google gibt diese über das technische Verfahren erhobenen
                    personenbezogenen Daten unter Umständen an Dritte weiter.</p>
                <p className={styles.text}>Sie können die Setzung von Cookies durch unsere Internetseite, wie oben
                    bereits dargestellt, jederzeit mittels einer entsprechenden Einstellung des genutzten
                    Internetbrowsers verhindern und damit der Setzung von Cookies dauerhaft widersprechen. Eine solche
                    Einstellung des genutzten Internetbrowsers würde auch verhindern, dass Google ein Cookie auf Ihrem
                    IT-System setzt. Zudem kann ein von Google Analytics bereits gesetzter Cookie jederzeit über den
                    Internetbrowser oder andere Softwareprogramme gelöscht werden.</p>
                <p className={styles.text}>Ferner besteht für Sie die Möglichkeit, der interessenbezogenen Werbung durch
                    Google zu widersprechen. Hierzu müssen Sie von jedem genutzten Internetbrowser aus den Link
                    www.google.de/settings/ads aufrufen und dort die gewünschten Einstellungen vornehmen.</p>
                <p className={styles.text}>Eine solche Auswertung erfolgt insbesondere gemäß Art. 6 Abs. 1 lit.f DS-GVO
                    auf Basis unseres berechtigten Interesses an der Einblendung personalisierter Werbung,
                    Marktforschung und/oder bedarfsgerechten Gestaltung seiner Website.</p>
                <p className={styles.text}>Weitere Informationen und die geltenden Datenschutzbestimmungen von Google
                    können unter https://www.google.de/intl/de/policies/privacy/ abgerufen werden.</p>

                <h2 className={styles.secondaryTitle}>5.3 Google (AdWords) Remarketing</h2>
                <p className={styles.text}>Unsere Website nutzt die Funktionen von Google AdWords Remarketing, hiermit
                    werben wir für diese Website in den Google-Suchergebnissen, sowie auf Dritt-Websites. Anbieter ist
                    die Google LLC., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (“Google”). Zu diesem Zweck
                    setzt Google ein Cookie im Browser Ihres Endgeräts, welches automatisch mittels einer pseudonymen
                    Cookie-ID und auf Grundlage der von Ihnen besuchten Seiten eine interessensbasierte Werbung
                    ermöglicht.</p>
                <p className={styles.text}>Die Verarbeitung erfolgt auf Basis unseres berechtigten Interesses an der
                    optimalen Vermarktung unserer Website gemäß Art. 6 Abs. 1 lit. f DS-GVO.</p>
                <p className={styles.text}>Eine darüberhinausgehende Datenverarbeitung findet nur statt, sofern Sie
                    gegenüber Google zugestimmt haben, dass Ihr Internet- und App-Browserverlauf von Google mit ihrem
                    Google-Konto verknüpft wird und Informationen aus ihrem Google-Konto zum Personalisieren von
                    Anzeigen verwendet werden, die sie im Web betrachten. Sind sie in diesem Fall während des
                    Seitenbesuchs unserer Webseite bei Google eingeloggt, verwendet Google Ihre Daten zusammen mit
                    Google Analytics-Daten, um Zielgruppenlisten für geräteübergreifendes Remarketing zu erstellen und
                    zu definieren. Dazu werden Ihre personenbezogenen Daten von Google vorübergehend mit Google
                    Analytics-Daten verknüpft, um Zielgruppen zu bilden.</p>
                <p className={styles.text}>Sie können die Setzung von Cookies für Anzeigenvorgaben dauerhaft
                    deaktivieren, indem Sie das unter folgendem Link verfügbare Browser-Plug-in herunterladen und
                    installieren: https://www.google.com/settings/ads/onweb/</p>
                <p className={styles.text}>Alternativ können Sie sich bei der Digital Advertising Alliance unter der
                    Internetadresse www.aboutads.info über das Setzen von Cookies informieren und Einstellungen hierzu
                    vornehmen. Schließlich können Sie Ihren Browser so einstellen, dass Sie über das Setzen von Cookies
                    informiert werden und einzeln über deren Annahme entscheiden oder die Annahme von Cookies für
                    bestimmte Fälle oder generell ausschließen. Bei der Nichtannahme von Cookies kann die Funktionalität
                    unserer Website eingeschränkt sein.</p>
                <p className={styles.text}>Google LLC mit Sitz in den USA ist für das us-europäische
                    Datenschutzübereinkommen “Privacy Shield” zertifiziert, welches die Einhaltung des in der EU
                    geltenden Datenschutzniveaus gewährleistet.</p>
                <p className={styles.text}>Weitergehende Informationen und die Datenschutzbestimmungen bezüglich Werbung
                    und Google können Sie hier einsehen: https://www.google.com/policies/technologies/ads/</p>

                <h2 className={styles.secondaryTitle}>5.4 Google AdWords mit Conversion-Tracking</h2>
                <p className={styles.text}>Wir haben auf dieser Internetseite Google AdWords integriert. Google AdWords
                    ist ein Dienst zur Internetwerbung, der es Werbetreibenden gestattet, sowohl Anzeigen in den
                    Suchmaschinenergebnissen von Google als auch im Google-Werbenetzwerk zu schalten. Google AdWords
                    ermöglicht es einem Werbetreibenden, vorab bestimmte Schlüsselwörter festzulegen, mittels derer eine
                    Anzeige in den Suchmaschinenergebnissen von Google ausschließlich dann angezeigt wird, wenn der
                    Nutzer mit der Suchmaschine ein schlüsselwortrelevantes Suchergebnis abruft. Im Google-Werbenetzwerk
                    werden die Anzeigen mittels eines automatischen Algorithmus und unter Beachtung der zuvor
                    festgelegten Schlüsselwörter auf themenrelevanten Internetseiten verteilt.</p>
                <p className={styles.text}>Betreibergesellschaft der Dienste von Google AdWords ist die Google Inc.,
                    1600 Amphitheatre Pkwy, Mountain View, CA 94043-1351, USA.</p>
                <p className={styles.text}>Der Zweck von Google AdWords ist die Bewerbung unserer Internetseite durch
                    die Einblendung von interessenrelevanter Werbung auf den Internetseiten von Drittunternehmen und in
                    den Suchmaschinenergebnissen der Suchmaschine Google und eine Einblendung von Fremdwerbung auf
                    unserer Internetseite.</p>
                <p className={styles.text}>Gelangen Sie über eine Google-Anzeige auf unsere Internetseite, wird auf
                    Ihrem IT-System durch Google ein sogenannter Conversion-Cookie abgelegt. Ein Conversion-Cookie
                    verliert nach dreißig Tagen seine Gültigkeit und dient nicht zu Ihrer Identifikation. Über den
                    Conversion-Cookie wird, sofern das Cookie noch nicht abgelaufen ist, nachvollzogen, ob bestimmte
                    Unterseiten, beispielsweise der Warenkorb von einem Online-Shop-System, auf unserer Internetseite
                    aufgerufen wurden. Durch den Conversion-Cookie können sowohl wir als auch Google nachvollziehen, ob
                    ein Nutzer, der über eine AdWords-Anzeige auf unsere Internetseite gelangt ist, einen Umsatz
                    generierte, also einen Warenkauf vollzogen oder abgebrochen hat.</p>
                <p className={styles.text}>Die durch die Nutzung des Conversion-Cookies erhobenen Daten und
                    Informationen werden von Google verwendet, um Besuchsstatistiken für unsere Internetseite zu
                    erstellen. Diese Besuchsstatistiken werden durch uns wiederum genutzt, um die Gesamtanzahl der
                    Nutzer zu ermitteln, welche über AdWords-Anzeigen an uns vermittelt wurden, also um den Erfolg oder
                    Misserfolg der jeweiligen AdWords-Anzeige zu ermitteln und um unsere AdWords-Anzeigen für die
                    Zukunft zu optimieren. Weder unser Unternehmen noch andere Werbekunden von Google-AdWords erhalten
                    Informationen von Google, mittels derer Sie identifiziert werden könnten.</p>
                <p className={styles.text}>Mittels des Conversion-Cookies werden personenbezogene Informationen,
                    beispielsweise die durch Sie besuchten Internetseiten, gespeichert. Bei jedem Besuch unserer
                    Internetseiten werden demnach personenbezogene Daten, einschließlich der IP-Adresse des von Ihnen
                    genutzten Internetanschlusses, an Google in den Vereinigten Staaten von Amerika übertragen. Diese
                    personenbezogenen Daten werden durch Google in den Vereinigten Staaten von Amerika gespeichert.
                    Google gibt diese über das technische Verfahren erhobenen personenbezogenen Daten unter Umständen an
                    Dritte weiter.</p>
                <p className={styles.text}>Sie können die Setzung von Cookies durch unsere Internetseite jederzeit
                    mittels einer entsprechenden Einstellung des genutzten Internetbrowsers verhindern und damit der
                    Setzung von Cookies dauerhaft widersprechen. Eine solche Einstellung des genutzten Internetbrowsers
                    würde auch verhindern, dass Google einen Conversion-Cookie auf Ihrem IT-System setzt. Zudem kann ein
                    von Google AdWords bereits gesetzter Cookie jederzeit über den Internetbrowser oder andere
                    Softwareprogramme gelöscht werden.</p>
                <p className={styles.text}>Ferner besteht für Sie die Möglichkeit, der interessenbezogenen Werbung durch
                    Google zu widersprechen. Hierzu müssen Sie von Ihrem genutzten Internetbrowser aus den Link
                    www.google.de/settings/ads aufrufen und dort die gewünschten Einstellungen vornehmen.</p>
                <p className={styles.text}>Eine solche Auswertung erfolgt insbesondere gemäß Art. 6 Abs. 1 lit.f DS-GVO
                    auf Basis unserer berechtigten Interessen an der Einblendung personalisierter Werbung,
                    Marktforschung und/oder bedarfsgerechten Gestaltung seiner Website.</p>
                <p className={styles.text}>Weitere Informationen und die geltenden Datenschutzbestimmungen von Google
                    können unter https://www.google.de/intl/de/policies/privacy/ abgerufen werden.</p>

                <h2 className={styles.secondaryTitle}>5.5 VWO</h2>
                <p className={styles.text}>Wir verwenden auf unserer Website Visual Website Optimizer („VWO“), einen
                    Webanalysedienst von Wingify, 14th Floor, KLJ Tower North, Netaji Subhash Place, Pitam Pura, Delhi
                    110034, India („Wingify“).</p>
                <p className={styles.text}>Wingify setzt Cookies ein, die eine Auswertung der Benutzung unserer Website
                    möglich machen. Diese Cookies generieren Informationen über das Nutzungsverhalten auf dieser Website
                    und speichern Ihre anonymisierte IP-Adresse ab. Anschließend werden die Daten an Server von Wingify
                    nach Indien übertragen und dort aufbewahrt. In unserem Auftrag wird Wingify diese Informationen
                    einsetzen, um Ihre Nutzung der Website zu analysieren und darauf aufbauend unsere Website zu
                    verbessern. Soweit die Cookies nicht schon am Ende der Sitzung verfallen, stehen sie maximal 100
                    Tage zur Verfügung (weitere Informationen dazu finden Sie hier:
                    https://vwo.com/knowledge/cookies-used-by-vwo/).</p>
                <p className={styles.text}>Sie können die Speicherung der Cookies durch eine entsprechende Anpassung in
                    Ihrem Browser wie oben beschrieben verhindern oder schon gespeicherte Cookies löschen. Außerdem
                    können Sie der Erfassung der durch das Cookie generierten und auf Ihre Nutzung der Website bezogenen
                    Daten (inkl. Ihrer anonymisierten IP-Adresse) an Wingify sowie der Verarbeitung dieser Daten
                    jederzeit für die Zukunft unter diesem Link widersprechen http://www.polyas.de?vwo_opt_out=1.</p>
                <p className={styles.text}>Weitere Informationen zum Datenschutz finden Sie hier:
                    https://vwo.com/terms-conditions/.</p>

                <h2 className={styles.secondaryTitle}>6. SSL/TLS-Verschlüsselung</h2>
                <p className={styles.text}>Diese Seite nutzt zur Gewährleistung der Sicherheit der Datenverarbeitung und
                    zum Schutz der Übertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen, Login-Daten oder
                    Kontaktanfragen, die Sie an uns als Betreiber senden, eine SSL-bzw. TLS-Verschlüsselung. Eine
                    verschlüsselte Verbindung erkennen Sie daran, dass in der Adresszeile des Browsers statt einem
                    “http://” ein “https://” steht und an dem Schloss-Symbol in Ihrer Browserzeile.</p>
                <p className={styles.text}>Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die
                    Sie an uns übermitteln, nicht von Dritten mitgelesen werden.</p>

                <h2 className={styles.secondaryTitle}>7. Dienste</h2>
                <h2 className={styles.secondaryTitle}>7.1 Google Maps</h2>
                <p className={styles.text}>Auf unserer Website verwenden wir Google Maps (API) von Google LLC., 1600
                    Amphitheatre Parkway, Mountain View, CA 94043, USA. Google Maps ist ein Webdienst zur Darstellung
                    von interaktiven (Land-)Karten, um geographische Informationen visuell darzustellen. Über die
                    Nutzung dieses Dienstes kann Ihnen beispielsweise unser Standort angezeigt und eine etwaige Anfahrt
                    erleichtert werden.</p>
                <p className={styles.text}>Bereits beim Aufrufen derjenigen Unterseiten, in die die Karte von Google
                    Maps eingebunden ist, werden Informationen über Ihre Nutzung unserer Website (wie z.B. Ihre
                    IP-Adresse) an Server von Google in den USA übertragen und dort gespeichert. Dies erfolgt unabhängig
                    davon, ob Google ein Nutzerkonto bereitstellt, über das Sie eingeloggt sind, oder ob kein
                    Nutzerkonto besteht. Wenn Sie bei Google eingeloggt sind, werden Ihre Daten direkt Ihrem Konto
                    zugeordnet. Wenn Sie die Zuordnung mit Ihrem Profil bei Google nicht wünschen, müssen Sie sich aus
                    Ihrem Google-Benutzerkonto ausloggen. Google speichert Ihre Daten (selbst für nicht eingeloggte
                    Nutzer) als Nutzungsprofile und wertet diese aus. Eine solche Auswertung erfolgt insbesondere gemäß
                    Art. 6 Abs. 1 lit.f DS-GVO auf Basis der berechtigten Interessen von Google an der Einblendung
                    personalisierter Werbung, Marktforschung und/oder bedarfsgerechten Gestaltung seiner Website. Ihnen
                    steht ein Widerspruchsrecht zu gegen die Bildung dieser Nutzerprofile, wobei Sie sich zur Ausübung
                    dessen an Google richten müssen.</p>
                <p className={styles.text}>Google LLC mit Sitz in den USA ist für das us-europäische
                    Datenschutzübereinkommen “Privacy Shield” zertifiziert, welches die Einhaltung des in der EU
                    geltenden Datenschutzniveaus gewährleistet.</p>
                <p className={styles.text}>Wenn Sie mit der künftigen Übermittlung Ihrer Daten an Google im Rahmen der
                    Nutzung von Google Maps nicht einverstanden sind, besteht auch die Möglichkeit, den Webdienst von
                    Google Maps vollständig zu deaktivieren, indem Sie die Anwendung JavaScript in Ihrem Browser
                    ausschalten. Google Maps und damit auch die Kartenanzeige auf dieser Internetseite kann dann nicht
                    genutzt werden.</p>
                <p className={styles.text}>Die Nutzung von Google Maps erfolgt im Interesse einer ansprechenden
                    Darstellung unserer Online-Angebote und an einer leichten Auffindbarkeit der von uns auf der Website
                    angegebenen Orte. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DS-GVO
                    dar.</p>
                <p className={styles.text}>Die Nutzungsbedingungen von Google können Sie unter
                    https://www.google.de/intl/de/policies/privacy/ einsehen, die zusätzlichen Nutzungsbedingungen für
                    Google Maps finden Sie unter https://www.google.com/intl/de_US/help/terms_maps.html</p>
                <p className={styles.text}>Ausführliche Informationen zum Datenschutz im Zusammenhang mit der Verwendung
                    von Google Maps finden Sie auf der Internetseite von Google (“Google Privacy Policy”):
                    https://www.google.de/intl/de/policies/privacy/</p>

                <h2 className={styles.secondaryTitle}>7.2 Google Tag Manager</h2>
                <p className={styles.text}>Diese Webseite verwendet Google Tag Manager, eine cookielose Domain die keine
                    personenbezogenen Daten erfasst.</p>
                <p className={styles.text}>Durch dieses Tool können “Website-Tags” (d.h. Schlagwörter, welche in HTML
                    Elemente eingebunden werden) implementiert und über eine Oberfläche verwaltet werden. Durch den
                    Einsatz des Google Tag Manager können wir automatisiert nachvollziehen, welchen Button, Link oder
                    welches personalisierte Bild Sie aktiv angeklickt haben und können sodann festhalten, welche Inhalte
                    unserer Webseite für Sie besonders interessant sind.</p>
                <p className={styles.text}>Das Tool sorgt zudem für die Auslösung anderer Tags, die ihrerseits unter
                    Umständen Daten erfassen. Google Tag Manager greift nicht auf diese Daten zu. Wenn Sie auf Domain-
                    oder Cookie-Ebene eine Deaktivierung vorgenommen haben, bleibt diese für alle Tracking-Tags
                    bestehen, die mit Google Tag Manager implementiert werden.</p>
                <p className={styles.text}>Die Nutzung von Google tag Manager erfolgt im Interesse einer komfortablen
                    und einfachen Nutzung unserer Internetseite. Dies stellt ein berechtigtes Interesse im Sinne von
                    Art. 6 Abs. 1 lit. f DS-GVO dar.</p>

                <h2 className={styles.secondaryTitle}>7.3 Google WebFonts</h2>
                <p className={styles.text}>Unsere Website nutzt zur einheitlichen Darstellung von Schriftarten so
                    genannte Web Fonts die von der Google LLC., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA
                    bereitgestellt werden. Beim Aufruf einer Seite lädt Ihr Browser die benötigten Web Fonts in ihren
                    Browser-Cache, um Texte und Schriftarten korrekt anzuzeigen.</p>
                <p className={styles.text}>Zu diesem Zweck muss der von Ihnen verwendete Browser Verbindung zu den
                    Servern von Google aufnehmen. Hierdurch erlangt Google Kenntnis darüber, dass über Ihre IP-Adresse
                    unsere Website aufgerufen wurde. Die Nutzung von Google Web Fonts erfolgt im Interesse einer
                    einheitlichen und ansprechenden Darstellung unserer Internetseite.</p>
                <p className={styles.text}>Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f
                    DS-GVO dar.</p>
                <p className={styles.text}>Google LLC mit Sitz in den USA ist für das us-europäische
                    Datenschutzübereinkommen “Privacy Shield” zertifiziert, welches die Einhaltung des in der EU
                    geltenden Datenschutzniveaus gewährleistet.</p>
                <p className={styles.text}>Weitere Informationen zu Google Web Fonts finden Sie unter
                    https://developers.google.com/fonts/faq und in der Datenschutzerklärung von Google:
                    https://www.google.com/policies/privacy/</p>

                <h2 className={styles.secondaryTitle}>7.4 YouTube (Videos)</h2>
                <p className={styles.text}>Wir haben auf dieser Internetseite Komponenten von YouTube integriert.
                    YouTube ist ein Internet-Videoportal, dass Video-Publishern das kostenlose Einstellen von Videoclips
                    und anderen Nutzern die ebenfalls kostenfreie Betrachtung, Bewertung und Kommentierung dieser
                    ermöglicht. YouTube gestattet die Publikation aller Arten von Videos, weshalb sowohl komplette Film-
                    und Fernsehsendungen, aber auch Musikvideos, Trailer oder von Nutzern selbst angefertigte Videos
                    über das Internetportal abrufbar sind.</p>
                <p className={styles.text}>Betreibergesellschaft von YouTube ist die YouTube, LLC, 901 Cherry Ave., San
                    Bruno, CA 94066, USA. Die YouTube, LLC ist einer Tochtergesellschaft der Google Inc., 1600
                    Amphitheatre Pkwy, Mountain View, CA 94043-1351, USA.</p>
                <p className={styles.text}>Durch jeden Aufruf einer der Einzelseiten dieser Internetseite, die von uns
                    betrieben wird und auf welcher eine YouTube-Komponente (YouTube-Video) integriert wurde, wird der
                    Internetbrowser auf Ihrem IT-System automatisch durch die jeweilige YouTube-Komponente veranlasst,
                    eine Darstellung der entsprechenden YouTube-Komponente von YouTube herunterzuladen. Weitere
                    Informationen zu YouTube können unter https://www.youtube.com/yt/about/de/ abgerufen werden. Im
                    Rahmen dieses technischen Verfahrens erhalten YouTube und Google Kenntnis darüber, welche konkrete
                    Unterseite unserer Internetseite durch Sie besucht wird.</p>
                <p className={styles.text}>Sofern die betroffene Person gleichzeitig bei YouTube eingeloggt ist, erkennt
                    YouTube mit dem Aufruf einer Unterseite, die ein YouTube-Video enthält, welche konkrete Unterseite
                    unserer Internetseite Sie besuchen. Diese Informationen werden durch YouTube und Google gesammelt
                    und Ihrem YouTube-Account zugeordnet.</p>
                <p className={styles.text}>YouTube und Google erhalten über die YouTube-Komponente immer dann eine
                    Information darüber, dass Sie unsere Internetseite besucht haben, wenn Sie zum Zeitpunkt des Aufrufs
                    unserer Internetseite gleichzeitig bei YouTube eingeloggt sind; dies findet unabhängig davon statt,
                    ob Sie ein YouTube-Video anklicken oder nicht. Ist eine derartige Übermittlung dieser Informationen
                    an YouTube und Google von Ihnen nicht gewollt, können Sie die Übermittlung dadurch verhindern, dass
                    sie sich vor einem Aufruf unserer Internetseite aus ihrem YouTube-Account ausloggt.</p>
                <p className={styles.text}>Die Nutzung von YouTube erfolgt im Interesse einer komfortablen und einfachen
                    Nutzung unserer Internetseite. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1
                    lit. f DS-GVO dar.</p>
                <p className={styles.text}>Die von YouTube veröffentlichten Datenschutzbestimmungen, die unter
                    https://www.google.de/intl/de/policies/privacy/ abrufbar sind, geben Aufschluss über die Erhebung,
                    Verarbeitung und Nutzung personenbezogener Daten durch YouTube und Google.</p>

                <h2 className={styles.secondaryTitle}>7.5 Optmyzer</h2>
                <h2 className={styles.secondaryTitle}>7.6 BING</h2>
                <h2 className={styles.secondaryTitle}>8. Social Media</h2>
                <p className={styles.text}>Smarketer unterhält Onlinepräsenzen in sozialen Netzwerken wie Facebook,
                    Instagram, LinkedIn, Xing und Twitter (sogenannte „Fanpages“). Auf unseren Fanpages veröffentlichen
                    und teilen wir regelmäßig Inhalte, Angebote und Dienstleistungen. Bei jeder Interaktion auf unseren
                    Fanpages oder anderen Facebook- oder Instagram-Webseiten erfassen die Betreiber der sozialen
                    Netzwerke mit Cookies und ähnlichen Technologien Ihr Nutzungsverhalten. Fanpage Betreiber können
                    allgemeine Statistiken zu den Interessen und demographischen Merkmalen (etwa Alter, Geschlecht,
                    Region) des Fanpage-Publikums einsehen. Wenn Sie soziale Netzwerke nutzen, werden die Art, der
                    Umfang und die Zwecke der Verarbeitung der Daten in sozialen Netzwerken in erster Linie von den
                    Betreibern der sozialen Netzwerke festgelegt.</p>
                <p className={styles.text}>Wir weisen darauf hin, dass dabei Daten der Nutzer außerhalb des Raumes der
                    Europäischen Union verarbeitet werden können. Hierdurch können sich für die Nutzer Risiken ergeben,
                    weil so z.B. die Durchsetzung der Rechte der Nutzer erschwert werden könnte. Im Hinblick auf
                    US-Anbieter die unter dem Privacy-Shield zertifiziert sind, weisen wir darauf hin, dass sie sich
                    damit verpflichten, die Datenschutzstandards der EU einzuhalten.</p>
                <p className={styles.text}>Ferner werden die Daten der Nutzer im Regelfall für Marktforschungs- und
                    Werbezwecke verarbeitet. So können z.B. aus dem Nutzungsverhalten und sich daraus ergebenden
                    Interessen der Nutzer Nutzungsprofile erstellt werden. Die Nutzungsprofile können wiederum verwendet
                    werden, um z.B. Werbeanzeigen innerhalb und außerhalb der Plattformen zu schalten, die mutmaßlich
                    den Interessen der Nutzer entsprechen. Zu diesen Zwecken werden im Regelfall Cookies auf den
                    Rechnern der Nutzer gespeichert, in denen das Nutzungsverhalten und die Interessen der Nutzer
                    gespeichert werden. Ferner können in den Nutzungsprofilen auch Daten unabhängig der von den Nutzern
                    verwendeten Geräte gespeichert werden (insbesondere, wenn die Nutzer Mitglieder der jeweiligen
                    Plattformen sind und bei diesen eingeloggt sind).</p>
                <p className={styles.text}>Die Verarbeitung der personenbezogenen Daten der Nutzer erfolgt auf Grundlage
                    unserer berechtigten Interessen an einer effektiven Information der Nutzer und Kommunikation mit den
                    Nutzern gem. Art. 6 Abs. 1 lit. f. DSGVO. Falls die Nutzer von den jeweiligen Anbietern der
                    Plattformen um eine Einwilligung in die vorbeschriebene Datenverarbeitung gebeten werden, ist die
                    Rechtsgrundlage der Verarbeitung Art. 6 Abs. 1 lit. a., Art. 7 DSGVO.</p>

                <h2 className={styles.secondaryTitle}>8.1 Anbieter / Verantwortlicher</h2>
                <p className={styles.text}>Die Smarketer GmbH als verantwortliches Unternehmen und Anbieter der Inhalte
                    unserer Onlinepräsenzen, können Sie unseren Impressumsangaben auf der jeweiligen Fanpage
                    entnehmen.</p>
                <p className={styles.text}>Soweit Sie über unsere Fanpages direkt mit uns kommunizieren oder persönliche
                    Inhalte mit uns teilen, ist Smarketer für die Verarbeitung Ihrer Daten verantwortlich.</p>

                <h2 className={styles.secondaryTitle}>8.2 Welche Daten werden erfasst?</h2>
                <p className={styles.text}>Wenn Sie unsere Fanpages besuchen, erfasst Smarketer grundsätzlich alle
                    Mitteilungen, Inhalte und sonstigen Informationen, die Sie uns dort direkt mitteilen, etwa wenn Sie
                    etwas auf einer Fanpage posten oder uns eine private Nachricht senden. Wenn Sie ein Konto bei dem
                    jeweiligen sozialen Netzwerk haben, können wir natürlich auch Ihre öffentlichen Informationen sehen,
                    beispielsweise Ihren Benutzernamen, Informationen in Ihrem öffentlichen Profil und Inhalte, die Sie
                    mit einer öffentlichen Zielgruppe teilen.</p>
                <p className={styles.text}>Facebook Seiten-Insights</p>
                <p className={styles.text}>Bei jeder Interaktion mit Fanpages erfasst Facebook mit Cookies und ähnlichen
                    Technologien das Nutzungsverhalten der Fanpage-Besuche. Auf dieser Grundlage erhalten die
                    Fanpage-Betreiber sogenannte „Seiten-Insights“. Seiten-Insights enthalten lediglich statistische,
                    entpersonalisierte (anonymisierte) Angaben zu Besuchern der Fanpage, die somit keiner konkreten
                    Person zugeordnet werden können. Auf die personenbezogenen Daten, die von Facebook für die
                    Erstellung von Seiten-Insights verwendet werden („Seiten-Insights-Daten“), haben wir keinen Zugriff.
                    Die Auswahl und Aufbereitung von Seiten-Insights-Daten erfolgen ausschließlich durch Facebook.</p>
                <p className={styles.text}>Mit Hilfe von Seiten-Insights erhalten wir Erkenntnisse darüber, wie unsere
                    Fanpages genutzt werden, welche Interessen die Besucher unserer Fanpages haben und welche Themen und
                    Inhalte besonders beliebt sind. Dadurch können wir unsere Fanpage-Aktivitäten optimieren,
                    beispielsweise indem wir bei der Planung und Auswahl unserer Inhalte besser auf die Interessen und
                    Nutzungsgewohnheiten unseres Publikums eingehen können.</p>
                <p className={styles.text}>Für die Verarbeitung Ihrer Daten für die Bereitstellung von Seiten-Insights
                    sind die Smarketer GmbH und Facebook gemeinsam verantwortlich. Zu diesem Zweck ist festgelegt,
                    welches Unternehmen in Bezug auf die Verarbeitung von Seiten-Insights-Daten welche
                    Datenschutzpflichten gemäß der DSGVO erfüllt.</p>
                <p className={styles.text}>Weitere Infos zu Seiten-Insights</p>
                <p className={styles.text}>Die Vereinbarung mit Facebook können Sie hier einsehen:
                    https://www.facebook.com/legal/terms/page_controller_addendum</p>
                <p className={styles.text}>Die wesentlichen Inhalte dieser Vereinbarung (einschließlich einer Liste der
                    Seiten-Insights-Daten) hat Facebook hier für Sie zusammengefasst:
                    https://www.facebook.com/legal/terms/information_about_page_insights_data</p>
                <p className={styles.text}>Rechtsgrundlagen:</p>
                <p className={styles.text}>Soweit Sie in Bezug auf die oben beschriebene Erstellung von Seiten-Insights
                    gegenüber Facebook eingewilligt haben, ist die Rechtsgrundlage Artikel 6 Absatz 1 Buchstabe a DSGVO
                    (Einwilligung). Im Übrigen ist die Rechtsgrundlage Artikel 6 Absatz 1 Buchstabe f DSGVO, wobei
                    unsere berechtigten Interessen in den oben genannten Zwecken liegen.</p>

                <h2 className={styles.secondaryTitle}>8.3 Opt Out</h2>
                <p className={styles.text}>Ihre beschriebenen Datenschutzrechte bestehen selbstverständlich auch in
                    Bezug auf die Verarbeitung Ihrer Daten im Zusammenhang mit unseren Fanpages. Sie können der
                    Erfassung und Verwendung Ihrer Daten widersprechen.</p>
                <p className={styles.text}>Für die Verarbeitung Ihrer Daten gemeinsam mit unseren Onlinepräsenzen ist
                    vereinbart, dass diese vorrangig dafür verantwortlich sind, Ihnen Informationen über die
                    Verarbeitung Ihrer Daten bereitzustellen und es Ihnen zu ermöglichen, Ihre Ihnen gemäß DSGVO
                    zustehenden Datenschutzrechte (z. B. Widerspruchsrecht) auszuüben.</p>
                <p className={styles.text}>Für eine detaillierte Darstellung der jeweiligen Verarbeitungen und der
                    Widerspruchsmöglichkeiten (Opt-Out), verweisen wir auf die nachfolgend verlinkten Angaben der
                    Anbieter.</p>
                <p className={styles.text}>Auch im Fall von Auskunftsanfragen und der Geltendmachung von Nutzerrechten,
                    weisen wir darauf hin, dass diese am effektivsten bei den Anbietern geltend gemacht werden können.
                    Nur die Anbieter haben jeweils Zugriff auf die Daten der Nutzer und können direkt entsprechende
                    Maßnahmen ergreifen und Auskünfte geben. Sollten Sie dennoch Hilfe benötigen, dann können Sie sich
                    an uns wenden.</p>
                <p className={styles.text}>Facebook, -Seiten, -Gruppen, (Facebook Ireland Ltd., 4 Grand Canal Square,
                    Grand Canal Harbour, Dublin 2, Irland) auf Grundlage einer Vereinbarung über gemeinsame Verarbeitung
                    personenbezogener Daten Datenschutzerklärung: https://www.facebook.com/about/privacy/,</p>
                <p className={styles.text}>Opt Out: https://www.facebook.com/settings?tab=ads und
                    https://www.youronlinechoices.com, Privacy</p>
                <p className={styles.text}>Shield:
                    https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active.</p>
                <p className={styles.text}>Google/ YouTube (Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA
                    94043, USA) – Datenschutzerklärung: https://policies.google.com/privacy, Opt-Out:
                    https://adssettings.google.com/authenticated, Privacy Shield:
                    httss://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active.</p>
                <p className={styles.text}>Instagram (Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA) –
                    Datenschutzerklärung/ Opt-Out: https://instagram.com/about/legal/privacy/.</p>
                <p className={styles.text}>Twitter (Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA
                    94103, USA) – Datenschutzerklärung:httpss://twitter.com/de/privacy, Opt-Out:
                    https://twitter.com/personalization, Privacy Shield:
                    https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active.</p>
                <p className={styles.text}>LinkedIn (LinkedIn Ireland Unlimited Company Wilton Place, Dublin 2, Irland)
                    – Datenschutzerklärung https://www.linkedin.com/legal/privacy-policy, Opt-Out:
                    https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out, Privacy Shield:
                    https://www.privacyshield.gov/participant?id=a2zt0000000L0UZAA0&status=Active.</p>
                <p className={styles.text}>Xing (XING AG, Dammtorstraße 29-32, 20354 Hamburg, Deutschland) –
                    Datenschutzerklärung/ Opt-Out: https://privacy.xing.com/de/datenschutzerklaerung.</p>
                <p className={styles.text}>Hinweis: Durch die Einstellungen wird ein „Opt-Out“-Cookie auf Ihrem Gerät
                    gespeichert. Wenn Sie die Cookies in diesem Browser löschen, dann müssen Sie diese Einstellung
                    erneut vornehmen. Ferner gilt das Opt-Out nur innerhalb des von Ihnen verwendeten Browsers und nur
                    innerhalb unserer Webdomain, auf der der Link geklickt wurde.</p>

                <h2 className={styles.secondaryTitle}>9. E-Mail-Kontakt</h2>
                <p className={styles.text}>Auf unsere Website befindet sich ein Kontaktformular, welches durch Sie für
                    die elektronische Kontaktaufnahme mit uns genutzt werden kann. Nehmen Sie diese Möglichkeit wahr, so
                    werden die in der Eingabemaske eingegebenen Daten an uns übermittelt und verarbeitet. Diese Daten
                    sind:</p>
                <p className={styles.text}>• Name</p>
                <p className={styles.text}>• E-Mail-Adresse</p>
                <p className={styles.text}>• Telefonnummer</p>
                <p className={styles.text}>• http:// – Website Adresse</p>
                <p className={styles.text}>• Sonstige im Nachrichtentextfeld eingegebene personenbezogene Daten</p>
                <p className={styles.text}>Im Prozess der Absendung dieser Nachricht werden folgende Daten
                    gespeichert:</p>
                <p className={styles.text}>• IP-Adresse</p>
                <p className={styles.text}>• Datum und Uhrzeit der Registrierung</p>
                <p className={styles.text}>• Weitere Daten ergänzen</p>
                <p className={styles.text}>Für die Verarbeitung der Daten wird im Rahmen des absende Vorgangs Ihre
                    Einwilligung eingeholt und auf diese Datenschutzerklärung verwiesen.</p>
                <p className={styles.text}>Alternativ haben Sie die Möglichkeit mit uns über folgende E-Mail-Adressen in
                    Kontakt zu treten:</p>
                <p className={styles.text}>• datenschutz@smarketer.de</p>
                <p className={styles.text}>• Weitere E-Mail-Adressen mit denen der Websitebesucher in Kontakt treten
                    kann ergänzen</p>
                <p className={styles.text}>In diesem Falle verarbeiten und speichern wir die durch die E-Mail
                    übermittelten personenbezogenen Daten. Das werden grundsätzlich Ihre Email-Adresse sein und alle
                    weiteren, von Ihnen preisgegebenen Informationen. Eine solche Datenverarbeitung ist für die
                    Verarbeitung der Konversation erforderlich. Die Datenverarbeitung beruht auf unserem berechtigten
                    Interesse im Sinne des Art. 6 Abs. 1 lit. f) DSGVO und ist damit rechtmäßig, soweit Sie dadurch
                    nicht unangemessen in Ihren Rechten eingeschränkt werden. Dient die Kontaktaufnahme dem Abschluss
                    eines Vertrages, so ist die Verarbeitung gemäß Art. 6 Abs. 1 lit. b) DSGVO rechtmäßig. Geben Sie
                    ungefragt weitere Daten an, so beruht die Verarbeitung auf Ihrer Einwilligung im Sinne des Art. 6
                    Abs. 1 lit. a) DSGVO. Wir Löschen diese Daten, sobald die Konversation beendet ist und kein Zweck
                    mehr zur Speicherung vorliegt. Beendet ist die Konversation dann, wenn sich aus den Umständen
                    entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist. Alle weiteren durch den
                    Absendevorgang übermittelten Daten werden spätestens nach einer Frist von 7 Tagen gelöscht. Der
                    Datenverarbeitung kann durch eine schriftliche Erklärung des Widerrufs der Datenverarbeitung an
                    datenschutz@smarketer.de widersprochen werden.</p>

                <h2 className={styles.secondaryTitle}>10. Offenlegung der Daten durch uns</h2>
                <p className={styles.text}>Wir legen personenbezogene Daten offen, um gesetzliche Verpflichtungen zu
                    erfüllen, Beschwerden über Angebote oder Inhalte, die die Rechte Dritter verletzen, nachzugehen
                    sowie die Rechte, das Eigentum oder die Sicherheit anderer zu schützen. Die Offenlegung dieser Daten
                    geschieht ausschließlich in Übereinstimmung mit den geltenden Gesetzen. Wie oben angegeben, legen
                    wir Ihre personenbezogenen Daten nicht ohne Ihre ausdrückliche Einwilligung gegenüber Dritten für
                    deren Marketingzwecke offen.</p>
                <p className={styles.text}>Wir sind außerdem berechtigt, Ihre personenbezogenen Daten weiterzugeben an
                    Strafverfolgungs- oder Aufsichtsbehörden oder autorisierte Dritte aufgrund eines Auskunftsersuchens
                    in Zusammenhang mit einem Ermittlungsverfahren oder dem Verdacht auf eine Straftat, eine
                    rechtswidrige Handlung oder andere Handlungen, aus denen sich für uns, Sie oder einen anderen Nutzer
                    eine rechtliche Haftung ergeben kann. In solchen Fällen werden wir für die Untersuchung notwendige
                    Daten wie Name, Ort, Postleitzahl, Telefonnummer, E-Mail-Adresse, bisherige Nutzernamen, IP-Adresse,
                    Betrugsbeschwerden und Angebotsübersicht offenlegen.</p>

                <h2 className={styles.secondaryTitle}>11. Rechte der betroffenen Personen</h2>
                <p className={styles.text}>Das geltende Datenschutzrecht gewährt Ihnen als betroffene Person gegenüber
                    dem Verantwortlichen (Spielfeld) hinsichtlich der Verarbeitung Ihrer personenbezogenen Daten
                    umfassende Betroffenenrechte (Auskunfts- und Interventionsrechte), über die wir Sie im Folgenden
                    informieren:</p>

                <h2 className={styles.secondaryTitle}>11.1. Recht auf Bestätigung, Art. 15 Abs. 1 Satz 1 DSGVO:</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, von dem Verantwortlichen eine
                    Bestätigung darüber zu verlangen, ob sie betreffende personenbezogene Daten verarbeitet werden.</p>

                <h2 className={styles.secondaryTitle}>11.2. Recht auf Auskunft, Art. 15 Abs. 1 Satz 2 DSGVO:</h2>
                <p className={styles.text}>Werden personenbezogene Daten der betroffenen Person verarbeitet, so hat die
                    das Recht auf Auskunft über diese personenbezogenen Daten und auf folgende Informationen:</p>
                <p className={styles.text}>a. die Verarbeitungszwecke;</p>
                <p className={styles.text}>b. die Kategorien personenbezogener Daten, die verarbeitet werden;</p>
                <p className={styles.text}>c. die Empfänger oder Kategorien von Empfängern, gegenüber denen die
                    personenbezogenen Daten offengelegt worden sind oder noch offengelegt werden, insbesondere bei
                    Empfängern in Drittländern oder bei internationalen Organisationen;</p>
                <p className={styles.text}>d. falls möglich die geplante Dauer, die für die die personenbezogenen Daten
                    gespeichert werden, oder, falls dies nicht möglich ist, die Kriterien für die Festlegung dieser
                    Dauer;</p>
                <p className={styles.text}>e. das Bestehen eines Rechts auf Berichtigung oder Löschung der sie
                    betreffenden personenbezogenen Daten oder auf Einschränkung der Verarbeitung durch den
                    Verantwortlichen oder eines Widerspruchsrechts gegen diese Verarbeitung;</p>
                <p className={styles.text}>f. das Bestehen eines Beschwerderechts bei einer Aufsichtsbehörde;</p>
                <p className={styles.text}>g. wenn die personenbezogenen Daten nicht bei der betroffenen Person erhoben
                    werden, alle verfügbaren Informationen über die Herkunft der Daten;</p>
                <p className={styles.text}>h. das Bestehen einer automatisierten Entscheidungsfindung einschließlich
                    Profiling gemäß Artikel 22 Absätze 1 und 4 und – zumindest in diesen Fällen – aussagekräftige
                    Informationen über die involvierte Logik sowie die Tragweite und die angestrebten Auswirkungen einer
                    derartigen Verarbeitung für die betroffene Person.</p>

                <h2 className={styles.secondaryTitle}>11.3. Recht auf Berichtigung und Vervollständigung, Art. 16
                    DSGVO:</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, von dem Verantwortlichen unverzüglich
                    die Berichtigung sie betreffender unrichtiger personenbezogener Daten zu verlangen.</p>
                <p className={styles.text}>Unter Berücksichtigung der Zwecke der Verarbeitung hat die betroffene Person
                    das Recht, die Vervollständigung unvollständiger personenbezogener Daten – auch mittels einer
                    ergänzenden Erklärung – zu verlangen.</p>

                <h2 className={styles.secondaryTitle}>11.4. Recht auf Löschung (Recht auf Vergessen werden), Art. 17
                    DSGVO</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, von dem Verantwortlichen zu verlangen,
                    dass sie betreffende personenbezogene Daten unverzüglich gelöscht werden, und der Verantwortliche
                    ist verpflichtet, personenbezogene Daten unverzüglich zu löschen, sofern einer der folgenden Gründe
                    zutrifft:</p>
                <p className={styles.text}>a. Die personenbezogenen Daten sind für die Zwecke, für die sie erhoben oder
                    auf sonstige Weise verarbeitet wurden, nicht mehr notwendig.</p>
                <p className={styles.text}>b. Die betroffene Person widerruft ihre Einwilligung, auf die sich die
                    Verarbeitung gemäß Artikel 6 Absatz 1 Buchstabe a oder Artikel 9 Absatz 2 Buchstabe a stützte, und
                    es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung.</p>
                <p className={styles.text}>c. Die betroffene Person legt gemäß Artikel 21 Absatz 1 Widerspruch gegen die
                    Verarbeitung ein und es liegen keine vorrangigen berechtigten Gründe für die Verarbeitung vor, oder
                    die betroffene Person legt gemäß Artikel 21 Absatz 2</p>
                <p className={styles.text}>d. Widerspruch gegen die Verarbeitung ein.</p>
                <p className={styles.text}>e. Die personenbezogenen Daten wurden unrechtmäßig verarbeitet.</p>
                <p className={styles.text}>f. Die Löschung der personenbezogenen Daten ist zur Erfüllung einer
                    rechtlichen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem
                    der Verantwortliche unterliegt.</p>
                <p className={styles.text}>g. Die personenbezogenen Daten wurden in Bezug auf angebotene Dienste der
                    Informationsgesellschaft gemäß Artikel 8 Absatz 1 erhoben.</p>
                <p className={styles.text}>Das eben genannte gilt allerdings nicht, soweit die Verarbeitung zu
                    rechtlichen Zwecken geschieht (siehe 8.).</p>

                <h2 className={styles.secondaryTitle}>11.5. Recht auf Einschränkung der Verarbeitung, Art. 18
                    DSGVO:</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, von dem Verantwortlichen die
                    Einschränkung der Verarbeitung zu verlangen, wenn eine der folgenden Voraussetzungen gegeben
                    ist:</p>
                <p className={styles.text}>a. die Richtigkeit der personenbezogenen Daten von der betroffenen Person
                    bestritten wird, und zwar für eine Dauer, die es dem Verantwortlichen ermöglicht, die Richtigkeit
                    der personenbezogenen Daten zu überprüfen,</p>
                <p className={styles.text}>b. die Verarbeitung unrechtmäßig ist und die betroffene Person die Löschung
                    der personenbezogenen Daten ablehnt und stattdessen die Einschränkung der Nutzung der
                    personenbezogenen Daten verlangt;</p>
                <p className={styles.text}>c. der Verantwortliche die personenbezogenen Daten für die Zwecke der
                    Verarbeitung nicht länger benötigt, die betroffene Person sie jedoch zur Geltendmachung, Ausübung
                    oder Verteidigung von Rechtsansprüchen benötigt, oder</p>
                <p className={styles.text}>d. die betroffene Person Widerspruch gegen die Verarbeitung gemäß Artikel 21
                    Absatz 1 eingelegt hat, solange noch nicht feststeht, ob die berechtigten Gründe des
                    Verantwortlichen gegenüber denen der betroffenen Person überwiegen.</p>
                <p className={styles.text}>Wurde die Verarbeitung demnach eingeschränkt, so dürfen diese
                    personenbezogenen Daten – von ihrer Speicherung abgesehen – nur mit Einwilligung der betroffenen
                    Person oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der
                    Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen
                    öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet werden.</p>
                <p className={styles.text}>Eine betroffene Person, die eine Einschränkung der Verarbeitung erwirkt hat,
                    wird von dem Verantwortlichen unterrichtet, bevor die Einschränkung aufgehoben wird.</p>

                <h2 className={styles.secondaryTitle}>11.6. Recht auf Datenübertragbarkeit, Art. 20 DSGVO</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, die sie betreffenden personenbezogenen
                    Daten, die sie einem Verantwortlichen bereitgestellt hat, in einem strukturierten, gängigen und
                    maschinenlesbaren Format zu erhalten, und sie hat das Recht, diese Daten einem anderen
                    Verantwortlichen ohne Behinderung durch den Verantwortlichen, dem die personenbezogenen Daten
                    bereitgestellt wurden, zu übermitteln, sofern:</p>
                <p className={styles.text}>a. die Verarbeitung auf einer Einwilligung gemäß Artikel 6 Absatz 1 Buchstabe
                    a oder Artikel 9 Absatz 2 Buchstabe a oder auf einem Vertrag gemäß Artikel 6 Absatz 1 Buchstabe b
                    beruht und</p>
                <p className={styles.text}>b. die Verarbeitung mithilfe automatisierter Verfahren erfolgt.</p>
                <p className={styles.text}>Bei der Ausübung ihres Rechts auf Datenübertragbarkeit hat die betroffene
                    Person das Recht, zu erwirken, dass die personenbezogenen Daten direkt von einem Verantwortlichen
                    einem anderen Verantwortlichen übermittelt werden, soweit dies technisch machbar ist.</p>

                <h2 className={styles.secondaryTitle}>11.7. Recht auf Widerspruch, Art. 21 DSGVO:</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, aus Gründen, die sich aus ihrer
                    besonderen Situation ergeben, jederzeit gegen die Verarbeitung sie betreffender personenbezogener
                    Daten, die aufgrund Art. 6 Abs. 1 lit. e) und f) erfolgt, Widerspruch einzulegen; dies gilt auch für
                    ein auf diese Bestimmungen gestütztes Profiling. Der Verantwortliche verarbeitet die
                    personenbezogenen Daten nicht mehr, es sei denn, er kann zwingende schutzwürdige Gründe für die
                    Verarbeitung nachweisen, die die Interessen, Rechte und Freiheiten der betroffenen Person
                    überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von
                    Rechtsansprüchen.</p>

                <h2 className={styles.secondaryTitle}>11.8. Recht auf Widerruf der erteilten Einwilligung, Art. 7 Abs. 3
                    DSGVO:</h2>
                <p className={styles.text}>Die betroffene Person hat das Recht, ihre Einwilligung jederzeit zu
                    widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der
                    Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt. Die betroffene Person wird vor
                    Abgabe der Einwilligung hiervon in Kenntnis gesetzt. Der Widerruf der Einwilligung muss so einfach
                    wie die Erteilung der Einwilligung sein.</p>

                <h2 className={styles.secondaryTitle}>11.9. Recht auf Beschwerde, Art. 77 Abs. 1 DSGVO</h2>
                <p className={styles.text}>Jede betroffene Person hat unbeschadet eines anderweitigen
                    verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs das Recht auf Beschwerde bei einer
                    Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres gewöhnlichen Aufenthaltsorts, ihres
                    Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, wenn die betroffene Person der Ansicht ist,
                    dass die Verarbeitung der sie betreffenden personenbezogenen Daten gegen diese Verordnung
                    verstößt.</p>

                <h2 className={styles.secondaryTitle}>12 Newsletterversand</h2>
                <h2 className={styles.secondaryTitle}>12.1 Newsletterversand an Bestandskunden</h2>
                <p className={styles.text}>Wenn Sie uns Ihre E-Mailadresse beim Kauf von Waren bzw. Dienstleistungen zur
                    Verfügung gestellt haben, behalten wir uns vor, Ihnen regelmäßig Angebote zu ähnlichen Waren bzw.
                    Dienstleistungen, wie den bereits gekauften, aus unserem Sortiment per E-Mail zuzusenden. Hierfür
                    müssen wir gemäß § 7 Abs. 3 UWG keine gesonderte Einwilligung von Ihnen einholen. Die
                    Datenverarbeitung erfolgt insoweit allein auf Basis unseres berechtigten Interesses an
                    personalisierter Direktwerbung gemäß Art. 6 Abs. 1 lit. f DS-GVO. Haben Sie der Nutzung Ihrer
                    E-Mailadresse zu diesem Zweck anfänglich widersprochen, findet ein Mailversand unsererseits nicht
                    statt. Sie sind berechtigt, der Nutzung Ihrer E-Mailadresse zu dem vorbezeichneten Werbezweck
                    jederzeit mit Wirkung für die Zukunft durch eine Mitteilung an den zu Beginn genannten
                    Verantwortlichen zu widersprechen. Hierfür fallen für Sie lediglich Übermittlungskosten nach den
                    Basistarifen an. Nach Eingang Ihres Widerspruchs wird die Nutzung Ihrer E-Mailadresse zu
                    Werbezwecken unverzüglich eingestellt.</p>

                <h2 className={styles.secondaryTitle}>12.2 Werbenewsletter</h2>
                <p className={styles.text}>Auf unserer Internetseite wird Ihnen die Möglichkeit eingeräumt, den
                    Newsletter unseres Unternehmens zu abonnieren. Welche personenbezogenen Daten bei der Bestellung des
                    Newsletters an uns übermittelt werden, ergibt sich aus der hierzu verwendeten Eingabemaske.</p>
                <p className={styles.text}>Wir informieren unsere Kunden und Geschäftspartner in regelmäßigen Abständen
                    im Wege eines Newsletters über unsere Angebote. Der Newsletter unseres Unternehmens kann von Ihnen
                    grundsätzlich nur dann empfangen werden, wenn</p>
                <p className={styles.text}>1. Sie über eine gültige E-Mail-Adresse verfügen und</p>
                <p className={styles.text}>2. sie sich für den Newsletterversand registriert haben.</p>
                <p className={styles.text}>An die von Ihnen erstmalig für den Newsletterversand eingetragene
                    E-Mail-Adresse wird aus rechtlichen Gründen eine Bestätigungsmail im Double-Opt-In-Verfahren
                    versendet. Diese Bestätigungsmail dient der Überprüfung, ob Sie als Inhaber der E-Mail-Adresse den
                    Empfang des Newsletters autorisiert haben.</p>
                <p className={styles.text}>Bei der Anmeldung zum Newsletter speichern wir ferner die von Ihrem
                    Internet-Service-Provider (ISP) vergebene IP-Adresse des von Ihnen zum Zeitpunkt der Anmeldung
                    verwendeten IT-Systems sowie das Datum und die Uhrzeit der Anmeldung. Die Erhebung dieser Daten ist
                    erforderlich, um den (möglichen) Missbrauch Ihrer E-Mail-Adresse zu einem späteren Zeitpunkt
                    nachvollziehen zu können und dient deshalb unserer rechtlichen Absicherung.</p>
                <p className={styles.text}>Die im Rahmen einer Anmeldung zum Newsletter erhobenen personenbezogenen
                    Daten werden ausschließlich zum Versand unseres Newsletters verwendet. Ferner könnten Abonnenten des
                    Newsletters per E-Mail informiert werden, sofern dies für den Betrieb des Newsletter-Dienstes oder
                    eine diesbezügliche Registrierung erforderlich ist, wie dies im Falle von Änderungen am
                    Newsletterangebot oder bei der Veränderung der technischen Gegebenheiten der Fall sein könnte. Es
                    erfolgt keine Weitergabe der im Rahmen des Newsletter-Dienstes erhobenen personenbezogenen Daten an
                    Dritte. Das Abonnement unseres Newsletters kann durch Sie jederzeit gekündigt werden. Die
                    Einwilligung in die Speicherung personenbezogener Daten, die Sie uns für den Newsletterversand
                    erteilt haben, kann jederzeit widerrufen werden. Zum Zwecke des Widerrufs der Einwilligung findet
                    sich in jedem Newsletter ein entsprechender Link. Ferner besteht die Möglichkeit, sich jederzeit
                    auch direkt auf unserer Internetseite vom Newsletterversand abzumelden oder uns dies auf andere
                    Weise mitzuteilen.</p>
                <p className={styles.text}>Rechtsgrundlage der Datenverarbeitung zum Zwecke des Newsletterversands ist
                    Art. 6 Abs. 1 lit. a DS-GVO.</p>

                <h2 className={styles.secondaryTitle}>12.3 Newslettertracking</h2>
                <p className={styles.text}>Unsere Newsletter enthalten sogenannte Zählpixel. Ein Zählpixel ist eine
                    Miniaturgrafik, die in solche E-Mails eingebettet wird, welche im HTML-Format versendet werden, um
                    eine Logdatei-Aufzeichnung und eine Logdatei-Analyse zu ermöglichen. Dadurch kann eine statistische
                    Auswertung des Erfolges oder Misserfolges von Online-Marketing-Kampagnen durchgeführt werden. Anhand
                    des eingebetteten Zählpixels kann die Firma erkennen, ob und wann eine E-Mail von Ihnen geöffnet
                    wurde und welche in der E-Mail befindlichen Links von Ihnen aufgerufen wurden.</p>
                <p className={styles.text}>Solche über die in den Newslettern enthaltenen Zählpixel erhobenen
                    personenbezogenen Daten, werden von uns gespeichert und ausgewertet, um den Newsletterversand zu
                    optimieren und den Inhalt zukünftiger Newsletter noch besser Ihren Interessen anzupassen. Diese
                    personenbezogenen Daten werden nicht an Dritte weitergegeben. Betroffene Personen sind jederzeit
                    berechtigt, die diesbezügliche gesonderte, über das Double-Opt-In-Verfahren abgegebene
                    Einwilligungserklärung zu widerrufen. Nach einem Widerruf werden diese personenbezogenen Daten von
                    uns gelöscht. Eine Abmeldung vom Erhalt des Newsletters deuten wir automatisch als Widerruf.</p>
                <p className={styles.text}>Eine solche Auswertung erfolgt insbesondere gemäß Art. 6 Abs. 1 lit.f DS-GVO
                    auf Basis unserer berechtigten Interessen an der Einblendung personalisierter Werbung,
                    Marktforschung und/oder bedarfsgerechten Gestaltung unserer Website.</p>

                <h2 className={styles.secondaryTitle}>12.4 Mailchimp</h2>
                <p className={styles.text}>Der Versand unserer E-Mail-Newsletter erfolgt über den technischen
                    Dienstleister The Rocket Science Group, LLC d/b/a MailChimp, 675 Ponce de Leon Ave NE, Suite 5000,
                    Atlanta, GA 30308, USA (https://www.mailchimp.com/), an die wir Ihre bei der Newsletteranmeldung
                    bereitgestellten Daten weitergeben. Diese Weitergabe erfolgt gemäß Art. 6 Abs. 1 lit. f DS-GVO und
                    dient unserem berechtigten Interesse an der Verwendung eines werbewirksamen, sicheren und
                    nutzerfreundlichen Newslettersystems. Bitte beachten Sie, dass Ihre Daten in der Regel an einen
                    Server von MailChimp in den USA übertragen und dort gespeichert werden.</p>
                <p className={styles.text}>MailChimp verwendet diese Informationen zum Versand und zur statistischen
                    Auswertung der Newsletter in unserem Auftrag. Für die Auswertung beinhalten die versendeten E-Mails
                    sog. Web-Beacons bzw. Trackings-Pixel, die Ein-Pixel-Bilddateien darstellen, die auf unserer Website
                    gespeichert sind. So kann festgestellt werden, ob eine Newsletter-Nachricht geöffnet und welche
                    Links ggf. angeklickt wurden. Außerdem werden technische Informationen erfasst (z.B. Zeitpunkt des
                    Abrufs, IP-Adresse, Browsertyp und Betriebssystem). Die Daten werden ausschließlich pseudonymisiert
                    erhoben und werden nicht mir Ihren weiteren persönlichen Daten verknüpft, eine direkte
                    Personenbeziehbarkeit wird ausgeschlossen. Diese Daten dienen ausschließlich der statistischen
                    Analyse von Newsletterkampagnen. Die Ergebnisse dieser Analysen können genutzt werden, um künftige
                    Newsletter besser an die Interessen der Empfänger anzupassen.</p>
                <p className={styles.text}>Wenn Sie der Datenanalyse zu statistischen Auswertungszwecken widersprechen
                    möchten, müssen Sie den Newsletterbezug abbestellen.</p>
                <p className={styles.text}>Des Weiteren kann MailChimp diese Daten gemäß Art. 6 Abs. 1 lit. f DS-GVO
                    selbst aufgrund seines eigenen berechtigten Interesses an der bedarfsgerechten Ausgestaltung und der
                    Optimierung des Dienstes sowie zu Marktforschungszwecken nutzen, um etwa zu bestimmen, aus welchen
                    Ländern die Empfänger kommen. MailChimp nutzt die Daten unserer Newsletterempfänger jedoch nicht, um
                    diese selbst anzuschreiben oder sie an Dritte weiterzugeben.</p>
                <p className={styles.text}>Zum Schutz Ihrer Daten in den USA haben wir mit MailChimp einen
                    Datenverarbeitungsauftrag (“Data-Processing-Agreement”) auf Basis der Standardvertragsklauseln der
                    Europäischen Kommission abgeschlossen, um die Übermittlung Ihrer personenbezogenen Daten an
                    MailChimp zu ermöglichen. Dieser Datenverarbeitungsvertrag kann bei Interesse unter nachstehender
                    Internetadresse eingesehen werden:
                    https://mailchimp.com/help/about-mailchimp-the-eu-swiss-privacy-shield-and-the-gdpr/.</p>
                <p className={styles.text}>MailChimp ist darüber hinaus unter dem us-europäischen Datenschutzabkommen
                    “Privacy Shield” zertifiziert und verpflichtet sich damit, die EU-Datenschutzvorgaben
                    einzuhalten.</p>
                <p className={styles.text}>Die Datenschutzbestimmungen von MailChimp können Sie hier einsehen:
                    https://mailchimp.com/legal/privacy/</p>

                <h2 className={styles.secondaryTitle}>12.5 Newsletter-Abmeldung</h2>
                <p className={styles.text}>Sie können sich jederzeit vom Newsletter abmelden. Dies geht über den
                    direkten Kontakt mit uns. Schreiben Sie uns eine Email mit folgenden Daten:</p>
                <p className={styles.text}>– Vorname, Nachname (persönliche Ansprache im Newsletter)</p>
                <p className={styles.text}>– E-Mail-Adresse (Empfänger des Newsletters)</p>
                <p className={styles.text}>an die Adresse Newsletterabmeldung@smarketer.de</p>

                <h2 className={styles.secondaryTitle}>13 Inhalte der Webseite</h2>
                <h2 className={styles.secondaryTitle}>13.1 Datenverarbeitung zur Bestellabwicklung</h2>
                <p className={styles.text}>Die von uns erhobenen personenbezogenen Daten werden im Rahmen der
                    Vertragsabwicklung an das mit der Lieferung beauftragte Transportunternehmen weitergegeben, soweit
                    dies zur Lieferung der Ware erforderlich ist. Ihre Zahlungsdaten geben wir im Rahmen der
                    Zahlungsabwicklung an das beauftragte Kreditinstitut weiter, sofern dies für die Zahlungsabwicklung
                    erforderlich ist. Sofern Zahlungsdienstleister eingesetzt werden, informieren wir hierüber
                    nachstehend explizit. Die Rechtsgrundlage für die Weitergabe der Daten ist hierbei Art. 6 Abs. 1
                    lit. b DS-GVO.</p>

                <h2 className={styles.secondaryTitle}>13.2 Kontaktaufnahme / Kontaktformular</h2>
                <p className={styles.text}>Im Rahmen der Kontaktaufnahme mit uns (z.B. per Kontaktformular oder E-Mail)
                    werden personenbezogene Daten erhoben. Welche Daten im Falle eines Kontaktformulars erhoben werden,
                    ist aus dem jeweiligen Kontaktformular ersichtlich. Diese Daten werden zur Beantwortung Ihres
                    Anliegens bzw. für die Kontaktaufnahme und die damit verbundene technische Administration
                    gespeichert und verwendet, zusätzlich werden Daten zur statistischen Zwecken gespeichert.
                    Rechtsgrundlage für die Verarbeitung der Daten ist unser berechtigtes Interesse an der Beantwortung
                    Ihres Anliegens gemäß Art. 6 Abs. 1 lit. f DS-GVO. Zielt Ihre Kontaktierung auf den Abschluss eines
                    Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Art. 6 Abs. 1 lit. b DS-GVO.
                    Ihre Daten werden nach abschließender Bearbeitung Ihrer Anfrage gelöscht, dies ist der Fall, wenn
                    sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist und
                    sofern keine gesetzlichen Aufbewahrungspflichten entgegenstehen.</p>

                <h2 className={styles.secondaryTitle}>13.3 Dienstleistungen / Digitale Güter</h2>
                <p className={styles.text}>Wir übermitteln personenbezogene Daten an Dritte nur dann, wenn dies im
                    Rahmen der Vertragsabwicklung notwendig ist, etwa an das mit der Zahlungsabwicklung beauftragte
                    Kreditinstitut.</p>
                <p className={styles.text}>Eine weitergehende Übermittlung der Daten erfolgt nicht bzw. nur dann, wenn
                    Sie der Übermittlung ausdrücklich zugestimmt haben. Eine Weitergabe Ihrer Daten an Dritte ohne
                    ausdrückliche Einwilligung, etwa zu Zwecken der Werbung, erfolgt nicht.</p>
                <p className={styles.text}>Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. b DS-GVO, der die
                    Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.</p>

                <h2 className={styles.secondaryTitle}>13.4 Kommentarfunktion Blog</h2>
                <p className={styles.text}>Wir bieten den Nutzern auf einem Blog, der sich auf unserer Website befindet,
                    die Möglichkeit, individuelle Kommentare zu einzelnen Blog-Beiträgen zu hinterlassen. Ein Blog ist
                    ein auf einer Internetseite geführtes, in der Regel öffentlich einsehbares Portal, in welchem eine
                    oder mehrere Personen, die Blogger oder Web-Blogger genannt werden, Artikel posten oder Gedanken in
                    sogenannten Blogposts niederschreiben können. Die Blogposts können in der Regel von Dritten
                    kommentiert werden.</p>
                <p className={styles.text}>Hinterlassen Sie einen Kommentar in dem auf dieser Website veröffentlichten
                    Blog, werden neben Ihren hinterlassenen Kommentaren auch Angaben zum Zeitpunkt der Kommentareingabe
                    sowie der gewählte Nutzername (Pseudonym) gespeichert und veröffentlicht. Ferner wird die von Ihrem
                    Internet-Service-Provider (ISP) vergebene IP-Adresse mitprotokolliert. Diese Speicherung der
                    IP-Adresse erfolgt aus Sicherheitsgründen und für den Fall, dass Sie durch einen abgegebenen
                    Kommentar die Rechte Dritter verletzt oder rechtswidrige Inhalte gepostet haben. Die Speicherung
                    dieser personenbezogenen Daten erfolgt daher in unserem eigenen Interesse, damit wir uns im Falle
                    einer Rechtsverletzung exkulpieren können. Dies stellt ein berechtigtes Interesse im Sinne von Art.
                    6 Abs. 1 lit. f DS-GVO dar.Es erfolgt keine Weitergabe dieser erhobenen personenbezogenen Daten an
                    Dritte, sofern eine solche Weitergabe nicht gesetzlich vorgeschrieben ist oder unserer
                    Rechtsverteidigung dient.</p>

                <h2 className={styles.secondaryTitle}>13.5 Bewerbungsmanagement / Stellenbörse</h2>
                <p className={styles.text}>Wir erheben und verarbeiten die personenbezogenen Daten von Bewerbern zum
                    Zwecke der Abwicklung des Bewerbungsverfahrens. Die Verarbeitung kann auch auf elektronischem Wege
                    erfolgen. Dies ist insbesondere dann der Fall, wenn ein Bewerber entsprechende Bewerbungsunterlagen
                    auf dem elektronischen Wege, beispielsweise per E-Mail oder über ein auf der Website befindliches
                    Webformular, an uns übermittelt. Schließen wir einen Anstellungsvertrag mit einem Bewerber, werden
                    die übermittelten Daten zum Zwecke der Abwicklung des Beschäftigungsverhältnisses unter Beachtung
                    der gesetzlichen Vorschriften gespeichert. Wird von uns kein Anstellungsvertrag mit dem Bewerber
                    geschlossen, so werden die Bewerbungsunterlagen sechs Monate nach Bekanntgabe der Absageentscheidung
                    automatisch gelöscht, sofern einer Löschung keine sonstigen berechtigten Interessen unsererseits
                    entgegenstehen. Sonstiges berechtigtes Interesse in diesem Sinne ist beispielsweise eine
                    Beweispflicht in einem Verfahren nach dem Allgemeinen Gleichbehandlungsgesetz (AGG).</p>
                <p className={styles.text}>Die Datenverarbeitung erfolgt insoweit allein auf Basis unseres berechtigten
                    Interesses gemäß Art. 6 Abs. 1 lit.f DSGVO.</p>

                <h2 className={styles.secondaryTitle}>13.6 LinkedIn Pixel</h2>
                <p className={styles.text}>Unsere Website nutzt zur Konversionsmessung das Besucheraktions-Pixel von
                    LinkedIn, LinkedIn Ireland Unlimited Company Wilton Place, Dublin 2, Irland.</p>
                <p className={styles.text}>So kann das Verhalten der Seitenbesucher nachverfolgt werden, nachdem diese
                    durch Klick auf z. B. eine LinkedIn-Werbeanzeige auf die Website des Anbieters weitergeleitet
                    wurden. Dadurch können die Wirksamkeit der LinkedIn-Werbeanzeigen für statistische und
                    Marktforschungszwecke ausgewertet werden und zukünftige Werbemaßnahmen optimiert werden.</p>
                <p className={styles.text}>Die erhobenen Daten sind für uns als Betreiber dieser Website anonym, wir
                    können keine Rückschlüsse auf die Identität der Nutzer ziehen. Die Daten werden aber von LinkedIn
                    gespeichert und verarbeitet, sodass eine Verbindung zum jeweiligen Nutzerprofil möglich ist und
                    LinkedIn die Daten für eigene Werbezwecke, entsprechend der LinkedIn-Datenverwendungsrichtlinie
                    verwenden kann. Diese Verwendung der Daten kann von uns als Seitenbetreiber nicht beeinflusst
                    werden.</p>
                <p className={styles.text}>In den Datenschutzhinweisen von LinkedIn finden Sie weitere Hinweise zum
                    Schutz Ihrer Privatsphäre: https://www.linkedin.com/legal/privacy-policy.</p>
                <p className={styles.text}>Privacy statement</p>
            </div>
        </div>
    </OverviewPage>;
};

export default PrivacyDe;
