import React from 'react';

import {fetchLocale} from "../../store/ducks/locale";
import {useDispatch, useSelector} from "react-redux";

import styles from './LangSelect.module.scss'

const LangSelect = props => {
    const {white} = props;

    const dispatch = useDispatch();
    const getLang = useSelector(state => state.localeState.locale ? state.localeState.locale.lang : 'de');

    return (
        <div className={styles.selectWrapper}>
            <select
                name="country"
                value={getLang}
                onChange={e => dispatch(fetchLocale(e.target.value))}
                className={`${styles.select} ${white ? styles.white : ''}`}
            >
                <option value="de">Deutsch</option>
                <option value="en">English</option>
            </select>
        </div>
    );
};

export default LangSelect;
