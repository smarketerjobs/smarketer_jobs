import styles from "./JobSearchHeader.module.scss";
import {NavLink, useHistory} from "react-router-dom";
import LogoWhite from "../../../media/LogoSmarketerJobsWhite.png";
import premiumPartner from "../../../media/Premium_Google_Partner.svg";
import cssPremiumPartner from "../../../media/CSS Premium Partner.svg";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMapMarkerAlt, faSearch} from "@fortawesome/fontawesome-free-solid";
import FlagsSelect from "../../MainSearchPage/FlagSelect/FlagSelect";
import {openLoginModal, openRegisterModal} from "../../store/ducks/modals";
import LangSelect from "../LangSelect/LangSelect";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {clearFilters} from "../../store/ducks/search";

const JobSearchHeader = ({isPreview, city, title, setCity, setTitle, closeCompany, setCountryCode}) => {

    const history = useHistory();
    const dispatch = useDispatch();

    const {userId} = useSelector(state => state.userIdState);
    const {locale} = useSelector(state => state.localeState);
    const {searchCountries} = useSelector(state => state.searchState);

    const setSpaceToPlus = name => name ? name.split(' ').join('+') : '';
    const setPlusToSpace = name => name ? name.split('+').join(' ') : '';
    const onCountryChange = countryCode => setCountryCode(countryCode);

    const onTitleChange = value => {
        if (isPreview) {
            history.push(`/job-search?query=${value}`);
        } else {
            setTitle(value);
        }
    };

    const onCityChange = value => {
        if (isPreview) {
            history.push(`/job-search?city=${value}`);
        } else {
            setCity(value);
        }
    };

    return <header className={styles.headerSection}>
        <div className={styles.wrapper}>
            <div className={styles.content}>
                <div className={styles.logoSection}>
                    <NavLink to={'/search'} className={styles.mainLogoWrapper}
                             onClick={() => dispatch(clearFilters())}>
                        <img src={LogoWhite} className={styles.mainLogo} alt='Logo: smarketer'/>
                    </NavLink>
                    <div className={styles.partners}>
                        <img src={premiumPartner} className={styles['partnerImg1']} alt='premium partner'/>
                        <img src={cssPremiumPartner} className={styles['partnerImg2']}
                             alt='Css premium partner'/>
                    </div>
                </div>
                <div className={styles.itemsInputsSection}>
                    <div className={styles.titleJob}>
                        <span className={styles.wrapperSearchIcon}>
                            <FontAwesomeIcon icon={faSearch} className={styles.searchJobIcon}/>
                        </span>
                        <input type="text" className={styles.input} value={setPlusToSpace(title)}
                               onInput={closeCompany} onChange={e => onTitleChange(e.target.value)}
                               onBlur={() => setTitle(setPlusToSpace(setPlusToSpace(title).trim()))}/>
                    </div>
                    <div className={styles.titleCountry}>
                        <FontAwesomeIcon icon={faMapMarkerAlt} className={styles.mapMarker}/>
                        <div className={styles.flagSelectWrapper}>
                            <FlagsSelect searchCountries={searchCountries} onCountryChange={onCountryChange}/>
                        </div>
                        <input type="text" className={styles.input} value={setPlusToSpace(city)}
                               onInput={closeCompany} onChange={e => onCityChange(e.target.value)}
                               onBlur={() => setCity(setSpaceToPlus(setPlusToSpace(city).trim()))}/>
                    </div>
                </div>
                <div className={styles.itemsLinksSection}>
                    {userId
                        ? <NavLink to="/job-add" className={styles.btnJobAd}>
                            {locale ? locale['Publish a job'] : ''}
                        </NavLink>
                        : <button onClick={() => dispatch(openRegisterModal())} className={styles.btnJobAd}>
                            {locale ? locale['Publish a job'] : ''}
                        </button>
                    }
                    {!userId
                        ? <span className={styles.alreadyRegisterText}>
                                    {locale ? locale['Already registered?'] : ''}{' '}
                            <button onClick={() => dispatch(openLoginModal())}
                                    className={styles.alreadyRegisterLogIn}>
                                {locale ? locale['Log in'] : ''}
                            </button>
                        </span>
                        : ''
                    }
                </div>
                <div className={styles.justifyRightSearch}>
                    <LangSelect white={true} isSearchPage={true}/>
                </div>
            </div>
            <div className={styles.itemsInputsSectionBottom}>
                <div className={styles.titleJob}>
                    <span className={styles.wrapperSearchIcon}>
                        <FontAwesomeIcon icon={faSearch} className={styles.searchJobIcon}/>
                    </span>
                    <input type="text" className={styles.input} value={setPlusToSpace(title)}
                           onInput={closeCompany} onChange={e => onTitleChange(e.target.value)}
                           onBlur={() => setTitle(setPlusToSpace(setPlusToSpace(title).trim()))}/>
                </div>
                <div className={styles.titleCountry}>
                    <FontAwesomeIcon icon={faMapMarkerAlt} className={styles.mapMarker}/>
                    <div className={styles.flagSelectWrapper}>
                        <FlagsSelect searchCountries={searchCountries} onCountryChange={onCountryChange}/>
                    </div>
                    <input type="text" className={styles.input} value={setPlusToSpace(city)}
                           onInput={closeCompany} onChange={e => onCityChange(e.target.value)}
                           onBlur={() => setCity(setSpaceToPlus(setPlusToSpace(city).trim()))}/>
                </div>
            </div>
        </div>
    </header>
};

export default JobSearchHeader;
