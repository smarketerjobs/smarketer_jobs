import React from 'react';

import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCheckCircle, faInfoCircle} from '@fortawesome/fontawesome-free-solid';
import {NavLink} from "react-router-dom";
import {openPaketModal} from "../../store/ducks/modals";
import {setAlertIsShown} from "../../store/ducks/alert";

import styles from './alertMessage.module.scss';
import closeImg from "../../../media/close.png";

const AlertMessage = () => {
    const {locale} = useSelector(state => state.localeState);
    const {status, message, isShown} = useSelector(state => state.alertState);

    const {isNotEmptyUserAccountData} = useSelector(state => state.userAccountDataState);

    const dispatch = useDispatch()

    const getMessageText = () => {
        switch (message) {
            case 'E-mail already exists':
                return locale['E-mail already exists'];
            case 'payment success':
                return locale['Payment is successful'];
            case 'error_429':
                return locale['Something went wrong'];
            case 'payment failure':
                return locale['Payment error'];
            case 'reset password success':
                return locale['Password successfully reset'];
            case 'reset password failure':
                return locale['Password reset error'];
            case 'billing info successfully saved':
                return locale[message];
            case 'Successfully saved':
                return locale[message];
            case 'Registration successful':
                return locale[message];
            case 'Application time must be in the future':
                return locale[message];
            case 'Your contingent of active job error':
                return locale[message];
            case 'Succesfully saved':
                return locale[message];
            case 'Cover letter sent successfully':
                return locale[message];
            case 'Error sending cover letter':
                return locale[message];
            case 'Error deleting account':
                return locale[message];
            case 'Password changed successfully':
                return locale[message];
            case 'Password change error':
                return locale[message];
            case 'Company information successfully updated':
                return locale[message];
            case 'Error updating company information':
                return locale[message];
            case 'Wrong e-mail format':
                return locale[message];
            case 'Required fields cannot be empty':
                return locale[message];
            case 'Company information saved successfully':
                return locale[message];
            case 'Duplicate company name':
                return locale[message];
            case 'Company deleted successfully':
                return locale[message];
            case 'Account successfully deleted':
                return locale[message];
            case 'job not found':
                return locale[message];
            case 'The logo file is too small':
                return locale[message];
            case 'Successfully Send':
                return locale[message];
            case 'Sorry, It`s an Error':
                return locale[message];
            case 'Coupon cannot be empty':
                return locale[message];
            case 'coupon is valid':
                return locale[message];
            case 'coupon is not valid':
                return locale[message];
            case 'The logo should be square':
                return locale[message];
            case 'Message has been sent':
                return locale[message];
            case 'choose professional plan':
                return locale[message];
            case 'Company is not active error':
                return locale[message];
            case 'Company name is already registered':
                return locale[message];
            case "You can't activate more companies on current plan.":
                return locale[message];
            case 'You can enable only job in active company.':
                return locale[message];
            case 'No payment information':
                return locale[message];
            case 'Password is incorrect':
                return locale[message];
            case 'Job already activated':
                return locale[message];
            case "You can't activate job with future start date":
                return locale[message];
            case "You can't activate job which date is in past":
                return locale[message];
            case "You can't activate more jobs on current plan":
                return locale[message];
            case "Application date is set to 30 days in the future":
                return locale[message];
            case "No active company found":
                return locale[message];
            case "Email is not valid":
                return locale[message];
            case "Company name cannot be empty":
                return locale[message];
            case "First name cannot be empty":
                return locale[message];
            case "Last name cannot be empty":
                return locale[message];
            case "Country cannot be empty":
                return locale[message];
            case "First name is not present":
                return locale[message];
            case "Company name is not present":
                return locale[message];
            case "Last name is not present":
                return locale[message];
            case "Country is not present":
                return locale[message];
            case "Email is not present":
                return locale[message];
            case 'Your contingent of active job error with link':
                return (
                    <>
                        {locale['Your contingent of active job error - p1']}
                        <button
                            className={styles.link}
                            onClick={() => {
                                dispatch(setAlertIsShown(false))
                                dispatch(openPaketModal())
                            }}>
                            {locale['select a higher plan']}
                        </button>
                    </>
                );
            case 'Company data is empty':
                return <span className={styles.whiteText}>{locale ? locale['no company data error message'] : ''}{' '}
                    <NavLink
                        to={'/company-info'}
                        onClick={() => dispatch(setAlertIsShown(false))}
                        className={styles.whiteLink}
                        activeClassName={styles.bgTransparent}
                    >
                        {locale ? locale['Company'] : ''}
                    </NavLink>
                </span>;
            case 'empty_data':
                return <>
                    {!isNotEmptyUserAccountData
                        ? <div className={styles.whiteText}>
                            {locale ? locale['no account data error message'] : ''}{' '}
                            <NavLink
                                to={'/profile'}
                                className={styles.whiteLink}
                                onClick={() => dispatch(setAlertIsShown(false))}
                                activeClassName={styles.bgTransparent}
                            >
                                {locale ? locale['Account'] : ''}
                            </NavLink>
                        </div>
                        : ''
                    }
                </>;
            default:
                return message;
        }
    };

    return <>
        {isShown ? <div className={`${styles.root} ${status !== 'success' ? styles.errorMsg : styles.successMsg}`}>
            <div>
                <FontAwesomeIcon
                    icon={status === 'success' ? faCheckCircle : faInfoCircle}
                    className={`${styles.icon} ${status !== 'success' ? styles.infoIcon : ''}`}
                />
            </div>
            <div className={styles.message}>
                {message && locale ? (
                    <span className={status === 'success' ? styles.alertMessageSuccess : styles.alertMessageFailure}>
                        {getMessageText()}
                    </span>
                ) : ''}
            </div>
            <div className={styles.button}>
                <button
                    className={styles.btnClose}
                    onClick={() => dispatch(setAlertIsShown(false))}
                >
                    <img src={closeImg} alt="close" className={styles.img}/>
                </button>
            </div>
        </div> : ''}
    </>;
};

export default AlertMessage;
