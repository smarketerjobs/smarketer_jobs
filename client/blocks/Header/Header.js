import React from 'react';

import {NavLink} from "react-router-dom";

import HeaderItems from "./HeaderItems";

import styles from './Header.module.scss';

import logo from "../../../media/logo.svg";
import premiumPartner from "../../../media/Premium_Google_Partner.svg";
import cssPremiumPartner from "../../../media/CSS Premium Partner.svg";

const Header = ({hiddenLangSwitcher}) => {
    return (
        <header className={styles.header}>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={styles.logoSection}>
                        <NavLink to="/job-add?active=overview" activeClassName={styles.mainLogoLink}>
                            <img src={logo} alt='Logo: smarketer' className={styles.mainLogo}/>
                        </NavLink>
                        <div className={styles.partners}>
                            <img src={premiumPartner} alt='premium partner'/>
                            <img src={cssPremiumPartner} alt='Css premium partner'/>
                        </div>
                    </div>
                    <div className={styles.itemsSection}>
                        <HeaderItems
                            hiddenLangSwitcher={hiddenLangSwitcher}
                            isOpen={false}
                        />
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
