import React, {useEffect} from 'react';

import axios from "axios";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import LangSelect from "../LangSelect/LangSelect";
import {getUserIdFailure} from "../../store/ducks/user";

import styles from "./Header.module.scss";
import {useHistory} from "react-router";

const Header = props => {

    const history = useHistory();

    const {hiddenLangSwitcher, isOpen} = props;

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);

    const logout = () => {
        axios.get('/ajax.php', {
            params: {model: 'user', mode: 'logout'},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    dispatch(getUserIdFailure(''));
                    history.push('/');
                } else {
                    console.error(data);
                }
            })
            .catch(err => console.error(err));
    };

    const hideScroll = () => {
        const body = document.getElementsByTagName('body')[0];
        if (body) {
            body.style.overflow = 'hidden';
        }
    }

    useEffect(() => {
        return () => {
            const body = document.getElementsByTagName('body')[0]
            if (body) {
                body.style.overflow = 'auto';
            }
        }
    }, []);

    useEffect(() => {
        if (isOpen) {
            hideScroll();
        }
    }, [isOpen]);

    return <>
        <ul id='header-items' className={styles.listOfLinks}>
            <li className={styles.item}>
                <NavLink
                    id="nav-link-job-ad"
                    to="/job-add?active=overview"
                    className={styles.itemLink}
                    activeClassName={styles.active}
                >
                    {locale ? locale['Job advertisements'] : ''}
                </NavLink>
            </li>
            <li className={styles.item}>
                <NavLink
                    id="nav-link-company-info"
                    to="/company-info"
                    className={styles.itemLink}
                    activeClassName={styles.active}
                >
                    {locale ? locale['Company'] : ''}
                </NavLink>
            </li>
            <li className={styles.item}>
                <NavLink
                    id="nav-link-profile"
                    to="/profile"
                    className={styles.itemLink}
                    activeClassName={styles.active}
                >
                    {locale ? locale['Account'] : ''}
                </NavLink>
            </li>
            <li className={styles.item}>
                <button
                    id="nav-btn-logout"
                    onClick={logout}
                    className={styles.itemLink}
                >
                    {locale ? locale['Logout'] : ''}
                </button>
            </li>
            {!hiddenLangSwitcher ? (
                <li className={styles.item}><LangSelect/></li>
            ) : null}
        </ul>
    </>;
};

export default Header;
