import React from 'react';
import Footer from '../Footer/Footer';
import Header from '../Header/Header';

import styles from './OverviewPage.module.scss';

import separator from "../../../media/separator_bg.png";

const OverviewPage = props => {
    const {hiddenLangSwitcher, children, isNoResponsive} = props;

    return (
        <div className={`${styles.root} ${isNoResponsive ? styles.noResponsive : ''}`}>
            <Header hiddenLangSwitcher={hiddenLangSwitcher}/>
            <div className={styles.wrapper}>{children}</div>
            <div className={styles.separator}>
                <img src={separator} alt="separator line" className={styles.separatorImg}/>
            </div>
            <Footer className={styles.footerPosition} isNoResponsive={true}/>
        </div>
    );
};

export default OverviewPage;
