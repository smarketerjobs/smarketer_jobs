import React from "react";

export class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {error: null, errorInfo: null};
    }

    componentDidCatch(error, errorInfo) {
        this.setState(() => ({error: error, errorInfo: errorInfo}));
    }

    render() {
        if (this.state.errorInfo) {
            console.error(this.state.error, this.state.errorInfo.componentStack);
            return <div/>;
        }

        return this.props.children;
    }
}
