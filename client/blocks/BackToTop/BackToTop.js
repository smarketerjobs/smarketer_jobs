import React, {useEffect, useRef, useState} from "react";

import styles from './BackToTop.module.scss';

import arrow from '../../../media/up-arrow.svg';

function useScrollY() {
    const [y, setY] = useState(window.scrollY);

    const listener = e => setY(window.scrollY);

    useEffect(() => {
        window.addEventListener("scroll", listener);
        return () => {
            window.removeEventListener("scroll", listener);
        };
    });

    return y;
}

const BackToTop = () => {
    const mounted = useRef({
        id: 0,
        state: true
    });
    const scrollY = useScrollY();

    useEffect(() => {
        if (scrollY === 0) {
            mounted.current.state = true;

            if (mounted.current.id) {
                clearTimeout(mounted.current.id);
                mounted.current.id = false;
            }
        }
    }, [scrollY])

    return <button
        className={`${styles.btnToTop} ${mounted.current && scrollY > 600 ? styles.show : ''}`}
        onClick={() => {
            if (mounted.current) {
                mounted.current.state = false;
                mounted.current.id = setTimeout(() => {
                    mounted.current.id = false;
                    mounted.current.state = true;
                }, 1000);
                window.scrollTo({
                    top: 0,
                    behavior: 'smooth'
                });
            }
        }}
    >
        <img src={arrow} alt="arrow" className={styles.arrow}/>
    </button>
};

export default BackToTop;