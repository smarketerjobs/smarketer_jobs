import React, {useEffect} from 'react';

import {useDispatch, useSelector} from "react-redux";

import styles from './CountrySelect.module.scss';
import {fetchCountries} from "../../../store/ducks/countries";

const CountrySelect = ({onChange, setCountries, selected}) => {

    const dispatch = useDispatch();
    const {countries} = useSelector(state => state.countriesState)

    useEffect(() => {
        dispatch(fetchCountries())
    }, []);

    return (
        <div className={styles.selectWrapper}>
            <select name="country" onChange={onChange} className={styles.select} value={selected}>
                {(setCountries ? setCountries : countries).map(country => <option
                    value={country.country_code}>{country.country}</option>)}
            </select>
        </div>
    );
};

export default CountrySelect;
