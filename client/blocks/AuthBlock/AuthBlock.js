import React from 'react';

import styles from './AuthBlock.module.scss';

const AuthBlock = props => {
    return (
        <section className={`${styles.root} ${!props.isRegisterPage && styles.mainRoot}`}>
            {props.children}
        </section>
    );
};

AuthBlock.defaultProps = {
    isRegisterPage: true,
};

export default AuthBlock;
