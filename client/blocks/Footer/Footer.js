import React from 'react';
import styles from './Footer.module.scss';
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";

const Footer = ({className, isNoResponsive}) => {
    const {locale} = useSelector(state => state.localeState);

    return (
        <footer className={`${styles.root} ${isNoResponsive ? styles.noResponsive : ''} ${className || ''}`}>
            <div className={styles.preFooter}>
                <div className={styles.preFooterFirstSection}>
                    <div className={styles.wrapper}>
                        <div className={styles.content}>
                            <span className={styles.text}>
                                &#169; {new Date().getFullYear()} Smarketer GmbH | <span className={styles.linksBlock}>
                                    <NavLink to={`/privacy-${locale ? locale['lang'] : 'en'}`} target="_blank"
                                             activeClassName={styles.bgTransparent} className={styles.link}>
                                        {locale ? locale['Privacy'] : ''}
                                    </NavLink> | <NavLink to={`/imprint-${locale ? locale['lang'] : 'en'}`}
                                                          activeClassName={styles.bgTransparent}
                                                          target="_blank"
                                                          className={styles.link}>
                                        {locale ? locale['Imprint'] : ''}
                                    </NavLink>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;
