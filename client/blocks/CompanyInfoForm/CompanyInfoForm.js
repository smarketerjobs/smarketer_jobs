import React, {useEffect, useState} from "react";
import {showAlert} from "../../store/ducks/alert";
import axios from "axios";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCloudUploadAlt, faLightbulb} from "@fortawesome/fontawesome-free-solid";
import {useDispatch, useSelector} from "react-redux";
import Toggle from "react-toggle";
import {fetchDeleteCompany, fetchUserCompaniesData, fetchUserJobs, removeDraftCompany,} from "../../store/ducks/user";
import {validateUrl} from "../../utils";

import styles from "../../CompanyInfo/CompanyInfo.module.scss";

const CompanyInfoForm = ({company}) => {
    const dispatch = useDispatch();
    const {plan} = useSelector(state => state.planState);
    const {locale} = useSelector(state => state.localeState);
    const {userId} = useSelector(state => state.userIdState);
    const {userAccountData} = useSelector(state => state.userAccountDataState);
    const {userCompaniesData} = useSelector(state => state.userCompaniesDataState);

    const [getNewLogo, setNewLogo] = useState(null);
    const [getSaveFail, setSaveFail] = useState(false);
    const [getConfirmDelete, setConfirmDelete] = useState(false);
    const [getIsValidWebsite, setIsValidWebsite] = useState(true);

    const [getId, setId] = useState('');
    const [getName, setName] = useState('');
    const [getLogo, setLogo] = useState('');
    const [getEmail, setEmail] = useState('');
    const [getStatus, setStatus] = useState(false);
    const [getWebsite, setWebsite] = useState('https://');
    const [getDescription, setDescription] = useState('');

    const responseError = data => {
        console.error(data);
        if (data.error === 'Duplicate company name') {
            dispatch(showAlert('failure', 'Company name is already registered'));
        } else if (data.error === 'The logo file is too small (min size is 112x112px)') {
            dispatch(showAlert('failure', 'The logo file is too small'));
        } else if (data.error === 'The logo should be square.') {
            dispatch(showAlert('failure', 'The logo should be square'));
        } else if (data.error === 'Company with this name already exist.') {
            dispatch(showAlert('failure', 'Company name is already registered'));
        } else {
            dispatch(showAlert('failure', 'failure'));
        }
    };

    const setCompanyStatus = status => {
        axios.post('/ajax.php', {
            mode: status ? 'activate' : 'deactivate',
            model: 'company',
            company_id: getId
        })
            .then(({data}) => {
                if (data.data === 'Company and related jobs deactivated successfully.') {
                    setStatus(false);
                } else if (data.data === 'Company and activated successfully') {
                    setStatus(true);
                } else if (data.error) {
                    dispatch(showAlert('failure', data.error))
                } else {
                    console.error(data);
                }
                dispatch(fetchUserJobs(userId));
            })
            .catch(err => {
                console.error(err);
                dispatch(fetchUserJobs(userId));
                dispatch(showAlert('failure', 'failure'));
            });
    };

    const save = () => {
        if (getName && getEmail && getWebsite && getIsValidWebsite) {
            const data = new FormData();
            data.append('model', 'company');
            data.append('mode', 'save_company_data');
            data.append('enctype', 'multipart/form-data');

            data.append('company_name', getName);
            data.append('company_email', getEmail);
            data.append('company_website', getWebsite);
            data.append('company_description', getDescription);
            data.append('company_logo', getNewLogo ? getNewLogo : getLogo);

            axios.post('/ajax.php', data, {})
                .then(({data}) => {
                    if (data.data) {
                        dispatch(showAlert('success', 'Company information saved successfully'));
                        setId(data.data.company_id);
                        if (data.data['company_logo_link']) {
                            setLogo(data.data['company_logo_link'])
                            setNewLogo(null)
                        }
                    } else {
                        responseError(data);
                    }
                })
                .catch(err => {
                    console.error(err);
                    dispatch(showAlert('failure', 'failure'));
                });
        } else {
            setSaveFail(true);
        }
    };

    const update = () => {
        if (getName && getEmail && getId && getWebsite && getIsValidWebsite) {
            const data = new FormData();
            data.append('model', 'company');
            data.append('mode', 'update_company_data');
            data.append('enctype', 'multipart/form-data');

            data.append('company_id', getId);
            data.append('company_name', getName);
            data.append('company_email', getEmail);
            data.append('company_description', getDescription);
            data.append('active', getStatus ? '1' : '0');
            data.append('company_logo', getNewLogo ? getNewLogo : getLogo);
            data.append('company_website', getWebsite);

            axios.post('/ajax.php', data, {})
                .then(({data}) => {
                    if (data.data) {
                        dispatch(showAlert('success', 'Company information saved successfully'));
                        if (data.data['company_logo_link']) {
                            setLogo(data.data['company_logo_link'])
                            setNewLogo(null)
                        }
                    } else {
                        responseError(data);
                        dispatch(fetchUserCompaniesData(userId));
                    }
                })
                .catch(err => {
                    console.error(err);
                    dispatch(showAlert('failure', 'failure'));
                    dispatch(fetchUserCompaniesData(userId));
                });
        } else {
            setSaveFail(true);
        }
    };

    const saveCompanyData = () => {
        if (getId) {
            update();
        } else {
            save();
        }
    };

    const isShowCompanyToggle = () => {
        if (getId && userCompaniesData.filter(c => c.company_id).length > 1) {
            return true;
        } else return !!(getId && userCompaniesData.filter(c => c.company_id).length === 1 &&
            !+userCompaniesData.filter(c => c.company_id)[0].active);
    }

    useEffect(() => {
        if (company) {
            setId(company['company_id']);
            setStatus(!!+company['active']);
            setName(company['company_name']);
            setLogo(company['company_logo']);
            setEmail(company['company_email']);
            setWebsite(company['company_website'] || 'https://');
            setDescription(company['company_description']);
        }
    }, [company]);

    useEffect(() => {
        if (plan && company && userAccountData) {
            if (company.company_email) {
                setEmail(company.company_email);
            } else {
                if (+plan['max_active_companies'] === 1) {
                    setEmail(userAccountData.email);
                }
            }
        }
    }, [userAccountData]);

    return <div className={styles.blockOfItem}>
        {!getId && userCompaniesData.length > 1
            ? <div className={styles.removeCompanyBtnWrapper}>
                <button className={styles.removeCompanyBtn} onClick={() => dispatch(removeDraftCompany(company))}>
                    <img src="../../../media/close.png" alt="close" className={styles.img}/>
                </button>
            </div>
            : ''
        }
        <div className={styles.title}>
            <h2 className={styles.blockName}>{getName ? getName : locale ? locale['Company'] : ''}</h2>
            <p className={styles.text}>{locale ? locale['Basic settings sub titel'] : ''}</p>
        </div>
        <div className={`${styles.blockDetails} ${styles.column}`} style={{marginTop: 0}}>
            <div className={styles.form}>
                {isShowCompanyToggle()
                    ? <div className={styles.formEl}>
                        <span className={styles.labelName}>{locale ? locale['Company Status'] : ''}</span>
                        <div className={styles.checkboxWrapper}>
                            <Toggle checked={getStatus} icons={false}
                                    onChange={e => setCompanyStatus(e.target.checked)}/>
                        </div>
                    </div>
                    : ''
                }
                <div className={styles.formEl}>
                    <span className={`${styles.labelName} ${getSaveFail && !getName ? styles.warningMessage : ''}`}>
                        {locale ? locale['Company'] : ''}*
                    </span>
                    <div className={styles.inputWrapper}>
                        <input type="text" name='company_name' value={getName}
                               className={`${styles.input} ${getSaveFail && !getName ? styles.warningMessage : ''}`}
                               onChange={e => setName(e.target.value)} onBlur={() => setName(getName.trim())}
                        />
                        <div className={styles.inputLabel}>
                            <div className={styles.blockWithIcon}>
                                <FontAwesomeIcon icon={faLightbulb} className={styles.itemHint}/>
                            </div>
                            <div className={styles.blockWithText}>
                                <span className={styles.bold}>{locale ? locale['hint'] : ''}: </span>
                                {locale ? locale['The company name influences'] : ''}
                                {' '}
                                <span className={styles.bold}>
                                    {getName ? getName : locale ? locale['Company'] : ''}
                                </span>
                                ".
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.formEl}>
                    <div className={styles.labelWrapper}>
                        <div className={`${styles.labelName} ${getSaveFail && !getEmail ? styles.warningMessage : ''}`}>
                            {locale ? locale['E-Mail Address'] : ''}*
                        </div>
                        <div className={styles.subName}>
                            {locale ? locale['For incoming applications of the candidates'] : ''}
                        </div>
                    </div>
                    <div className={styles.inputWrapper}>
                        <input value={getEmail} type='email' name='company_email'
                               className={`${styles.input} ${getSaveFail && !getEmail ? styles.warningMessage : ''}`}
                               placeholder='name@companyname.de' onChange={e => setEmail(e.target.value)}
                               onBlur={() => setEmail(getEmail.trim())}
                        />
                    </div>
                </div>
                <div className={styles.formEl}>
                    <span
                        className={`
                            ${styles.labelName}
                            ${getSaveFail && !getWebsite ? styles.warningMessage : ''}
                            ${!getIsValidWebsite ? styles.warningMessage : ''}
                        `}
                    >
                        {locale ? locale['website'] : ''}*
                    </span>
                    <div className={styles.inputWrapper}>
                        <input
                            type="text"
                            name="company_website"
                            value={getWebsite}
                            onBlur={() => {
                                setWebsite(getWebsite.trim());
                                setIsValidWebsite(validateUrl(getWebsite.trim()));
                            }}
                            onFocus={() => setIsValidWebsite(true)}
                            onChange={e => setWebsite(e.target.value)}
                            className={`
                                ${styles.input}
                                ${getSaveFail && !getWebsite ? styles.warningMessage : ''}
                                ${!getIsValidWebsite ? styles.warningMessage : ''}
                            `}
                            placeholder="https://www.google.com/"
                        />
                    </div>
                </div>
                <div className={styles.formEl}>
                    <div className={styles.labelWrapper}>
                        <div className={styles.labelName}>{locale ? locale['Company logo'] : ''}</div>
                        <div className={styles.subName}>{locale ? locale['companyLogoRequirement'] : ''}</div>
                    </div>
                    <div className={`${styles.inputWrapper} ${styles.setPositionRelative} ${styles.fileInput}`}>
                        <input
                            type="file"
                            name="company_logo"
                            className={`${styles.input} ${styles.file}`}
                            accept="image/x-png,image/gif,image/jpeg"
                            onChange={e => {
                                setLogo(e.target.files[0]);
                                setNewLogo(e.target.files[0]);
                            }}
                        />
                        <div className={styles.inputLabel}>
                            <div className={styles.blockWithIcon}>
                                <FontAwesomeIcon icon={faCloudUploadAlt} className={styles.itemSky}/>
                            </div>
                            <div className={styles.blockWithText}>
                                <span className={`${styles.bold} ${styles.text}`}>
                                    {locale ? locale['Choose a file'] : ''}
                                </span>
                            </div>
                            {getNewLogo ? <span className={styles.successMsg}>
                                {locale ? locale['Logo added'] : ''}
                            </span> : ''}
                        </div>
                        {getLogo && !getNewLogo ? <div className={styles.companyLogoWrapper}>
                            <img src={`../../../../${getLogo}`} alt="logo" className={styles.companyLogo}/>
                        </div> : ''}
                        <div className={styles.fileInputHintWrapper}>
                            <div className={styles.blockWithIcon}>
                                <FontAwesomeIcon icon={faLightbulb} className={styles.itemHint}/>
                            </div>
                            <div className={styles.blockWithText}>
                                <span className={styles.bold}>{locale ? locale['hint'] : ''}: </span>
                                {locale ? locale['Your uploaded logo should match'] : ''}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`${styles.formEl} ${styles.alignCenter}`}>
                    <span className={styles.labelName}>{locale ? locale['Introduction'] : ''}</span>
                    <div className={styles.inputWrapper}>
                        <textarea value={getDescription} name='company_description'
                                  className={`${styles.input} ${styles.textarea}`}
                                  onChange={e => setDescription(e.target.value)}
                                  onBlur={() => setDescription(getDescription.trim())}
                        />
                    </div>
                </div>
                <div className={styles.btnWrapper}>
                    {getConfirmDelete
                        ? <div className={styles.confirmDeleteWrapper}>
                            <div className={styles.confirmDeleteMessage}>
                                {locale ? locale['Confirm delete company message'] : ''}
                            </div>
                            <div className={styles.confirmDeleteBtnWrapper}>
                                <button className={styles.confirmDeleteYes}
                                        onClick={() => dispatch(fetchDeleteCompany(getId, userId))}>
                                    {locale ? locale['Yes'] : ''}
                                </button>
                                <button className={styles.confirmDeleteNo}
                                        onClick={() => setConfirmDelete(false)}>
                                    {locale ? locale['No'] : ''}
                                </button>
                            </div>
                        </div>
                        : <>
                            <button type='button' className={styles.saveBtn} onClick={saveCompanyData}>
                                {locale ? locale['to save'] : ''}
                            </button>
                            {getId && userCompaniesData.filter(c => c.company_id).length > 1
                                ? <button type='button' className={styles.deleteBtn}
                                          onClick={() => setConfirmDelete(true)}>
                                    {locale ? locale['Delete Company'] : ''}
                                </button>
                                : ''
                            }
                        </>
                    }
                </div>
            </div>
        </div>
    </div>;
};

export default CompanyInfoForm;