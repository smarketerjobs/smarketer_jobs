import React, {useEffect, useState} from 'react';

import axios from "axios";
import {faCheck} from "@fortawesome/fontawesome-free-solid";
import {planPrice} from "../utils/planPrice";
import {useDispatch, useSelector} from "react-redux";

import AuthBlock from "../blocks/AuthBlock/AuthBlock";
import {setCoupon} from "../store/ducks/cart";
import {showAlert} from "../store/ducks/alert";
import OverviewPage from "../blocks/OverviewPage/OverviewPage";
import {formatMoney} from "../utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import subscribe from '../../js/stripe-checkout';

import styles from './Сart.module.scss'

const Cart = () => {
    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {plan, coupon, period} = useSelector(state => state.cartState);

    const [getPrising, setPrising] = useState(null);
    const [getFetchingRecalculate, setFetchingRecalculate] = useState(false);

    const recalculate = (coupon) => {
        if (plan && !getFetchingRecalculate) {
            setFetchingRecalculate(true);
            axios.get('/ajax.php', {
                params: {
                    'model': 'subscription',
                    'mode': 'recalculate',
                    'plan_name': plan === 'starter' ? plan : plan + period,
                    'coupon': coupon
                },
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    setFetchingRecalculate(false);
                    if (data.data) {
                        if (coupon) {
                            if (+data.data['coupon_valid']) {
                                dispatch(showAlert('success', 'coupon is valid'));
                            } else {
                                dispatch(showAlert('failure', 'coupon is not valid'));
                            }
                        }
                        setPrising(data.data);
                    } else {
                        if (coupon) {
                            dispatch(showAlert('failure', 'failure'));
                        }
                        console.error(data);
                    }
                })
                .catch(err => {
                    setFetchingRecalculate(false);
                    console.error(err);
                    dispatch(showAlert('failure', 'failure'));
                });
        }
    };

    const checkCoupon = e => {
        e ? e.preventDefault() : '';
        coupon ? recalculate(coupon) : dispatch(showAlert('failure', 'Coupon cannot be empty'));
    };

    useEffect(() => {
        recalculate();

        const stripeScript = document.getElementById('stripe-js');

        if (!stripeScript) {
            const script = document.createElement('script');

            script.setAttribute('src', 'https://js.stripe.com/v3/');
            script.setAttribute('id', 'stripe-js');
            script.setAttribute('data-checked-head', 'false');
            document.head.appendChild(script);
        }
    }, []);

    return <>
        <OverviewPage isNoResponsive={true}>
            <div className={styles.cartWrapper}>
                <div className={`${styles.couponWrapper} ${styles.mb30}`}>
                    <AuthBlock isRegisterPage={false}>
                        <div className={styles.AuthBlockHeader}>
                            <h3 className={styles.title}>{locale ? locale['Coupon'] : ''}</h3>
                        </div>
                        <div>
                            <p className={styles.description}>
                                {locale ? locale['Enter your coupon and get a discount'] : ''}
                            </p>
                            <div className={styles.authForm}>
                                <label className={styles.inputLabel}>
                                    <span className={styles.inputLabelText}>{locale ? locale['Coupon'] : ''}:</span>
                                    <input type='text' name='coupon' className={styles.input} value={coupon}
                                           onChange={e => dispatch(setCoupon(e.target.value))}
                                           onBlur={() => dispatch(setCoupon(coupon.trim()))}/>
                                </label>
                                <div className={styles.submitBtnWrapper}>
                                    <button type='button' className={`${styles.submitBtn} ${styles.resetMargins}`}
                                            onClick={checkCoupon} disabled={getFetchingRecalculate}>
                                        {locale ? locale['Check the coupon'] : ''}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </AuthBlock>
                </div>
                <div className={`${styles.planOverview} ${plan === 'plus' ? styles.pt50 : ''}`}>
                    <ul className={styles.listOfPlans}>
                        {plan === 'starter'
                            ? <li className={`${styles.planItem} ${styles.starterPlan}`}>
                                <span className={styles.planName}>{planPrice.starter.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span
                                        className={styles.planValue}>{planPrice.starter.montPrice}</span>
                                    <span className={styles.planPeriod}>/mt</span>
                                    <span className={`${styles.planPriceDetail} ${styles.hidden}`}>0</span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['1 job ad for free'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Online for 30 days'] : ''}</span>
                                    </li>
                                </ul>
                            </li>
                            : ''
                        }
                        {plan === 'basic'
                            ? <li className={styles.planItem}>
                                <span className={styles.planName}>{planPrice.basic.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>
                                        {period === '_year' ? planPrice.basic.monthPrice : planPrice.basic.yearPrice}
                                    </span>
                                    <span className={styles.planPeriod}>
                                        /mt
                                        {period === '_year'
                                            ? <span className={styles.rechtek}>
                                                &#8364;{planPrice.basic.yearPrice}
                                            </span>
                                            : ''
                                        }
                                    </span>
                                    <span className={styles.planPriceDetail}>
                                        {period === '_year' && planPrice.basic.monthPrice * 12}
                                        {period === '_year' && locale && locale['annual payment']}
                                    </span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['5 jobs included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Optimization of your data'] : ''}</span>
                                    </li>
                                </ul>
                            </li>
                            : ''
                        }
                        {plan === 'plus'
                            ? <li className={`${styles.planItem} ${styles.topsellerPlan}`}>
                                <span className={styles.topSelletContent}>{locale ? locale['Topseller'] : ''}</span>
                                <span className={styles.planName}>{planPrice.plus.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>
                                        {period === '_year'
                                            ? planPrice.plus.monthPrice
                                            : planPrice.plus.yearPrice
                                        }
                                    </span>
                                    <span className={styles.planPeriod}>
                                        /mt
                                        {period === '_year' &&
                                        <span className={styles.rechtek}>
                                            &#8364;{planPrice.plus.yearPrice}
                                        </span>
                                        }
                                    </span>
                                    <span className={styles.planPriceDetail}>
                                        {period === '_year' && planPrice.plus.monthPrice * 12}
                                        {period === '_year' && locale && locale['annual payment']}
                                    </span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['10 jobs included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Optimization of your data'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Reporting included'] : ''}</span>
                                    </li>
                                </ul>
                            </li>
                            : ''
                        }
                        {plan === 'professional'
                            ? <li className={styles.planItem}>
                                <span className={styles.planName}>{planPrice.professional.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>
                                        {period === '_year'
                                            ? planPrice.professional.monthPrice
                                            : planPrice.professional.yearPrice
                                        }
                                    </span>
                                    <span className={styles.planPeriod}>
                                        /mt
                                        {period === '_year' && <span className={styles.rechtek}>
                                            &#8364;{planPrice.professional.yearPrice}
                                        </span>}
                                    </span>
                                    <span className={styles.planPriceDetail}>
                                        {period === '_year' && planPrice.professional.monthPrice * 12}
                                        {period === '_year' && locale && locale['annual payment']}
                                    </span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['unlimited job ads'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Reporting included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Support included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Branded Landingpage & Video'] : ''}</span>
                                    </li>
                                </ul>
                            </li>
                            : ''
                        }
                    </ul>
                </div>
            </div>
            <div className={styles.delimiter}/>
            <div className={styles.cartWrapper}>
                <div className={styles.paymentInfo}>
                    <div className={styles.infoBlock}>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Plan price'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Plan Price description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising ? (
                                    <>
                                        {`${formatMoney(
                                            getPrising['plan_price'],
                                            locale ? locale['lang'] : null,
                                        )}`}
                                        &#8364;
                                        {`${period === '_year'
                                            ? locale ? locale['year'] : ''
                                            : locale ? locale['month'] : ''}`}
                                    </>
                                ) : ''}
                            </div>
                        </div>
                    </div>
                    <div className={styles.infoBlock}>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Coupon valid'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Coupon Valid description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising && +getPrising['coupon_valid']
                                    ? locale ? locale['yes'] : ''
                                    : locale ? locale['no'] : ''
                                }
                            </div>
                        </div>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Coupon amount'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Coupon Amount description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising && +getPrising['coupon_valid'] && getPrising['coupon_amount'] ? (
                                    <>
                                        {`${formatMoney(
                                            getPrising['coupon_amount'],
                                            locale ? locale['lang'] : null,
                                        )}`}
                                        &#8364;
                                    </>
                                ) : '0'}
                            </div>
                        </div>
                    </div>
                    <div className={styles.infoBlock}>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Tax rate'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Tax Rate description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising ? `${getPrising['tax_rate']}%` : ''}
                            </div>
                        </div>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Tax value'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Tax Value description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising ? (
                                    <>
                                        {`${formatMoney(
                                            getPrising['tax_value'],
                                            locale ? locale['lang'] : null,
                                        )}`}
                                        &#8364;
                                    </>
                                ) : ''}
                            </div>
                        </div>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Plan with taxes'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Plan With Taxes description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising ? (
                                    <>
                                        {`${formatMoney(
                                            getPrising['plan_with_taxes'],
                                            locale ? locale['lang'] : null,
                                        )}`}
                                        &#8364;
                                    </>
                                ) : ''}
                            </div>
                        </div>
                        <div className={styles.paymentInfoRow}>
                            <div className={`${styles.paymentTitle} ${styles.tooltip}`}>
                                {locale ? locale['Total price'] : ''}:
                                <div className={styles.tooltipText}>
                                    <span>{locale ? locale['Total Price description'] : ''}</span>
                                </div>
                            </div>
                            <div className={styles.paymentDescription}>
                                {getPrising ? (
                                    <>
                                        {`${formatMoney(
                                            getPrising['total_price'],
                                            locale ? locale['lang'] : null,
                                        )}`}
                                        &#8364;
                                    </>
                                ) : ''}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.delimiter}/>
            <div className={styles.subscribeBlock}>
                <button className={`${styles.submitBtn} ${styles.mb50}`}
                        onClick={() => subscribe(plan === 'starter' ? plan : plan + period, coupon)}>
                    {locale ? locale['Subscribe'] : ''}
                </button>
            </div>
        </OverviewPage>
    </>;
};

export default Cart;
