import React from 'react';
import ReactDOM from "react-dom";

import ReactGA from 'react-ga';
import {Provider} from "react-redux";
import TagManager from 'react-gtm-module';

import {store} from "./store/store";

import {App} from "./App";

const tagManagerArgs = {gtmId: 'GTM-WGWKMRR'};

TagManager.initialize(tagManagerArgs);
ReactGA.initialize('UA-8768206-12');
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
