import React, {useEffect, useState} from 'react';

import {useDispatch, useSelector} from "react-redux";

import {useHistory} from "react-router";
import {useLocation} from "react-router-dom";

import BackToTop from "../blocks/BackToTop/BackToTop";
import JobContent from "./JobContent";
import OverviewPage from '../blocks/OverviewPage/OverviewPage';
import JobStatusContent from "./JobStatusContent";
import {LeftColumnList} from "./LeftColumnList";

import {getListLocationParams} from "../utils";

import {fetchPlan} from "../store/ducks/plan";
import {fetchUserAccountData, fetchUserBillingData, fetchUserCompaniesData, fetchUserJobs} from "../store/ducks/user";
import {fetchJobCatalogs} from "../store/ducks/job";

const JobAd = () => {

    const history = useHistory();
    const location = useLocation();
    const params = new URLSearchParams(location.search);
    const [getActiveTab, setActiveTab] = useState(params.get('active') || 'overview');

    const dispatch = useDispatch();
    const {userJobs} = useSelector(state => state.userJobsState);
    const {userId, error} = useSelector(state => state.userIdState);

    const [search, setSearch] = useState('');
    const [showActiveStatus, setActiveStatus] = useState(false);
    const [getSelectedJobId, setSelectedJobId] = useState(0);

    const [filteredListOfJobs, setFilteredListOfJobs] = useState([]);

    const handleSetActiveJob = job_id => {
        setSelectedJobId(job_id || 0);
        if (getActiveTab !== 'create') {
            history.push(location.pathname + '?active=create');
        }
    };

    useEffect(() => {
        const params = getListLocationParams(location);
        if (params?.active?.length) {
            setActiveTab(params.active[0]);
        }
    }, [location]);

    const addStyle = () => {
        const link = document.createElement('link');
        const head = document.getElementsByTagName('head')[0];
        link.setAttribute('href', '../css/react-datepicker.css');
        link.setAttribute('rel', 'stylesheet');
        head.appendChild(link);
    }

    useEffect(() => {
        if (userId) {
            dispatch(fetchUserJobs(userId));
            dispatch(fetchUserCompaniesData(userId));
        }
    }, [userId]);

    useEffect(() => {
        dispatch(fetchPlan());
        dispatch(fetchUserAccountData());
        dispatch(fetchUserBillingData());

        dispatch(fetchJobCatalogs());

        addStyle();
    }, []);

    useEffect(() => {
        if (userJobs && search) {
            setFilteredListOfJobs(userJobs.filter(job => job.title.toLowerCase().includes(search.toLowerCase())));
        } else {
            setFilteredListOfJobs(userJobs);
        }
    }, [search, userJobs]);

    useEffect(() => {
        if (!userId && error) {
            history.push('/');
        }
    }, [userId, error]);

    return (
        <>
            <BackToTop/>
            <OverviewPage isNoResponsive={true}>
                <LeftColumnList
                    search={search}
                    setSearch={(q) => setSearch(q)}
                    getActiveTab={getActiveTab}
                    setActiveStatus={setActiveStatus}
                    showActiveStatus={showActiveStatus}
                    handleSetActiveJob={handleSetActiveJob}
                    filteredListOfJobs={filteredListOfJobs}
                />
                {getActiveTab === 'overview' ? (
                    <JobStatusContent
                        jobs={filteredListOfJobs}
                        handleSetActiveJob={handleSetActiveJob}
                    />
                ) : ''}
                {getActiveTab === 'create' ? (
                    <JobContent
                        countActive={userJobs.filter(job => !!+job.active).length}
                        jobId={getSelectedJobId}
                        setSelectedJobId={id => setSelectedJobId(id)}
                    />
                ) : ''}
            </OverviewPage>
        </>
    );
};

export default JobAd;
