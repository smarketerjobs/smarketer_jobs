import React from 'react';
import * as moment from "moment";
import {getUserDate, kitcut} from "../utils";
import {faClock} from "@fortawesome/fontawesome-free-solid";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import styles from "./JobAd.module.scss";

const GenerateJobList = ({value, handleSetActiveJob}) => (
    <div
        className={`${styles.jobList} ${!!+value.active && styles.activeJobStatus}`}
        onClick={() => handleSetActiveJob(value.job_id)}
    >
        <div className={styles.titleWithStatus}>
            <div className={styles.circle}/>
            <div title={value.title.length > 40 ? value.title : ''}>
                {kitcut(value.title, 40)}
            </div>
        </div>
        <span className={styles.date}>
            <FontAwesomeIcon icon={faClock} className={styles.jobEndTimeIcon}/>
            {value['date_start'] ? (
                moment(getUserDate(value['date_start'])).format('DD.MM.YYYY')
            ) : (
                moment(getUserDate(value['date_posted'])).format('DD.MM.YYYY')
            )}
        </span>
    </div>
);

export default GenerateJobList;
