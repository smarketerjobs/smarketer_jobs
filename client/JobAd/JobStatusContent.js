import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";

import ListOfJobs from "./ListOfJobs";

import styles from './JobStatusCounterAd.module.scss';
import {getPlanTitle} from "../utils/plan";

const JobStatusContent = props => {
    const {jobs, handleSetActiveJob} = props;

    const {plan} = useSelector(state => state.planState);
    const {locale} = useSelector(state => state.localeState);
    const {userCompaniesData} = useSelector(state => state.userCompaniesDataState);

    const [getCounter, setCounter] = useState({view: 0, interactions: 0, direct: 0});
    const [showListOfJobs, setListOfJobs] = useState([]);
    const [getPlanName, setPlanName] = useState('');
    const [getPlanMaxJob, setPlanMaxJob] = useState(0);
    const [countActive, setCountActive] = useState(0);

    const statisticCounter = arrayName => {
        const resultObject = {view: 0, interactions: 0, direct: 0};
        arrayName.map(job => {
            resultObject.view += +job['job_view'];
            resultObject.direct += +job['job_conversion'];
            resultObject.interactions += +job['job_share'] + +job['job_redirect'];
        });

        return resultObject;
    };

    useEffect(() => {
        setCountActive(jobs.filter(job => !!+job.active).length)
    }, [jobs])

    useEffect(() => {
        if (plan) {
            setPlanName(getPlanTitle(plan.plan_name));
            setPlanMaxJob(plan['max_active_jobs']);
        }
    }, [plan]);

    useEffect(() => {
        setCounter(statisticCounter(jobs));
        setListOfJobs(jobs);
    }, [jobs]);

    return <div className={styles.rightColumnList}>
        <div className={`${styles.wrapper}`}>
            <div className={styles.header}>
                <h2 className={styles.title}>{locale ? locale['Overview'] : ''}</h2>
            </div>
            <div>
                <p className={styles.description}>{locale ? locale['Overview sub titel'] : ''}</p>
                {plan && locale ? (
                    <p className={`${styles.description} ${styles.ml10}`}>
                        {getPlanName.includes('Professional') ? (
                            <>
                                {locale ? locale['You have chosen the'] : ''}
                                {' '}
                                <span className={styles.green}>{getPlanName}</span>
                                {' '}
                                {locale ? locale['plan. You can publish an'] : ''}
                                {' '}
                                <span className={styles.green}>{locale ? locale['unlimited'] : ''}</span>
                                {' '}
                                {locale ? locale['number of job ads.'] : ''}
                            </>
                        ) : (
                            <>
                                {locale ? locale['You have chosen the'] : ''}
                                {' '}
                                <span className={getPlanMaxJob - countActive ? styles.green : styles.red}>
                                {getPlanName}
                            </span>
                                {' '}
                                {locale ? locale['plan. You can still publish'] : ''}
                                {' '}
                                <span className={getPlanMaxJob - countActive ? styles.green : styles.red}>
                                {getPlanMaxJob - countActive}
                            </span>
                                {' '}
                                {getPlanMaxJob - countActive > 1
                                    ? locale ? locale['job ads.'] : ''
                                    : locale ? locale['job ad.'] : ''
                                }
                            </>
                        )}
                    </p>
                ) : <p className={styles.subTitle}>{locale ? locale['Loading'] + '...' : ''}</p>}
                <button
                    onClick={() => handleSetActiveJob(0)}
                    className={styles.btnJobAdd}
                >
                    {locale ? '+ ' + locale['New Job Ad'] : ''}
                </button>
                <span className={styles.titleOfStatus}>{locale ? locale['Job advertisements'] : ''}</span>
                <table className={styles.statisticTable}>
                    <thead>
                    <tr>
                        <th className={styles.tableHeadItem}>{locale ? locale['Title'] : ''}</th>
                        {userCompaniesData.length > 1
                        && <th className={styles.tableHeadItem}>{locale ? locale['Company'] : ''}</th>
                        }
                        <th className={styles.tableHeadItem}>{locale ? locale['Preview'] : ''}</th>
                        <th className={styles.tableHeadItem}>{locale ? locale['Views'] : ''}</th>
                        <th className={styles.tableHeadItem}>{locale ? locale['Interaction'] : ''}</th>
                        <th className={styles.tableHeadItem}>{locale ? locale['Direct applications'] : ''}</th>
                        <th className={styles.tableHeadItem}>{locale ? locale['Status'] : ''}</th>
                        <th className={styles.tableHeadItem}>{locale ? locale['To edit'] : ''}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {showListOfJobs.map(job => (
                        <ListOfJobs job={job} key={job.job_id} handleSetActiveJob={handleSetActiveJob}/>
                    ))}
                    </tbody>
                </table>
                <div className={styles.counterBlock}>
                    <div className={styles.counterItem}>
                        <div className={styles.titleWrapper}>
                            <div className={styles.titleText}>{locale ? locale['Total views'] : ''}</div>
                            <div className={styles.tooltip}>
                                ?
                                <div className={styles.tooltiptext}>
                                    <span>{locale ? locale['Shows the number of views of your job ads.'] : ''}</span>
                                </div>
                            </div>
                        </div>
                        <span className={styles.totalNumber}>{getCounter['view']}</span>
                    </div>
                    <div className={styles.counterItem}>
                        <div className={styles.titleWrapper}>
                            <div className={styles.titleText}>{locale ? locale['Interactions'] : ''}</div>
                            <div className={styles.tooltip}>
                                ?
                                <div className={styles.tooltiptext}>
                                    <span>
                                        {locale ? locale['Shows the number of clicks on the button to apply directly on your website.'] : ''}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <span className={styles.totalNumber}>{getCounter['interactions']}</span>
                    </div>
                    <div className={styles.counterItem}>
                        <div className={styles.titleWrapper}>
                            <div className={styles.titleText}>{locale ? locale['Direct applications'] : ''}</div>
                            <div className={styles.tooltip}>
                                ?
                                <div className={styles.tooltiptext}>
                                    <span>
                                        {locale ? locale['Shows the number of direct applications via the button "apply now".'] : ''}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <span className={styles.totalNumber}>{getCounter['direct']}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>;
};

export default JobStatusContent;
