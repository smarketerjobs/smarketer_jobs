import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router";
import DatePicker, {registerLocale} from 'react-datepicker';
import {useDispatch, useSelector} from "react-redux";
import ReactGA from "react-ga";

import axios from "axios";
import csc from 'country-state-city';
import CKEditor from '@ckeditor/ckeditor5-react';
import * as moment from 'moment';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import en from 'date-fns/esm/locale/en-GB';
import de from 'date-fns/esm/locale/de';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/fontawesome-free-solid";

import {showAlert} from "../store/ducks/alert";
import {fetchPlan} from "../store/ducks/plan";
import {fetchUserJobs} from "../store/ducks/user";
import {fetchGetJobAddress, fetchJobCatalogs, fetchSaveJobAddress} from "../store/ducks/job";

import {formatMoney, getUserDate, parseMoney, validateUrl} from "../utils";
import {getPlanTitle} from "../utils/plan";

import styles from './JobAd.module.scss';

class BulletEditor extends ClassicEditor {
}

BulletEditor.defaultConfig = {toolbar: null};

const JobContent = props => {
    const {jobId, setSelectedJobId, countActive} = props;

    const history = useHistory();

    const dispatch = useDispatch();
    const {plan} = useSelector(state => state.planState);
    const {locale} = useSelector(state => state.localeState);
    const {userId} = useSelector(state => state.userIdState);
    const {activeJobs} = useSelector(state => state.userJobsState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);
    const {userCompaniesData} = useSelector(state => state.userCompaniesDataState);
    const {isNotEmptyUserAccountData} = useSelector(state => state.userAccountDataState);

    const {dataJobCatalogs} = useSelector(state => state.jobState);
    const {dataJobAddress} = useSelector(state => state.jobState);

    const [getActiveCompanies, setActiveCompanies] = useState([]);

    const [getLink, setLink] = useState('https://');
    const [getTitle, setTitle] = useState('');
    const [getCompanyId, setCompanyId] = useState(0);
    const [getIndustryId, setIndustryId] = useState('');
    const [getCategoryId, setCategoryId] = useState('');
    const [getEmployments, setEmployments] = useState([]);

    const [getCountries, setCountries] = useState([]);
    const [getStates, setStates] = useState([]);
    const [getCities, setCities] = useState([]);

    const [getCountryCode, setCountryCode] = useState('DE');
    const [getStateCode, setStateCode] = useState('DE-BE');
    const [getCityName, setCityName] = useState('');
    const [getLat, setLat] = useState('');
    const [getLng, setLng] = useState('');
    const [getStreet, setStreet] = useState('');

    useEffect(() => {
        if (getCountryCode) {
            setStates(csc.getStatesOfCountry(getCountryCode));
        }
    }, [getCountryCode]);

    useEffect(() => {
        if (getStateCode) {
            const code = getStateCode.split('-')
            if (code && code.length === 2) {
                setCities(csc.getCitiesOfState(code[0], code[1]));
            }
        }
    }, [getStateCode]);

    const emptyBulletList = '<ul><li/></ul>';
    const placeholderJobBenefits = `<ul><li>${locale ? locale['Job benefits placeholder'] : ''}</li></ul>`
    const placeholderQualifications = `<ul><li>${locale ? locale['Qualifications placeholder'] : ''}</li></ul>`

    const [getBenefits, setBenefits] = useState(placeholderJobBenefits);
    const [getQualifications, setQualifications] = useState(placeholderQualifications);

    const [getDescription, setDescription] = useState('');
    const [getFinalDescription, setFinalDescription] = useState('');
    const [getStartDescription, setStartDescription] = useState('');

    const [getDateEnd, setDateEnd] = useState(new Date(new Date().setDate(new Date().getDate())).getTime());

    const [getTypeId, setTypeId] = useState(null);
    const [getPostCode, setPostCode] = useState('');

    const [getActive, setActive] = useState(false);
    const [getAutoActivate, setAutoActivate] = useState(true);

    const [saveFail, setSaveFail] = useState(false);
    const [getStartSave, setStartSave] = useState(false);
    const [saveSuccess, setSaveSuccess] = useState(false);
    const [getIsValidUrl, setIsValidUrl] = useState(true);

    const [getSalaryTo, setSalaryTo] = useState('');
    const [getLocationId, setLocationId] = useState('1');
    const [getCurrencyId, setCurrencyId] = useState('1');
    const [getSalaryFrom, setSalaryFrom] = useState('');
    const [getSalaryMode, setSalaryMode] = useState('3');
    const [getSalaryGross, setSalaryGross] = useState('');
    const [getSalaryPeriodId, setSalaryPeriodId] = useState('4');

    const [getPlanName, setPlanName] = useState('');
    const [getPlanMaxJob, setPlanMaxJob] = useState(0);

    const [isSaveJobAddress, setIsSaveJobAddress] = useState(false);

    let isShouldToChangeJobStatus = false;

    registerLocale('en', en)
    registerLocale('de', de)

    const isNotEmptyAllRequiredFields = () => !isEmptyFirstSection() && !isEmptySecondSection();

    const checkRequiredFields = () => {
        if (!getActiveCompanies.length) {
            dispatch(showAlert('failure', 'Company data is empty'));
            return;
        }

        if (isNotEmptyAllRequiredFields()) {
            saveJob()
        } else {
            setSaveFail(true);
        }
    };

    const activateJob = id => {
        if (!getActive) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'activate', job_id: id},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        if (data.data === 'Application date is set to 30 days in the future') {
                            dispatch(showAlert(
                                'success',
                                'Application date is set to 30 days in the future',
                                10000
                            ));
                        }
                        ReactGA.event({
                            label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                            action: 'post_new_job',
                            category: 'Job'
                        });
                        getJobById(id);
                        history.push('/job-add?active=overview');
                    } else {
                        if (data.error) {
                            dispatch(showAlert('failure', data.error))
                        } else {
                            console.error(data);
                        }
                    }
                    dispatch(fetchUserJobs(userId));
                })
                .catch(err => {
                    dispatch(fetchUserJobs(userId));
                    console.error(err)
                });
        } else {
            history.push('/job-add?active=overview');
        }
        dispatch(fetchUserJobs(userId));
    };

    const checkMaxActiveJobs = (id) => {
        if (activeJobs < getPlanMaxJob || getActive) {
            activateJob(id);
        } else {
            setTimeout(() => {
                dispatch(showAlert('failure', 'Your contingent of active job error with link'));
            }, 3500);
            dispatch(fetchUserJobs(userId));
            setSelectedJobId(id);
            getJobById(id);
        }
    };

    const checkValidUserInfo = (id) => {
        if (getActiveCompanies.length && isNotEmptyUserAccountData) {
            checkMaxActiveJobs(id);
        } else {
            dispatch(showAlert('failure', 'empty_data', 8000));
            dispatch(fetchUserJobs(userId));
            setSelectedJobId(id);
            getJobById(id);
        }
    };

    const changeJobStatus = (id) => {
        if (isShouldToChangeJobStatus) {
            checkValidUserInfo(id);
        } else {
            dispatch(fetchUserJobs(userId));
            setSelectedJobId(id);
            getJobById(id);
        }
    };

    const getJobDataToSave = () => {
        const data = new FormData();

        data.append('model', 'job');
        data.append('mode', 'save');
        data.append('job_id', jobId);
        data.append('link', getLink && getLink !== 'https://' ? getLink : '');
        data.append('city', getCityName);
        data.append('lat', getLat);
        data.append('lng', getLng);
        data.append('title', getTitle);
        data.append('street', getStreet);
        data.append('type_id', getTypeId);
        data.append('post_code', getPostCode);
        data.append('state_code', getStateCode);
        data.append('category_id', getCategoryId);
        data.append('industry_id', getIndustryId);
        data.append('currency_id', getCurrencyId);
        data.append('location_id', getLocationId);
        data.append('employments', getEmployments);
        data.append('country_code', getCountryCode);
        data.append('company_id_fk', getCompanyId);
        data.append('currency_id_fk', getCurrencyId);

        data.append('salary_to', parseMoney(getSalaryTo));
        data.append('salary_from', parseMoney(getSalaryFrom));
        data.append('salary_mode', getSalaryMode);
        data.append('salary_gross', parseMoney(getSalaryGross));
        data.append('salary_period_id', getSalaryPeriodId);

        data.append(
            'benefits',
            getBenefits !== placeholderQualifications ? getBenefits : ''
        );
        data.append(
            'qualifications',
            getQualifications !== placeholderQualifications ? getQualifications : ''
        );

        data.append('description', getDescription);
        data.append('start_description', getStartDescription);
        data.append('final_description', getFinalDescription);

        data.append('date_end', moment(new Date(getDateEnd)).format("YYYY-MM-DD"));

        data.append(
            'active',
            getActiveCompanies.length && isNotEmptyUserAccountData ? getActive ? '1' : '0' : '0'
        );
        data.append('auto_active', getAutoActivate ? '1' : '0');

        return data;
    };

    const successSaveJob = (id) => {
        setTimeout(() => {
            setSaveSuccess(false);
        }, 3000);
        dispatch(showAlert('success', 'Succesfully saved'));
        setSaveSuccess(true);
        changeJobStatus(id);
    };

    const saveJob = () => {
        if (!getStartSave) {
            setStartSave(true);
            axios.post('/ajax.php', getJobDataToSave(), {})
                .then(({data}) => {
                    setStartSave(false);
                    if (data.data && data.data.job_id) {
                        if (jobId) {
                            ReactGA.event({
                                label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                                action: 'set_up_new_job_listing',
                                category: 'Job'
                            });
                        }
                        successSaveJob(data.data.job_id);
                    } else {
                        if (data.error === 'You can enable only job in active company.') {
                            dispatch(showAlert('failure', 'You can enable only job in active company.'));
                        } else {
                            console.error(data);
                            dispatch(showAlert('failure', 'failure'));
                        }
                    }
                })
                .catch(err => {
                    setStartSave(false);
                    console.error(err);
                });
            if (isSaveJobAddress) {
                dispatch(fetchSaveJobAddress({
                    lng: getLng,
                    lat: getLat,
                    city: getCityName,
                    street: getStreet,
                    post_code: getPostCode,
                    state_code: getStateCode,
                    country_code: getCountryCode,
                }))
            }
        }
    };

    const RenderEmploymentsList = () => {
        const setEmploymentsCustom = input => {
            if (input.checked) {
                setEmployments([input.name]);
            } else {
                setEmployments([]);
            }
        };

        return <div className={styles.groupCheckBox}>
            <div className={styles.leftColumn}>
                {dataJobCatalogs?.employments?.map(({employment_id, employment_title}, i) => {
                    const getHalfOfArray = Math.floor(dataJobCatalogs.employments.length / 2);

                    if (i < getHalfOfArray) {
                        return (
                            <label className={styles.labelBlock} key={employment_id}>
                                <input
                                    autoComplete={false}
                                    aria-autocomplete={"none"}
                                    id={'test_employments_checkbox' + i}
                                    type='checkbox'
                                    name={employment_id}
                                    className={styles.checkbox}
                                    checked={getEmployments.includes(employment_id)}
                                    onChange={e => setEmploymentsCustom(e.target)}
                                />
                                <div className={styles.checkIconBlock}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div className={styles.inputLabel}>{employment_title}</div>
                            </label>
                        )
                    }
                })}
            </div>
            <div className={styles.rightColumn}>
                {dataJobCatalogs?.employments?.map(({employment_id, employment_title}, i) => {
                    const getHalfOfArray = Math.floor(dataJobCatalogs.employments.length / 2);

                    if (i >= getHalfOfArray) {
                        return (
                            <label className={styles.labelBlock} key={employment_id}>
                                <input
                                    autoComplete={false}
                                    aria-autocomplete={"none"}
                                    id={'test_employments_checkbox' + i}
                                    type='checkbox'
                                    name={employment_id}
                                    className={styles.checkbox}
                                    checked={getEmployments.includes(employment_id)}
                                    onChange={e => setEmploymentsCustom(e.target)}
                                />
                                <div className={styles.checkIconBlock}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div className={styles.inputLabel}>{employment_title}</div>
                            </label>
                        )
                    }
                })}
            </div>
        </div>;
    };

    const updateJobAddress = () => {
        if (!jobId) {
            if (dataJobAddress) {
                setLat(dataJobAddress.lat);
                setLng(dataJobAddress.lng);
                setCityName(dataJobAddress.city);
                setStreet(dataJobAddress.street);
                setPostCode(dataJobAddress.post_code);
                setStateCode(dataJobAddress.state_code);
                setCountryCode(dataJobAddress.country_code);
            } else {
                const defaultCountryCode = 'DE';
                const defaultStateCode = 'DE-BE';
                const states = csc.getStatesOfCountry(defaultCountryCode);
                const cities = csc.getCitiesOfState(defaultCountryCode, defaultStateCode.split('-')[1]);

                setCountryCode(defaultCountryCode);
                setStateCode(defaultStateCode);

                if (states?.length) {
                    setStates(states);
                }

                if (cities?.length) {
                    const berlinCity = cities.find(city => city.name === "Berlin");
                    setCities(cities)

                    if (berlinCity) {
                        setCityName(berlinCity.name);
                        setLat(berlinCity.latitude);
                        setLng(berlinCity.longitude);
                    } else {
                        setCityName(cities[0].name);
                        setLat(cities[0].latitude);
                        setLng(cities[0].longitude);
                    }
                }
            }
        }
    }

    useEffect(updateJobAddress, [dataJobAddress]);

    const resetFields = () => {
        setLink('https://');
        setTitle('');
        setStreet('');
        setActive(false);
        setAutoActivate(true);
        setDateEnd(new Date(new Date().setDate(new Date().getDate() + 30)).getTime());
        setSalaryTo('');
        setPostCode('');
        setSaveFail(false);
        setCompanyId(getActiveCompanies[0]?.company_id);
        setIndustryId('');
        setCategoryId('');
        setLocationId('1');
        setCurrencyId('1');
        setSalaryFrom('');
        setSalaryMode('3');
        setSalaryGross('');
        setEmployments([]);
        setSaveSuccess(false);
        setSalaryPeriodId('4');
        setIsSaveJobAddress(false);

        setBenefits(placeholderQualifications);
        setQualifications(placeholderQualifications);
        setDescription('');
        setFinalDescription('');
        setStartDescription('');

        updateJobAddress();
    };

    const getItem = str => `${'<li'}${'>'}${str}${'</li'}${'>'})`;
    const getData = (str) => {
        try {
            const array = JSON.parse(str);
            if (array) {
                return `${'<'}${'ul'}${'>'}${array.map(getItem)}${'<'}${'/ul'}${'>'}`;
            }
        } catch (err) {
            console.warn(err);
        }
        return str;
    }

    const getJobById = () => {
        setSaveFail(false);

        if (jobId) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'get_job', job_id: jobId},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        const codes = data.data['state_code']?.split('-');
                        setCountryCode(data.data['country_code']);
                        setStates(csc.getStatesOfCountry(data.data['country_code']));
                        setStateCode(data.data['state_code']);
                        setCities(csc.getCitiesOfState(codes[0], codes[1]))
                        setCityName(data.data['city']);
                        setLat(data.data['lat']);
                        setLng(data.data['lng']);
                        setStreet(data.data['street']);

                        setLink(data.data['link'] || 'https://');
                        setTitle(data.data['title'] || '');
                        setTypeId(data.data['type_id'] || '');
                        setPostCode(data.data['post_code'] || '');
                        setCompanyId(data.data['company_id'] || getActiveCompanies[0]?.company_id);
                        setSalaryMode(data.data['salary_mode'] || '3');
                        setCategoryId(data.data['category_id'] || '');
                        setIndustryId(data.data['industry_id'] || '');
                        setEmployments(data.data['employments'] || []);
                        setLocationId(data.data['location_id'] || '1');
                        setCurrencyId(data.data['currency_id'] || '1');
                        setSalaryPeriodId(data.data['salary_period_id'] || '4');

                        setDateEnd(getUserDate(data.data['date_end']).getTime());

                        setBenefits(
                            data.data['benefits'] ? getData(data.data['benefits']) : placeholderQualifications
                        );
                        setQualifications(
                            data.data['qualifications']
                                ? getData(data.data['qualifications'])
                                : placeholderQualifications
                        );
                        setDescription(data.data['description'] || '');
                        setStartDescription(data.data['start_description'] || '');
                        setFinalDescription(data.data['final_description'] || '');

                        setSalaryTo(data.data['salary_to'] && data.data['salary_to'] !== '0.00'
                            && formatMoney(data.data['salary_to'], locale ? locale['lang'] : '') || '');
                        setSalaryFrom(data.data['salary_from'] && data.data['salary_from'] !== '0.00'
                            && formatMoney(data.data['salary_from'], locale ? locale['lang'] : '') || '');
                        setSalaryGross(data.data['salary_gross'] && data.data['salary_gross'] !== '0.00'
                            && formatMoney(data.data['salary_gross'], locale ? locale['lang'] : '') || '');

                        setActive(!!+data.data['active']);
                        setAutoActivate(!!+data.data['auto_active']);
                    } else {
                        console.error(data);
                    }
                })
                .catch(err => console.error(err));
        } else {
            resetFields();
        }
    };

    const getPeriodTitleInLocale = period => {
        switch (period) {
            case 'hour':
                return locale ? locale['Hour'] : '';
            case 'day':
                return locale ? locale['Day'] : '';
            case 'week':
                return locale ? locale['Week'] : '';
            case 'month':
                return locale ? locale['Month'] : '';
            case 'year':
                return locale ? locale['Year'] : '';
            default:
                return '';
        }
    };

    const isEmptyFirstSection = () => {
        return !getTitle || !getDescription || !getDateEnd || !getCategoryId || !getIndustryId
            || getDescription === placeholderQualifications
            || !getQualifications || getQualifications === placeholderQualifications
            || !Object.entries(getEmployments).some(ar => ar[1]) || !getIsValidUrl;
    };

    const isEmptySecondSection = () => {
        return !getStreet || !getCityName || !getPostCode || !getLocationId || !getStateCode || !getCountryCode
            || !getActiveCompanies.some(c => c.company_id === getCompanyId);
    };

    const getMinEndDate = () => new Date().setDate(new Date().getDate() + 1);

    const prepareDataAsList = str => {
        const newStr = str
            .replace(/<i>/g, '')
            .replace(/<\/i>/g, '')
            .replace(/<strong>/g, '')
            .replace(/<\/strong>/g, '')
            .replace(/<p>/g, '<li>')
            .replace(/<\/p>/g, '</li>')
            .replace(/<h[1-6]>/g, '<li>')
            .replace(/<\/h[1-6]>/g, '</li>')
            .replace(/<ol>/g, '')
            .replace(/<\/ol>/g, '')
            .replace(/<ul>/g, '')
            .replace(/<\/ul>/g, '')
            .replace(/<li>\s*<\/li>/g, '')
            .replace(/<li>(&nbsp;)*<\/li>/g, '')

        return newStr
            ? `${'<'}` + 'ul>' + newStr + `${'<'}` + '/ul>'
            : `${'<'}` + 'ul>' + `${'<'}` + 'li/>' + `${'<'}` + '/ul>';
    }

    useEffect(() => {
        setCountries(csc.getAllCountries());
        dispatch(fetchPlan());
        dispatch(fetchGetJobAddress());
    }, []);

    useEffect(getJobById, [jobId]);

    useEffect(() => {
        if (plan) {
            setPlanName(getPlanTitle(plan.plan_name));
            setPlanMaxJob(plan['max_active_jobs']);
        }
    }, [plan]);

    useEffect(() => {
        if (userCompaniesData.length) {
            const activeCompanies = userCompaniesData.filter(c => +c.active);

            if (activeCompanies.length) {
                setActiveCompanies(activeCompanies);
                setCompanyId(activeCompanies[0].company_id);
            }
        }
    }, [userCompaniesData]);

    useEffect(() => {
        dispatch(fetchJobCatalogs());
    }, [locale]);

    return (
        <div className={styles.rightColumnList}>
            <div className={`${styles.wrapper} ${saveFail && isEmptyFirstSection() ? styles.saveFailBg : ''}`}>
                <div className={styles.header}>
                    <h2 className={`${styles.title} ${styles.ml10}`}>
                        {locale ? locale['Create Job Ad'] : ''}
                    </h2>
                    <span
                        className={`
                            ${styles.counter}
                            ${saveSuccess ? styles.saveBg : ''}
                            ${saveFail && isEmptyFirstSection() ? styles.saveFailBg : ''}
                        `}
                    >
                        1
                    </span>
                </div>
                <div>
                    {plan && locale ? (
                        <p className={`${styles.description} ${styles.ml10}`}>
                            {getPlanName.includes('Professional') ? (
                                <>
                                    {locale ? locale['You have chosen the'] : ''}
                                    {' '}
                                    <span className={styles.green}>{getPlanName}</span>
                                    {' '}
                                    {locale ? locale['plan. You can publish an'] : ''}
                                    {' '}
                                    <span className={styles.green}>{locale ? locale['unlimited'] : ''}</span>
                                    {' '}
                                    {locale ? locale['number of job ads.'] : ''}
                                </>
                            ) : (
                                <>
                                    {locale ? locale['You have chosen the'] : ''}
                                    {' '}
                                    <span className={getPlanMaxJob - countActive ? styles.green : styles.red}>
                                        {getPlanName}
                                    </span>
                                    {' '}
                                    {locale ? locale['plan. You can still publish'] : ''}
                                    {' '}
                                    <span className={getPlanMaxJob - countActive ? styles.green : styles.red}>
                                    {getPlanMaxJob - countActive}
                                </span>
                                    {' '}
                                    {getPlanMaxJob - countActive > 1
                                        ? locale ? locale['job ads.'] : ''
                                        : locale ? locale['job ad.'] : ''
                                    }
                                </>
                            )}
                        </p>
                    ) : (
                        <p className={styles.subTitle}>
                            {locale ? locale['Loading'] + '...' : ''}
                        </p>
                    )}
                    <div className={styles.formDetails}>
                        <label className={styles.inputLabel}>
                            <span
                                className={`${styles.inputLabelText} ${saveFail && !getTitle ? styles.saveFailBg : ''}`}
                            >
                                {locale ? locale['Job title'] : ''}*
                            </span>
                            <input
                                autoComplete={false}
                                aria-autocomplete={"none"}
                                name='job-input-title'
                                placeholder={locale ? locale['Job title placeholder'] : ''}
                                className={`${styles.input} ${saveFail && !getTitle ? styles.saveFailBg : ''}`}
                                onChange={e => setTitle(e.target.value)} value={getTitle}
                                onBlur={() => setTitle(getTitle.trim())}
                            />
                        </label>
                        <div className={`${styles.inputLabel} ${styles.alignStart}`}>
                            <span className={styles.inputLabelText}>
                                {locale ? locale['Introduction'] : ''}
                            </span>
                            <div className={styles.ckEditorWrapper}>
                                <CKEditor
                                    data={getStartDescription}
                                    editor={ClassicEditor}
                                    onChange={(event, editor) => {
                                        setStartDescription(editor.getData())
                                    }}
                                />
                            </div>
                        </div>
                        <div className={`${styles.inputLabel} ${styles.alignStart}`}>
                            <span className={`
                                ${styles.inputLabelText} ${saveFail && !getDescription ? styles.saveFailBg : ''}
                            `}>
                                {locale ? locale['Description'] : ''}*
                            </span>
                            <div
                                className={`
                                    ${styles.ckEditorWrapper}
                                    ${saveFail && !getDescription ? styles.saveFailBg : ''}
                                `}
                            >
                                <CKEditor
                                    data={getDescription}
                                    editor={ClassicEditor}
                                    onChange={(event, editor) => {
                                        setDescription(editor.getData())
                                    }}
                                />
                            </div>
                        </div>
                        <div className={`${styles.inputLabel} ${styles.alignStart}`}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getQualifications ? styles.saveFailBg : ''}
                                    ${saveFail &&
                                getQualifications === placeholderQualifications
                                    ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Qualifications'] : ''}*
                            </span>
                            <div
                                className={`
                                    ${styles.ckEditorWrapper}
                                    ${saveFail && !getQualifications ? styles.saveFailBg : ''}
                                    ${saveFail &&
                                getQualifications === placeholderQualifications
                                    ? styles.saveFailBg : ''}
                                `}
                            >
                                <CKEditor
                                    data={getQualifications}
                                    editor={BulletEditor}
                                    onChange={(event, editor) => {
                                        setQualifications(prepareDataAsList(editor.getData()))
                                    }}
                                    onFocus={() => {
                                        if (getQualifications === placeholderQualifications) {
                                            setQualifications(emptyBulletList)
                                        }
                                    }}
                                    onBlur={() => {
                                        if (getQualifications === emptyBulletList) {
                                            setQualifications(placeholderQualifications)
                                        }
                                    }}
                                />
                            </div>
                        </div>
                        <div className={`${styles.inputLabel} ${styles.alignStart}`}>
                            <span className={styles.inputLabelText}>
                                {locale ? locale['Job benefits'] : ''}
                            </span>
                            <div className={styles.ckEditorWrapper}>
                                <CKEditor
                                    data={getBenefits}
                                    editor={BulletEditor}
                                    onChange={(event, editor) => {
                                        setBenefits(prepareDataAsList(editor.getData()))
                                    }}
                                    onFocus={() => {
                                        if (getBenefits === placeholderQualifications) {
                                            setBenefits(emptyBulletList);
                                        }
                                    }}
                                    onBlur={() => {
                                        if (getBenefits === emptyBulletList) {
                                            setBenefits(placeholderQualifications);
                                        }
                                    }}
                                />
                            </div>
                        </div>
                        <div className={`${styles.inputLabel} ${styles.alignStart}`}>
                            <span className={styles.inputLabelText}>
                                {locale ? locale['Final words'] : ''}
                            </span>
                            <div className={styles.ckEditorWrapper}>
                                <CKEditor
                                    data={getFinalDescription}
                                    editor={ClassicEditor}
                                    onChange={(event, editor) => {
                                        setFinalDescription(editor.getData())
                                    }}
                                />
                            </div>
                        </div>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getCategoryId ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Job Category'] : ''}*
                            </span>
                            <div className={styles.selectWrapper}>
                                <select
                                    name="job-select-category"
                                    className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getCategoryId ? styles.saveFailBg : ''}
                                    `}
                                    onChange={e => setCategoryId(e.target.value)}
                                    value={getCategoryId}
                                >
                                    {!getCategoryId ? <option disabled value="" key="this is unique key"/> : ''}
                                    {dataJobCatalogs?.categories?.map(({category_id}) => (
                                        <option value={category_id} key={category_id}>
                                            {locale ? locale[`job_category_id_${category_id}`] : ''}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </label>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getIndustryId ? styles.saveFailBg : ''}
                                `}>
                                {locale ? locale['Branch'] : ''}*
                            </span>
                            <div className={styles.selectWrapper}>
                                <select
                                    name='job-select-industry'
                                    className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getIndustryId ? styles.saveFailBg : ''}
                                    `}
                                    onChange={e => setIndustryId(e.target.value)}
                                    value={getIndustryId}
                                >
                                    {!getIndustryId ? <option disabled value="" key="this is unique key"/> : ''}
                                    {dataJobCatalogs?.industries?.map(({industry_id}) => (
                                        <option value={industry_id} key={industry_id}>
                                            {locale ? locale[`job_industry_id_${industry_id}`] : ''}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </label>
                        <div className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${styles.alignStart}
                                    ${saveFail && !Object.entries(getEmployments).some(ar => ar[1]) ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Type of employment'] : ''}*
                            </span>
                            {getEmployments && <RenderEmploymentsList/>}
                        </div>
                        <div className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getDateEnd ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Application time'] : ''}*
                            </span>
                            <label className={styles.input}>
                                <DatePicker
                                    locale={locale ? locale['lang'] === 'de' ? de : en : en}
                                    dateFormat="dd.MM.yyyy"
                                    selected={new Date(getDateEnd)}
                                    onChange={date => setDateEnd(date.getTime())}
                                    minDate={getMinEndDate()}
                                />
                            </label>
                        </div>
                        <div className={`${styles.inputLabel} ${styles.inputCheckboxAutoActivate}`}>
                            <span className={`${styles.inputLabelText} ${styles.titleCheckboxAutoActivate}`}>
                                {locale ? locale['Automatic renewal'] : ''}
                            </span>
                            <label
                                key="checkbox_automatic_renewal_label"
                                className={`${styles.labelBlock} ${styles.labelAutoActivate}`}
                                onChange={() => setAutoActivate(!getAutoActivate)}
                            >
                                <input
                                    id="checkbox_automatic_renewal"
                                    type="checkbox"
                                    name="checkbox_automatic_renewal"
                                    checked={getAutoActivate}
                                    className={styles.checkbox}
                                    autoComplete={false}
                                    aria-autocomplete="none"
                                />
                                <div className={styles.checkIconBlock}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div className={`${styles.inputLabel} ${styles.checkboxTextAutoActivate}`}>
                                    {locale ? locale['Application time will be'] : ''}
                                </div>
                            </label>
                        </div>
                        <label className={styles.inputLabel}>
                            <span className={`${styles.inputLabelText} ${!getIsValidUrl ? styles.saveFailBg : ''}`}>
                                {locale ? locale['Link to your website'] : ''}
                            </span>
                            <input
                                name="job-input-link"
                                value={getLink}
                                autoComplete={false}
                                aria-autocomplete={"none"}
                                className={` ${styles.input} ${!getIsValidUrl ? styles.saveFailBg : ''}`}
                                placeholder="https://www.google.com"
                                onChange={e => {
                                    setLink(e.target.value)
                                    if (!e.target.value || e.target.value === 'https://') {
                                        setIsValidUrl(true);
                                    }
                                    if (!getIsValidUrl) {
                                        setIsValidUrl(
                                            e.target.value && e.target.value !== 'https://'
                                                ? validateUrl(e.target.value.trim())
                                                : true
                                        );
                                    }
                                }}
                                onBlur={() => {
                                    setLink(getLink ? getLink.trim() : 'https://')
                                    setIsValidUrl(
                                        getLink && getLink !== 'https://'
                                            ? validateUrl(getLink.trim())
                                            : true
                                    );
                                }}
                            />
                        </label>
                    </div>
                </div>
            </div>
            <div
                className={`${styles.wrapper} ${saveFail && isEmptySecondSection() ? styles.saveFailBg : ''}`}>
                <div className={styles.header}>
                    <h2 className={`${styles.title} ${styles.ml10}`}>
                        {locale ? locale['Location'] : ''}
                    </h2>
                    <span
                        className={`
                            ${styles.counter}
                            ${saveSuccess ? styles.saveBg : ''}
                            ${saveFail && isEmptySecondSection() ? styles.saveFailBg : ''}
                        `}
                    >
                        2
                    </span>
                </div>
                <div>
                    <p className={`${styles.description} ${styles.ml10}`}>
                        {locale ? locale['Location sub titel'] : ''}
                    </p>
                    <div className={styles.formDetails}>
                        {userCompaniesData.length > 1 ? (
                            <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getActiveCompanies.some(c => c.company_id === getCompanyId)
                                    ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Company'] : ''}*
                            </span>
                                <div className={styles.selectWrapper}>
                                    <select
                                        name='job-select-company'
                                        className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getActiveCompanies.some(c => c.company_id === getCompanyId)
                                            ? styles.saveFailBg : ''
                                        }
                                    `}
                                        onChange={e => setCompanyId(e.target.value)}
                                        value={getCompanyId}
                                    >
                                        {!getCompanyId ? (
                                            <option disabled value={0} key="this is unique key"/>
                                        ) : ''}
                                        {userCompaniesData.map(({company_id, company_name, active}) => (
                                            <option value={company_id} key={company_id} disabled={!+active}>
                                                {company_name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </label>
                        ) : ''}
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getLocationId ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Art'] : ''}*
                            </span>
                            <div className={styles.selectWrapper}>
                                <select
                                    name='job-select-location'
                                    onChange={e => setLocationId(e.target.value)}
                                    value={getLocationId}
                                    className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getLocationId ? styles.saveFailBg : ''}
                                    `}
                                >
                                    {dataJobCatalogs?.locations?.map(({location_id, location_title}) => (
                                        <option key={location_id} value={location_id}>
                                            {location_title}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </label>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getPostCode ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['post code'] : ''}*
                            </span>
                            <input
                                autoComplete={false}
                                aria-autocomplete={"none"}
                                name="job-input-post-code"
                                value={getPostCode}
                                onChange={e => setPostCode(e.target.value)}
                                className={`${styles.input} ${saveFail && !getPostCode ? styles.saveFailBg : ''}`}
                                onBlur={() => setPostCode(getPostCode.trim())}
                            />
                        </label>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getCountryCode ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Country'] : ''}*
                            </span>
                            <div className={styles.selectWrapper}>
                                <select
                                    name='job-select-country'
                                    value={getCountryCode}
                                    onChange={e => {
                                        const newStates = csc.getStatesOfCountry(e.target.value);
                                        setCountryCode(e.target.value);

                                        if (newStates?.length) {
                                            setStates(newStates);
                                            setStateCode(newStates[0].countryCode + '-' + newStates[0].isoCode);

                                            const newCities = csc.getCitiesOfState(newStates[0].countryCode, newStates[0].isoCode);

                                            if (newCities?.length) {
                                                setCities(newCities);
                                                setCityName(newCities[0].name);
                                                setLat(newCities[0].latitude);
                                                setLng(newCities[0].longitude);
                                            } else {
                                                setCities(newCities);
                                                setCityName(newCities[0].name);
                                                setLat(newCities[0].latitude);
                                                setLng(newCities[0].longitude);
                                            }
                                        } else {
                                            setStates([]);
                                            setStateCode('');
                                            setCities([]);
                                            setCityName('');
                                            setLat('');
                                            setLng('');
                                        }
                                    }}
                                    className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getCountryCode ? styles.saveFailBg : ''}
                                    `}
                                >
                                    {getCountries.map(({isoCode, name}) => (
                                        <option value={isoCode} key={isoCode}>
                                            {name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </label>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getStateCode ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['State'] : ''}*
                            </span>
                            <div className={styles.selectWrapper}>
                                <select
                                    name='job-select-state'
                                    onChange={e => {
                                        const newCities = csc.getCitiesOfState(getCountryCode, e.target.value.split('-')[1])
                                        setStateCode(e.target.value);

                                        if (newCities?.length) {
                                            setCities(newCities)
                                            setCityName(newCities[0]?.name);
                                            setLat(newCities[0]?.latitude);
                                            setLng(newCities[0]?.longitude);
                                        } else {
                                            setCities([])
                                            setCityName('');
                                            setLat('');
                                            setLng('');
                                        }
                                    }}
                                    value={getStateCode}
                                    className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getStateCode ? styles.saveFailBg : ''}
                                    `}
                                >
                                    {getStates.map(({countryCode, isoCode, name}) => (
                                        <option value={countryCode + '-' + isoCode} key={countryCode + '-' + isoCode}>
                                            {name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </label>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getCityName ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['City'] : ''}*
                            </span>
                            <div className={styles.selectWrapper}>
                                <select
                                    name="job-select-city"
                                    value={getCityName}
                                    onChange={e => {
                                        const city = getCities.find(city => city.name === e.target.value);
                                        if (city) {
                                            setLat(city.latitude);
                                            setLng(city.longitude);
                                            setCityName(city.name);
                                        }
                                    }}
                                    className={`
                                        ${styles.input}
                                        ${styles.select}
                                        ${saveFail && !getCityName ? styles.saveFailBg : ''}
                                    `}
                                >
                                    {getCities?.map(({name}) => (
                                        <option value={name} key={name}>
                                            {name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        </label>
                        <label className={styles.inputLabel}>
                            <span
                                className={`
                                    ${styles.inputLabelText}
                                    ${saveFail && !getStreet ? styles.saveFailBg : ''}
                                `}
                            >
                                {locale ? locale['Street + number'] : ''}*
                            </span>
                            <input
                                autoComplete={false}
                                aria-autocomplete={"none"}
                                name="job-input-city"
                                value={getStreet}
                                onChange={e => {
                                    setStreet(e.target.value)
                                }}
                                className={`${styles.input} ${saveFail && !getStreet ? styles.saveFailBg : ''}`}
                                onBlur={() => setStreet(getStreet.trim())}
                            />
                        </label>
                        <div className={`${styles.inputLabel} ${styles.inputCheckboxAutoActivate}`}>
                            <span className={`${styles.inputLabelText} ${styles.titleCheckboxAutoActivate}`}>
                                Save Job Address
                            </span>
                            <label
                                key="checkbox_save_job_address"
                                className={`${styles.labelBlock} ${styles.labelAutoActivate}`}
                                onChange={() => setIsSaveJobAddress(!isSaveJobAddress)}
                            >
                                <input
                                    id="checkbox_save_job_address"
                                    type="checkbox"
                                    name="checkbox_save_job_address"
                                    checked={isSaveJobAddress}
                                    className={styles.checkbox}
                                    autoComplete={false}
                                    aria-autocomplete="none"
                                />
                                <div className={styles.checkIconBlock}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div className={`${styles.inputLabel} ${styles.checkboxTextAutoActivate}`}>
                                    {locale ? locale['Save address for future jobs'] : ''}
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.wrapper}>
                <div className={styles.header}>
                    <h2 className={`${styles.title} ${styles.ml10}`}>
                        {locale ? locale['Salary'] : ''}
                    </h2>
                    <span className={`${styles.counter} ${saveSuccess ? styles.saveBg : ''}`}>
                        3
                    </span>
                </div>
                <div>
                    <p className={`${styles.description} ${styles.ml10}`}>
                        {locale ? locale['Salary sub titel'] : ''}
                    </p>
                    <div className={styles.formDetails}>
                        <div className={`${styles.inputLabel} ${styles.alignTop} ${styles.alignStart}`}>
                            <div className={styles.inputLabelText}>
                                {locale ? locale['Gross salary'] : ''}
                            </div>
                            <div className={styles.salaryWrapper}>
                                <div>
                                    <input
                                        autoComplete={false}
                                        aria-autocomplete={"none"}
                                        id="first-group"
                                        tabIndex="-1"
                                        name="job-radio-salary-mode-1"
                                        type="radio"
                                        onChange={() => setSalaryMode('1')}
                                        checked={getSalaryMode === '1'}
                                        className={styles.radio}
                                    />
                                    <label className={styles.groupPriceInfo} htmlFor='first-group'>
                                        <input
                                            autoComplete={false}
                                            aria-autocomplete={"none"}
                                            name="job-input-street"
                                            value={getSalaryGross}
                                            onChange={e => {
                                                setSalaryMode('1');
                                                setSalaryGross(e.target.value);
                                            }}
                                            onFocus={() => setSalaryMode('1')}
                                            className={styles.input}
                                            onBlur={() => setSalaryGross(getSalaryGross.trim())}
                                        />
                                        <div className={styles.selectWrapper}>
                                            <select
                                                name="job-select-currency-1"
                                                value={getCurrencyId}
                                                onChange={e => setCurrencyId(e.target.value)}
                                                onFocus={() => setSalaryMode('1')}
                                                className={`${styles.input} ${styles.select}`}
                                            >
                                                {dataJobCatalogs?.currencies?.map(({currency_id, currency_code}) => (
                                                    <option value={currency_id} key={currency_id}>
                                                        {currency_code}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className={styles.selectWrapper}>
                                            <select
                                                name='job-select-salary-period-1'
                                                value={getSalaryPeriodId}
                                                onChange={e => setSalaryPeriodId(e.target.value)}
                                                onFocus={() => setSalaryMode('1')}
                                                className={styles.inputBorder}>
                                                {dataJobCatalogs?.salary_periods?.map(({period_id, period_title}) => (
                                                    <option value={period_id} key={period_id}>
                                                        {getPeriodTitleInLocale(period_title)}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                    </label>
                                </div>
                                <span className={styles.seperateMsg}>
                                    {locale ? locale['Or salary range'] : ''}
                                </span>
                                <div>
                                    <input
                                        autoComplete={false}
                                        aria-autocomplete={"none"}
                                        id="second-group"
                                        type="radio"
                                        name="job-radio-button-salary-mode-2"
                                        tabIndex="-2"
                                        checked={getSalaryMode === '2'}
                                        onChange={() => setSalaryMode('2')}
                                        className={styles.radio}
                                    />
                                    <label className={styles.groupPriceInfo} htmlFor='second-group'>
                                        <span>von</span>
                                        <input
                                            autoComplete={false}
                                            aria-autocomplete={"none"}
                                            name='job-input-salary-from'
                                            value={getSalaryFrom}
                                            onChange={e => {
                                                setSalaryMode('2');
                                                setSalaryFrom(e.target.value)
                                            }}
                                            onFocus={() => setSalaryMode('2')}
                                            className={`${styles.input} ${styles.spaceBetween}`}
                                            onBlur={() => setSalaryFrom(getSalaryFrom.trim())}/>
                                        <span>bis</span>
                                        <input
                                            name='job-input-salary-to'
                                            value={getSalaryTo}
                                            onChange={e => {
                                                setSalaryMode('2');
                                                setSalaryTo(e.target.value)
                                            }}
                                            onFocus={() => setSalaryMode('2')}
                                            className={`${styles.input} ${styles.spaceOnlyBefore}`}
                                            onBlur={() => setSalaryTo(getSalaryTo.trim())}
                                        />
                                        <div className={styles.selectWrapper}>
                                            <select
                                                name="job-select-currency-2"
                                                value={getCurrencyId}
                                                onChange={e => setCurrencyId(e.target.value)}
                                                onFocus={() => setSalaryMode('2')}
                                                className={`${styles.input} ${styles.select}`}
                                            >
                                                {dataJobCatalogs?.currencies?.map(({currency_id, currency_code}) => (
                                                    <option value={currency_id} key={currency_id}>
                                                        {currency_code}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className={styles.selectWrapper}>
                                            <select
                                                name='job-select-salary-period-2'
                                                value={getSalaryPeriodId}
                                                onChange={e => setSalaryPeriodId(e.target.value)}
                                                onFocus={() => setSalaryMode('2')}
                                                className={styles.inputBorder}
                                            >
                                                {dataJobCatalogs?.salary_periods?.map(({period_id, period_title}) => (
                                                    <option value={period_id} key={period_id}>
                                                        {getPeriodTitleInLocale(period_title)}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                    </label>
                                </div>
                                <div className={styles.mt25}>
                                    <input
                                        autoComplete={false}
                                        aria-autocomplete="none"
                                        name="job-radio-button-salary-mode-3"
                                        type="radio"
                                        tabIndex="-3"
                                        checked={getSalaryMode === '3'}
                                        onChange={() => setSalaryMode('3')}
                                        id="third-group"
                                        className={styles.radio}
                                    />
                                    <label className={styles.groupPriceInfo} htmlFor='third-group'>
                                        <span className={styles.bold}>
                                            {locale ? locale['Do not specify salary'] : ''}
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {saveFail && !isNotEmptyAllRequiredFields() && <div className={styles.errorMessageWrapper}>
                <div className={styles.errorMessage}>
                    <div className={styles.infoImgWrapper}>
                        <img className={styles.infoImg} src="../../media/info-danger.png" alt="info"/>
                    </div>
                    <div className={styles.errorMessageText}>
                        <span>
                            {locale ? locale['The most important information'] : ''}
                        </span>
                    </div>
                </div>
            </div>}
            <div className={`${styles.wrapper} ${styles.btnSubmitWrapper}`}>
                <button
                    className={styles.submitBtn}
                    onClick={() => {
                        isShouldToChangeJobStatus = true;
                        checkRequiredFields();
                    }}
                >
                    {locale ? locale['Save & Publish'] : ''}
                </button>
                <button
                    className={styles.submitBtn} disabled={getStartSave}
                    onClick={() => {
                        isShouldToChangeJobStatus = false;
                        checkRequiredFields()
                    }}
                >
                    {locale ? locale['Save changes'] : ''}
                </button>
            </div>
        </div>
    );
};

export default JobContent;
