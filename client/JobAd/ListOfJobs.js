import React, {useEffect, useState} from 'react';

import axios from "axios";
import ReactGA from "react-ga";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import Toggle from "react-toggle";
import {faEye} from "@fortawesome/fontawesome-free-solid";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {kitcut} from "../utils";
import {showAlert} from "../store/ducks/alert";
import {fetchUserJobs} from "../store/ducks/user";
import {fetchDeleteJob} from "../store/ducks/job";

import styles from "./JobStatusCounterAd.module.scss";

import copyImg from '../../media/copy.png';
import editImg from '../../media/edit.png';
import deleteImg from '../../media/delete.png';

const ListOfJobs = props => {
    const {job, handleSetActiveJob,} = props;
    const {title, company_name, job_id, active, job_view, job_redirect, job_conversion, job_share} = job;

    const dispatch = useDispatch();
    const {plan} = useSelector(state => state.planState);
    const {userId} = useSelector(state => state.userIdState);
    const {locale} = useSelector(state => state.localeState);
    const {userJobs} = useSelector(state => state.userJobsState);
    const {activeJobs} = useSelector(state => state.userJobsState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);
    const {userCompaniesData} = useSelector(state => state.userCompaniesDataState);
    const {isNotEmptyUserAccountData} = useSelector(state => state.userAccountDataState);

    const [getIsDisabled, setIsDisabled] = useState(false);
    const [isClickToRemoveJob, setClickToRemoveJob] = useState(false);

    const copyJob = () => {
        axios.get('/ajax.php', {
            params: {model: 'job', mode: 'copy', job_id: job_id},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    ReactGA.event({
                        label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                        action: 'set_up_new_job_listing',
                        category: 'Job'
                    });
                    dispatch(fetchUserJobs(userId));
                } else {
                    console.error(data);
                }
            })
            .catch(err => console.error(err));
    };

    const setStatusRequest = () => {
        axios.get('/ajax.php', {
            params: {model: 'job', mode: +active ? 'deactivate' : 'activate', job_id: job_id},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    if (data.data === 'Application date is set to 30 days in the future') {
                        dispatch(showAlert('success', 'Application date is set to 30 days in the future', 10000));
                    }
                    if (!+active) {
                        ReactGA.event({
                            label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                            action: 'post_new_job',
                            category: 'Job'
                        });
                    }
                } else {
                    if (activeJobs >= plan['max_active_jobs']) {
                        dispatch(showAlert('failure', 'Your contingent of active job error with link'));
                    } else if (data.error) {
                        dispatch(showAlert('failure', data.error))
                    } else {
                        console.error(data);
                    }
                }
                dispatch(fetchUserJobs(userId));
            })
            .catch(err => {
                dispatch(fetchUserJobs(userId));
                console.error(err)
            });
    }

    const setJobStatus = () => {
        if (+job.active) {
            setStatusRequest()
        } else {
            if (userCompaniesData.filter(company => !!+company.active).length && isNotEmptyUserAccountData) {
                if (!getIsDisabled) {
                    setStatusRequest();
                } else {
                    dispatch(showAlert('failure', 'Your contingent of active job error with link'));
                }
            } else {
                dispatch(showAlert('failure', 'No active company found', 8000));
            }
        }
    };

    useEffect(() => {
        if (plan && userJobs && job) {
            if (+active) {
                setIsDisabled(false);
            } else {
                setIsDisabled(userJobs.filter(job => !!+job.active).length >= plan['max_active_jobs']);
            }
        }
    }, [plan, userJobs, job]);

    return (
        <tr className={styles.tableWrapper}>
            <td className={styles.tableItem} title={title.length > 25 ? title : ''}>
               <span
                   className={`${styles.cursorPointer} ${styles.hoverUnderLine}`}
                   onClick={() => handleSetActiveJob(job_id)}
               >
                   {kitcut(title, 25)}
               </span>
            </td>
            {userCompaniesData.length > 1
                ? <td className={styles.tableItem} title={company_name.length > 25 ? company_name : ''}>
                    {kitcut(company_name, 25)}
                </td>
                : ''
            }
            <td className={styles.tableItem}>
                <NavLink to={`/overview?job_id=${job_id}`}>
                    <FontAwesomeIcon icon={faEye} className={styles.actionIcon}/>
                </NavLink>
            </td>
            <td className={styles.tableItem}>{job_view}</td>
            <td className={styles.tableItem}>{+job_redirect + +job_share}</td>
            <td className={styles.tableItem}>{job_conversion}</td>
            <td className={`${styles.tableItem} ${styles.labelBlock}`}>
                <Toggle checked={!!+active} icons={false} onChange={setJobStatus}/>
            </td>
            <td className={`${styles.tableItem} ${styles.imgRow}`}>
                {isClickToRemoveJob
                    ? <span>{locale ? locale['Are you sure?'] : ''}
                        <button onClick={() => dispatch(fetchDeleteJob({userId, jobId: job_id}))}>
                            {locale ? locale['Yes'] : 'Yes'}
                        </button>
                        {' / '}
                        <button onClick={() => setClickToRemoveJob(false)}>{locale ? locale['No'] : ''}</button>
                    </span>
                    : <div className={styles.controlWrapper}>
                        <div className={styles.iconWrapper2}>
                            <img src={editImg} alt="" title={locale ? locale['Edit'] : ''}
                                 onClick={() => handleSetActiveJob(job_id)} className={styles.iconImg}/>
                        </div>
                        <div className={styles.iconWrapper}>
                            <img src={deleteImg} alt="" title={locale ? locale['Delete'] : ''}
                                 onClick={() => setClickToRemoveJob(true)} className={styles.iconImg}/>
                        </div>
                        <div className={styles.iconWrapper}>
                            <img src={copyImg} alt="" title={locale ? locale['Copy'] : ''}
                                 onClick={copyJob} className={styles.iconImg}/>
                        </div>
                    </div>
                }
            </td>
        </tr>
    );
};

export default ListOfJobs;
