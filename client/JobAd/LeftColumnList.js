import React from "react";
import {useSelector} from "react-redux";
import GenerateJobList from "./GenerateJobList";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBriefcase, faChartBar, faPlus, faSearch} from "@fortawesome/fontawesome-free-solid";

import styles from "./JobAd.module.scss";
import {useHistory} from "react-router";

export const LeftColumnList = (
    {
        search,
        setSearch,
        getActiveTab,
        handleSetActiveJob,
        filteredListOfJobs,
    }
) => {
    const history = useHistory();
    const {locale} = useSelector(state => state.localeState);

    return (
        <div className={styles.leftColumnList}>
            <div className={styles.itemBlock}>
                <div className={`${styles.item}`}>
                    <div className={styles.searchBlock}>
                        <FontAwesomeIcon icon={faSearch} className={styles.searchIcon}/>
                        <input
                            type="text"
                            className={`${styles.input} ${styles.searchInput}`}
                            onChange={(e) => setSearch(e.target.value || '')}
                            value={search || ''}
                            onBlur={() => setSearch(search?.trim() || '')}
                        />
                        <button
                            type='button'
                            className={styles.searchBtn}
                            onClick={() => handleSetActiveJob(0)}
                        >
                            <FontAwesomeIcon icon={faPlus} className={styles.searchBtnIcon}/>
                        </button>
                    </div>
                    <div>
                        <span className={styles.countResult}>
                            {filteredListOfJobs?.length}
                            {' '}
                            {locale ? locale['Job advertisements'] : ''}
                        </span>
                    </div>
                </div>
                <div className={styles.item}>
                    <FontAwesomeIcon icon={faChartBar} className={styles.itemBlockIcon}/>
                    <span
                        className={`${styles.itemBlockName} ${getActiveTab === 'overview' ? styles.activeItem : ''}`}
                        onClick={() => {
                            history.push(location.pathname + '?active=overview');
                        }}
                    >
                        {locale ? locale['Overview'] : ''}
                    </span>
                </div>
                <div className={styles.item}>
                    <FontAwesomeIcon icon={faBriefcase} className={styles.itemBlockIcon}/>
                    <span
                        className={`${styles.itemBlockName} ${getActiveTab === 'create' ? styles.activeItem : ''}`}
                        onClick={() => handleSetActiveJob(0)}
                    >
                        {locale ? locale['Job advertisements'] : ''}
                    </span>
                    {filteredListOfJobs?.length ? (
                        filteredListOfJobs.map((value, i) => (
                            <GenerateJobList
                                key={i}
                                value={value}
                                handleSetActiveJob={handleSetActiveJob}
                            />
                        ))
                    ) : ''}
                </div>
            </div>
        </div>
    );
};
