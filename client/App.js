import React, {lazy, Suspense, useEffect} from 'react';

import {Route, Switch} from "react-router";
import {BrowserRouter} from "react-router-dom";

import {useDispatch, useSelector} from "react-redux";

import {ErrorBoundary} from "./blocks/ErrorBoundary/ErrorBoundary";

import {fetchPlan} from "./store/ducks/plan";
import {fetchLocale} from "./store/ducks/locale";
import {fetchUserId} from "./store/ducks/user";

const Cart = lazy(() => import('./Cart/Cart'));
const JobAd = lazy(() => import('./JobAd/JobAd'));
const Profile = lazy(() => import('./Profile/Profile'));
const MainPage = lazy(() => import('./MainPage/MainPage'));
const PrivacyEn = lazy(() => import('./StaticPages/PrivacyEn'));
const PrivacyDe = lazy(() => import('./StaticPages/PrivacyDe'));
const ImprintEn = lazy(() => import('./StaticPages/ImprintEn'));
const ImprintDe = lazy(() => import('./StaticPages/ImprintDe'));
const JobSearch = lazy(() => import('./JobSearchPage/JobSearch'));
const CompanyInfo = lazy(() => import('./CompanyInfo/CompanyInfo'));
const JobForGoogle = lazy(() => import('./JobForGoogle/JobForGoogle'));
const MainSearchPage = lazy(() => import('./MainSearchPage/MainSearchPage'));

const PaketModal = lazy(() => import('./Modal/PaketModal'));
const LoginModal = lazy(() => import('./Modal/LoginModal'));
const AlertMessage = lazy(() => import('./blocks/Alert/AlertMessage'));
const RequiredData = lazy(() => import('./RequiredData/RequiredData'));
const RegisterModal = lazy(() => import('./Modal/RegisterModal'));
const ContactFormModal = lazy(() => import('./Modal/ContactFormModal/ContactFormModal'));
const BillingAddressModal = lazy(() => import('./Modal/BillingAddressModal'));

export const App = () => {

    const dispatch = useDispatch();

    const {userId} = useSelector(state => state.userIdState);
    const {isShown} = useSelector(state => state.alertState);
    const {
        isOpenPaketModal,
        isOpenLoginModal,
        isOpenRegisterModal,
        isOpenBillingInfoModal,
        isOpenContactFormModal,
    } = useSelector(state => state.modalsState);

    useEffect(() => {
        dispatch(fetchUserId());
        dispatch(fetchLocale());
    }, []);

    useEffect(() => {
        if (userId) {
            dispatch(fetchPlan());
        }
    }, [userId]);

    return <BrowserRouter>
        {isShown &&
        <ErrorBoundary>
            <Suspense fallback={<div/>}>
                <AlertMessage/>
            </Suspense>
        </ErrorBoundary>
        }
        {isOpenPaketModal &&
        <ErrorBoundary>
            <Suspense fallback={<div/>}>
                <PaketModal/>
            </Suspense>
        </ErrorBoundary>
        }
        {isOpenLoginModal &&
        <ErrorBoundary>
            <Suspense fallback={<div/>}>
                <LoginModal/>
            </Suspense>
        </ErrorBoundary>
        }
        {isOpenRegisterModal &&
        <ErrorBoundary>
            <Suspense fallback={<div/>}>
                <RegisterModal/>
            </Suspense>
        </ErrorBoundary>
        }
        {isOpenBillingInfoModal &&
        <ErrorBoundary>
            <Suspense fallback={<div/>}>
                <BillingAddressModal/>
            </Suspense>
        </ErrorBoundary>
        }
        {isOpenContactFormModal &&
        <ErrorBoundary>
            <Suspense fallback={<div/>}>
                <ContactFormModal/>
            </Suspense>
        </ErrorBoundary>
        }

        <Switch>
            <Route path={'/required'}>
                <Suspense fallback={<div/>}>
                    <RequiredData/>
                </Suspense>
            </Route>
            <Route path={'/cart'}>
                <Suspense fallback={<div/>}>
                    <Cart/>
                </Suspense>
            </Route>
            <Route path={'/search'}>
                <Suspense fallback={<div/>}>
                    <MainSearchPage/>
                </Suspense>
            </Route>
            <Route path={'/overview'}>
                <Suspense fallback={<div/>}>
                    <JobForGoogle/>
                </Suspense>
            </Route>
            <Route path={'/privacy-en'}>
                <Suspense fallback={<div/>}>
                    <PrivacyEn/>
                </Suspense>
            </Route>
            <Route path={'/privacy-de'}>
                <Suspense fallback={<div/>}>
                    <PrivacyDe/>
                </Suspense>
            </Route>
            <Route path={'/imprint-en'}>
                <Suspense fallback={<div/>}>
                    <ImprintEn/>
                </Suspense>
            </Route>
            <Route path={'/imprint-de'}>
                <Suspense fallback={<div/>}>
                    <ImprintDe/>
                </Suspense>
            </Route>
            <Route path={'/job-search'}>
                <Suspense fallback={<div/>}>
                    <JobSearch/>
                </Suspense>
            </Route>

            <Route path={'/job-add'}>
                <Suspense fallback={<div/>}>
                    <JobAd/>
                </Suspense>
            </Route>
            <Route path={'/profile'}>
                <Suspense fallback={<div/>}>
                    <Profile/>
                </Suspense>
            </Route>
            <Route path={'/company-info'}>
                <Suspense fallback={<div/>}>
                    <CompanyInfo/>
                </Suspense>
            </Route>

            <Route>
                <Suspense fallback={<div/>}>
                    <MainPage/>
                </Suspense>
            </Route>
        </Switch>
    </BrowserRouter>
};
