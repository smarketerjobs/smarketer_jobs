import React, {useEffect, useState} from 'react';

import axios from 'axios';
import Toggle from 'react-toggle';
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import AuthBlock from '../blocks/AuthBlock/AuthBlock';
import {showAlert} from "../store/ducks/alert";
import {fetchCountries} from "../store/ducks/countries";
import {fetchUserAccountData, getUserIdFailure} from "../store/ducks/user";
import OverviewPage from "../blocks/OverviewPage/OverviewPage";

import styles from './Profile.module.scss';

const Profile = () => {

    const history = useHistory();

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {countries} = useSelector(state => state.countriesState);
    const {userId, error} = useSelector(state => state.userIdState);
    const {userAccountData} = useSelector(state => state.userAccountDataState);

    const [getOldPassword, setOldPassword] = useState('');
    const [getNewPassword, setNewPassword] = useState('');

    const [getName, setName] = useState('');
    const [getPhone, setPhone] = useState('');
    const [getEmail, setEmail] = useState('');
    const [getNewEmail, setNewEmail] = useState('');
    const [getMailing, setMailing] = useState(true);
    const [getLastName, setLastName] = useState('');
    const [getCountryCode, setCountryCode] = useState('DE');

    const [getConfirmDelete, setConfirmDelete] = useState(false);
    const [isAccountDeleted, setAccountDeleted] = useState(false);

    const updateProfile = () => {
        if ((getEmail || getNewEmail) && getName && getLastName && getCountryCode) {
            axios.get('/ajax.php', {
                params: {
                    model: 'user',
                    mode: 'update_data',
                    email: getEmail || getNewEmail,
                    last_name: getLastName,
                    first_name: getName,
                    phone_number: getPhone,
                    country_code: getCountryCode,
                    receive_emails: getMailing ? 1 : 0,
                },
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        dispatch(fetchUserAccountData(userId));
                        dispatch(showAlert('success', 'Company information successfully updated'));
                    } else {
                        if (data.error === 'Wrong e-mail format') {
                            dispatch(showAlert('failure', 'Wrong e-mail format'));
                        } else {
                            dispatch(showAlert('failure', 'Error updating company information'));
                        }
                    }
                })
                .catch(err => console.error(err));
        } else {
            dispatch(showAlert('failure', 'Required fields cannot be empty'));
        }
    };

    const updatePassword = () => {
        axios.get('/ajax.php', {
            params: {
                model: 'user',
                mode: 'update_password',
                old_password: getOldPassword,
                password: getNewPassword,
            },
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    dispatch(showAlert('success', 'Password changed successfully'));
                } else {
                    console.error(data);
                    dispatch(showAlert('failure', 'Password change error'));
                }
            })
            .catch(err => console.error(err));
    };

    const deleteAccount = () => {
        axios.get('/ajax.php', {
            params: {model: 'user', mode: 'delete'},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    dispatch(getUserIdFailure(''));
                    setAccountDeleted(true);
                    dispatch(showAlert('success', 'Account successfully deleted'));
                } else {
                    if (data.error === 'Password is incorrect') {
                        dispatch(showAlert('failure', 'Password is incorrect'));
                    } else {
                        console.error(data.error);
                        dispatch(showAlert('failure', data.error));
                    }
                }
            })
            .catch(err => console.error(err));
    };

    useEffect(() => {
        dispatch(fetchCountries());
        dispatch(fetchUserAccountData());
    }, []);

    useEffect(() => {
        if (userAccountData) {
            setMailing(!!+userAccountData.receive_emails);
            userAccountData.email ? setEmail(userAccountData.email) : '';
            userAccountData.last_name ? setLastName(userAccountData.last_name) : '';
            userAccountData.first_name ? setName(userAccountData.first_name) : '';
            userAccountData.phone_number ? setPhone(userAccountData.phone_number) : '';
            userAccountData.country_code ? setCountryCode(userAccountData.country_code) : '';
        }
    }, [userAccountData]);

    useEffect(() => {
        if (!userId && error || isAccountDeleted) {
            history.push('/');
        }
    }, [userId, error, isAccountDeleted]);

    return (
        <OverviewPage isNoResponsive={true}>
            <div className={styles.root}>
                <AuthBlock isRegisterPage={false}>
                    <div className={styles.header}>
                        <h3 className={styles.title}>{locale ? locale['Account Information'] : ''}</h3>
                    </div>
                    <div>
                        <p className={styles.description}>
                            {locale ? locale['Account Information sub titel'] : ''}</p>
                        <div className={styles.authForm}>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>
                                    {locale ? locale['E-Mail Address'] : ''}</span>
                                <input
                                    type="email"
                                    name="email"
                                    value={getNewEmail || getEmail}
                                    onBlur={() => setNewEmail(getNewEmail.trim())}
                                    onChange={e => setNewEmail(e.target.value)}
                                    disabled={getEmail}
                                    className={styles.input}
                                    placeholder="name@companyname.de"
                                />
                            </label>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>{locale ? locale['Newsletter'] : ''}</span>
                                <span className={styles.inputDescribeLabel}>
                                     <label className={styles.toggleBtn}>
                                        <Toggle
                                            checked={!!getMailing}
                                            icons={false}
                                            name='receive_emails'
                                            className={styles.checkbox}
                                            onChange={e => setMailing(e.target.checked)}/>
                                            <span className={styles.toggleSpan}>
                                                {locale ? locale['Newsletter toggle label'] : ''}
                                            </span>
                                    </label>
                                </span>
                            </label>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>
                                    {locale ? locale['First name'] : ''}</span>
                                <input type='text' name='first_name'
                                       value={getName}
                                       onBlur={() => setName(getName.trim())}
                                       className={styles.input} onChange={e => setName(e.target.value)}/>
                            </label>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>{locale ? locale['Name'] : ''}</span>
                                <input type='text' name='last_name' value={getLastName} className={styles.input}
                                       onChange={e => setLastName(e.target.value)}
                                       onBlur={() => setLastName(getLastName.trim())}/>
                            </label>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>
                                    {locale ? locale['Phone number'] : ''}</span>
                                <input type='text' name='phone'
                                       value={getPhone}
                                       onBlur={() => setPhone(getPhone.trim())}
                                       className={styles.input}
                                       onChange={e => setPhone(e.target.value)}/>
                            </label>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>Country</span>
                                <div className={styles.selectWrapper}>
                                    <select name='countries'
                                            className={`${styles.input} ${styles.select}`}
                                            onChange={e => setCountryCode(e.target.value)}
                                            value={getCountryCode}>
                                        {countries.map(country => (
                                            <option value={country.country_code} key={country.country_code}>
                                                {country.country}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </label>
                            <div className={styles.submitBtnWrapper}>
                                <button type='button' className={`${styles.submitBtn} ${styles.resetMargins}`}
                                        onClick={updateProfile}>
                                    {locale ? locale['Save changes'] : ''}
                                </button>
                            </div>
                        </div>
                    </div>
                </AuthBlock>
                <AuthBlock isRegisterPage={false}>
                    <div className={styles.header}>
                        <h3 className={styles.title}>{locale ? locale['Account Information'] : ''}</h3>
                    </div>
                    <div>
                        <p className={styles.description}>
                            {locale ? locale['change password sub titel'] : ''}</p>
                        <div className={styles.authForm}>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>{locale ? locale['Old password'] : ''}</span>
                                <input type='password' name='old_password'
                                       className={styles.input}
                                       onBlur={() => setOldPassword(getOldPassword.trim())}
                                       onChange={e => setOldPassword(e.target.value)}
                                />
                            </label>
                            <label className={styles.inputLabel}>
                                <span className={styles.inputLabelText}>{locale ? locale['New password'] : ''}</span>
                                <input type='password' name='password'
                                       className={styles.input}
                                       onBlur={() => setNewPassword(getNewPassword.trim())}
                                       onChange={e => setNewPassword(e.target.value)}
                                />
                            </label>
                            <div className={styles.submitBtnWrapper}>
                                <button type='button' className={`${styles.submitBtn} ${styles.resetMargins}`}
                                        onClick={updatePassword}>
                                    {locale ? locale['Save new password'] : ''}
                                </button>
                            </div>
                        </div>
                    </div>
                </AuthBlock>
                <AuthBlock isRegisterPage={false}>
                    <div className={styles.header}>
                        <h3 className={styles.title}>
                            {locale ? locale['You can delete Account'] : ''}
                        </h3>
                    </div>
                    <div>
                        <div className={styles.authForm}>
                            <div className={styles.submitBtnWrapper}>
                                {getConfirmDelete
                                    ? (
                                        <div className={styles.confirmDeleteWrapper}>
                                            <div className={styles.confirmDeleteMessage}>
                                                {locale ? locale['Confirm delete account message'] : ''}
                                            </div>
                                            <div className={styles.confirmDeleteBtnWrapper}>
                                                <button
                                                    className={styles.confirmDeleteYes}
                                                    onClick={deleteAccount}
                                                >
                                                    {locale ? locale['Yes'] : ''}
                                                </button>
                                                <button
                                                    className={styles.confirmDeleteNo}
                                                    onClick={() => setConfirmDelete(false)}
                                                >
                                                    {locale ? locale['No'] : ''}
                                                </button>
                                            </div>
                                        </div>
                                    )
                                    : (
                                        <button
                                            type='button'
                                            className={`${styles.submitBtn} ${styles.resetMargins}`}
                                            onClick={() => setConfirmDelete(true)}
                                        >
                                            {locale ? locale['Delete account'] : ''}
                                        </button>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </AuthBlock>
            </div>
        </OverviewPage>
    );
};

export default Profile;

