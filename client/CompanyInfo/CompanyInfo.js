import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";

import * as moment from "moment";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBuilding, faLightbulb, faStar} from '@fortawesome/fontawesome-free-solid';

import Deposit from '../icons/Deposit';
import Document from '../icons/Document';
import downloadImg from '../../media/download.png';

import subscribe from '../../js/stripe-checkout';
import OverviewPage from '../blocks/OverviewPage/OverviewPage';
import {formatMoney} from "../utils";
import CompanyInfoForm from "../blocks/CompanyInfoForm/CompanyInfoForm";

import {fetchPlan} from "../store/ducks/plan";
import {showAlert} from "../store/ducks/alert";
import {fetchBills} from "../store/ducks/bills";
import {openBillingInfoModal, openContactFormModal, openPaketModal} from "../store/ducks/modals";
import {addNewCompany, fetchUserAccountData, fetchUserBillingData, fetchUserCompaniesData} from "../store/ducks/user";

import styles from './CompanyInfo.module.scss';

const CompanyInfo = () => {
    const history = useHistory();

    const [getPlanName, setPlanName] = useState('Kein Aktueller Plan');
    const [showLastName, setLastName] = useState('');
    const [showPostCode, setPostCode] = useState('');
    const [isActivePlan, setActivePlan] = useState(true);
    const [showFirstName, setFirstName] = useState('');
    const [showFirstAddressName, setFirstAddressName] = useState('');
    const [showSecondAddressName, setSecondAddressName] = useState('');

    const [getCurrentBillingAt, setCurrentBillingAt] = useState('');
    const [getUserRegisteredAt, setUserRegisteredAt] = useState('');

    const [getAddCompany, setAddCompany] = useState(true);

    const dispatch = useDispatch();
    const {plan} = useSelector(state => state.planState);
    const {locale} = useSelector(state => state.localeState);
    const billsState = useSelector(state => state.billsState);
    const {userId, error} = useSelector(state => state.userIdState);
    const {userCompaniesData} = useSelector(state => state.userCompaniesDataState);
    const {userAccountData} = useSelector(state => state.userAccountDataState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);

    const getPlanTitle = planTitle => {
        switch (planTitle) {
            case 'starter':
                return 'Starter';
            case 'professional_year':
                return 'Professional year';
            case 'professional_month':
                return 'Professional month';
            case 'plus_year':
                return 'Plus year';
            case 'plus_month':
                return 'Plus month';
            case 'one_day':
                return 'One day';
            case 'basic_year':
                return 'Basic year';
            case 'basic_month':
                return 'Basic month';
            default:
                return '';
        }
    };

    const addCompany = () => {
        if (plan && plan['max_active_companies'] > userCompaniesData.length) {
            dispatch(addNewCompany());
        } else {
            dispatch(showAlert('fail', 'choose professional plan'));
        }
    };

    const getValidDate = invalidDate => {
        const [DD, MM, YYYY] = invalidDate.split('-');
        return `${DD}.${MM}.${YYYY}`
    }

    const checkPayment = () => {
        const fail = location.search.includes('fail');
        const success = location.search.includes('success');

        if (fail) {
            dispatch(showAlert('failure', 'payment failure'));
        }

        if (success) {
            dispatch(showAlert('success', 'payment success'));
        }
    };

    useEffect(() => {
        checkPayment();
        dispatch(fetchPlan());
        dispatch(fetchBills());
        dispatch(fetchUserBillingData());

        const stripeScript = document.getElementById('stripe-js');

        if (!stripeScript) {
            const script = document.createElement('script');

            script.setAttribute('src', 'https://js.stripe.com/v3/');
            script.setAttribute('id', 'stripe-js');
            script.setAttribute('data-checked-head', 'false');
            document.head.appendChild(script);
        }
    }, []);

    useEffect(() => {
        if (plan && userCompaniesData.length) {
            setAddCompany(+plan['max_active_companies'] > userCompaniesData.length);
        }
    }, [plan, userCompaniesData]);

    useEffect(() => {
        if (userId) {
            dispatch(fetchUserAccountData(userId));
            dispatch(fetchUserCompaniesData(userId));
        }
    }, [userId]);

    useEffect(() => {
        if (plan) {
            setActivePlan(true);
            setPlanName(getPlanTitle(plan.plan_name));
            setCurrentBillingAt(plan['current_billing_at'] ? plan['current_billing_at'] : '');
            setUserRegisteredAt(plan['user_registered_at'] ? plan['user_registered_at'] : '');
        }
    }, [plan]);

    useEffect(() => {
        if (userBillingData) {
            userBillingData['last_name'] ? setLastName(userBillingData['last_name']) : '';
            userBillingData['post_code'] ? setPostCode(userBillingData['post_code']) : '';
            userBillingData['first_name'] ? setFirstName(userBillingData['first_name']) : '';
            userBillingData['address_1'] ? setFirstAddressName(userBillingData['address_1']) : '';
            userBillingData['address_2'] ? setSecondAddressName(userBillingData['address_2']) : '';
            userBillingData['current_billing_at'] ? setCurrentBillingAt(userBillingData['current_billing_at']) : '';
            userBillingData['user_registered_at'] ? setUserRegisteredAt(userBillingData['user_registered_at']) : '';
        }
    }, [userBillingData]);

    useEffect(() => {
        if (!userCompaniesData.length) {
            dispatch(addNewCompany());
        }
    }, [userCompaniesData]);

    useEffect(() => {
        if (!userId && error) {
            history.push('/');
        }
    }, [userId, error]);

    return (
        <OverviewPage isNoResponsive={true}>
            <div className={styles.leftColumnList}>
                <div className={styles.itemBlock}>
                    <FontAwesomeIcon icon={faBuilding} className={styles.itemBlockIcon}/>
                    <span className={styles.itemBlockName}>{locale ? locale['Company'] : ''}</span>
                </div>
            </div>
            <div className={styles.rightColumnList}>
                <div className={styles.listWrapper}>
                    <div className={styles.blockOfItem}>
                        <div className={styles.title}>
                            <h2 className={styles.blockName}>{locale ? locale['Package'] : ''}</h2>
                            <p className={styles.blockDescription}>{locale ? locale['Package sub text'] : ''}</p>
                        </div>
                        <div className={styles.blockDetails}>
                            <div className={styles.itemEl}>
                                <div className={styles.iconDescription}>
                                    <FontAwesomeIcon
                                        icon={faStar}
                                        className={`${styles.itemBlockIcon} ${isActivePlan ? styles.activeIcon : ''}`}/>
                                    {isActivePlan
                                        ? <div className={styles.iconText}>
                                            <span className={styles.text}>
                                                {locale ? locale['Your current package'] : ''}:
                                                <span className={styles.paketName}> {getPlanName}</span>
                                            </span>
                                            {getCurrentBillingAt || getUserRegisteredAt
                                                ? <span className={styles.periodOfPlan}>
                                                    {locale ? locale['booked on'] : ''}
                                                    {': '}
                                                    <span className={styles.time}>
                                                        {getValidDate(getCurrentBillingAt ? getCurrentBillingAt : getUserRegisteredAt)}
                                                    </span>
                                                </span>
                                                : ''
                                            }
                                        </div>
                                        : <div className={styles.iconText}>
                                            <span className={`${styles.text} ${styles.bold}`}>
                                                {locale ? locale['No package booked'] : ''}
                                            </span>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div className={styles.itemPopup}>
                                <button type='button' className={styles.button}
                                        onClick={() => dispatch(openPaketModal())}>
                                    {locale ? locale['Change package'] : ''}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className={styles.blockOfItem}>
                        <div className={styles.title}>
                            <h2 className={styles.blockName}>{locale ? locale['Payment method'] : ''}</h2>
                            <p className={styles.blockDescription}>{locale ? locale['Payment method sub text'] : ''}</p>
                            <div className={styles.blockDescription}>
                                <div className={styles.blockWrapper}>
                                    <div className={styles.blockWithIcon}>
                                        <FontAwesomeIcon icon={faLightbulb} className={styles.itemBlockIcon}/>
                                    </div>
                                    <div className={styles.blockWithText}>
                                        <span className={styles.bold}>{locale ? locale['hint'] : ''}: </span>
                                        {locale ? locale['hint payment'] : ''}
                                        <button
                                            className={styles.btnContactUs}
                                            onClick={() => dispatch(openContactFormModal())}
                                        >
                                            {locale ? locale['hint payment contact'] : ''}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={styles.blockDetails}>
                            <div className={styles.itemEl}>
                                <div className={styles.iconDescription}>
                                    <Deposit styling={{marginRight: '15px'}}/>
                                    <div className={styles.iconText} style={{justifyContent: 'flex-end'}}>
                                        <span className={`${styles.text} ${styles.bold}`}>
                                            {locale ? locale['payment method:'] : ''}
                                            {' '}
                                            {locale ? (
                                                userAccountData && userAccountData['stripe_payment_method_id']
                                                    ? locale['Credit card']
                                                    : locale['None selected']
                                            ) : ''
                                            }
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.itemPopup}>
                                <button
                                    type="button"
                                    className={styles.button}
                                    onClick={() => subscribe('change_payment_method', 0)}
                                >
                                    {locale ? locale['Change payment method'] : ''}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className={styles.blockOfItem}>
                        <div className={styles.title}>
                            <h2 className={styles.blockName}>{locale ? locale['Billing address'] : ''}</h2>
                            <p className={styles.blockDescription}>{locale ? locale['Billing sub text'] : ''}</p>
                        </div>
                        <div className={styles.blockDetails}>
                            <div className={styles.itemEl}>
                                <div className={styles.iconDescription}>
                                    <Document styling={{marginRight: '15px'}}/>
                                    <div className={styles.iconText} style={{justifyContent: 'flex-end'}}>
                                        {showFirstName || showLastName
                                            ? <span className={`${styles.text}`}>
                                                {showFirstName} {showLastName || 'Nachname'}
                                            </span>
                                            : ''
                                        }
                                        {showFirstAddressName
                                            ? <span className={`${styles.text}`}>{showFirstAddressName}</span>
                                            : ''
                                        }
                                        {showSecondAddressName
                                            ? <span className={`${styles.text}`}>{showSecondAddressName}</span>
                                            : ''
                                        }
                                        {showPostCode ? <span className={`${styles.text}`}>{showPostCode}</span> : ''}
                                    </div>
                                </div>
                            </div>
                            <div className={styles.itemPopup}>
                                <button type='button' className={styles.button}
                                        onClick={() => dispatch(openBillingInfoModal())}>
                                    {locale ? locale['Edit billing address'] : ''}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className={styles.blockOfItem}>
                        <div className={styles.title}>
                            <h2 className={styles.blockName}>{locale ? locale['Bills'] : ''}</h2>
                            <div className={styles.blockDescription}>
                                <span className={styles.text}>{locale ? locale['Past invoices'] : ''}</span>
                            </div>
                        </div>
                        <div className={styles.blockDetails}>
                            {billsState.loading && !billsState.data ? (
                                <p className={`${styles.text} ${styles.italic}`}>
                                    {locale ? locale['Loading...'] : ''}
                                </p>
                            ) : (
                                billsState.data?.length && !billsState.error ? (
                                    <div className={styles.billsTable}>
                                        <div className={styles.billsHeaderRow}>
                                            <div className={styles.billsItem}>
                                                {locale ? locale['Billing Date'] : ''}
                                            </div>
                                            <div className={styles.billsItem}>
                                                {locale ? locale['Billing Amount'] : ''}
                                            </div>
                                            <div className={styles.billsItem}>
                                                {locale ? locale['Billing Payment Method'] : ''}
                                            </div>
                                            <div className={styles.billsItem}>
                                                {locale ? locale['Billing Status'] : ''}
                                            </div>
                                            <div className={styles.billsItem}>
                                                {locale ? locale['Billing Download'] : ''}
                                            </div>
                                        </div>
                                        {billsState.data?.map(bill => (
                                            <div key={bill.id} className={styles.billsRow}>
                                                <div className={styles.billsItem}>
                                                    {moment(new Date(bill.date)).format("DD.MM.YYYY")}
                                                </div>
                                                <div className={styles.billsItem}>
                                                    {bill.amount ? (
                                                        <>
                                                            {bill.currency === 'usd' ? '$' : ''}
                                                            {formatMoney(bill.amount, locale ? locale['lang'] : 'en')}
                                                            {bill.currency === 'eur' ? '€' : ''}
                                                        </>
                                                    ) : 0}
                                                </div>
                                                <div className={styles.billsItem}>
                                                    {bill.payment_method === 'card' ? (
                                                        locale ? locale['Credit card'] : ''
                                                    ) : bill.payment_method}
                                                </div>
                                                <div className={styles.billsItem}>
                                                    {bill.status === 'paid' ? (locale ? locale['Paid'] : '') : bill.status}
                                                </div>
                                                <div className={styles.billsItem}>
                                                    <a href={bill.link} download className={styles.downloadLink}>
                                                        <img src={downloadImg} alt="Download"
                                                             className={styles.downloadImg}/>
                                                    </a>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                ) : (
                                    <p className={`${styles.text} ${styles.italic}`}>
                                        {locale ? locale['There are no bills.'] : ''}
                                    </p>
                                )
                            )}
                        </div>
                    </div>
                    {userCompaniesData?.map((company, i) => (
                        <CompanyInfoForm key={company.company_id || `company-key-${i}`} company={company}/>)
                    )}
                    <div className={styles.btnAddCompanyWrapper}>
                        <button
                            className={`${styles.btnAddCompany} ${getAddCompany ? styles.green : styles.orange}`}
                            onClick={addCompany}
                        >
                            <span className={styles.btnAddCompanyText}>
                                {locale ? locale['Add company btn'] : ''}
                            </span>
                            <span className={styles.imgWrapper}>
                                <img
                                    width="512"
                                    height="512"
                                    src="../../media/plus.png"
                                    alt="plus"
                                    className={styles.img}
                                />
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </OverviewPage>
    );
};

export default CompanyInfo;
