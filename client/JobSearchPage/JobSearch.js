import {useHistory} from "react-router";
import React, {useEffect, useState} from 'react';
import {useLocation} from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import {useDispatch, useSelector} from "react-redux";

import axios from 'axios';
import * as moment from "moment";

import Footer from "../blocks/Footer/Footer";
import BackToTop from "../blocks/BackToTop/BackToTop";
import JobContent from "./JobContent";
import useIsMobile from "../../hooks/useIsMobile";
import JobSearchHeader from "../blocks/JobSearchHeader/JobSearchHeader";
import {fetchMoreSearchJobs, fetchSearchJobs, fetchSearchJobsDefault, setSearchCountry,} from "../store/ducks/search";
import {createLocationSearch, getListLocationParams, kitcut,} from "../utils";
import {HeaderSearch} from "./HeaderSearch";
import {fetchJobCatalogs} from "../store/ducks/job";

import styles from './JobSearch.module.scss';

import pcImg from '../../media/pc.png';
import clockImg from '../../media/clock.png';
import closeImg from '../../media/close.png';
import searchImg from '../../media/searchInCompany.png';
import sortAZImg from '../../media/sortA-Z.png';
import locationImg from '../../media/location.png';
import portfolioImg from '../../media/portfolio.png';
import listOfJobsImg from '../../media/listOfJobs.png';
import separator from "../../media/separator_bg.png";

const defaultRange = 25;

const JobSearch = () => {
    const history = useHistory();
    const location = useLocation();

    const dispatch = useDispatch();

    const {locale} = useSelector(state => state.localeState);
    const {countries} = useSelector(state => state.countriesState);
    const {dataJobCatalogs} = useSelector(state => state.jobState);
    const {
        jobs,
        cities,
        companies,
        defaultSearch,
        categories,
        isNoResults,
        isNewJobs,
        fetching,
        fetchingMore,
        maxLength,
    } = useSelector(state => state.searchState);
    const {
        city: [city = ''] = [],
        page: [page = 1] = [],
        range: [range = 25] = [],
        query: [query = ''] = [],
        job_id: [job_id = ''] = [],
        country: [country = ''] = [],
        company: [company = ''] = [],
        distance: [distance = city ? '50' : ''] = [],
        category: [category = ''] = [],
        company_sort: [company_sort = ''] = [],
        company_job_title: [company_job_title = ''] = [],
        company_sort_method: [company_sort_method = ''] = [],
        company_job_category: [company_job_category = ''] = [],
    } = getListLocationParams(location);

    const [getEmployerInfo, setEmployerInfo] = useState(null);

    const [getLengthCategory, setLengthCategory] = useState(9);
    const [getLengthEmployer, setLengthEmployer] = useState(9);
    const [getLengthLocations, setLengthLocations] = useState(9);

    const [getCompanyJobs, setCompanyJobs] = useState([]);
    const [getFilteredCompanyJobs, setFilteredCompanyJobs] = useState([]);

    const [getSelectedSearchSection, setSelectedSearchSection] = useState('category');

    const [getCityTimeoutId, setCityTimeoutId] = useState(undefined);
    const [getTitleTimeoutId, setTitleTimeoutId] = useState(undefined);

    const getValidUrl = url => url.includes('http') ? url : `http://${url}`;
    const setSpaceToPlus = name => name ? name.split(' ').join('+') : '';
    const setPlusToSpace = name => name ? name.split('+').join(' ') : '';

    const historyReplace = changes => {
        history.replace(location.pathname + createLocationSearch({
            ...getListLocationParams(location),
            ...changes,
        }));
    }

    const setSelectedCompany = employerSlug => {
        historyReplace({
            company: employerSlug,
            query: isNoResults ? null : query,
            city: isNoResults ? null : city,
            distance: isNoResults ? null : distance,
        })
    }

    const closeCompany = () => {
        historyReplace({company: null});
        setEmployerInfo(null);
    };

    const clearCompanyJobFilters = () => {
        historyReplace({
            company_job_title: null,
            company_job_category: null,
            company_sort: null,
            company_sort_method: null,
        });
    };

    const requestSearchJobs = () => {
        if (!fetching) {
            historyReplace({
                page: 1,
                range: defaultRange,
            });
            dispatch(fetchSearchJobs({
                city: setPlusToSpace(city) || '',
                page: '1',
                range: defaultRange,
                keyword: setPlusToSpace(query) || '',
                distance: distance || '',
                countryCode: country,
                categorySlug: category || '',
                isDefaultSearch: false,
                filters: '1',
            }));
        }
    };

    useEffect(() => {
        if (jobs && jobs.length && jobs.length >= defaultRange && +page > 1 && !fetching && !fetchingMore) {
            dispatch(fetchMoreSearchJobs({
                city: setPlusToSpace(city) || '',
                page: page,
                range: range,
                keyword: setPlusToSpace(query) || '',
                distance: distance || '',
                countryCode: country,
                categorySlug: category || '',
                isDefaultSearch: false,
            }));
        }
    }, [page, range]);

    const getDayPasted = postedDate => {
        const days = moment().diff(postedDate * 1000, 'days');
        if (days) {
            if (locale && locale['lang'] === 'en') {
                if (days === 1) {
                    return `${days} Day ago`;
                } else {
                    return `${days} Days ago`;
                }
            } else {
                if (days === 1) {
                    return `vor ${days} Tag`;
                } else {
                    return `vor ${days} Tagen`;
                }
            }
        } else {
            return locale ? locale['Today'] : '';
        }
    };

    const JobList = jobs => {
        return jobs.map(job => (
            <div className={`${styles.item} ${job_id === job.job_id ? styles.active : ''}`}
                 key={job.job_id}
                 onClick={() => {
                     history.push(location.pathname + createLocationSearch({
                         ...getListLocationParams(location),
                         job_id: job.job_id
                     }))
                 }}>
                {job['company_logo']
                    ? <div
                        className={styles.image}
                        onClick={() => historyReplace({
                            company: job.company_slug,
                            query: isNoResults ? null : query,
                            city: isNoResults ? null : city,
                            distance: isNoResults ? null : distance,
                        })}
                    >
                        <img src={`../../../../logotypes/${job['company_logo']}`} alt="Logo" className={styles.img}/>
                    </div>
                    : <div
                        className={styles.image2}
                        onClick={() => historyReplace({
                            company: job.company_slug,
                            query: isNoResults ? null : query,
                            city: isNoResults ? null : city,
                            distance: isNoResults ? null : distance,
                        })}
                    />
                }
                <div className={styles.description}>
                    <div className={styles.titleJobItem} title={job.title.length > 60 ? job.title : ''}>
                        <span>{kitcut(job.title, 60)}</span>
                    </div>
                    <div className={styles.companyJobItemDescription}><span>{job.company_name}</span></div>
                    <div className={styles.companyJobItemDescription}><span>{job.city}</span></div>
                    <div className={styles.jobDescriptionWrapper}>
                        <div className={styles.jobInfoItem}>
                            <div className={styles.jobInfoIconWrapper}>
                                <img src={clockImg} alt="clock" className={styles.jobInfoIcon}/>
                            </div>
                            <div className={styles.jobInfoDescription}>{getDayPasted(job['date_posted'])}</div>
                        </div>
                        <div className={styles.jobInfoItem}>
                            <div className={styles.jobInfoIconWrapper}>
                                <img src={portfolioImg} alt="clock" className={styles.jobInfoIcon}/>
                            </div>
                            <div className={styles.jobInfoDescription}>
                                {
                                    dataJobCatalogs?.locations?.find(loc => loc['location_id'] === job['location_id'])
                                    && dataJobCatalogs?.locations?.find(loc => loc['location_id'] === job['location_id'])['location_title']
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ));
    };

    useEffect(() => {
        const script = document.createElement('script');

        const scriptTimeoutId = setTimeout(() => {
            script.setAttribute('src', 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
            script.setAttribute('data-ad-client', 'ca-pub-6586482158296741');
            script.setAttribute('data-checked-head', 'false');
            document.head.appendChild(script);
        }, 3000);

        dispatch(fetchJobCatalogs());

        return () => {
            const timeoutId = setTimeout(() => {
                if (script) {
                    document.head.removeChild(script)
                }

                const insTags = document.getElementsByName('ins');
                const iFrames = document.getElementsByName('iframe');

                insTags.forEach(ins => document.removeChild(ins));
                iFrames.forEach(iFrame => document.removeChild(iFrame));

                clearTimeout(scriptTimeoutId);
                clearTimeout(timeoutId);
            }, 3000);
        }
    }, []);

    useEffect(() => {
        if (getCompanyJobs.length) {
            setFilteredCompanyJobs(getCompanyJobs
                .sort((prevLob, nextJob) => prevLob['date_posted'] - nextJob['date_posted'] ? 1 : -1));
        }
    }, [getCompanyJobs]);

    useEffect(requestSearchJobs, [country, category]);

    useEffect(() => {
        if (city) {
            if (getCityTimeoutId) {
                clearTimeout(getCityTimeoutId);
            }

            const timeoutId = setTimeout(requestSearchJobs, 1000);

            setCityTimeoutId(timeoutId);
        } else {
            requestSearchJobs();
        }
    }, [city, distance]);

    useEffect(() => {
        if (query) {
            if (getTitleTimeoutId) {
                clearTimeout(getTitleTimeoutId);
            }

            const timeoutId = setTimeout(requestSearchJobs, 1000);

            setTitleTimeoutId(timeoutId);
        } else {
            requestSearchJobs();
        }
    }, [query]);

    useEffect(() => {
        if (countries.length && country) {
            dispatch(setSearchCountry(countries.find(c => c?.country_code === country)));
        }
    }, [countries]);

    useEffect(() => {
        if (isNoResults) {
            dispatch(fetchSearchJobsDefault(defaultSearch))

            historyReplace({
                city: city,
                query: query,
                company: null,
                category: null,
                distance: city ? distance || 50 : null,
            });
        }
    }, [isNoResults]);

    useEffect(() => {
        if (company) {
            if (company !== getEmployerInfo?.company_slug) {
                axios.get('/ajax.php', {
                    params: {
                        model: 'company',
                        mode: 'get_company_data_by_slug',
                        company_slug: company
                    },
                    headers: {'Cache-Control': 'no-cache'}
                })
                    .then(({data}) => {
                        if (data.data) {
                            setEmployerInfo(data.data);
                        } else {
                            console.error(data);
                        }
                    })
                    .catch(err => {
                        console.error(err)
                    });

                axios.get('/ajax.php', {
                    params: {
                        q: '',
                        mode: 'get_jobs_by_company',
                        model: 'job',
                        sort_by: 'date_posted',
                        sort_method: 'desc',
                        company_slug: company,
                    },
                    headers: {'Cache-Control': 'no-cache'}
                })
                    .then(({data}) => {
                        if (data.data) {
                            setCompanyJobs(data.data);
                        } else {
                            console.error(data);
                        }
                    })
                    .catch(err => {
                        console.error(err)
                    });
            }

            setFilteredCompanyJobs(
                getCompanyJobs
                    .filter(job => company_job_title ? job.title.toLowerCase().includes(company_job_title?.toLowerCase()) : true)
                    .filter(job => company_job_category ? +job['category_id'] === +company_job_category : true)
                    .sort((jobPrev, jobNext) => {
                        if (company_sort === 'date_posted') {
                            if (company_sort_method === 'asc') {
                                return jobPrev['date_posted'] - jobNext['date_posted'] ? 1 : -1;
                            } else if (company_sort_method === 'desc') {
                                return jobPrev['date_posted'] - jobNext['date_posted'] ? -1 : 1;
                            }
                        } else {
                            if (company_sort_method === 'asc') {
                                return jobPrev.title.toLowerCase().localeCompare(jobNext.title.toLowerCase());
                            } else if (company_sort_method === 'desc') {
                                return jobNext.title.toLowerCase().localeCompare(jobPrev.title.toLowerCase());
                            }
                        }
                    })
            );
        }
    }, [
        company, company_job_title, company_job_category, company_sort, company_sort_method
    ]);

    useEffect(() => {
        if (isNewJobs) {
            if (jobs.length && !company && (!job_id || jobs.indexOf(jobs.find(j => j.job_id === job_id)) !== 0)) {
                const body = document.getElementsByTagName('body')[0];
                if (body && body.clientWidth > 595) {
                    historyReplace({job_id: jobs[0].job_id});
                }
            }
        }
    }, [jobs]);

    useEffect(() => {
        if (getFilteredCompanyJobs.length) {
            if (!getFilteredCompanyJobs.find(job => job_id === job.job_id)) {
                historyReplace({job_id: getFilteredCompanyJobs[0].job_id});
            }
        }
    }, [getFilteredCompanyJobs]);

    return (
        <div className={styles.root}>
            <BackToTop/>
            <JobSearchHeader
                city={city || ''}
                title={query || ''}
                setCity={city => historyReplace({
                    city: city ? city : null,
                    distance: city ? distance || 50 : null,
                    query: query ? query : null,
                    company: isNoResults ? null : company,
                    category: isNoResults ? null : category,
                })}
                setTitle={title => historyReplace({
                    query: title ? title : null,
                    city: city ? city : null,
                    distance: city ? distance || 50 : null,
                    company: isNoResults ? null : company,
                    category: isNoResults ? null : category,
                })}
                closeCompany={closeCompany}
                setCountryCode={code => historyReplace({
                    country: code,
                    city: null,
                    query: query ? query : null,
                    job_id: null,
                    company: null,
                    distance: null,
                    category: category ? category : null,
                    company_sort: null,
                    company_job_title: null,
                    company_sort_method: null,
                    company_job_category: null,
                })}
            />
            <div className={styles.wrapper}>
                {getEmployerInfo ? (
                    <>
                        <div className={styles.companyInfoWrapper}>
                            <div className={styles.companyInfoInnerWrapper}>
                                <div className={styles.companyLogoWrapper}>
                                    {getEmployerInfo['company_logo'] ? (
                                        <div className={styles.companyLogoInnerWrapper}>
                                            <img
                                                src={getEmployerInfo['company_logo']}
                                                alt={getEmployerInfo['company_name']}
                                                className={styles.logo}
                                            />
                                        </div>
                                    ) : (
                                        <div className={styles.companyLogoInnerWrapper2}/>
                                    )}
                                </div>
                                <div className={styles.companyMainInfoWrapper}>
                                    {getEmployerInfo['company_name'] ? (
                                        <div className={styles.companyName}>
                                            {getEmployerInfo['company_name']}
                                        </div>
                                    ) : ''}
                                    {getEmployerInfo['company_location'] ? (
                                        <div className={styles.companyDescription}>
                                            <div className={styles.descriptionIconWrapper}>
                                                <img
                                                    src={locationImg}
                                                    className={styles.descriptionIcon}
                                                    alt='location'
                                                />
                                            </div>
                                            <div className={styles.descriptionText}>
                                                {getEmployerInfo['company_location']}
                                            </div>
                                        </div>
                                    ) : ''}
                                    {getEmployerInfo['company_website'] ? (
                                        <div className={styles.companyDescription}>
                                            <div className={styles.descriptionIconWrapper}>
                                                <img
                                                    src={pcImg}
                                                    className={styles.descriptionIcon}
                                                    alt='personal computer'
                                                />
                                            </div>
                                            <div className={styles.descriptionText}>
                                                <a href={getValidUrl(getEmployerInfo['company_website'])}
                                                   className={styles.link}
                                                   target="_blank">
                                                    {getEmployerInfo['company_website']}
                                                </a>
                                            </div>
                                        </div>
                                    ) : ''}
                                    <div className={styles.companyDescription}>
                                        <div className={styles.descriptionIconWrapper}>
                                            <img
                                                src={listOfJobsImg}
                                                className={styles.descriptionIcon}
                                                alt="list of jobs"
                                            />
                                        </div>
                                        <div className={styles.descriptionText}>
                                            {getCompanyJobs.length} {locale ? locale['Job offers'] : ''}
                                        </div>
                                    </div>
                                </div>
                                {getEmployerInfo['company_description'] ? (
                                    <div className={styles.companyDescription}>
                                        <div className={styles.companyDescriptionTitle}>
                                            {locale ? locale['This is us'] : ''}
                                        </div>
                                        <p className={styles.companyDescriptionText}>
                                            {getEmployerInfo['company_description']}
                                        </p>
                                    </div>
                                ) : ''}
                                <div className={styles.closeCompanyImgWrapper} onClick={closeCompany}>
                                    <img src={closeImg} alt="close" className={styles.img}/>
                                </div>
                            </div>
                            {getEmployerInfo['company_description'] ? (
                                <div className={styles.companyDescription2}>
                                    <div className={styles.companyDescriptionTitle}>
                                        {locale ? locale['This is us'] : ''}
                                    </div>
                                    <p className={styles.companyDescriptionText}>
                                        {getEmployerInfo['company_description']}
                                    </p>
                                </div>
                            ) : ''}
                        </div>
                        <div className={styles.jobSearchByCompanyWrapper}>
                            <div className={styles.sortedWrapper}>
                                <button
                                    className={`
                                        ${styles.sortBtn}
                                        ${company_sort === 'date_posted' ? styles.active : ''}
                                    `}
                                    onClick={() => historyReplace({
                                        company_sort: 'date_posted',
                                        company_sort_method: 'asc',
                                    })}
                                >
                                    {locale ? locale['Latest'] : ''}
                                </button>
                                <button
                                    className={`
                                            ${styles.sortBtn}
                                            ${company_sort === 'title' ? styles.active : ''}
                                        `}
                                    onClick={() => historyReplace({
                                        company_sort: 'title',
                                        company_sort_method: 'asc',
                                    })}
                                >
                                    <img src={sortAZImg} alt="sort A-Z" className={styles.img}/>
                                </button>
                                <div className={styles.searchJobByCompanyJobCategoriesWrapper2}>
                                    <select
                                        name="job-categories" id="job-categories"
                                        className={styles.jobCategoriesSelect}
                                        value={company_job_category || ''}
                                        onChange={e => historyReplace({
                                            company_job_category: e.target.value,
                                        })}
                                    >
                                        <option key='all-category' value="">
                                            {locale ? locale['All the categories'] : ''}
                                        </option>
                                        {Object.entries(getEmployerInfo['company_job_categories'][0]).map(category => (
                                            <option key={category[0]} value={category[0]}>
                                                {locale ? locale[`job_category_id_${category[0]}`] : ''}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div className={styles.searchJobByCompanyJobCategoriesWrapper}>
                                <select
                                    name="job-categories" id="job-categories"
                                    className={styles.jobCategoriesSelect}
                                    value={company_job_category || ''}
                                    onChange={e => historyReplace({
                                        company_job_category: e.target.value,
                                    })}
                                >
                                    <option key='all-category' value="">
                                        {locale ? locale['All the categories'] : ''}
                                    </option>
                                    {Object.entries(getEmployerInfo['company_job_categories'][0]).map(category => (
                                        <option key={category[0]} value={category[0]}>
                                            {locale ? locale[`job_category_id_${category[0]}`] : ''}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className={styles.searchJobByCompanyForm}>
                                <label className={styles.searchJobByCompanyWrapper}>
                                    <div className={styles.searchIconWrapper}>
                                        <img src={searchImg} alt="search"
                                             className={styles.searchJobByCompanyIcon}/>
                                    </div>
                                    <input
                                        id='input-search-job-in-company'
                                        className={styles.searchJobByCompanyInput}
                                        value={company_job_title || ''}
                                        onChange={e => historyReplace({
                                            company_job_title: e.target.value,
                                        })}
                                        onBlur={() => historyReplace({
                                            company_job_title: company_job_title?.trim(),
                                        })}
                                    />
                                </label>
                                <button
                                    className={styles.clearCompanyJobFiltersBtn}
                                    onClick={clearCompanyJobFilters}
                                >
                                    {locale ? locale['Clear Filters'] : ''}
                                </button>
                            </div>
                        </div>
                    </>
                ) : (
                    <HeaderSearch
                        city={city}
                        query={query}
                        cities={cities}
                        company={company}
                        distance={distance}
                        category={category}
                        companies={companies}
                        categories={categories}
                        isNoResults={isNoResults}
                        closeCompany={closeCompany}
                        setSpaceToPlus={setSpaceToPlus}
                        setPlusToSpace={setPlusToSpace}
                        historyReplace={historyReplace}
                        getLengthEmployer={getLengthEmployer}
                        setLengthEmployer={setLengthEmployer}
                        getLengthCategory={getLengthCategory}
                        setLengthCategory={setLengthCategory}
                        getLengthLocations={getLengthLocations}
                        setLengthLocations={setLengthLocations}
                        getSelectedSearchSection={getSelectedSearchSection}
                        setSelectedSearchSection={setSelectedSearchSection}
                    />
                )}
                {isNoResults && !company ? (
                    <h3 className={styles.noJobFound}>
                        {locale ? locale['No jobs found'] : ''}
                    </h3>
                ) : ''}
                {useIsMobile(596) ? (
                    job_id ? (
                        <JobContent
                            jobId={job_id}
                            setSelectedCompany={setSelectedCompany}
                            backToJobList={() => historyReplace({
                                job_id: null
                            })}
                        />
                    ) : (
                        <div id="InfiniteScrollBlock" className={styles.leftColumnList}>
                            <div className={styles.itemBlock}>
                                {!!company && !!getFilteredCompanyJobs.length ? (
                                    JobList(getFilteredCompanyJobs)
                                ) : ''}
                                {!company && !!jobs.length && (
                                    <InfiniteScroll
                                        dataLength={jobs.length}
                                        scrollableTarget="InfiniteScrollBlock"
                                        next={() => {
                                            if (+page * +range < +maxLength) {
                                                historyReplace({
                                                    page: +page + 1,
                                                })
                                            }
                                        }}
                                        hasMore={+page * +range < +maxLength}
                                        loader={(() => {
                                            if (+page * +range < +maxLength) {
                                                return (
                                                    <h3 className={styles.flexCenter}>
                                                        {locale ? locale['Loading'] + '...' : ''}
                                                    </h3>
                                                );
                                            }
                                            return null;
                                        })()}
                                        endMessage={null}
                                    >
                                        {JobList(jobs)}
                                    </InfiniteScroll>
                                )}
                            </div>
                        </div>
                    )
                ) : (
                    <div className={`${styles.flex} ${styles.mb40}`} id='overview_content'>
                        <div id="InfiniteScrollBlock" className={styles.leftColumnList}>
                            <div className={styles.itemBlock}>
                                {!!company && !!getFilteredCompanyJobs.length && JobList(getFilteredCompanyJobs)}
                                {!company && !!jobs.length ? (
                                    <InfiniteScroll
                                        dataLength={jobs.length}
                                        scrollableTarget="InfiniteScrollBlock"
                                        next={() => {
                                            if (+page * +range < +maxLength) {
                                                historyReplace({
                                                    page: +page + 1,
                                                })
                                            }
                                        }}
                                        hasMore={+page * +range < +maxLength}
                                        loader={(() => {
                                            if (+page * +range < +maxLength) {
                                                return <h3 className={styles.flexCenter}>
                                                    {locale ? locale['Loading'] + '...' : ''}
                                                </h3>;
                                            }
                                            return null;
                                        })()}
                                        endMessage={null}
                                    >
                                        {jobs.map(job => (
                                            <div
                                                key={job.job_id}
                                                className={`${styles.item} ${job_id === job.job_id ? styles.active : ''}`}
                                                onClick={() => {
                                                    history.push(location.pathname + createLocationSearch({
                                                        ...getListLocationParams(location),
                                                        job_id: job.job_id
                                                    }))
                                                }}>
                                                {job['company_logo'] ? (
                                                    <div
                                                        className={styles.image}
                                                        onClick={() => historyReplace({
                                                            company: job.company_slug,
                                                            query: isNoResults ? null : query,
                                                            city: isNoResults ? null : city,
                                                            distance: isNoResults ? null : distance,
                                                        })}
                                                    >
                                                        <img
                                                            src={`../../../../logotypes/${job['company_logo']}`}
                                                            alt="Logo" className={styles.img}
                                                        />
                                                    </div>
                                                ) : (
                                                    <div
                                                        className={styles.image2}
                                                        onClick={() => historyReplace({
                                                            company: job.company_slug,
                                                            query: isNoResults ? null : query,
                                                            city: isNoResults ? null : city,
                                                            distance: isNoResults ? null : distance,
                                                        })}
                                                    />
                                                )}
                                                <div className={styles.description}>
                                                    <div
                                                        className={styles.titleJobItem}
                                                        title={job.title.length > 60 ? job.title : ''}
                                                    >
                                                        <span>
                                                            {kitcut(job.title, 60)}
                                                        </span>
                                                    </div>
                                                    <div className={styles.companyJobItemDescription}>
                                                        <span>
                                                            {job.company_name}
                                                        </span>
                                                    </div>
                                                    <div className={styles.companyJobItemDescription}>
                                                        <span>
                                                            {job.city}
                                                        </span>
                                                    </div>
                                                    <div className={styles.jobDescriptionWrapper}>
                                                        <div className={styles.jobInfoItem}>
                                                            <div className={styles.jobInfoIconWrapper}>
                                                                <img
                                                                    src={clockImg} alt="clock"
                                                                    className={styles.jobInfoIcon}
                                                                />
                                                            </div>
                                                            <div className={styles.jobInfoDescription}>
                                                                {getDayPasted(job['date_posted'])}
                                                            </div>
                                                        </div>
                                                        <div className={styles.jobInfoItem}>
                                                            <div className={styles.jobInfoIconWrapper}>
                                                                <img
                                                                    src={portfolioImg} alt="clock"
                                                                    className={styles.jobInfoIcon}
                                                                />
                                                            </div>
                                                            <div className={styles.jobInfoDescription}>
                                                                {dataJobCatalogs?.locations?.find(loc => loc['location_id'] === job['location_id']) &&
                                                                dataJobCatalogs?.locations?.find(loc => loc['location_id'] === job['location_id'])['location_title']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}
                                    </InfiniteScroll>
                                ) : ''}
                            </div>
                        </div>
                        {job_id ? (
                            <JobContent
                                jobId={job_id}
                                backToJobList={() => historyReplace({job_id: null})}
                                setSelectedCompany={setSelectedCompany}
                            />
                        ) : ''}
                    </div>
                )}
            </div>
            <div className={styles.separator}>
                <img src={separator} alt="separator line" className={styles.separatorImg}/>
            </div>
            <Footer className={styles.footerPosition}/>
        </div>
    );
};

export default JobSearch;
