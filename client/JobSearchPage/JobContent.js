import React, {useEffect, useState} from 'react';

import axios from "axios";
import ReactGA from "react-ga";
import * as moment from "moment";
import {NavLink} from "react-router-dom";
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {CopyToClipboard} from 'react-copy-to-clipboard';

import {showAlert} from "../store/ducks/alert";

import Modal from "../Modal/Modal";
import {formatMoney, getUserDate} from "../utils";
import {fetchJobCatalogs} from "../store/ducks/job";

import styles from './JobContent.module.scss';

import clip from "../../media/clip.png";
import linkImg from "../../media/link_bold.svg";
import clockImg from "../../media/clock.png";
import closeImg from "../../media/close.png";
import portfolioImg from "../../media/portfolio.png";

const JobContent = props => {
    const {jobId, isOverview, setSelectedCompany, backToJobList} = props;

    const history = useHistory();

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {dataJobCatalogs} = useSelector(state => state.jobState);
    const {error, userId} = useSelector(state => state.userIdState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);

    const [getLargeCV, setLargeCV] = useState(false);
    const [getJobInfo, setJobInfo] = useState(false);
    const [getSendCvValid, setSendCvValid] = useState(true);
    const [getLargeOptDoc, setLargeOptDoc] = useState(false);
    const [isClickToShare, setShareToClick] = useState(false);
    const [getOpenModalSendCV, setOpenModalSendCV] = useState(false);

    const [getCity, setCity] = useState('');
    const [getLink, setLink] = useState('');
    const [getTitle, setTitle] = useState('');
    const [getActive, setActive] = useState(false);
    const [getDateEnd, setDateEnd] = useState(null);
    const [getIsOwner, setIsOwner] = useState(false);
    const [getCategoryId, setCategoryId] = useState(null);
    const [getCurrencyId, setCurrencyId] = useState('');
    const [getDatePosted, setDatePosted] = useState(null);
    const [getLocationId, setLocationId] = useState(null);

    const [getCompanyId, setCompanyId] = useState('');
    const [getCompanySlug, setCompanySlug] = useState('');
    const [getCompanyLogo, setCompanyLogo] = useState('');
    const [getCompanyName, setCompanyName] = useState('');
    const [getCompanyWebsite, setCompanyWebsite] = useState('');

    const [getCountryCode, setCountryCode] = useState('');

    const [getBenefits, setBenefits] = useState([]);
    const [getQualifications, setQualifications] = useState([]);

    const [getSalaryTo, setSalaryTo] = useState('');
    const [getSalaryMode, setSalaryMode] = useState(null);
    const [getSalaryFrom, setSalaryFrom] = useState('');
    const [getSalaryGross, setSalaryGross] = useState('');
    const [getSalaryPeriodId, setSalaryPeriodId] = useState('');

    const [getDescription, setDescription] = useState('');
    const [getFinalDescription, setFinalDescription] = useState('');
    const [getStartDescription, setStartDescription] = useState('');

    const [getCandidateName, setCandidateName] = useState('');
    const [getCandidateEmail, setCandidateEmail] = useState('');
    const [getCandidateAgree, setCandidateAgree] = useState(false);
    const [getCandidateSurname, setCandidateSurname] = useState('');
    const [getCandidateSocialLink, setCandidateSocialLink] = useState('');
    const [getCandidateCoverLetter, setCandidateCoverLetter] = useState(null);
    const [getCandidateOptionalDocument, setCandidateOptionalDocument] = useState(null);

    const [getCopyTimeOutId, setCopyTimeOutId] = useState(null);
    const [salaryRow, setSalaryRow] = useState('');

    const getItem = str => `${'<'}${'li'}${'>'}${str}${'<'}${'/li'}${'>'}`;
    const getData = (str) => {
        try {
            let array = JSON.parse(str);
            if (typeof array === "string") {
                array = JSON.parse(array)
            }

            if (array) {
                return (`${'<'}${'ul'}${'>'}${array.map(getItem)}${'<'}${'/ul'}${'>'}`
                        .replace(/<\/li>,<li>/g, `${'<'}${'/li'}${'>'}${'<'}${'li'}${'>'}`)
                );
            }
        } catch (e) {
        }

        return str;
    }

    const getDayPasted = postedDate => {
        const days = moment().diff(postedDate, 'days');
        if (days) {
            if (locale && locale['lang'] === 'en') {
                if (days === 1) {
                    return `${days} Day ago`;
                } else {
                    return `${days} Days ago`;
                }
            } else {
                if (days === 1) {
                    return `vor ${days} Tag`;
                } else {
                    return `vor ${days} Tagen`;
                }
            }
        } else {
            return locale ? locale['Today'] : '';
        }
    };

    const closeModal = () => {
        setSendCvValid(true);
        setOpenModalSendCV(false);
        setCandidateCoverLetter(null);
        setCandidateOptionalDocument(null);
    };

    const clickOnApply = () => {
        if (getIsOwner) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'share', job_id: jobId},
                headers: {'Cache-Control': 'no-cache'}
            }).then();
        }
        setOpenModalSendCV(true);
    }

    const countJobShare = () => {
        if (getCopyTimeOutId) {
            window.clearTimeout(getCopyTimeOutId);
        }

        setCopyTimeOutId(window.setTimeout(() => setShareToClick(false), 3000));
        setShareToClick(true);

        if (getIsOwner) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'share', job_id: jobId},
                headers: {'Cache-Control': 'no-cache'}
            }).then();
        }
    }

    const countJobConverse = () => {
        if (getIsOwner) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'converse', job_id: jobId},
                headers: {'Cache-Control': 'no-cache'}
            }).then().catch();

            ReactGA.event({
                label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                action: 'click_apply',
                category: 'Apply'
            });
        }
    }

    const countJobRedirect = () => {
        if (getIsOwner) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'redirect_to_website', job_id: jobId},
                headers: {'Cache-Control': 'no-cache'}
            }).then().catch()

            ReactGA.event({
                label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                action: 'click_go_to_website',
                category: 'Apply'
            });
        }
    }

    const sendCV = e => {
        if (!getCandidateAgree || !getCandidateEmail || !getCandidateName || !getCandidateCoverLetter || !getCandidateSurname) {
            setSendCvValid(false);
            e.preventDefault();
            return;
        }
        setOpenModalSendCV(false);

        const data = new FormData();
        data.append('model', 'job');
        data.append('mode', 'send_contacts');
        data.append('enctype', 'multipart/form-data');

        data.append('company_id', getCompanyId);
        data.append('email', getCandidateEmail);
        data.append('first_name', getCandidateName);
        data.append('last_name', getCandidateSurname);
        data.append('linkedin', getCandidateSocialLink);
        data.append('document1', getCandidateCoverLetter);
        data.append('document2', getCandidateOptionalDocument);

        axios.post('/ajax.php', data, {})
            .then(({data}) => {
                if (data.data) {
                    countJobConverse()
                    dispatch(showAlert('success', 'Cover letter sent successfully'));
                    setSendCvValid(true);
                } else {
                    dispatch(showAlert('failure', 'Error sending cover letter'));
                }
            })
            .catch(err => console.error(err));
    };

    const getSalaryInfo = () => {
        const getPeriodTitleInLocale = period => {
            switch (period) {
                case 'hour':
                    return locale ? locale['Hour'] : '';
                case 'day':
                    return locale ? locale['Day'] : '';
                case 'week':
                    return locale ? locale['Week'] : '';
                case 'month':
                    return locale ? locale['Month'] : '';
                case 'year':
                    return locale ? locale['Year'] : '';
                default:
                    return '';
            }
        };

        const salary = locale ? locale['Salary'] : '';

        let currencyTitle = '';
        let periodTitle = '';

        if (getSalaryMode === '2' && !getSalaryFrom && !getSalaryTo) {
            return '';
        }

        if (getSalaryMode === '1' && !getSalaryGross) {
            return '';
        }

        if (dataJobCatalogs?.currencies && dataJobCatalogs.currencies.length && getCurrencyId) {
            const currency = dataJobCatalogs.currencies.find(currency => currency['currency_id'] === getCurrencyId);
            if (currency) {
                currencyTitle = currency['currency_code'];
            }
        }

        if (dataJobCatalogs?.salary_periods && dataJobCatalogs.salary_periods.length && getSalaryPeriodId) {
            const period = dataJobCatalogs.salary_periods.find(period => period['period_id'] === getSalaryPeriodId);
            if (period) {
                periodTitle = period['period_title'];
            }
        }

        if (currencyTitle && locale) {
            return (
                <div className={styles.jobInfoOverview}>
                    <span className={styles.jobInfoOverviewBold}>{salary}{': '}</span>
                    {getSalaryMode === '2' ? (
                        formatMoney(getSalaryFrom, locale['lang']) + currencyTitle
                        + ' - ' + formatMoney(getSalaryTo, locale['lang']) + currencyTitle
                    ) : (
                        formatMoney(getSalaryGross, locale['lang']) + currencyTitle
                    )}
                    {' / ' + getPeriodTitleInLocale(periodTitle)}
                </div>
            );
        } else {
            return (
                <div className={styles.jobInfoOverview}>
                    <span className={styles.jobInfoOverviewBold}>
                        {salary}{': '}
                    </span>
                    {getSalaryMode === '2' ? (
                        getSalaryFrom + ' - ' + getSalaryTo + ' ' + currencyTitle
                    ) : (
                        getSalaryGross + ' ' + currencyTitle
                    )}
                    {' / ' + getPeriodTitleInLocale(periodTitle)}
                </div>
            );
        }
    };

    const getValidUrl = () => {
        if (getLink) {
            return getLink;
        }
        if (getCompanyWebsite) {
            return getCompanyWebsite;
        }
        return null;
    }

    useEffect(() => {
        if (locale && dataJobCatalogs && getSalaryMode) {
            setSalaryRow(getSalaryInfo());
        }
    }, [
        locale,
        getSalaryTo,
        getSalaryMode,
        getSalaryFrom,
        dataJobCatalogs,
        getSalaryGross,
        getSalaryPeriodId
    ]);

    useEffect(() => {
        dispatch(fetchJobCatalogs());
    }, []);

    useEffect(() => {
        if (jobId) {
            setShareToClick(false);

            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'get_job', job_id: jobId},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        if ((+userId !== +data.data.user_id) || error) {
                            axios.get('/ajax.php', {
                                params: {model: 'job', mode: 'view', job_id: jobId},
                                headers: {'Cache-Control': 'no-cache'}
                            }).then();
                        }

                        setIsOwner((+userId !== +data.data.user_id) || !!error)

                        setJobInfo(true);
                        data.data['city'] ? setCity(data.data['city']) : setCity('');
                        data.data['link'] ? setLink(data.data['link']) : setLink('');
                        data.data['title'] ? setTitle(data.data['title']) : setTitle('');
                        +data.data['active'] ? setActive(true) : setActive(false);
                        data.data['date_end'] ? setDateEnd(data.data['date_end'] * 1000) : setDateEnd(null);
                        data.data['date_posted'] ? setDatePosted(getUserDate(data.data['date_posted']).getTime()) : setDatePosted(null);
                        data.data['category_id'] ? setCategoryId(data.data['category_id']) : setCategoryId('');
                        data.data['location_id'] ? setLocationId(data.data['location_id']) : setLocationId('');
                        data.data['currency_id'] ? setCurrencyId(data.data['currency_id']) : setCurrencyId('');
                        data.data['salary_mode'] ? setSalaryMode(data.data['salary_mode']) : setSalaryMode('');
                        data.data['description'] ? setDescription(data.data['description']) : setDescription('');
                        data.data['country_code'] ? setCountryCode(data.data['country_code']) : setCountryCode('');

                        data.data['company_id'] ? setCompanyId(data.data['company_id']) : setCompanyId('');
                        data.data['company_logo'] ? setCompanyLogo(data.data['company_logo']) : setCompanyLogo('');
                        data.data['company_name'] ? setCompanyName(data.data['company_name']) : setCompanyName('');
                        data.data['company_slug'] ? setCompanySlug(data.data['company_slug']) : setCompanySlug('');
                        data.data['company_website'] ? setCompanyWebsite(data.data['company_website']) : setCompanyWebsite('');

                        setBenefits(data.data['benefits'] ? getData(data.data['benefits']) : '');
                        setQualifications(data.data['qualifications'] ? getData(data.data['qualifications']) : '');

                        data.data['final_description']
                            ? setFinalDescription(data.data['final_description'])
                            : setFinalDescription('');
                        data.data['start_description']
                            ? setStartDescription(data.data['start_description'])
                            : setStartDescription('');
                        data.data['salary_period_id']
                            ? setSalaryPeriodId(data.data['salary_period_id'])
                            : setSalaryPeriodId('');
                        data.data['salary_to'] && data.data['salary_to'] !== '0.00'
                            ? setSalaryTo(data.data['salary_to'])
                            : setSalaryTo('');
                        data.data['salary_from'] && data.data['salary_from'] !== '0.00'
                            ? setSalaryFrom(data.data['salary_from'])
                            : setSalaryFrom('');
                        data.data['salary_gross'] && data.data['salary_gross'] !== '0.00'
                            ? setSalaryGross(data.data['salary_gross'])
                            : setSalaryGross('');
                    } else {
                        console.error(data);
                        dispatch(showAlert('failure', 'job not found'));
                        history.push('/search');
                    }
                })
                .catch(err => console.error(err));
        }
    }, [jobId]);

    useEffect(() => {
        if (getCandidateCoverLetter && getCandidateCoverLetter.size > 20971520) {
            setLargeCV(true);
            setCandidateCoverLetter(null);
        }

        if (getCandidateOptionalDocument && getCandidateOptionalDocument.size > 20971520) {
            setLargeOptDoc(true);
            setCandidateOptionalDocument(null);
        }
    }, [getCandidateCoverLetter, getCandidateOptionalDocument]);

    return getJobInfo ? (
        <div className={styles.jobOverviewWrapper}>
            <div className={styles.jobOverview}>
                <div className={styles.mainTitleWrapper}>
                    <h2 className={styles.jobOverviewTitle}>
                    <span>
                        {getTitle}
                    </span>
                    </h2>
                    <div className={styles.jobControllerWrapper}>
                        {isClickToShare ? (
                            <span className={styles.copiedMsg}>
                                {locale ? locale['Link copied'] : ''}
                            </span>
                        ) : null}
                        <CopyToClipboard text={`http://smarketer.jobs/overview?job_id=${jobId}`} onCopy={countJobShare}>
                            <img src={linkImg} width='20px' height='20px' alt="share" className={styles.pointer}/>
                        </CopyToClipboard>
                        <div className={styles.closeJobWrapper} onClick={backToJobList}>
                            <img src={closeImg} alt="close" className={styles.img}/>
                        </div>
                    </div>
                </div>
                <div className={styles.jobOverviewWrapperMain}>
                    <div className={styles.companyInfo}>
                        {getCompanyLogo ? (
                            <div className={styles.companyContentLogoWrapper}>
                                <img
                                    src={`../../../../logotypes/${getCompanyLogo}`}
                                    alt="Logo"
                                    className={styles.companyContentLogo}
                                />
                            </div>
                        ) : (
                            <div className={styles.companyContentEmptyLogo}/>
                        )}
                        <div className={styles.jobOverviewCompanyDescription}>
                            <div className={styles.jobDescriptionWrapper}>
                                <div className={styles.jobOverviewCompanyName}>
                                    {getCompanyName}
                                </div>
                                <div className={styles.jobOverviewCompanyName}>
                                    {getCity}
                                </div>
                                <div className={styles.flex}>
                                    <div className={styles.jobInfoItem}>
                                        <div className={styles.jobInfoIconWrapper}>
                                            <img src={clockImg} alt="clock" className={styles.jobInfoIcon}/>
                                        </div>
                                        <div className={styles.jobInfoDescription}>
                                            {getDayPasted(getDatePosted)}
                                        </div>
                                    </div>
                                    <div className={styles.jobInfoItem}>
                                        <div className={styles.jobInfoIconWrapper}>
                                            <img src={portfolioImg} alt="clock" className={styles.jobInfoIcon}/>
                                        </div>
                                        <div className={styles.jobInfoDescription}>
                                            {
                                                dataJobCatalogs?.locations
                                                && getLocationId
                                                && dataJobCatalogs.locations.find(loc => loc['location_id'] === getLocationId)
                                                && dataJobCatalogs?.locations.find(loc => loc['location_id'] === getLocationId)['location_title']
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles.jobInfoOverviewWrapper}>
                        <div className={styles.jobInfoOverview}>
                            <span className={styles.jobInfoOverviewBold}>
                                {locale ? locale['Category'] : ''}{': '}
                            </span>
                            {locale ? locale[`job_category_id_${getCategoryId}`] : ''}
                        </div>
                        <div className={styles.jobInfoOverview}>
                            <span className={styles.jobInfoOverviewBold}>
                                {locale ? locale['Application time'] : ''}{': '}
                            </span>
                            {getDateEnd ? moment(new Date(getDateEnd)).format("DD.MM.YYYY") : ''}
                        </div>
                        {getSalaryMode === '3' ? null : salaryRow}
                    </div>
                </div>
                <div className={styles.delimiter}/>
                <div className={styles.btnAcceptBlock}>
                    <button className={styles.btnApplyJob} type='button' onClick={clickOnApply}>
                        {locale ? locale['Apply'] : ''}
                    </button>
                    {getValidUrl() ? (
                        <a href={getValidUrl()} target="_blank" id='go_to_website_top_btn' onClick={countJobRedirect}
                           className={`${styles.buttonLight} ${styles.mr15}`}>
                            {locale ? locale['Go to website'] : ''}
                        </a>
                    ) : null}
                    {getActive && getCompanyName ? (
                        <span className={`${styles.selectAllCompanyJobs} ${styles.mr15}`}>
                            <span
                                className={styles.text}
                                onClick={() => {
                                    if (isOverview) {
                                        history.push(`/job-search?country=${getCountryCode}&company=${getCompanySlug}`);
                                    } else {
                                        setSelectedCompany(getCompanySlug);
                                    }
                                }}
                            >
                                {locale ? locale['More jobs at'] : ''}{'  '}
                                <strong>{getCompanyName}</strong>
                            </span>
                        </span>
                    ) : (
                        <span/>
                    )}
                </div>
                <div className={styles.delimiter}/>
                <div className={styles.jobOverviewDescriptionWrapper}>
                    {getStartDescription ? <div dangerouslySetInnerHTML={{__html: getStartDescription}}/> : ''}
                    {getDescription ? <div dangerouslySetInnerHTML={{__html: getDescription}}/> : ''}
                    {getQualifications ? (
                        <>
                            <div className={styles.delimiter}/>
                            <h3 className={styles.jobOverviewDescriptionTitle}>{locale ? locale['Qualifications'] : ''}</h3>
                            <div dangerouslySetInnerHTML={{__html: getQualifications}}/>
                        </>
                    ) : ''}
                    {getBenefits ? (
                        <>
                            <div className={styles.delimiter}/>
                            <h3 className={styles.jobOverviewDescriptionTitle}>{locale ? locale['Benefits'] : ''}</h3>
                            <div dangerouslySetInnerHTML={{__html: getBenefits}}/>
                        </>
                    ) : ''}
                    {getFinalDescription ? <div dangerouslySetInnerHTML={{__html: getFinalDescription}}/> : ''}
                    <div className={styles.delimiter}/>
                    {getValidUrl() ? (
                        <a href={getValidUrl()} target="_blank" id='go_to_website_bottom_btn'
                           className={`${styles.buttonLight} ${styles.mb15}`}
                           onClick={countJobRedirect}>
                            {locale ? locale['Go to website'] : ''}
                        </a>
                    ) : ''}
                    <button className={styles.btnApplyJobLarge} type='button' onClick={clickOnApply}>
                        {locale ? locale['Apply'] : ''}
                    </button>
                </div>
            </div>
            <Modal onRequestClose={closeModal} ariaHideApp={false} isOpen={getOpenModalSendCV} isMainModal={true}>
                <div className={styles.modalSendCV}>
                    <div className={styles.modalSendCVHeader}>
                        {getCompanyLogo ? <div className={styles.modalSendCVHeaderLogoWrapper}>
                            <img src={`../../../../logotypes/${getCompanyLogo}`}
                                 alt="Logo" className={styles.modalSendCVHeaderLogo}/>
                        </div> : ''}
                        <div className={styles.modalSendCVHeaderTitle}>
                            <div>{locale ? locale['Apply directly to'] : ''}</div>
                            {getCompanyName ?
                                <div className={styles.modalSendCVHeaderTitleBold}>{getCompanyName}</div> : ''}
                            <div>{locale ? locale['send'] : ''}</div>
                        </div>
                        <div className={styles.closeCompanyImgWrapper} onClick={closeModal}>
                            <img src={closeImg} alt="close" className={styles.img}/>
                        </div>
                    </div>
                    <form onSubmit={e => sendCV(e)} className={styles.modalSendCVForm}>
                        <label className={styles.modalSendCVInputWrapper}>
                            <span
                                className={`
                                    ${styles.modalSendCVLabel}
                                    ${!getCandidateEmail && !getSendCvValid ? styles.warn : ''}
                                `}
                            >
                                {locale ? locale['E-Mail'] : ''}*
                            </span>
                            <input type="email" name="email" placeholder="name@companyname.de"
                                   className={`${styles.modalSendCVInput} ${!getCandidateEmail && !getSendCvValid ? styles.warn : ''}`}
                                   value={getCandidateEmail}
                                   onChange={e => setCandidateEmail(e.target.value)}
                                   onBlur={() => setCandidateEmail(getCandidateEmail.trim())}
                            />
                        </label>
                        <label className={styles.modalSendCVInputWrapper}>
                            <span
                                className={`${styles.modalSendCVLabel} ${!getCandidateName && !getSendCvValid ? styles.warn : ''}`}>
                                {locale ? locale['First name'] : ''}*
                            </span>
                            <input type="text" name="first_name"
                                   placeholder={locale ? locale['First name'] : ''}
                                   className={`${styles.modalSendCVInput} ${!getCandidateName && !getSendCvValid ? styles.warn : ''}`}
                                   value={getCandidateName}
                                   onBlur={() => setCandidateName(getCandidateName.trim())}
                                   onChange={e => setCandidateName(e.target.value)}/>
                        </label>
                        <label className={styles.modalSendCVInputWrapper}>
                            <span
                                className={`${styles.modalSendCVLabel} ${!getCandidateSurname && !getSendCvValid ? styles.warn : ''}`}>
                                {locale ? locale['Surname'] : ''}*
                            </span>
                            <input type="text" name="last_name"
                                   placeholder={locale ? locale['Surname'] : ''}
                                   className={`${styles.modalSendCVInput} ${!getCandidateSurname && !getSendCvValid ? styles.warn : ''}`}
                                   value={getCandidateSurname}
                                   onBlur={() => setCandidateSurname(getCandidateSurname.trim())}
                                   onChange={e => setCandidateSurname(e.target.value)}/>
                        </label>
                        <label className={styles.modalSendCVInputWrapper}>
                            <span className={styles.modalSendCVLabel}>
                                {locale ? locale['LinkedIn'] : ''}
                            </span>
                            <input type="text" name="linkedin"
                                   placeholder="https://www.linkedin.com/in/vasiliy-bonner"
                                   className={styles.modalSendCVInput}
                                   value={getCandidateSocialLink}
                                   onBlur={() => setCandidateSocialLink(getCandidateSocialLink.trim())}
                                   onChange={e => setCandidateSocialLink(e.target.value)}/>
                        </label>
                        <div className={styles.modalSendCVInputFileWrapper}>
                            <div className={styles.modalSendCVInputFileLabel}>
                                <div
                                    className={`${styles.modalSendCVLabel} ${!getCandidateCoverLetter && !getSendCvValid ? styles.warn : ''}`}>
                                    {locale ? locale['Document'] : ''}*
                                </div>
                                <div className={`${styles.modalSendCVLabel} ${styles.modalSendCVLabelLight}`}>
                                    .pdf, .docx, .rtf, .txt
                                </div>
                            </div>
                            <div className={styles.modalSendCVFileLabelWrapper}>
                                {getCandidateCoverLetter ?
                                    <div className={styles.documentName}>
                                        <span>{getCandidateCoverLetter.name}</span>
                                        <div className={styles.closeWrapper}
                                             onClick={() => setCandidateCoverLetter(null)}>
                                            <img src={closeImg} alt="close" className={styles.img}/>
                                        </div>
                                    </div>
                                    : ''
                                }
                                <label
                                    className={`${styles.modalSendCVFileLabel} ${!getCandidateCoverLetter && !getSendCvValid ? styles.warn : ''}`}>
                                    <div className={styles.modalSendCVFileLabelClipWrapper}>
                                        <img src={clip} alt="clip" className={styles.modalSendCVFileLabelClip}/>
                                    </div>
                                    <span className={styles.modalSendCVLabel}>CV</span>
                                    <input type="file" name="document1" className={styles.modalSendCVInputFile}
                                           accept=".pdf, .docx, .rtf, .txt"
                                           onChange={e => setCandidateCoverLetter(e.target.files[0])}/>
                                </label>
                                {getCandidateCoverLetter
                                    ? <div className={`${styles.uploadSuccess} ${styles.uploadSuccessAligns}`}>
                                        {locale ? locale['CV document successfully uploaded'] : ''}
                                    </div>
                                    : ''
                                }
                                {!getCandidateCoverLetter && getLargeCV
                                    ? <div className={`${styles.warn} ${styles.uploadSuccessAligns}`}>
                                        {locale ? locale['The uploaded file exceeds the maximum size'] : ''}
                                    </div>
                                    : ''
                                }
                                {getCandidateOptionalDocument ?
                                    <div className={styles.documentName}>
                                        <span>{getCandidateOptionalDocument.name}</span>
                                        <div className={styles.closeWrapper}
                                             onClick={() => setCandidateOptionalDocument(null)}>
                                            <img src={closeImg} alt="close" className={styles.img}/>
                                        </div>
                                    </div>
                                    : ''
                                }
                                <label className={styles.modalSendCVFileLabel}>
                                    <div className={styles.modalSendCVFileLabelClipWrapper}>
                                        <img src={clip} alt="clip" className={styles.modalSendCVFileLabelClip}/>
                                    </div>
                                    <span className={styles.modalSendCVLabel}>
                                        {locale ? locale['other documents'] : ''}
                                    </span>
                                    {' '}
                                    <span className={styles.modalSendCVLabelLight}>
                                        ({locale ? locale['optional'] : ''})
                                    </span>
                                    <input type="file" name="document2" className={styles.modalSendCVInputFile}
                                           accept=".pdf, .docx, .rtf, .txt"
                                           onChange={e => setCandidateOptionalDocument(e.target.files[0])}/>
                                </label>
                                {getCandidateOptionalDocument
                                    ? <div className={`${styles.uploadSuccess} ${styles.uploadSuccessAligns}`}>
                                        {locale ? locale['Additional document uploaded successfully'] : ''}
                                    </div>
                                    : ''
                                }
                                {!getCandidateOptionalDocument && getLargeOptDoc
                                    ? <div className={`${styles.warn} ${styles.uploadSuccessAligns}`}>
                                        {locale ? locale['The uploaded file exceeds the maximum size'] : ''}
                                    </div>
                                    : ''
                                }
                            </div>
                        </div>
                        <div className={styles.modalSendCVInputWrapper}>
                            <div className={styles.modalSendCVLabel}/>
                            <label
                                className={`${styles.modalSendCVCheckboxWrapper} ${!getCandidateAgree && !getSendCvValid ? styles.warnBorder : ''}`}>
                                <input
                                    type="checkbox"
                                    checked={getCandidateAgree}
                                    className={styles.modalSendCVCheckbox}
                                    onChange={() => setCandidateAgree(!getCandidateAgree)}
                                />
                                <span>
                                    {locale ? locale['I agree to the'] : ''}
                                    {' '}
                                    <a href={locale && locale.lang === 'de'
                                        ? 'https://www.smarketer.de/agb-smarketer-jobs/'
                                        : 'https://www.smarketer.de/general-terms-and-conditions-smarketer-jobs/'}
                                       target="_blank"
                                       className={styles.link}>
                                        {locale ? locale['terms and conditions'] : ''}
                                    </a>
                                    {' '}
                                    {locale ? locale['and the'] : ''}
                                    {' '}
                                    <NavLink to={`/privacy-${locale ? locale['lang'] : 'en'}`}
                                             className={styles.link} target="_blank">
                                        {locale ? locale['privacy policy'] : ''}
                                    </NavLink>
                                    {locale ? locale['lang'] === 'de' ? ' zu' : '' : ''}
                                </span>
                            </label>
                        </div>
                        <div className={styles.modalSendCVInputWrapper}>
                            <div className={styles.modalSendCVLabel}/>
                            <button type="submit" id='apply_cv' className={styles.modalSendCVBtn}>
                                {locale ? locale['Apply now'] : ''}
                            </button>
                        </div>
                    </form>
                </div>
            </Modal>
        </div>
    ) : '';
};

export default JobContent;
