import React from "react";
import {useSelector} from "react-redux";

import styles from "./JobSearch.module.scss";

export const HeaderSearch = (
    {
        city,
        query,
        cities,
        company,
        distance,
        category,
        companies,
        categories,
        isNoResults,
        closeCompany,
        setSpaceToPlus,
        setPlusToSpace,
        historyReplace,
        getLengthEmployer,
        setLengthEmployer,
        getLengthCategory,
        setLengthCategory,
        getLengthLocations,
        setLengthLocations,
        getSelectedSearchSection,
        setSelectedSearchSection,
    }
) => {
    const {locale} = useSelector(state => state.localeState);

    return (
        <div className={styles.headerSearch}>
            <div className={styles.sortedWrapper}>
                <button
                    onClick={() => setSelectedSearchSection('category')}
                    className={`${styles.sortButton} ${getSelectedSearchSection === 'category' ? styles.active : ''}`}
                >
                    {locale ? locale['Category'] : ''}
                </button>
                <button
                    onClick={() => setSelectedSearchSection('location')}
                    className={`${styles.sortButton} ${getSelectedSearchSection === 'location' ? styles.active : ''}`}
                >
                    {locale ? locale['Location'] : ''}
                </button>
                <button
                    onClick={() => setSelectedSearchSection('employer')}
                    className={`${styles.sortButton} ${getSelectedSearchSection === 'employer' ? styles.active : ''}`}
                >
                    {locale ? locale['Employer'] : ''}
                </button>
            </div>
            {getSelectedSearchSection === 'category' ? (
                <div className={styles.filtersWrapper}>
                    <div className={styles.distanceBtnWrapper}>
                        <button
                            onClick={() => historyReplace({
                                category: null,
                            })}
                            className={`${styles.btnRounded} ${!category ? styles.active : ''}`}
                        >
                            {locale ? locale['All'] : ''}
                        </button>
                        {categories
                            .sort((prev, next) => +prev['category_id'] < +next['category_id'] ? -1 : 1)
                            .map(({category_id, category_slug}, i) => {
                                if (i <= getLengthCategory) {
                                    return (
                                        <button
                                            key={category_id}
                                            className={`
                                            ${styles.btnRounded} 
                                            ${category === category_slug ? styles.active : ''}
                                        `}
                                            onClick={() => historyReplace({
                                                category: category_slug,
                                                query: isNoResults ? null : query,
                                                city: isNoResults ? null : city,
                                                distance: isNoResults ? null : distance,
                                            })}
                                        >
                                            {locale ? locale[`job_category_id_${category_id}`] : ''}
                                        </button>
                                    );
                                }
                            })
                        }
                        {categories.length - 1 > getLengthCategory ? (
                            <button
                                className={styles.btnRounded}
                                onClick={() => setLengthCategory(getLengthCategory + 10)}
                            >
                                {locale ? locale['More'] : ''}...
                            </button>
                        ) : ''}
                    </div>
                </div>
            ) : ''}
            {getSelectedSearchSection === 'location' ? (
                <div className={styles.filtersWrapper}>
                    <div className={styles.distanceBtnWrapper}>
                        <button
                            className={`${styles.btnRounded} ${!city ? styles.active : ''}`}
                            onClick={() => historyReplace({
                                city: null,
                                distance: null,
                            })}
                        >
                            {locale ? locale['All'] : ''}
                        </button>
                        {cities
                            .sort((prev, next) => prev.id < next.id ? -1 : 1)
                            .map(({id, title}, i) => {
                                if (i <= getLengthLocations) {
                                    return (
                                        <button
                                            key={id}
                                            onClick={() => historyReplace({
                                                city: setSpaceToPlus(title),
                                                distance: distance || 50,
                                                query: isNoResults ? null : query,
                                            })}
                                            className={`
                                                ${styles.btnRounded}
                                                ${setPlusToSpace(city)
                                                .toLowerCase() === title.toLowerCase() ? styles.active : ''}
                                            `}
                                        >
                                            {title}
                                        </button>
                                    );
                                }
                            })
                        }
                        {cities.length - 1 > getLengthLocations ? (
                            <button
                                className={styles.btnRounded}
                                onClick={() => setLengthLocations(getLengthLocations + 10)}
                            >
                                {locale ? locale['More'] : ''}...
                            </button>
                        ) : ''}
                    </div>
                    {city ? (
                        <div className={`${styles.distanceBtnWrapper} ${styles.w450}`}>
                            <button
                                className={`${styles.btnSquare} ${distance === '2' ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    distance: '2',
                                    query: isNoResults ? null : query,
                                })}
                            >
                                2 km
                            </button>
                            <button
                                className={`${styles.btnSquare} ${distance === '10' ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    distance: '10',
                                    query: isNoResults ? null : query,
                                })}
                            >
                                10 km
                            </button>
                            <button
                                className={`${styles.btnSquare} ${distance === '25' ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    distance: '25',
                                    query: isNoResults ? null : query,
                                })}
                            >
                                25 km
                            </button>
                            <button
                                className={`${styles.btnSquare} ${distance === '50' ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    distance: '50',
                                    query: isNoResults ? null : query,
                                })}
                            >
                                50 km
                            </button>
                            <button
                                className={`${styles.btnSquare} ${distance === '100' ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    distance: '100',
                                    query: isNoResults ? null : query,
                                })}
                            >
                                100 km
                            </button>
                            <button
                                className={`${styles.btnSquare} ${distance === '300' ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    distance: '300',
                                    query: isNoResults ? null : query,
                                })}
                            >
                                300 km
                            </button>
                            <button
                                className={`${styles.btnSquare} ${!distance ? styles.active : ''}`}
                                onClick={() => historyReplace({
                                    city: null,
                                    distance: null,
                                })}
                            >
                                {locale ? locale['All over'] : ''}
                            </button>
                        </div>
                    ) : ''}
                </div>
            ) : ''}
            {getSelectedSearchSection === 'employer' ? (
                <div className={styles.filtersWrapper}>
                    <div className={styles.distanceBtnWrapper}>
                        <button
                            className={`${styles.btnRounded} ${!company ? styles.active : ''}`}
                            onClick={closeCompany}
                        >
                            {locale ? locale['All'] : ''}
                        </button>
                        {companies
                            .sort((prev, next) => prev.company_id < next.company_id ? -1 : 1)
                            .map(({company_id, company_name, company_slug}, i) => {
                                if (i <= getLengthEmployer) {
                                    return (
                                        <button
                                            key={company_id}
                                            onClick={() => historyReplace({
                                                company: company_slug,
                                                query: isNoResults ? null : query,
                                                city: isNoResults ? null : city,
                                                distance: isNoResults ? null : distance,
                                            })}
                                            className={
                                                `${styles.btnRounded}
                                                 ${company === company_slug ? styles.active : ''}
                                            `}
                                        >
                                            {company_name}
                                        </button>
                                    );
                                }
                            })
                        }
                        {companies.length - 1 > getLengthEmployer ? (
                            <button
                                className={styles.btnRounded}
                                onClick={() => setLengthEmployer(getLengthEmployer + 10)}
                            >
                                {locale ? locale['More'] : ''}...
                            </button>
                        ) : ''}
                    </div>
                </div>
            ) : ''}
        </div>
    );
};