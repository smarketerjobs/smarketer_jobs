export const planPrice = {
    starter: {
        name: 'Starter',
        montPrice: 0,
    },
    basic: {
        name: 'Basic',
        monthPrice: 16,
        yearPrice: 19,
    },
    plus: {
        name: 'Plus',
        monthPrice: 24,
        yearPrice: 29,
    },
    professional: {
        name: 'Professional',
        monthPrice: 415,
        yearPrice: 499,
    }
}
