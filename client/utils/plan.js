export const getPlanTitle = planTitle => {
    switch (planTitle) {
        case 'starter':
            return 'Starter';
        case 'professional_year':
            return 'Professional year';
        case 'professional_month':
            return 'Professional month';
        case 'plus_year':
            return 'Plus year';
        case 'plus_month':
            return 'Plus month';
        case 'one_day':
            return 'One day';
        case 'basic_year':
            return 'Basic year';
        case 'basic_month':
            return 'Basic month';
        default:
            return '';
    }
};