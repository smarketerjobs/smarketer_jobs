export const createLocationSearch = (params, options) => {
    const {exclude, only} = options || {};

    function getValue(param) {
        if (Array.isArray(param)) {
            const data = param[0];

            if (typeof data === 'object' && data.value) {
                return param.map(({value}) => value).join(',');
            }

            return param.join(',');
        }

        if (typeof param === 'string' || typeof param === 'number') {
            return param.toString();
        }

        return JSON.stringify(param);
    }

    let list = Object.keys(params);

    if (only && only.length) {
        list = list.filter((name) => only.indexOf(name) !== -1);
    }

    let newPath = '?';
    list.forEach((key) => {
        const param = params[key];
        const result = getValue(param);

        if (result && result !== '[]' && result !== '{}' && result !== 'null'
            && (!exclude || (result !== '0,0' && getValue(exclude[key]) !== result))
        ) {
            newPath += `${key}=${encodeURIComponent(getValue(param).replace(/%(?![0-9][0-9a-fA-F]+)/g, '%25'))}&`;
        }
    });
    newPath = newPath.slice(0, newPath.length - 1);

    return newPath;
};

export const getListLocationParams = ({search}) => {
    if (!search) {
        return {};
    }

    const decodeSearch = decodeURI(search);

    if (decodeSearch && !decodeSearch.indexOf('?')) {
        return decodeSearch
            .slice(1)
            .split('&')
            .map((value) => value.split('='))
            .reduce(
                (accumulator, [key, value]) => {
                    const str = decodeURIComponent(value);
                    const list = str.split(',');

                    return Object.assign(accumulator, {
                        [key.toLowerCase()]: list.filter((v) => v !== '').length !== list.length ? [str] : list,
                    });
                },
                {},
            );
    }

    return {};
};

export const kitcut = (text, limit) => {
    if (text.trim().length <= limit) return text;

    text = text.slice(0, limit);
    const lastSpace = text.lastIndexOf(" ");

    if (lastSpace > 0) {
        text = text.substr(0, lastSpace);
    }
    return text + "...";
};

export const formatMoney = (amount, lang) => {
    if (amount) {
        return (+amount).toLocaleString(lang, {maximumFractionDigits: 2, minimumFractionDigits: 2});
    }
    return ''
}

export function parseMoney(str) {
    return str.replace(/(\.00|,00)$/g, '').replace(/\D/g, '');
}

const urlR = /^(ftp|http|https):\/\/[^ "]+$/;
export const validateUrl = (url) => urlR.test(url);

export const getUserDate = sec => new Date(
    sec * 1000
    + new Date().getUTCHours() * 60 * 60 * 1000
    + new Date().getUTCMinutes() * 60 * 1000
    + new Date().getTimezoneOffset() * 60000
);
