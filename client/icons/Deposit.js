import React from 'react';

const Deposit = (props) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 34 34" style={{...props.styling}}>
            <g id="prefix__deposit" transform="translate(-2 -2)">
                <g id="prefix__Gruppe_8896" data-name="Gruppe 8896" transform="translate(5.4 2)">
                    <path id="prefix__Pfad_10674" d="M35.2 13.333v-2.29L21.6 2 8 11.043v2.29z" className="prefix__cls-1"
                          style={{fill: '#edeff1'}}
                          data-name="Pfad 10674" transform="translate(-8 -2)"/>
                    <path id="prefix__Pfad_10675" d="M10 22h5.667v1.7H10z" className="prefix__cls-1"
                          style={{fill: '#edeff1'}}
                          data-name="Pfad 10675" transform="translate(-8.867 -10.667)"/>
                    <path id="prefix__Pfad_10676" d="M10 39h5.667v1.7H10z" className="prefix__cls-1"
                          style={{fill: '#edeff1'}}
                          data-name="Pfad 10676" transform="translate(-8.867 -18.033)"/>
                    <path id="prefix__Pfad_10677" d="M15.6 34.633h-5.1L11.067 25h3.967z" className="prefix__cls-1"
                          style={{fill: '#edeff1'}}
                          data-name="Pfad 10677" transform="translate(-9.083 -11.967)"/>
                    <path id="prefix__Pfad_10678" d="M8 42h27.2v2.267H8z" className="prefix__cls-1"
                          style={{fill: '#edeff1'}}
                          data-name="Pfad 10678" transform="translate(-8 -19.333)"/>
                </g>
                <path id="prefix__Pfad_10679" d="M8 45h27.2v.567H8z" className="prefix__cls-2" style={{fill: '#d3d3d3'}}
                      data-name="Pfad 10679"
                      transform="translate(-2.6 -18.633)"/>
                <path id="prefix__Pfad_10680" d="M6 46h29.467v2.267H6z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}} data-name="Pfad 10680"
                      transform="translate(-1.733 -19.067)"/>
                <path id="prefix__Pfad_10681" d="M10 22h5.667v.567H10z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}} data-name="Pfad 10681"
                      transform="translate(-3.467 -8.667)"/>
                <path id="prefix__Pfad_10682" d="M10 41h5.667v.567H10z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}} data-name="Pfad 10682"
                      transform="translate(-3.467 -16.9)"/>
                <path id="prefix__Pfad_10683" d="M15.475 25.567L15.441 25h-3.967l-.033.567z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}}
                      data-name="Pfad 10683" transform="translate(-4.091 -9.967)"/>
                <path id="prefix__Pfad_10684" d="M27 22h5.667v1.7H27z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}} data-name="Pfad 10684"
                      transform="translate(-10.833 -8.667)"/>
                <path id="prefix__Pfad_10685" d="M27 39h5.667v1.7H27z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}} data-name="Pfad 10685"
                      transform="translate(-10.833 -16.033)"/>
                <path id="prefix__Pfad_10686" d="M32.6 34.633h-5.1L28.067 25h3.967z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}}
                      data-name="Pfad 10686" transform="translate(-11.05 -9.967)"/>
                <path id="prefix__Pfad_10687" d="M27 22h5.667v.567H27z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}} data-name="Pfad 10687"
                      transform="translate(-10.833 -8.667)"/>
                <path id="prefix__Pfad_10688" d="M27 41h5.667v.567H27z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}} data-name="Pfad 10688"
                      transform="translate(-10.833 -16.9)"/>
                <path id="prefix__Pfad_10689" d="M32.475 25.567L32.441 25h-3.967l-.033.567z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}}
                      data-name="Pfad 10689" transform="translate(-11.458 -9.967)"/>
                <path id="prefix__Pfad_10690" d="M44 22h5.667v1.7H44z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}} data-name="Pfad 10690"
                      transform="translate(-18.2 -8.667)"/>
                <path id="prefix__Pfad_10691" d="M44 39h5.667v1.7H44z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}} data-name="Pfad 10691"
                      transform="translate(-18.2 -16.033)"/>
                <path id="prefix__Pfad_10692" d="M49.6 34.633h-5.1L45.067 25h3.967z" className="prefix__cls-1"
                      style={{fill: '#edeff1'}}
                      data-name="Pfad 10692" transform="translate(-18.417 -9.967)"/>
                <path id="prefix__Pfad_10693" d="M44 22h5.667v.567H44z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}} data-name="Pfad 10693"
                      transform="translate(-18.2 -8.667)"/>
                <path id="prefix__Pfad_10694" d="M44 41h5.667v.567H44z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}} data-name="Pfad 10694"
                      transform="translate(-18.2 -16.9)"/>
                <path id="prefix__Pfad_10695" d="M49.475 25.567L49.441 25h-3.967l-.033.567z" className="prefix__cls-2"
                      style={{fill: '#d3d3d3'}}
                      data-name="Pfad 10695" transform="translate(-18.824 -9.967)"/>
                <path id="prefix__Pfad_10696" d="M12 32h28.333v14.733H12z" className="prefix__cls-3"
                      style={{fill: '#98ca69'}}
                      data-name="Pfad 10696" transform="translate(-4.333 -13)"/>
                <circle id="prefix__Ellipse_1378" cx="4.5" cy="4.5" r="4.5" className="prefix__cls-4"
                        style={{fill: '#75aa40'}}
                        data-name="Ellipse 1378" transform="translate(17 22)"/>
                <path id="prefix__Pfad_10697"
                      d="M36.332 34.633A3.984 3.984 0 0 0 39 37.3v5.43a3.984 3.984 0 0 0-2.668 2.67H17.3a3.984 3.984 0 0 0-2.668-2.668V37.3a3.984 3.984 0 0 0 2.668-2.667zM37.3 33.5H16.333a2.834 2.834 0 0 1-2.833 2.833V43.7a2.834 2.834 0 0 1 2.833 2.833H37.3a2.834 2.834 0 0 1 2.833-2.833v-7.367A2.834 2.834 0 0 1 37.3 33.5z"
                      className="prefix__cls-4" style={{fill: '#75aa40'}} data-name="Pfad 10697"
                      transform="translate(-4.983 -13.65)"/>
                <path id="prefix__Pfad_10698"
                      d="M35.833 41.833H34.7a.567.567 0 0 1 0-1.133h1.133a.567.567 0 0 1 .567.567h1.133a1.7 1.7 0 0 0-1.7-1.7V39H34.7v.567a1.7 1.7 0 0 0 0 3.4h1.133a.567.567 0 1 1 0 1.133H34.7a.567.567 0 0 1-.567-.567H33a1.7 1.7 0 0 0 1.7 1.7v.567h1.133v-.567a1.7 1.7 0 1 0 0-3.4z"
                      className="prefix__cls-3" style={{fill: '#98ca69'}} data-name="Pfad 10698"
                      transform="translate(-13.433 -16.033)"/>
                <path id="prefix__Pfad_10699" d="M13 58h27.2v1.133H13z" className="prefix__cls-3"
                      style={{fill: '#98ca69'}} data-name="Pfad 10699"
                      transform="translate(-4.767 -24.267)"/>
                <path id="prefix__Pfad_10700" d="M13 58h27.2v.567H13z" className="prefix__cls-4"
                      style={{fill: '#75aa40'}} data-name="Pfad 10700"
                      transform="translate(-4.767 -24.267)"/>
                <path id="prefix__Pfad_10701" d="M14 60h26.067v1.133H14z" className="prefix__cls-3"
                      style={{fill: '#98ca69'}}
                      data-name="Pfad 10701" transform="translate(-5.2 -25.133)"/>
                <path id="prefix__Pfad_10702" d="M14 60h26.067v.567H14z" className="prefix__cls-4"
                      style={{fill: '#75aa40'}}
                      data-name="Pfad 10702" transform="translate(-5.2 -25.133)"/>
                <circle id="prefix__Ellipse_1379" cx="7" cy="7" r="7" className="prefix__cls-5"
                        style={{fill: '#6e83b7'}} data-name="Ellipse 1379"
                        transform="translate(2 22)"/>
                <circle id="prefix__Ellipse_1380" cx="5" cy="5" r="5" className="prefix__cls-1"
                        style={{fill: '#edeff1'}} data-name="Ellipse 1380"
                        transform="translate(4 24)"/>
                <path id="prefix__Pfad_10703" d="M14.866 50.067L13 48.2V44h1.133v3.732l1.534 1.534z"
                      className="prefix__cls-2" style={{fill: '#d3d3d3'}} data-name="Pfad 10703"
                      transform="translate(-4.767 -18.2)"/>
                <path id="prefix__Pfad_10704" d="M13 40h1.133v1.133H13z" className="prefix__cls-5"
                      style={{fill: '#6e83b7'}}
                      data-name="Pfad 10704" transform="translate(-4.767 -16.467)"/>
                <path id="prefix__Pfad_10705" d="M22 49h1.133v1.133H22z" className="prefix__cls-5"
                      style={{fill: '#6e83b7'}}
                      data-name="Pfad 10705" transform="translate(-8.667 -20.367)"/>
                <path id="prefix__Pfad_10706" d="M13 58h1.133v1.133H13z" className="prefix__cls-5"
                      style={{fill: '#6e83b7'}}
                      data-name="Pfad 10706" transform="translate(-4.767 -24.267)"/>
                <path id="prefix__Pfad_10707" d="M4 49h1.133v1.133H4z" className="prefix__cls-5"
                      style={{fill: '#6e83b7'}} data-name="Pfad 10707"
                      transform="translate(-.867 -20.367)"/>
            </g>
        </svg>
    )
};

export default Deposit;
