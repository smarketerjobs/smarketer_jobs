import React, {useEffect, useState} from 'react';
import ReactGA from 'react-ga';
import {useHistory} from "react-router";
import {NavLink, useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAlignJustify, faCheck, faMapMarkerAlt} from '@fortawesome/fontawesome-free-solid';

import {openLoginModal, openRegisterModal} from "../store/ducks/modals";
import {clearSearchResults, fetchSearchJobsDefault} from "../store/ducks/search";

import LangSelect from "../blocks/LangSelect/LangSelect";
import FlagsSelect from "./FlagSelect/FlagSelect";

import styles from './MainSearchPage.module.scss';

import separator from "../../media/separator_bg.png";
import LogoSmarketer from "../../media/LogoSmarketerJobsDarck.png";

const MainSearchPage = () => {
    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {userId} = useSelector(state => state.userIdState);
    const {country} = useSelector(state => state.searchState);

    const history = useHistory();
    const location = useLocation();

    const params = new URLSearchParams(location.search);

    const [getCity, setCity] = useState(params.get('city') || '');
    const [getKeyword, setKeyword] = useState(params.get('query') || '');
    const [getCountryCode, setCountryCode] = useState(params.get('country') || 'DE');
    const [getCategorySlug, setCategorySlug] = useState(params.get('category') || '');

    const [getQuery, setQuery] = useState('');
    const [getOnSubmit, setOnSubmit] = useState(false);

    const searchState = useSelector(state => state.searchState);
    const {userCompaniesData} = useSelector(state => state.userCompaniesDataState);

    const [isSearchJobs, setSearchJobs] = useState(false);

    const setSpaceToPlus = name => name.split(' ').join('+');
    const setPlusToSpace = name => name.split('+').join(' ');

    const onCountryChange = countryCode => setCountryCode(countryCode);

    const setQueryStr = isSearch => {
        let query = '';

        getCity ? query ? query = query + `&city=${getCity}` : query = `?city=${getCity}` : '';
        getKeyword ? query ? query = query + `&query=${getKeyword}` : query = `?query=${getKeyword}` : '';
        getCountryCode ? query ? query = query + `&country=${getCountryCode}` : query = `?country=${getCountryCode}` : '';
        getCategorySlug ? query ? query = query + `&category=${getCategorySlug}` : query = `?category=${getCategorySlug}` : '';

        setQuery(query);

        if (isSearch) {
            searchJob();
        }
    };

    const searchJob = () => {
        ReactGA.event({
            label: userCompaniesData.length && userCompaniesData[0]['company_name'] || 'user_not_logged',
            action: 'start_search',
            category: 'Search'
        });
        setSearchJobs(true);
    };

    const DropdownExamplePointingTwo = () => (
        <div className={styles.dropdownWrapper}>
            <span className={`${styles.title} ${styles.dropdownTitle}`}>
                <FontAwesomeIcon icon={faAlignJustify} className={styles.chooseBtnIcon}/>
                <span className={styles.title}>
                    {locale ? locale['All the categories'] : ''}
                </span>
            </span>
            <div className={styles.dropdown}>
                {searchState.categories.map(({category_id, category_slug}) => (
                    <button key={category_id} className={styles.dropdownItem}
                            onClick={() => setCategorySlug(category_slug)}>
                        {locale ? locale[`job_category_id_${category_id}`] : ''}
                    </button>
                ))}
            </div>
        </div>
    );

    useEffect(() => {
        const script = document.createElement('script');

        const scriptTimeoutId = setTimeout(() => {
            script.setAttribute('src', 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js');
            script.setAttribute('data-ad-client', 'ca-pub-6586482158296741');
            script.setAttribute('data-checked-head', 'false');
            document.head.appendChild(script);
        }, 3000);

        return () => {
            const timeoutId = setTimeout(() => {
                if (script) {
                    document.head.removeChild(script)
                }

                const insTags = document.getElementsByName('ins');
                const iFrames = document.getElementsByName('iframe');

                insTags.forEach(ins => document.removeChild(ins));
                iFrames.forEach(iFrame => document.removeChild(iFrame));

                clearTimeout(scriptTimeoutId);
                clearTimeout(timeoutId);
            }, 3000);
        }
    }, []);

    useEffect(setQueryStr, [getCity, getKeyword, getCountryCode]);

    useEffect(() => {
        if (country) {
            setCountryCode(country.country_code);
        }
    }, [country]);

    useEffect(() => {
        if (getCategorySlug || getOnSubmit) {
            setQueryStr(true);
        }
    }, [getCategorySlug, getOnSubmit]);

    useEffect(() => {
        if (searchState && !getCategorySlug) {
            dispatch(fetchSearchJobsDefault({...searchState.defaultSearch, range: 1}));
        }
    }, [getQuery]);

    useEffect(() => {
        if (isSearchJobs) {
            dispatch(clearSearchResults());
            history.push(`/job-search${getQuery}`);
        }
    }, [isSearchJobs]);

    return (
        <div className={styles.root}>
            <section className={styles.settingsSection}>
                <div className={styles.wrapper}>
                    <div className={styles.content}>
                        <div className={styles['featureWrapper']}>
                            <ul className={styles.listOfItems}>
                                <li className={styles.item}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                    <span>{locale ? locale['Discover your dream job'] : ''}</span>
                                </li>
                                <li className={styles.item}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                    <span>{locale ? locale['1-Click application'] : ''}</span>
                                </li>
                                <li className={styles.item}>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                    <span>{locale ? locale['Apply with LinkedIn'] : ''}</span>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <ul className={styles.listOfItems}>
                                {!userId
                                    ? <li className={styles.item}>
                                        <button type='button' className={styles.btnLogin}
                                                onClick={() => dispatch(openLoginModal())}>
                                            {locale ? locale['Login'] : ''}
                                        </button>
                                    </li>
                                    : ''
                                }
                                <li className={styles.item}>
                                    {userId
                                        ? <NavLink to="/job-add" className={styles.btnRegister}>
                                            {locale ? locale['Publish a job'] : ''}
                                        </NavLink>
                                        : <button className={styles.btnRegister}
                                                  onClick={() => dispatch(openRegisterModal())}>
                                            {locale ? locale['Publish a job'] : ''}
                                        </button>
                                    }
                                </li>
                                <li className={styles.item}>
                                    <LangSelect/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section className={styles.categorySection}>
                <div className={styles.wrapper}>
                    <div className={styles.listOfCategories}>
                        <div className={styles.chooseCategory}><DropdownExamplePointingTwo/></div>
                        {searchState.categories.map(({category_id, category_slug}, index) => {
                            if (index <= 4) {
                                return (
                                    <button key={category_id} className={styles.categoryItem}
                                            onClick={() => setCategorySlug(category_slug)}>
                                        <span className={styles.title}>
                                            {locale ? locale[`job_category_id_${category_id}`] : ''}
                                        </span>
                                    </button>
                                )
                            }
                        })}
                    </div>
                </div>
            </section>
            <section className={styles.mainSection}>
                <NavLink to="/" className={styles.mainLogoWrapperSearch}>
                    <img src={LogoSmarketer} className={styles.mainLogo} alt='Logo: smarketer'/>
                </NavLink>
                <h2 className={styles.mainTitle}>
                    {locale ? locale['Where do you start your next career?'] : ''}
                </h2>
                <form
                    onSubmit={e => {
                        e.preventDefault();
                        setOnSubmit(true);
                    }}
                    className={styles.groupSearchFilter}>
                    <div className={styles.flex}>
                        <div className={`${styles.searchByPosition} ${styles.searchInputWrapper}`}>
                        <span className={styles.placeholderName}>
                            {locale ? locale['Job Title'] : ''}: </span>
                            <input className={styles.input} name='position' value={setPlusToSpace(getKeyword)}
                                   onChange={e => setKeyword(setSpaceToPlus(e.target.value))}
                                   onBlur={() => setKeyword(setSpaceToPlus(setPlusToSpace(getKeyword).trim()))}
                            />
                        </div>
                        <div className={`${styles.searchByPlace} ${styles.searchInputWrapper}`}>
                            <FontAwesomeIcon icon={faMapMarkerAlt} className={styles.mapMarker}/>
                            <div className={styles.flagSelectWrapper}>
                                <FlagsSelect
                                    searchCountries={searchState.searchCountries}
                                    onCountryChange={onCountryChange}
                                />
                            </div>
                            <input
                                className={styles.input}
                                name='place'
                                value={setPlusToSpace(getCity)}
                                placeholder={`${locale ? locale['City: city, state or zip code'] : ''}`}
                                onChange={e => setCity(setSpaceToPlus(e.target.value))}
                                onBlur={() => setCity(setSpaceToPlus(setPlusToSpace(getCity).trim()))}
                            />
                        </div>
                    </div>
                    <button className={styles.acceptBtn} id='start_search' onClick={e => {
                        e.preventDefault();
                        setOnSubmit(true);
                    }}>
                        {locale ? locale['Find Jobs'] : ''}
                    </button>
                </form>
                <div className={styles.imgWrapper}>
                    <img src="../../media/searchImage.png" alt="go to dream" className={styles.img}/>
                </div>
            </section>
            <div className={styles.footerImgWrapper}>
                <img src={separator} alt="separator line" className={styles.footerImg}/>
            </div>
            <footer className={styles.footer}>
                <div className={styles.content}>
                    <span className={styles.text}>
                        &#169; {new Date().getFullYear()} Smarketer GmbH | <span className={styles.linksBlock}>
                        <NavLink to={`/privacy-${locale ? locale['lang'] : 'en'}`} target="_blank"
                                 activeClassName={styles.bgTransparent} className={styles.link}>
                            {locale ? locale['Privacy'] : ''}
                        </NavLink> | <NavLink to={`/imprint-${locale ? locale['lang'] : 'en'}`} target="_blank"
                                              activeClassName={styles.bgTransparent} className={styles.link}>
                        {locale ? locale['Imprint'] : ''}</NavLink></span>
                    </span>
                </div>
            </footer>
        </div>
    );
};

export default MainSearchPage;
