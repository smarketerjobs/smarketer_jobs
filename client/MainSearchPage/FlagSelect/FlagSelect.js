import React, {useState} from 'react';

import {useDispatch, useSelector} from "react-redux";

import {setSearchCountry} from "../../store/ducks/search";

import styles from "./FlagSelect.module.scss";

const FlagsSelect = props => {
    const {onCountryChange} = props;
    const dispatch = useDispatch();
    const {country, searchCountries} = useSelector(state => state.searchState);

    const [isOpenFlagsSelect, setIsOpenFlagsSelect] = useState(false);

    const countryList = countries => countries.map(country => (
        <li
            key={country.country_code}
            className={`${styles.wrapper} ${styles.listItem}`}
            onClick={() => {
                dispatch(setSearchCountry(country));
                onCountryChange(country.country_code);
            }}
        >
            <div className={styles.flagWrapper}>
                <img
                    src={`../../../../../..${country.flag}`}
                    alt={country.country}
                />
            </div>
            <div className={styles.countryName}>
                {country.country}
            </div>
        </li>
    ));

    return (
        <div
            className={styles.flagsSelect}
            onClick={() => setIsOpenFlagsSelect(!isOpenFlagsSelect)}
        >
            <div className={styles.wrapper}>
                <img
                    src={`../../../../../..${country.flag}`}
                    alt={country.country}
                    title={country.country}
                />
            </div>
            {isOpenFlagsSelect ? (
                <ul className={styles.flagList}>
                    {countryList(searchCountries)}
                </ul>
            ) : ''}
        </div>
    );
};

export default FlagsSelect;
