import React, {useEffect, useState} from 'react';

import {useHistory} from "react-router";
import {useLocation} from "react-router-dom";

import axios from "axios";

import Footer from "../blocks/Footer/Footer";
import JobContent from "../JobSearchPage/JobContent";
import JobSearchHeader from "../blocks/JobSearchHeader/JobSearchHeader";

import styles from './JobForGoogle.module.scss';

const JobForGoogle = () => {
    const history = useHistory();
    const location = useLocation();

    const params = new URLSearchParams(location.search);

    const [getQuery, setQuery] = useState('');
    const [getJobInfo, setJobInfo] = useState(null);

    const getJobId = params.get('job_id') || '';

    const setSpaceToPlus = name => name.split(' ').join('+');

    const setQueryStr = () => {
        if (getJobInfo) {
            let query = '';

            getJobId ? query = `?job_id=${getJobId}` : '';
            getJobInfo.title ? query ? query = query + `&job_title=${setSpaceToPlus(getJobInfo.title)}` : query = `?job_title=${setSpaceToPlus(getJobInfo.title)}` : '';
            getJobInfo.company_name ? query ? query = query + `&company_name=${setSpaceToPlus(getJobInfo.company_name)}` : query = `?company_name=${setSpaceToPlus(getJobInfo.company_name)}` : '';

            setQuery(query);
        }
    };

    useEffect(setQueryStr, [getJobInfo]);

    useEffect(() => {
        if (getQuery) {
            history.replace({pathname: location.pathname, search: getQuery, isActive: false});
        }
    }, [getQuery]);

    useEffect(() => {
        if (getJobId) {
            axios.get('/ajax.php', {
                params: {model: 'job', mode: 'get_job', job_id: getJobId},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        setJobInfo(data.data);
                    } else {
                        console.error(data);
                        history.push('/search');
                    }
                })
                .catch(err => console.error(err));
        }
    }, [getJobId]);

    return <div className={styles.root}>
        <JobSearchHeader
            isPreview={true}
            city=''
            title=''
            setCity={() => null}
            setTitle={() => null}
            closeCompany={() => null}
            setCountryCode={() => null}
        />
        <div className={styles.jobWrapper}>
            <JobContent
                jobId={getJobId}
                setSelectedCompany={() => ''}
                isOverview={true}
            />
        </div>
        <Footer/>
    </div>;
};

export default JobForGoogle;
