import React, {useEffect} from 'react';

import styles from './RequiredData.module.scss';

import {Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import axios from "axios";
import {showAlert} from "../store/ducks/alert";
import {closeModals} from "../store/ducks/modals";
import {fetchCountries} from "../store/ducks/countries";
import {fetchUserAccountData, fetchUserCompaniesData} from "../store/ducks/user";

const validationSchema = Yup.object().shape({
    website: Yup.string().required('Required').matches(/^(ftp|http|https):\/\/[^ "]+$/, 'Invalid URL'),
    lastName: Yup.string().required('Required'),
    firstName: Yup.string().required('Required'),
    companyName: Yup.string().required('Required'),
    countryCode: Yup.string().required('Required'),
    phoneNumber: null
});

const RequiredData = ({initialValues}) => {

    const history = useHistory();
    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {userId} = useSelector(state => state.userIdState);
    const {countries} = useSelector(state => state.countriesState);
    const {userAccountData} = useSelector(state => state.userAccountDataState);

    const submit = ({firstName, lastName, companyName, website, countryCode, phoneNumber}) => {
        axios.get('/ajax.php', {
            params: {
                model: 'user',
                mode: 'init_user',
                last_name: lastName,
                first_name: firstName,
                company_name: companyName,
                country_code: countryCode,
                phone_number: phoneNumber,
                company_website: website,
            },
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    if (userId) {
                        dispatch(fetchUserAccountData(userId));
                        dispatch(fetchUserCompaniesData(userId));
                    }
                    if (data.error === 'Duplicate company name') {
                        dispatch(showAlert('failure', data.error));
                    }
                } else {
                    dispatch(showAlert('failure', data.error));
                    console.error(data);
                }
            })
            .catch(err => console.error(err));
    }

    useEffect(() => {
        dispatch(fetchCountries());
    }, []);

    useEffect(() => {
        if (userId) {
            if (userAccountData?.first_name) {
                dispatch(showAlert('success', 'Registration successful'));
                history.push('/job-add?active=create');
                dispatch(closeModals());
            }
        }
    }, [userAccountData, userId]);

    return (
        <div className={styles.requiredDataWrapper}>
            <div className={styles.contactForm}>
                <div className={styles.header}>
                    <h2 className={styles.title}>{locale ? locale['Almost done'] : ''}</h2>
                </div>
                <p className={styles.subTitle}>{locale ? locale['We only need'] : ''}</p>
                <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={values => submit(values)}
                >
                    {({errors, touched}) => (
                        <Form className={styles.form}>
                            <div className={`${styles.inputColumn} ${styles.mr10}`}>
                                <div
                                    className={`${styles.label} ${errors.firstName && touched.firstName ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['First name'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="firstName"
                                        className={`${styles.input} ${errors.firstName && touched.firstName ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.lastName && touched.lastName ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Last name'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="lastName"
                                        className={`${styles.input} ${errors.lastName && touched.lastName ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div className={styles.label}>
                                    <div className={styles.labelText}>{locale ? locale['Phone number'] : ''}</div>
                                    <Field name="phoneNumber" className={styles.input}/>
                                </div>
                                <div
                                    className={`${styles.label} ${errors.companyName && touched.companyName ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Company name'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="companyName"
                                        className={`${styles.input} ${errors.companyName && touched.companyName ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.website && touched.website ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Website URL'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="website"
                                        className={`${styles.input} ${errors.website && touched.website ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.countryCode && touched.countryCode ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Country'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        as="select"
                                        name="countryCode"
                                        className={`${styles.input} ${errors.countryCode && touched.countryCode ? styles.invalidInput : ''}`}>
                                        {countries?.map(({country_code, country}) => (
                                            <option value={country_code} key={country_code}>
                                                {country}
                                            </option>
                                        ))}
                                    </Field>
                                </div>
                            </div>
                            <div className={styles.mandatory}>*{locale ? locale['Mandatory fields'] : ''}</div>
                            <button className={styles.btnSubmit}>
                                {locale ? locale['Register now'] : ''}
                            </button>
                        </Form>
                    )}
                </Formik>
            </div>
        </div>
    )
};

RequiredData.defaultProps = {
    initialValues: {
        website: 'https://',
        lastName: '',
        firstName: '',
        companyName: '',
        phoneNumber: '',
        countryCode: 'DE',
    }
}

export default RequiredData;
