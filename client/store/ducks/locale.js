import axios from "axios";
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export const localeActionTypes = {
    'GET_LOCALE_REQUEST': 'GET_LOCALE_REQUEST',
    'GET_LOCALE_SUCCESS': 'GET_LOCALE_SUCCESS',
    'GET_LOCALE_FAILURE': 'GET_LOCALE_FAILURE',
};

export const getLocaleRequest = lang => ({
    type: localeActionTypes.GET_LOCALE_REQUEST,
    payload: {lang}
});

export const getLocaleSuccess = locale => ({
    type: localeActionTypes.GET_LOCALE_SUCCESS,
    payload: {
        locale
    }
});

export const getLocaleFailure = error => ({
    type: localeActionTypes.GET_LOCALE_FAILURE,
    payload: {error}
});

export const fetchLocale = lang => dispatch => {
    const getSelectedLang = () => {
        const savedUserSelectedLocale = localStorage.getItem('smarketer_locale');

        if (savedUserSelectedLocale) {
            const locale = JSON.parse(savedUserSelectedLocale);
            dispatch(getLocaleSuccess(locale));
            setLang(locale.lang)
        } else {
            axios.get('/ajax.php', {
                params: {"mode": "get", "model": "locale"},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => data.data ? setLang(data.data) : console.error(data))
                .catch(err => console.error(err));
        }
    };

    const setLang = lang => {
        dispatch(getLocaleRequest(lang));
        axios.get('/ajax.php', {
            params: {"model": "locale", "mode": "set", "locale": lang},
            headers: {'Cache-Control': 'no-cache'}
        }).then();
        getLocale(lang);
    };

    const getLocale = lang => {
        axios.get('/ajax.php', {
            params: {"mode": "get_all", "model": "locale", "locale": lang},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    localStorage.setItem('smarketer_locale', JSON.stringify(data.data));
                    cookies.set('locale', data.data.lang);
                    dispatch(getLocaleSuccess(data.data));
                } else {
                    console.error(data);
                    dispatch(getLocaleFailure(data));
                }
            })
            .catch(error => {
                console.error(error);
                dispatch(getLocaleFailure(error));
            });
    };

    if (lang) {
        setLang(lang);
    } else {
        getSelectedLang();
    }
};

const initialLocaleState = {
    error: '',
    locale: null,
    fetching: false
};

export const localeReducer = (state = initialLocaleState, {type, payload}) => {
    switch (type) {
        case localeActionTypes.GET_LOCALE_REQUEST:
            return {
                ...state,
                fetching: true
            };
        case localeActionTypes.GET_LOCALE_SUCCESS:
            return {
                ...state,
                error: null,
                locale: payload.locale,
                fetching: false
            };
        case localeActionTypes.GET_LOCALE_FAILURE:
            return {
                ...state,
                error: payload.error,
                locale: null,
                fetching: false
            };
        default:
            return state;
    }
};
