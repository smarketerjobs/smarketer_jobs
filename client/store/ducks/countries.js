import axios from "axios";

export const countriesActions = {
    'GET_COUNTRIES_REQUEST': 'GET_COUNTRIES_REQUEST',
    'GET_COUNTRIES_SUCCESS': 'GET_COUNTRIES_SUCCESS',
    'GET_COUNTRIES_FAILURE': 'GET_COUNTRIES_FAILURE',
};

const getCountriesSuccess = payload => ({
    type: countriesActions.GET_COUNTRIES_SUCCESS,
    payload
});

export const fetchCountries = () => dispatch => {
    axios.get('/ajax.php', {params: {model: 'catalog', mode: 'get_countries'}})
        .then(({data}) => {
            if (data.data) {
                dispatch(getCountriesSuccess(data.data));
            } else {
                console.error(data);
            }
        })
        .catch(err => console.error(err));
};

const initialCountriesState = {
    countries: []
};

export const countriesReducer = (state = initialCountriesState, {type, payload}) => {
    switch (type) {
        case countriesActions.GET_COUNTRIES_SUCCESS:
            return {
                ...state,
                countries: payload
            };
        default:
            return state;
    }
};
