import axios from "axios";
import {userIdActionTypes} from "./user";

export const billsActions = {
    'GET_BILLS_START': 'GET_BILLS_START',
    'GET_BILLS_SUCCESS': 'GET_BILLS_SUCCESS',
    'GET_BILLS_ERROR': 'GET_BILLS_ERROR',
};

const getBillsRequest = () => ({
    type: billsActions.GET_BILLS_START,
});

const getBillsSuccess = payload => ({
    type: billsActions.GET_BILLS_SUCCESS,
    payload
});

const getBillsError = payload => ({
    type: billsActions.GET_BILLS_ERROR,
    payload
});

export const fetchBills = () => dispatch => {
    dispatch(getBillsRequest())
    axios.get('/ajax.php', {
        params: {model: 'subscription', mode: 'user_invoices'},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data) {
                dispatch(getBillsSuccess(data.data));
            } else {
                dispatch(getBillsError(data.data));
                console.error(data);
            }
        })
        .catch(err => console.error(err));
};

const initialBillsState = {
    data: null,
    loading: false,
    error: null,
};

export const billsReducer = (state = initialBillsState, {type, payload}) => {
    switch (type) {
        case billsActions.GET_BILLS_START:
            return {
                ...state,
                loading: true,
                error: null,
            };
        case billsActions.GET_BILLS_SUCCESS:
            return {
                ...state,
                data: payload,
                loading: false,
                error: null,
            };
        case billsActions.GET_BILLS_ERROR:
            return {
                ...state,
                data: null,
                loading: false,
                error: payload,
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                data: null,
                loading: false,
                error: null,
            };
        default:
            return state;
    }
};
