export const modalsActions = {
    'CLOSE_MODALS': 'CLOSE_MODALS',
    'OPEN_LOGIN_MODAL': 'OPEN_LOGIN_MODAL',
    'OPEN_PAKET_MODAL': 'OPEN_PAKET_MODAL',
    'OPEN_REGISTER_MODAL': 'OPEN_REGISTER_MODAL',
    'OPEN_BILLING_INFO_MODAL': 'OPEN_BILLING_INFO_MODAL',
    'OPEN_CONTACT_FORM_MODAL': 'OPEN_CONTACT_FORM_MODAL',
};

export const closeModals = () => ({type: modalsActions.CLOSE_MODALS});
export const openLoginModal = () => ({type: modalsActions.OPEN_LOGIN_MODAL});
export const openPaketModal = () => ({type: modalsActions.OPEN_PAKET_MODAL});
export const openRegisterModal = () => ({type: modalsActions.OPEN_REGISTER_MODAL});
export const openBillingInfoModal = () => ({type: modalsActions.OPEN_BILLING_INFO_MODAL});
export const openContactFormModal = () => ({type: modalsActions.OPEN_CONTACT_FORM_MODAL});

const initialModalsState = {
    isOpenLoginModal: false,
    isOpenPaketModal: false,
    isOpenRegisterModal: false,
    isOpenBillingInfoModal: false,
    isOpenContactFormModal: false,
};

export const modalsReducer = (state = initialModalsState, {type}) => {
    switch (type) {
        case modalsActions.CLOSE_MODALS:
            return {
                isOpenLoginModal: false,
                isOpenPaketModal: false,
                isOpenRegisterModal: false,
                isOpenBillingInfoModal: false,
                isOpenContactFormModal: false,
            };
        case modalsActions.OPEN_LOGIN_MODAL:
            return {
                isOpenLoginModal: true,
                isOpenPaketModal: false,
                isOpenRegisterModal: false,
                isOpenBillingInfoModal: false,
                isOpenContactFormModal: false,
            };
        case modalsActions.OPEN_PAKET_MODAL:
            return {
                isOpenLoginModal: false,
                isOpenPaketModal: true,
                isOpenRegisterModal: false,
                isOpenBillingInfoModal: false,
                isOpenContactFormModal: false,
            };
        case  modalsActions.OPEN_REGISTER_MODAL:
            return {
                isOpenLoginModal: false,
                isOpenPaketModal: false,
                isOpenRegisterModal: true,
                isOpenBillingInfoModal: false,
                isOpenContactFormModal: false,
            };
        case modalsActions.OPEN_BILLING_INFO_MODAL:
            return {
                isOpenLoginModal: false,
                isOpenPaketModal: false,
                isOpenRegisterModal: false,
                isOpenBillingInfoModal: true,
                isOpenContactFormModal: false,
            };
        case modalsActions.OPEN_CONTACT_FORM_MODAL:
            return {
                isOpenLoginModal: false,
                isOpenPaketModal: false,
                isOpenRegisterModal: false,
                isOpenBillingInfoModal: false,
                isOpenContactFormModal: true,
            };
        default:
            return state;
    }
};
