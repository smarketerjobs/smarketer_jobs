import axios from "axios";
import {userIdActionTypes} from "./user";

export const planActions = {
    'GET_PLAN_SUCCESS': 'GET_PLAN_SUCCESS'
};

export const getPlanSuccess = data => ({
    type: planActions.GET_PLAN_SUCCESS,
    payload: data
});

export const fetchPlan = () => dispatch => {
    axios.get('/ajax.php', {
        params: {model: 'plan', mode: 'get_current_plan'},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data) {
                dispatch(getPlanSuccess(data.data));
            } else {
                if (data.error !== 'User is not logged') {
                    console.error(data);
                }
            }
        })
        .catch(err => console.error(err));
};

const initialPlanState = {
    plan: null
};

export const planReducer = (state = initialPlanState, {type, payload}) => {
    switch (type) {
        case planActions.GET_PLAN_SUCCESS:
            return {
                ...state,
                plan: payload
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                plan: null
            };
        default:
            return state;
    }
};
