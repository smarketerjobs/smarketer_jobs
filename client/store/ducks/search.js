import axios from "axios";

export const searchActionTypes = {
    'GET_SEARCH_JOBS_REQUEST': 'GET_SEARCH_JOBS_REQUEST',
    'GET_SEARCH_JOBS_SUCCESS': 'GET_SEARCH_JOBS_SUCCESS',
    'GET_SEARCH_JOBS_FAILURE': 'GET_SEARCH_JOBS_FAILURE',

    'GET_MORE_SEARCH_JOBS_REQUEST': 'GET_MORE_SEARCH_JOBS_REQUEST',
    'GET_MORE_SEARCH_JOBS_SUCCESS': 'GET_MORE_SEARCH_JOBS_SUCCESS',
    'GET_MORE_SEARCH_JOBS_FAILURE': 'GET_MORE_SEARCH_JOBS_FAILURE',

    'SET_SEARCH_COUNTRY': 'SET_SEARCH_COUNTRY',

    'CLEAR_FILTERS': 'CLEAR_FILTERS',
    'SET_IS_NO_RESULTS': 'SET_IS_NO_RESULTS',
    'CLEAR_SEARCH_RESULTS': 'CLEAR_SEARCH_RESULTS',
};

export const clearFilters = () => ({type: searchActionTypes.CLEAR_FILTERS});
export const clearSearchResults = () => ({type: searchActionTypes.CLEAR_SEARCH_RESULTS});
export const getSearchJobsRequest = () => ({type: searchActionTypes.GET_SEARCH_JOBS_REQUEST});

export const getSearchJobsSuccess = payload => ({
    type: searchActionTypes.GET_SEARCH_JOBS_SUCCESS,
    payload
});

export const getSearchJobsFailure = payload => ({
    type: searchActionTypes.GET_SEARCH_JOBS_FAILURE,
    payload,
});

export const getMoreSearchJobsRequest = () => ({type: searchActionTypes.GET_MORE_SEARCH_JOBS_REQUEST});
export const getMoreSearchJobsSuccess = payload => ({
    type: searchActionTypes.GET_MORE_SEARCH_JOBS_SUCCESS,
    payload
});
export const getMoreSearchJobsFailure = error => ({
    type: searchActionTypes.GET_MORE_SEARCH_JOBS_FAILURE,
    payload: {error}
});

export const setSearchCountry = country => ({
    type: searchActionTypes.SET_SEARCH_COUNTRY,
    payload: {country}
});

export const setSearchIsNoResults = payload => ({
    type: searchActionTypes.SET_IS_NO_RESULTS,
    payload
});

export const fetchSearchJobs = s => dispatch => {
    dispatch(getSearchJobsRequest());

    axios.get('/ajax.php', {
        params: {
            model: 'job',
            mode: 'search_jobs',
            sort_by: 'date_posted',
            sort_method: 'desc',
            q: s.keyword || '',
            country_code: s.countryCode || 'DE',
            page: s.page,
            range: s.range,
            employer: '',
            industry_id: '',
            address: s.city || '',
            distance: s.distance || '',
            category_slug: s.categorySlug || '',
            filters: '1',
        },
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.error === 'No results') {
                dispatch(getSearchJobsFailure(data));
            }
            if (data.data && data.data.result && data.data.result.length) {
                dispatch(getSearchJobsSuccess(data.data));
                dispatch(setSearchIsNoResults(!data.data.result.length));
            } else {
                dispatch(getSearchJobsFailure(data));
                console.error(data);
            }
        })
        .catch(err => {
            dispatch(getSearchJobsFailure(err));
            console.error(err);
        });
};

export const fetchMoreSearchJobs = s => dispatch => {
    dispatch(getMoreSearchJobsRequest());

    axios.get('/ajax.php', {
        params: {
            model: 'job',
            mode: 'search_jobs',
            sort_by: 'date_posted',
            sort_method: 'desc',
            q: s.keyword || '',
            country_code: s.countryCode || 'DE',
            page: s.page,
            range: s.range,
            employer: '',
            industry_id: '',
            address: s.city || '',
            distance: s.distance || '',
            category_slug: s.categorySlug || '',
        },
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data && data.data.result && data.data.result.length) {
                dispatch(getMoreSearchJobsSuccess(data.data));
            } else {
                dispatch(getMoreSearchJobsFailure(data));
            }
        })
        .catch(err => {
            console.error(err);
            dispatch(getMoreSearchJobsFailure(err));
        });
};

export const fetchSearchJobsDefault = s => dispatch => {
    dispatch(getSearchJobsRequest());

    axios.get('/ajax.php', {
        params: {
            model: 'job',
            mode: 'search_jobs',

            q: '',
            page: '1',
            range: '25',
            sort_by: 'date_posted',
            sort_method: 'desc',
            country_code: s.countryCode || 'DE',
            filters: '1',
        },
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data?.data?.result?.length) {
                dispatch(getSearchJobsSuccess(data.data));
                if (!s.isDefaultSearch) {
                    dispatch(setSearchIsNoResults(!data.data.result.length));
                }
            } else if (data?.data?.countries?.length) {
                dispatch(setSearchCountry(data.data.countries[0]))
            }
        })
        .catch(err => {
            dispatch(getSearchJobsFailure(err));
            console.error(err);
        });
};

const initialSearchState = {
    jobs: [],
    maxLength: null,
    error: '',
    cities: [],
    employer: '',
    fetching: false,
    fetchingMore: false,
    companies: [],
    categories: [],
    isNoResults: false,
    filteredJobs: [],
    searchCountries: [],
    country: {
        flag: "/flags/de.png",
        country: "Germany",
        country_code: "DE"
    },
    defaultSearch: {
        countryCode: "DE",
        isDefaultSearch: true,
        page: 1,
        range: 25,
    },
    isNewJobs: true
};

const objectToArray = obj => {
    const arr = [];
    Object.entries(obj).map(entry => {
        arr.push({
            id: entry[0],
            title: entry[1]
        })
    });

    return arr;
};

export const searchReducer = (state = initialSearchState, {type, payload}) => {
    switch (type) {
        case searchActionTypes.GET_SEARCH_JOBS_REQUEST:
            return {
                ...state,
                error: '',
                fetching: true
            };
        case searchActionTypes.GET_SEARCH_JOBS_SUCCESS:
            return {
                ...state,
                jobs: payload.result,
                error: '',
                cities: objectToArray(payload.cities),
                fetching: false,
                companies: payload.companies,
                categories: payload.categories,
                searchCountries: payload.countries,
                isNewJobs: true,
                maxLength: payload.length,
            };
        case searchActionTypes.GET_SEARCH_JOBS_FAILURE:
            return {
                ...state,
                error: payload.error,
                fetching: false,
                isNoResults: true,
                searchCountries: payload?.data?.countries || [],
            };

        case searchActionTypes.GET_MORE_SEARCH_JOBS_REQUEST:
            return {
                ...state,
                error: '',
                fetchingMore: true,
            };
        case searchActionTypes.GET_MORE_SEARCH_JOBS_SUCCESS:
            return {
                ...state,
                jobs: payload.result.length ? state.jobs.concat(payload.result) : state.jobs,
                fetchingMore: false,
                isNewJobs: false,
            };
        case searchActionTypes.GET_MORE_SEARCH_JOBS_FAILURE:
            return {
                ...state,
                error: payload.error,
                fetchingMore: false,
            };

        case searchActionTypes.SET_SEARCH_COUNTRY:
            if (payload.country) {
                return {
                    ...state,
                    country: payload.country,
                    defaultSearch: {
                        ...state.defaultSearch,
                        countryCode: payload.country.country_code
                    }
                };
            }
            return state;
        case searchActionTypes.SET_IS_NO_RESULTS:
            return {
                ...state,
                isNoResults: payload
            };
        case searchActionTypes.CLEAR_SEARCH_RESULTS:
            return {
                ...state,
                jobs: [],
                cities: [],
                companies: [],
                categories: [],
                filteredJobs: [],
                searchCountries: [],
            };
        case searchActionTypes.CLEAR_FILTERS:
            return {
                ...state,
                error: '',
                employer: '',
                filterBy: [],
                categories: [],
            };
        default:
            return state;
    }
};
