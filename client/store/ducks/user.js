import axios from "axios";
import {showAlert} from "./alert";
import {jobActions} from "./job";

export const userAccountDataActions = {
    'GET_USER_ACCOUNT_DATA_REQUEST': 'GET_USER_ACCOUNT_DATA_REQUEST',
    'GET_USER_ACCOUNT_DATA_SUCCESS': 'GET_USER_ACCOUNT_DATA_SUCCESS',
    'GET_USER_ACCOUNT_DATA_FAILURE': 'GET_USER_ACCOUNT_DATA_FAILURE',
};

export const getUserAccountDataRequest = () => ({type: userAccountDataActions.GET_USER_ACCOUNT_DATA_SUCCESS});

export const getUserAccountDataSuccess = data => ({
    type: userAccountDataActions.GET_USER_ACCOUNT_DATA_SUCCESS,
    payload: data
});

export const getUserAccountDataFailure = () => ({type: userAccountDataActions.GET_USER_ACCOUNT_DATA_SUCCESS});

export const fetchUserAccountData = () => dispatch => {
    dispatch(getUserAccountDataRequest());
    axios.get('/ajax.php', {
        params: {model: 'user', mode: 'get_data'},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
                if (data.data) {
                    dispatch(getUserAccountDataSuccess(data.data));
                } else {
                    dispatch(getUserAccountDataFailure());
                    if (data.error !== 'User is not logged') {
                        console.error(data);
                    }
                }
            }
        )
        .catch(err => {
            dispatch(getUserAccountDataFailure());
            console.error(err)
        });
};

const initialUserAccountDataState = {
    fetching: false,
    userAccountData: null,
    isNotEmptyUserAccountData: false
};

const inNotEmpty = data => {
    if (data) {
        return Object.entries(data).every(entry => {
            switch (entry[0]) {
                case 'city':
                    return true;
                case 'post_code':
                    return true;
                case 'address_1':
                    return true;
                case 'address_2':
                    return true;
                case 'company_url':
                    return true;
                case 'company_logo':
                    return true;
                case 'company_description':
                    return true;
                case 'vat_id':
                    return true;
                case 'phone_number':
                    return true;
                case 'stripe_payment_method_id':
                    return true;
                case 'stripe_user_id':
                    return true;
                default:
                    return !!entry[1];
            }
        });
    }
};

export const userAccountDataReducer = (state = initialUserAccountDataState, {type, payload}) => {
    switch (type) {
        case userAccountDataActions.GET_USER_ACCOUNT_DATA_REQUEST:
            return {
                ...state,
                fetching: true,
            };
        case userAccountDataActions.GET_USER_ACCOUNT_DATA_SUCCESS:
            return {
                ...state,
                fetching: false,
                userAccountData: payload,
                isNotEmptyUserAccountData: inNotEmpty(payload)
            };
        case userAccountDataActions.GET_USER_ACCOUNT_DATA_FAILURE:
            return {
                ...state,
                fetching: false,
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                fetching: false,
                userAccountData: null,
                isNotEmptyUserAccountData: false
            };
        default:
            return state;
    }
};

export const userBillingDataActions = {
    'GET_USER_BILLING_DATA_SUCCESS': 'GET_USER_BILLING_DATA_SUCCESS'
};

export const getUserBillingDataSuccess = data => ({
    type: userBillingDataActions.GET_USER_BILLING_DATA_SUCCESS,
    payload: data
});

export const fetchUserBillingData = () => dispatch => {
    axios.get('/ajax.php', {
        params: {model: 'user', mode: 'get_billing_info'},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data) {
                dispatch(getUserBillingDataSuccess(data.data));
            } else {
                if (data.error !== 'User is not logged') {
                    console.error(data);
                }
            }
        })
        .catch(err => console.error(err));
};

const initialUserBillingDataState = {
    userBillingData: null,
    isNotEmptyUserBillingData: false
};

const inNotEmptyUserBillingData = data => {
    return Object.entries(data).every(entry => {
        switch (entry[0]) {
            case 'city':
                return true;
            case 'post_code':
                return true;
            case 'address_1':
                return true;
            case 'address_2':
                return true;
            case 'company_url':
                return true;
            case 'company_logo':
                return true;
            case 'company_description':
                return true;
            case 'vat_id':
                return true;
            default:
                return !!entry[1];
        }
    });
};

export const userBillingDataReducer = (state = initialUserBillingDataState, {type, payload}) => {
    switch (type) {
        case userBillingDataActions.GET_USER_BILLING_DATA_SUCCESS:
            return {
                ...state,
                userBillingData: payload,
                isNotEmptyUserBillingData: inNotEmptyUserBillingData(payload)
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                userBillingData: null,
                isNotEmptyUserBillingData: false
            };
        default:
            return state;
    }
};

export const userCompaniesDataActions = {
    'GET_USER_COMPANIES_DATA_SUCCESS': 'GET_USER_COMPANIES_DATA_SUCCESS',

    'DELETE_COMPANY_REQUEST': 'DELETE_COMPANY_REQUEST',
    'DELETE_COMPANY_SUCCESS': 'DELETE_COMPANY_SUCCESS',
    'DELETE_COMPANY_FAILURE': 'DELETE_COMPANY_FAILURE',

    'ADD_NEW_COMPANY': 'ADD_NEW_COMPANY',
    'REMOVE_DRAFT_COMPANY': 'REMOVE_DRAFT_COMPANY',
};

export const getUserCompaniesDataSuccess = data => ({
    type: userCompaniesDataActions.GET_USER_COMPANIES_DATA_SUCCESS,
    payload: data
});

export const deleteCompanyRequest = () => ({type: userCompaniesDataActions.DELETE_COMPANY_REQUEST});
export const deleteCompanySuccess = id => ({type: userCompaniesDataActions.DELETE_COMPANY_SUCCESS, payload: id});
export const deleteCompanyFailure = () => ({type: userCompaniesDataActions.DELETE_COMPANY_FAILURE});

export const addNewCompany = () => ({
    type: userCompaniesDataActions.ADD_NEW_COMPANY,
    payload: {
        active: "0",
        company_id: "",
        company_slug: "",
        company_name: "",
        company_email: '',
        company_logo: null,
        company_website: "",
        company_description: "",
        company_job_categories: []
    }
});

export const removeDraftCompany = company => ({
    type: userCompaniesDataActions.REMOVE_DRAFT_COMPANY,
    payload: company
});

export const fetchUserCompaniesData = userId => dispatch => {
    axios.get('/ajax.php', {
        params: {model: 'company', mode: 'get_companies_data', user_id: userId},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data) {
                dispatch(getUserCompaniesDataSuccess(data.data));
            } else {
                if (data.error !== 'No data') {
                    console.error(data);
                }
            }
        })
        .catch(err => {
            console.error(err);
        });
};

export const fetchDeleteCompany = (companyId, userId) => dispatch => {
    dispatch(deleteCompanyRequest());

    axios.post('/ajax.php', {'model': 'company', 'mode': 'delete_company', 'company_id': companyId})
        .then(({data}) => {
            if (data.data.id) {
                dispatch(deleteCompanySuccess(data.data.id));
                dispatch(showAlert('success', 'Company deleted successfully'));
            } else {
                console.error(data);
                dispatch(showAlert('failure', 'failure'));
                dispatch(deleteCompanyFailure());
                dispatch(fetchUserCompaniesData(userId));
            }
        })
        .catch(err => {
            console.error(err);
            dispatch(showAlert('failure', 'failure'));
            dispatch(deleteCompanyFailure());
            dispatch(fetchUserCompaniesData(userId));
        });
};

const initialUserCompaniesDataState = {
    fetching: false,
    fetchDelete: false,
    userCompaniesData: [],
};

export const userCompaniesDataReducer = (state = initialUserCompaniesDataState, {type, payload}) => {
    switch (type) {
        case userCompaniesDataActions.GET_USER_COMPANIES_DATA_SUCCESS:
            return {
                ...state,
                userCompaniesData: payload,
            };
        case userCompaniesDataActions.DELETE_COMPANY_REQUEST:
            return {
                ...state,
                fetchDelete: true
            };
        case userCompaniesDataActions.DELETE_COMPANY_SUCCESS:
            return {
                ...state,
                fetchDelete: false,
                userCompaniesData: state.userCompaniesData.filter(company => company.company_id !== payload)
            };

        case userCompaniesDataActions.DELETE_COMPANY_FAILURE:
            return {
                ...state,
                fetchDelete: false
            };
        case userCompaniesDataActions.ADD_NEW_COMPANY:
            return {
                ...state,
                userCompaniesData: [
                    ...state.userCompaniesData,
                    payload
                ]
            };
        case userCompaniesDataActions.REMOVE_DRAFT_COMPANY:
            return {
                ...state,
                userCompaniesData: state.userCompaniesData.filter(company => company !== payload)
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                fetching: false,
                fetchDelete: false,
                userCompaniesData: [],
            };
        default:
            return state;
    }
};

export const userIdActionTypes = {
    'GET_USER_ID_REQUEST': 'GET_USER_ID_REQUEST',
    'GET_USER_ID_SUCCESS': 'GET_USER_ID_SUCCESS',
    'GET_USER_ID_FAILURE': 'GET_USER_ID_FAILURE',
};

export const getUserIdRequest = () => ({
    type: userIdActionTypes.GET_USER_ID_REQUEST
});

export const getUserIdSuccess = userId => ({
    type: userIdActionTypes.GET_USER_ID_SUCCESS,
    payload: {userId}
});

export const getUserIdFailure = error => ({
    type: userIdActionTypes.GET_USER_ID_FAILURE,
    payload: {error}
});

export const fetchUserId = () => dispatch => {
    dispatch(getUserIdRequest());
    axios.get('/ajax.php', {
        params: {model: 'user', mode: 'get_logged_user_id'},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data && +data.data > 0) {
                dispatch(getUserIdSuccess(data.data));
            } else {
                dispatch(getUserIdFailure(data));
            }
        })
        .catch(err => {
            if (err.error === 'User is not logged') {
                dispatch(getUserIdFailure('User is not logged'));
            } else {
                console.error(err);
                dispatch(getUserIdFailure(err));
            }
        });
};

const initialUserIdState = {
    error: '',
    userId: null,
    fetching: false
};

export const userIdReducer = (state = initialUserIdState, {type, payload}) => {
    switch (type) {
        case userIdActionTypes.GET_USER_ID_REQUEST:
            return {
                ...state,
                fetching: true,
                error: ''
            };
        case userIdActionTypes.GET_USER_ID_SUCCESS:
            return {
                ...state,
                fetching: false,
                userId: payload.userId,
                error: ''
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                error: '',
                userId: null,
                fetching: false
            };
        default:
            return state;
    }
};

export const userJobsActionTypes = {
    'GET_USER_JOBS_REQUEST': 'GET_USER_JOBS_REQUEST',
    'GET_USER_JOBS_SUCCESS': 'GET_USER_JOBS_SUCCESS',
    'GET_USER_JOBS_FAILURE': 'GET_USER_JOBS_FAILURE',
};

export const getUserJobsSuccess = jobs => ({
    type: userJobsActionTypes.GET_USER_JOBS_SUCCESS,
    payload: {
        jobs
    }
});

export const getUserJobsFailure = error => ({
    type: userJobsActionTypes.GET_USER_JOBS_FAILURE,
    payload: {error}
});

export const fetchUserJobs = userId => dispatch => {
    axios.get('/ajax.php', {
        params: {model: 'job', mode: 'get_all_jobs', active: 'all', user_id: userId},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data) {
                dispatch(getUserJobsSuccess(data.data));
            } else {
                console.error(data);
                dispatch(getUserJobsFailure(data));
            }
        })
        .catch(err => console.error(err));
};

const initialUserJobsState = {
    error: '',
    userJobs: [],
    fetching: false,
    activeJobs: null
};

export const userJobsReducer = (state = initialUserJobsState, {type, payload}) => {
    switch (type) {
        case userJobsActionTypes.GET_USER_JOBS_REQUEST:
            return {
                ...state,
                fetching: true,
                error: ''
            };
        case userJobsActionTypes.GET_USER_JOBS_SUCCESS:
            return {
                ...state,
                error: '',
                fetching: false,
                userJobs: payload.jobs,
                activeJobs: payload.jobs.filter(job => !!+job.active).length
            };
        case userJobsActionTypes.GET_USER_JOBS_FAILURE:
            return {
                ...state,
                fetching: false,
                error: ''
            };
        case jobActions.DELETE_JOB_SUCCESS:
            return {
                ...state,
                userJobs: state.userJobs.filter(job => job.job_id !== payload.jobId),
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                error: '',
                userJobs: [],
                fetching: false,
                activeJobs: null
            };
        default:
            return state;
    }
};
