import axios from "axios";
import {fetchUserJobs, userIdActionTypes} from "./user";

export const jobActions = {
    'SAVE_JOB_ADDRESS_START': 'JOB_SAVE_ADDRESS_START',
    'SAVE_JOB_ADDRESS_SUCCESS': 'JOB_SAVE_ADDRESS_SUCCESS',
    'SAVE_JOB_ADDRESS_FAILURE': 'JOB_SAVE_ADDRESS_FAILURE',

    'GET_JOB_ADDRESS_START': 'JOB_GET_ADDRESS_START',
    'GET_JOB_ADDRESS_SUCCESS': 'JOB_GET_ADDRESS_SUCCESS',
    'GET_JOB_ADDRESS_FAILURE': 'JOB_GET_ADDRESS_FAILURE',

    'DELETE_JOB_START': 'DELETE_JOB_START',
    'DELETE_JOB_SUCCESS': 'DELETE_JOB_SUCCESS',
    'DELETE_JOB_FAILURE': 'DELETE_JOB_FAILURE',

    'GET_JOB_CATALOGS_START': 'GET_JOB_CATALOGS_START',
    'GET_JOB_CATALOGS_SUCCESS': 'GET_JOB_CATALOGS_SUCCESS',
    'GET_JOB_CATALOGS_FAILURE': 'GET_JOB_CATALOGS_FAILURE',
};

export const fetchSaveJobAddress = (payload) => dispatch => {
    dispatch({type: jobActions.SAVE_JOB_ADDRESS_START});
    const {street, city, post_code, country_code, state_code, lat, lng} = payload

    axios.get('/ajax.php', {
        params: {
            model: 'job', mode: 'save_address',
            city, street, post_code, state_code, country_code, lat, lng,
        }
    })
        .then(({data}) => {
            if (data.data) {
                dispatch({type: jobActions.SAVE_JOB_ADDRESS_SUCCESS, payload: data.data});
                dispatch(fetchGetJobAddress());
            } else {
                console.error(data);
                dispatch({type: jobActions.SAVE_JOB_ADDRESS_FAILURE, payload: data.error});
            }
        })
        .catch(err => {
            console.error(err);
            dispatch({type: jobActions.SAVE_JOB_ADDRESS_FAILURE, payload: err});
        });
};

export const fetchGetJobAddress = () => dispatch => {
    dispatch({type: jobActions.GET_JOB_ADDRESS_START});

    axios.get('/ajax.php', {params: {model: 'job', mode: 'get_address'}})
        .then(({data}) => {
            if (data.data) {
                dispatch({type: jobActions.GET_JOB_ADDRESS_SUCCESS, payload: data.data});
            } else {
                if (data.error !== 'No address found') {
                    console.error(data);
                }
                dispatch({type: jobActions.GET_JOB_ADDRESS_FAILURE, payload: data.error});
            }
        })
        .catch(err => {
            if (err.error !== 'No address found') {
                console.error(err);
            }
            dispatch({type: jobActions.GET_JOB_ADDRESS_FAILURE, payload: err});
        });
};

export const fetchDeleteJob = ({userId, jobId}) => dispatch => {
    dispatch({type: 'DELETE_JOB_START'})

    axios.get('/ajax.php', {
        params: {model: 'job', mode: 'delete', job_id: jobId},
        headers: {'Cache-Control': 'no-cache'}
    })
        .then(({data}) => {
            if (data.data) {
                dispatch({type: 'DELETE_JOB_SUCCESS', payload: {jobId}})
                dispatch(fetchUserJobs(userId));
            } else {
                dispatch(fetchUserJobs(userId));
                console.error(data);
            }
        })
        .catch(err => {
            console.error(err);
            dispatch(fetchUserJobs(userId));
        });
}

const initialJobCategoriesState = {
    dataJobAddress: null,
    loadingJobAddress: false,
    errorJobAddress: null,

    dataDeleteJob: null,
    loadingDeleteJob: false,

    dataJobCatalogs: null,
    loadingJobCatalogs: false,
    errorJobCatalogs: null,
};

export const jobReducer = (state = initialJobCategoriesState, {type, payload}) => {
    switch (type) {
        case jobActions.GET_JOB_ADDRESS_SUCCESS:
            return {
                ...state,
                dataJobAddress: payload,
                loadingJobAddress: false,
                errorJobAddress: null,
            };
        case jobActions.GET_JOB_ADDRESS_FAILURE:
            return {
                ...state,
                dataDeleteJob: null,
                loadingDeleteJob: false,
                errorDeleteJob: payload,
            };

        case jobActions.DELETE_JOB_START:
            return {
                ...state,
                dataDeleteJob: null,
                loadingDeleteJob: true,
                errorDeleteJob: null,
            };
        case jobActions.DELETE_JOB_SUCCESS:
            return {
                ...state,
                dataDeleteJob: payload,
                loadingDeleteJob: false,
                errorDeleteJob: null,
            };
        case jobActions.DELETE_JOB_FAILURE:
            return {
                ...state,
                dataDeleteJob: null,
                loadingDeleteJob: false,
                errorDeleteJob: payload,
            };

        case jobActions.GET_JOB_CATALOGS_START:
            return {
                ...state,
                loadingJobCatalogs: true,
                errorJobCatalogs: null,
            };
        case jobActions.GET_JOB_CATALOGS_SUCCESS:
            return {
                ...state,
                dataJobCatalogs: payload,
                loadingJobCatalogs: false,
                errorJobCatalogs: null,
            };
        case jobActions.GET_JOB_CATALOGS_FAILURE:
            return {
                ...state,
                jobSalaryPeriods: [],
                errorJobSalaryPeriods: payload,
                loadingJobSalaryPeriods: false,
            };

        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                jobCategories: [],

                dataJobAddress: null,
                loadingJobAddress: false,
                errorJobAddress: null,

                dataDeleteJob: null,
                loadingDeleteJob: false,
                errorDeleteJob: null,
            };

        default:
            return state;
    }
};

export const fetchJobCatalogs = () => dispatch => {
    dispatch({type: jobActions.GET_JOB_CATALOGS_START});

    axios.get('/ajax.php', {params: {model: 'catalog', mode: 'get_catalog'}})
        .then(({data}) => {
            if (data.data) {
                dispatch({type: jobActions.GET_JOB_CATALOGS_SUCCESS, payload: data.data});
            } else {
                dispatch({type: jobActions.GET_JOB_CATALOGS_FAILURE, payload: data.error});
                console.error(data);
            }
        })
        .catch(err => {
            dispatch({type: jobActions.GET_JOB_CATALOGS_FAILURE, payload: err});
            console.error(err)
        });
};
