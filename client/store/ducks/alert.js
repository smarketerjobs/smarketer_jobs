let timeoutId = null;

export const alertActionTypes = {
    'SET_ALERT_IS_SHOWN': 'SET_ALERT_IS_SHOWN',
    'SET_ALERT_STATUS': 'SET_ALERT_STATUS',
    'SET_ALERT_MESSAGE': 'SET_ALERT_MESSAGE',
};

export const setAlertIsShown = isShown => ({
    type: alertActionTypes.SET_ALERT_IS_SHOWN,
    payload: isShown
});

export const setAlertStatus = status => ({
    type: alertActionTypes.SET_ALERT_STATUS,
    payload: status
});

export const setAlertMessage = message => ({
    type: alertActionTypes.SET_ALERT_MESSAGE,
    payload: message
});


export const showAlert = (status, message, duration) => dispatch => {
    if (timeoutId) {
        clearTimeout(timeoutId);
    }

    dispatch(setAlertStatus(status));
    dispatch(setAlertMessage(message));

    timeoutId = setTimeout(() => {
        dispatch(setAlertIsShown(false));
        dispatch(setAlertMessage(''));
    }, duration || 5000);

    dispatch(setAlertIsShown(true));
};

const initialAlertState = {
    status: null,
    message: '',
    isShown: false,
};

export const alertReducer = (state = initialAlertState, {type, payload}) => {
    switch (type) {
        case alertActionTypes.SET_ALERT_STATUS:
            return {
                ...state,
                status: payload
            };
        case alertActionTypes.SET_ALERT_MESSAGE:
            return {
                ...state,
                message: payload
            };
        case alertActionTypes.SET_ALERT_IS_SHOWN:
            return {
                ...state,
                isShown: payload
            };
        default:
            return state;
    }
};
