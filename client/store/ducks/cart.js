import {userIdActionTypes} from "./user";

export const cartActionsTypes = {
    'SET_PLAN': 'SET_PLAN',
    'SET_COUPON': 'SET_COUPON',
    'SET_PERIOD': 'SET_PERIOD',
};

export const setPlan = planName => ({
    type: cartActionsTypes.SET_PLAN,
    payload: planName
});

export const setCoupon = coupon => ({
    type: cartActionsTypes.SET_COUPON,
    payload: coupon
});

export const setPeriod = period => ({
    type: cartActionsTypes.SET_PERIOD,
    payload: period
});

const initialCartState = {
    plan: '',
    coupon: '',
    period: '_year',
};

export const cartReducer = (state = initialCartState, {type, payload}) => {
    switch (type) {
        case cartActionsTypes.SET_PLAN:
            return {
                ...state,
                plan: payload
            };
        case cartActionsTypes.SET_COUPON:
            return {
                ...state,
                coupon: payload
            };
        case cartActionsTypes.SET_PERIOD:
            return {
                ...state,
                period: payload
            };
        case userIdActionTypes.GET_USER_ID_FAILURE:
            return {
                ...state,
                plan: '',
                coupon: '',
                period: '_year',
            };
        default:
            return state;
    }
};
