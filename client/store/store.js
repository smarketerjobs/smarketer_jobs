import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";

import {jobReducer} from "./ducks/job";
import {cartReducer} from "./ducks/cart";
import {planReducer} from "./ducks/plan";
import {billsReducer} from "./ducks/bills";
import {alertReducer} from "./ducks/alert";
import {localeReducer} from "./ducks/locale";
import {searchReducer} from "./ducks/search";
import {modalsReducer} from "./ducks/modals";
import {countriesReducer} from "./ducks/countries";
import {
    userAccountDataReducer,
    userBillingDataReducer,
    userCompaniesDataReducer,
    userIdReducer,
    userJobsReducer,
} from "./ducks/user";

export const appReducer = combineReducers({
    jobState: jobReducer,
    cartState: cartReducer,
    planState: planReducer,
    billsState: billsReducer,
    alertState: alertReducer,
    userIdState: userIdReducer,
    localeState: localeReducer,
    searchState: searchReducer,
    modalsState: modalsReducer,
    userJobsState: userJobsReducer,
    countriesState: countriesReducer,
    userAccountDataState: userAccountDataReducer,
    userBillingDataState: userBillingDataReducer,
    userCompaniesDataState: userCompaniesDataReducer,
});

export const store = createStore(appReducer, applyMiddleware(thunkMiddleware));
