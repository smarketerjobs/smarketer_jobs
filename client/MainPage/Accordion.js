import React, {useEffect, useRef, useState} from "react";

import {faPlus} from '@fortawesome/fontawesome-free-solid';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import styles from './Accordion.module.scss';

const Accordion = props => {
    const [getActive, setActive] = useState("");
    const [getHeight, setHeight] = useState("0px");
    const [getDefaultTab, setDefaultTab] = useState(false);

    const content = useRef(null);

    const toggleAccordion = () => {
        if (getDefaultTab) {
            setDefaultTab(false);
            setActive('');
            setHeight('0px');
        } else {
            setActive(getActive === "" ? "active" : "");
            setHeight(getActive === "active" ? "0px" : `${content.current.scrollHeight}px`);
        }
    };

    useEffect(() => {
        setDefaultTab(props.isOpen);
    }, []);

    return (
        <div className={styles.accordionSection}>
            <button className={`${styles.accordion} ${getActive === 'active' || getDefaultTab ? styles.active : ''}`}
                    onClick={toggleAccordion}>
                <p className={styles.accordionTitle}>{props.title}</p>
                <FontAwesomeIcon icon={faPlus} className={styles.accordionIcon}/>
            </button>
            <div ref={content} style={{maxHeight: `${getDefaultTab ? `100%` : getHeight}`}}
                 className={styles.accordionContent}>
                <div className={styles.accordionText}>
                    {props.children}
                </div>
            </div>
        </div>
    );
};

export default Accordion;
