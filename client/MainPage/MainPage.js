import React, {useEffect, useState} from "react";
import {useHistory} from "react-router";
import {NavLink} from "react-router-dom";
import Toggle from 'react-toggle';
import {useDispatch, useSelector} from "react-redux";

import axios from "axios";
import queryString from 'query-string';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAlignJustify, faCheck, faTimes} from '@fortawesome/fontawesome-free-solid';

import {planPrice} from '../utils/planPrice';

import {showAlert} from "../store/ducks/alert";
import {fetchUserId} from "../store/ducks/user";
import {setPeriod, setPlan} from "../store/ducks/cart";
import {openContactFormModal, openRegisterModal} from "../store/ducks/modals";

import Header from './HeaderDesktop';
import BackToTop from "../blocks/BackToTop/BackToTop";
import Accordion from './Accordion';
import useIsMobile from '../../hooks/useIsMobile';

import styles from './MainPage.module.scss';

import logo from '../../media/logo.svg';
import paket from '../../media/paket.svg';
import register from '../../media/Stellenanzeige.svg';
import manageJob from '../../media/Verwaltung Ihrer Stellenanzeigen.png';
import separator from '../../media/separator_bg.png';
import jakobsImg from '../../media/c-jakobus.png';
import howItWorks from '../../media/how-it-works.png';
import footerLogo from '../../media/logo_footer.svg';
import smarketerJob from '../../media/jobSmarketer.png';
import buildMaterial from '../../media/Bildmaterial.svg';
import premiumPartner from '../../media/Premium_Google_Partner.svg';
import googleSearchImg from '../../media/google_job_image.png';
import cssPremiumPartner from '../../media/CSS Premium Partner.svg';
import googlePremiumPartner from '../../media/Google Premium Partner.jpg';
import microsoftPartnerFooter from '../../media/Microsoft Channel Partner light.svg';
import cssPremiumPartnerFooter from '../../media/CSS Premium Partner_footer.svg';

const MainPage = () => {
    const history = useHistory();

    const dispatch = useDispatch();
    const {period} = useSelector(state => state.cartState);
    const {locale} = useSelector(store => store.localeState);
    const {userId} = useSelector(state => state.userIdState);

    const [isCLickOnBurger, setClickOnBurger] = useState(false);

    const clickOnBurger = () => setClickOnBurger(!isCLickOnBurger);

    const handleSelectPlan = name => {
        dispatch(setPlan(name));
        history.push('/cart');
    };

    const scrollTo = sectionId => {
        setClickOnBurger(false);
        const header = document.getElementById('header');
        const section = document.getElementById(sectionId);
        const sectionTopPosition = section ? section.offsetTop : 0;
        const headerHeight = header ? header.clientHeight : 0;

        window.scrollTo(0, sectionTopPosition - headerHeight);
    };

    const resetPassword = () => {
        const hash = queryString.parse(location.search).h;
        if (hash) {
            axios.get('/ajax.php', {
                params: {model: 'user', mode: 'reset_password', 'h': hash},
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        dispatch(showAlert('success', 'reset password success'));
                    } else {
                        dispatch(showAlert('failure', 'reset password failure'));
                        console.error(data);
                    }
                })
                .catch(err => console.error(err));
        }
    };

    useEffect(() => {
        if (!userId) {
            dispatch(fetchUserId());
        }
        resetPassword();
    }, []);

    return <>
        <BackToTop/>
        <header className={styles.header} id="header">
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={styles.headerLogos}>
                        <h1 className={styles.logoImg}>
                            <NavLink to={userId ? '/job-add?active=overview' : "/"}
                                     activeClassName={styles.bgTransparent}>
                                <span className={styles.mainLogoWrapper}>
                                    <img width="280" height="40" className={styles.mailLogo} src={logo}
                                         alt='Logo: smarketer'/>
                                </span>
                            </NavLink>
                        </h1>
                        <div className={styles.partnersWrapper}>
                            <div className={styles.partner1}>
                                <img width="74" height="25" className={styles.partner1Logo}
                                     src={premiumPartner} alt='premium partner'/>
                            </div>
                            <div className={styles.partner2}>
                                <img width="104" height="26" className={styles.partner2Logo} src={cssPremiumPartner}
                                     alt='Css premium partner'/>
                            </div>
                        </div>
                    </div>
                    <div>
                        {useIsMobile(1285)
                            ? <FontAwesomeIcon
                                icon={isCLickOnBurger ? faTimes : faAlignJustify}
                                className={styles.burgerMenu}
                                onClick={clickOnBurger}
                            />
                            : <Header
                                scrollTo={scrollTo}
                                isLogged={userId}
                                isOpen={isCLickOnBurger}
                            />
                        }
                        {isCLickOnBurger ? <Header
                            scrollTo={scrollTo}
                            isLogged={userId}
                            isOpen={isCLickOnBurger}
                        /> : ''}
                    </div>
                </div>
            </div>
        </header>
        <section className={styles.mainSection}>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={styles.leftColumn}>
                        <h2 className={styles.title}>
                            {locale ? locale['Google for jobs'] : ''}
                            <span className={styles.secondLineText}>
                                {locale ? locale['Your job advertisement'] : ''}
                            </span>
                        </h2>
                        <p className={styles.description}>{locale ? locale['google for jobs sub text'] : ''}</p>
                        <ul className={styles.listOfItems}>
                            <li className={`${styles.item} ${styles.doneSentence}`}>
                                <div>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div>
                                    <span className={styles.itemText}>
                                        {locale ? locale['Millions of job searches start on Google'] : ''}
                                    </span>
                                </div>
                            </li>
                            <li className={`${styles.item} ${styles.doneSentence}`}>
                                <div>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div>
                                    <span className={styles.itemText}>
                                        {locale ? locale['Post your first job for free'] : ''}
                                    </span>
                                </div>
                            </li>
                            <li className={`${styles.item} ${styles.doneSentence}`}>
                                <div>
                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                </div>
                                <div>
                                    <span className={styles.itemText}>
                                        {locale ? locale['Manage all your jobs ads online'] : ''}
                                    </span>
                                </div>
                            </li>
                            <li className={styles.item}>
                                <div>
                                    {userId
                                        ? <NavLink to="/job-add" className={styles.registerBtn}>
                                            {locale ? locale['Publish a job'] : ''}
                                        </NavLink>
                                        : <button type='button' className={styles.registerBtn}
                                                  onClick={() => dispatch(openRegisterModal())}>
                                            {locale ? locale['Publish a job'] : ''}
                                        </button>
                                    }
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className={`${styles.rightColumn} ${styles.columnWithImg}`}>
                        <img width="100%" height="100%" className={styles.mainImg} src={howItWorks}
                             alt="how it works image"/>
                        <span className={styles.pricing}>
                            <div className={styles.textWrapper}>
                                <span className={styles.pricingPrefix}>1 Job</span>
                                <span className={styles.pricingPrefix}>
                                    {locale ? locale['for free'] : ''}
                                </span>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
        </section>
        <section className={styles.features}>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <ul className={styles.listOfItems}>
                        <li className={`${styles.item} ${styles.doneSentence}`}>
                            <div>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                            </div>
                            <div>
                                <span className={styles.itemText}>
                                    {locale ? locale['Create a job ad for free'] : ''}
                                </span>
                            </div>
                        </li>
                        <li className={`${styles.item} ${styles.doneSentence}`}>
                            <div>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                            </div>
                            <div>
                                <span className={styles.itemText}>
                                    {locale ? locale['Job advertisement directly on Google'] : ''}
                                </span>
                            </div>
                        </li>
                        <li className={`${styles.item} ${styles.doneSentence}`}>
                            <div>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                            </div>
                            <div>
                                <span className={styles.itemText}>
                                    {locale ? locale['Best Practices for your Job posting'] : ''}
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section className={styles.howItWorks} id='how'>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <h2 className={styles.sectionTitle}>
                        {locale ? locale['How it works'] : ''}
                    </h2>
                    <ul className={styles.listOfItems}>
                        <li className={styles.item}>
                            <img width="100%" height="100%" src={register} alt="register logo"
                                 className={styles.itemImg}/>
                            <span className={styles.itemNumber}>1.</span>
                            <h4 className={styles.itemTitle}>
                                {locale ? locale['Create a Job Posting'] : ''}
                            </h4>
                            <span className={styles.itemDescription}>
                                {locale ? locale['Create an ideally optimized'] : ''}
                            </span>
                        </li>
                        <li className={styles.item}>
                            <img width="100%" height="100%" src={paket} alt="paket logo" className={styles.itemImg}/>
                            <span className={styles.itemNumber}>2.</span>
                            <h4 className={styles.itemTitle}>
                                {locale ? locale['Choose a Plan'] : ''}
                            </h4>
                            <span className={styles.itemDescription}>
                                {locale ? locale['Choose from our flexible'] : ''}
                            </span>
                        </li>
                        <li className={styles.item}>
                            <img width="100%" height="100%" src={manageJob} alt="manage job logo"
                                 className={styles.itemImg}/>
                            <span className={styles.itemNumber}>3.</span>
                            <h4 className={styles.itemTitle}>
                                {locale ? locale['Manage your job ads'] : ''}
                            </h4>
                            <span className={styles.itemDescription}>
                                {locale ? locale['Manage the publication'] : ''}
                            </span>
                        </li>
                    </ul>
                    {userId
                        ? <NavLink to="/job-add" className={styles.registerBtn}>
                            {locale ? locale['Publish a job'] : ''}
                        </NavLink>
                        : <button type='button' className={styles.registerBtn}
                                  onClick={() => dispatch(openRegisterModal())}>
                            {locale ? locale['Publish a job'] : ''}
                        </button>
                    }
                </div>
            </div>
        </section>
        <div className={styles.seperator}>
            <img width="100%" height="100%" src={separator} alt="separator line" className={styles.seperatorImg}/>
        </div>
        <section className={styles.lookingFor} id='job'>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={`${styles.leftColumn} ${styles.columnWithImg}`}>
                        <img width="100%" height="100%" src={smarketerJob} alt="" className={styles.secondImg}/>
                    </div>
                    <div className={styles.rightColumn}>
                        <h2 className={styles.sectionTitle}>
                            {locale ? locale['Erreichen Sie Millionen von Bewerbern'] : ''}</h2>
                        <h4 className={styles.secondSectionTitle}>
                            {locale ? locale['Erreichen Sie sub titel'] : ''}
                        </h4>
                        <p className={styles.description}>
                            {locale ? locale['Erreichen Sie text'] : ''}
                        </p>
                        <span className={styles.listTitle}>
                            {locale ? locale['Your advantages at a glance:'] : ''}
                        </span>
                        <ul className={`${styles.listOfItems} ${styles.paddingLeftZero}`}>
                            <li className={`${styles.item} ${styles.displayFlex} ${styles.column}`}>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                <span className={styles.itemText}>
                                    {locale ? locale['Present your job ads on the'] : ''}
                                </span>
                            </li>
                            <li className={`${styles.item} ${styles.displayFlex} ${styles.column}`}>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                <span className={styles.itemText}>
                                    {locale ? locale['Create your first job'] : ''}
                                </span>
                            </li>
                            <li className={`${styles.item} ${styles.displayFlex} ${styles.column}`}>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                <span className={styles.itemText}>
                                    {locale ? locale['We optimize your'] : ''}
                                </span>
                            </li>
                            <li className={`${styles.item} ${styles.displayFlex} ${styles.column}`}>
                                <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                <span className={styles.itemText}>
                                    {locale ? locale['Keep control of your job'] : ''}
                                </span>
                            </li>
                            <li className={styles.item}>
                                <NavLink to='/search' className={styles.registerBtn}>
                                    {locale ? locale['To find a job'] : ''}
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section className={styles.googleSearchSection}>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={`${styles.leftColumn} ${styles.columnWithImg}`}>
                        <img width="100%" height="100%" src={googleSearchImg} alt="" className={styles.secondImg}/>
                    </div>
                    <div className={styles.rightColumn}>
                        <p className={styles.description}>
                            {locale ? locale['Present your job advertisement'] : ''}
                        </p>
                        <div className={styles.btnCenter}>
                            {userId
                                ? <NavLink to="/job-add" className={styles.registerBtn}>
                                    {locale ? locale['Publish a job'] : ''}
                                </NavLink>
                                : <button type='button' className={styles.registerBtn}
                                          onClick={() => dispatch(openRegisterModal())}>
                                    {locale ? locale['Publish a job'] : ''}
                                </button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section className={styles.plan} id='plan'>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div>
                        <h2 className={styles.sectionTitle}>
                            {locale ? locale['Fair Prices'] : ''}
                        </h2>
                        <h4 className={styles.secondSectionTitle}>
                            {locale ? locale['Choose the right package'] : ''}
                        </h4>
                    </div>
                    <div className={styles.choosePlanSection}>
                            <span
                                className={`${styles.monthPlan} ${styles.planPeriod} ${period === '_month' ? styles.bold : ''}`}>
                                {locale ? locale['Monthly subscription'] : ''}
                            </span>
                        <label className={styles.toggleBtn}>
                            <Toggle checked={period === '_year'} icons={false}
                                    onChange={e => dispatch(setPeriod(e.target.checked ? '_year' : '_month'))}/>
                        </label>
                        <span
                            className={`${styles.planPeriod} ${period === '_year' ? styles.bold : ''}`}>
                                {locale ? locale['Annual subscription'] : ''}
                            </span>
                        <span className={styles.tipForPlan}>
                            <span>{locale ? locale['2 months free'] : ''}</span>
                        </span>
                    </div>
                    <div className={styles.selectPlan}>
                        <ul className={styles.listOfPlans}>
                            <li className={`${styles.planItem} ${styles.starterPlan}`}>
                                <span className={styles.planName}>{planPrice.starter.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>{planPrice.starter.montPrice}</span>
                                    <span className={styles.planPeriod}>/mt</span>
                                    <span className={`${styles.planPriceDetail} ${styles.hidden}`}>0</span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['1 job ad for free'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Online for 30 days'] : ''}</span>
                                    </li>
                                    <li className={`${styles.planFeature} ${styles.hidden}`}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>hidden</span>
                                    </li>
                                    <li className={`${styles.planFeature} ${styles.hidden}`}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>hidden</span>
                                    </li>
                                </ul>
                                <button type='button' className={styles.getPlanBtn}
                                        onClick={() => userId ? handleSelectPlan('starter') : dispatch(openRegisterModal())}>
                                    {locale ? locale['Choose'] : ''}
                                </button>
                            </li>
                            <li className={styles.planItem}>
                                <span className={styles.planName}>{planPrice.basic.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>
                                            {period === '_year' ? planPrice.basic.monthPrice : planPrice.basic.yearPrice}
                                    </span>
                                    <span className={styles.planPeriod}>
                                        /mt
                                        {period === '_year'
                                            ? <span className={styles.rechtek}>
                                                &#8364;
                                                {planPrice.basic.yearPrice}
                                            </span> : ''
                                        }
                                        </span>
                                    <span className={styles.planPriceDetail}>
                                        {period === '_year' && planPrice.basic.monthPrice * 12}
                                        {period === '_year' && locale && locale['annual payment']}
                                    </span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['5 jobs included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Optimization of your data'] : ''}</span>
                                    </li>
                                    <li className={`${styles.planFeature} ${styles.hidden}`}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>hidden</span>
                                    </li>
                                    <li className={`${styles.planFeature} ${styles.hidden}`}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>hidden</span>
                                    </li>
                                </ul>
                                <button type='button' className={styles.getPlanBtn}
                                        onClick={() => userId ? handleSelectPlan('basic') : dispatch(openRegisterModal())}>
                                    {locale ? locale['Choose'] : ''}
                                </button>
                            </li>
                            <li className={`${styles.planItem} ${styles.topsellerPlan}`}>
                                    <span className={styles.topSelletContent}>
                                        {locale ? locale['Topseller'] : ''}
                                    </span>
                                <span className={styles.planName}>{planPrice.plus.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>
                                        {period === '_year' ? planPrice.plus.monthPrice : planPrice.plus.yearPrice}</span>
                                    <span className={styles.planPeriod}>
                                        /mt
                                        {period === '_year' &&
                                        <span className={styles.rechtek}>
                                                &#8364;{planPrice.plus.yearPrice}
                                            </span>
                                        }
                                        </span>
                                    <span className={styles.planPriceDetail}>
                                        {period === '_year' && planPrice.plus.monthPrice * 12}
                                        {period === '_year' && locale && locale['annual payment']}
                                    </span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['10 jobs included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Optimization of your data'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Reporting included'] : ''}</span>
                                    </li>
                                    <li className={`${styles.planFeature} ${styles.hidden}`}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>hidden</span>
                                    </li>
                                    <li className={`${styles.planFeature} ${styles.hidden}`}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>hidden</span>
                                    </li>
                                </ul>
                                <button type='button' className={styles.getPlanBtn}
                                        onClick={() => userId ? handleSelectPlan('plus') : dispatch(openRegisterModal())}>
                                    {locale ? locale['Choose'] : ''}
                                </button>
                            </li>
                            <li className={styles.planItem}>
                                <span className={styles.planName}>{planPrice.professional.name}</span>
                                <div className={styles.planPricing}>
                                    <span className={styles.planCurrency}>&#8364;</span>
                                    <span className={styles.planValue}>
                                            {period === '_year'
                                                ? planPrice.professional.monthPrice
                                                : planPrice.professional.yearPrice
                                            }
                                    </span>
                                    <span className={styles.planPeriod}>
                                            /mt
                                        {period === '_year' && <span
                                            className={styles.rechtek}>&#8364;{planPrice.professional.yearPrice}</span>}
                                        </span>
                                    <span className={styles.planPriceDetail}>
                                        {period === '_year' && planPrice.professional.monthPrice * 12}
                                        {period === '_year' && locale && locale['annual payment']}
                                    </span>
                                </div>
                                <ul className={styles.listOfFeatures}>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['unlimited job ads'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Reporting included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Support included'] : ''}</span>
                                    </li>
                                    <li className={styles.planFeature}>
                                        <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                        <span>{locale ? locale['Branded Landingpage & Video'] : ''}</span>
                                    </li>
                                </ul>
                                <button
                                    type="button"
                                    className={styles.getPlanBtn}
                                    onClick={() => dispatch(openContactFormModal())}
                                >
                                    {locale ? locale['Contact us (btn on select plan)'] : ''}
                                </button>
                            </li>
                        </ul>
                    </div>
                    <p className={styles.subTitlePrices}>{locale ? locale['Prices ex VAT; *Number of active Job Postings'] : ''}</p>
                </div>
            </div>
        </section>
        <section className={styles.cJakobs}>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={styles.img}>
                        <img width="100%" height="100%" src={jakobsImg} alt="Jakobs image"/>
                    </div>
                    <div>
                        <p className={styles.text}>
                            {locale ? locale['Not sure which plan is'] : ''}
                        </p>
                        <p className={styles.text}>
                            {locale ? locale['Call us and'] : ''} <a
                            href='tel:+49 (0) 30 577 008 136' className={styles.link}>
                            +49 (0) 30 577 008 136</a>
                        </p>
                        <button className={styles.contactUsBtnStr}
                                onClick={() => dispatch(openContactFormModal())}>
                            {locale ? locale['Contact Us str'] : ''}
                        </button>
                    </div>
                </div>
            </div>
        </section>
        <section className={styles.faqsSection} id='faqs'>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={`${styles.leftColumn} ${styles.mr50}`}>
                        <h2 className={styles.sectionTitle}>
                            {locale ? locale['FAQ - Frequently asked questions'] : ''}
                        </h2>
                        <p className={styles.description}>
                            {locale ? locale['Lean back and use the'] : ''}
                        </p>
                        <p className={styles.description}>
                            {locale ? locale['Find suitable candidates'] : ''}
                        </p>
                        <div className={styles.buildMaterialWrapper}>
                            <img width="100%" height="100%" className={styles.faqsImage} src={buildMaterial}
                                 alt="FAQs image"/>
                        </div>
                    </div>
                    <div className={styles.rightColumn}>
                        <Accordion title={locale ? locale['What does Google for Jobs cost'] : ''}>
                            <p>{locale ? locale['The basic listing of your job'] : ''}</p>
                        </Accordion>
                        <Accordion title={locale ? locale['How can I place job ads on Google for Jobs?'] : ''}>
                            <p>{locale ? locale['Create an account on smarketer.jobs for free'] : ''}</p>
                        </Accordion>
                        <Accordion isOpen
                                   title={locale ? locale['What advantage does Google for Jobs offer compared to other job boards?'] : ''}>
                            <p>{locale ? locale['Millions of people use Google Search every'] : ''}</p>
                        </Accordion>
                        <Accordion title={locale ? locale['How can I increase the reach'] : ''}>
                            <p>{locale ? locale['We will be happy to advise you on'] : ''}</p>
                        </Accordion>
                        <Accordion title={locale ? locale['Does my website need to be adapted'] : ''}>
                            <p>{locale ? locale['We take care of the complete integration'] : ''}</p>
                        </Accordion>
                        <Accordion title={locale ? locale['In which countries is Google for Jobs available?'] : ''}>
                            <p>{locale ? locale['Smarketer.jobs offers to publish your'] : ''}
                                {' '}
                                <a href="https://developers.google.com/search/docs/data-types/job-posting"
                                   target="_blank" className={styles.link}>
                                    {locale ? locale['Google for Jobs country list'] : ''}
                                </a>.
                            </p>
                        </Accordion>
                    </div>
                </div>
            </div>
        </section>
        <div className={styles.seperator}>
            <img width="100%" height="100%" src={separator} alt="separator line" className={styles.seperatorImg}/>
        </div>
        <section className={styles.preFooter}>
            <div className={styles.preFooterFirstSection}>
                <div className={styles.wrapper}>
                    <div className={styles.content}>
                        <p className={styles.description}>
                            {locale ? locale['Google for Jobs is the largest job'] : ''}
                        </p>
                    </div>
                </div>
            </div>
            <div className={styles.preFooterSecondSection}>
                <div className={styles.wrapper}>
                    <div className={styles.content}>
                        <ul className={`${styles.listOfItems} ${styles.pl0}`}>
                            <li className={styles.mainItem}>
                                <img width="100%" height="100%" src={footerLogo} alt="footer logo"
                                     className={styles.title}/>
                                <ul className={styles.childList}>
                                    <li className={styles.childTitle}>
                                        {locale ? locale['Alte Jakobstraße 83/84'] : ''}
                                    </li>
                                    <li className={styles.childTitle}>
                                        {locale ? locale['10179 Berlin'] : ''}
                                    </li>
                                    <li className={styles.listItem}>
                                        <span className={`${styles.childTitle} ${styles.hidden}`}>None</span>
                                    </li>
                                    <li className={styles.listItem}>
                                        <NavLink to={`/imprint-${locale ? locale['lang'] : 'en'}`}
                                                 activeClassName={styles.bgTransparent}
                                                 target="_blank"
                                                 className={styles.childTitle}>
                                            {locale ? locale['Imprint'] : ''}
                                        </NavLink>
                                    </li>
                                    <li className={styles.listItem}>
                                        <a href={locale && locale.lang === 'de'
                                            ? 'https://www.smarketer.de/agb-smarketer-jobs/'
                                            : 'https://www.smarketer.de/general-terms-and-conditions-smarketer-jobs/'}
                                           target="_blank"
                                           className={styles.childTitle}>
                                            {locale ? locale['Conditions'] : ''}
                                        </a>
                                    </li>
                                    <li className={styles.listItem}>
                                        <NavLink to={`/privacy-${locale ? locale['lang'] : 'en'}`}
                                                 activeClassName={styles.bgTransparent} target="_blank"
                                                 className={styles.childTitle}>
                                            {locale ? locale['Privacy'] : ''}
                                        </NavLink>
                                    </li>
                                    <li className={styles.listItem}>
                                        <span className={`${styles.childTitle} ${styles.hidden}`}>None</span>
                                    </li>
                                </ul>
                            </li>
                            <li className={styles.mainItem}>
                                <span className={styles.title}>{locale ? locale['Contact'] : ''}</span>
                                <ul className={styles.childList}>
                                    <li className={styles.listItem}>
                                        <a href='tel:+49 (0) 30 577 008 136' className={styles.childTitle}>
                                            +49 (0) 30 577 008 136
                                        </a>
                                    </li>
                                    <li className={styles.listItem}>
                                        <a href='mailto:info@smarketer.jobs'
                                           className={styles.childTitle}>info@smarketer.jobs</a>
                                    </li>
                                    <li className={styles.listItem}>
                                        <button
                                            className={styles.childBtn}
                                            onClick={() => dispatch(openContactFormModal())}
                                        >
                                            {locale ? locale['Contact Us btn'] : ''}
                                        </button>
                                    </li>
                                    <li className={styles.listItem}>
                                        {userId
                                            ?
                                            <NavLink to="/job-add"
                                                     className={`${styles.childTitle} ${styles.childBtn}`}>
                                                {locale ? locale['Publish a job'] : ''}
                                            </NavLink>
                                            :
                                            <button type='button' className={`${styles.childTitle} ${styles.childBtn}`}
                                                    onClick={() => dispatch(openRegisterModal())}>
                                                {locale ? locale['Register for free'] : ''}
                                            </button>
                                        }
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <footer className={styles.footer}>
            <div className={styles.wrapper}>
                <div className={styles.content}>
                    <div className={styles.footerPartner1Wrapper}>
                        <img width="126" height="70" className={styles.footerPartner1}
                             src={googlePremiumPartner} alt="partner logo"/>
                    </div>
                    <div className={styles.footerPartner2Wrapper}>
                        <img width="167" height="42" className={styles.footerPartner2}
                             src={cssPremiumPartnerFooter} alt="partner logo"/>
                    </div>
                    <div className={styles.footerPartner3Wrapper}>
                        <img width="87" height="43" className={styles.footerPartner3}
                             src={microsoftPartnerFooter} alt="partner logo"/>
                    </div>
                </div>
                <span className={styles.copyright}>
                    &#169; {new Date().getFullYear()} Smarketer GmbH{' | '}
                    <NavLink to={`/privacy-${locale ? locale['lang'] : 'en'}`} target="_blank"
                             activeClassName={styles.bgTransparent} className={styles.footerLinks}>
                        {locale ? locale['Privacy'] : ''}
                    </NavLink>
                    {' | '}
                    <NavLink to={`/imprint-${locale ? locale['lang'] : 'en'}`} target="_blank"
                             activeClassName={styles.bgTransparent} className={styles.footerLinks}>
                        {locale ? locale['Imprint'] : ''}
                    </NavLink>
                </span>
            </div>
        </footer>
    </>;
};

export default MainPage;
