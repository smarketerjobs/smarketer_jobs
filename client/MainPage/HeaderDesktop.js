import React, {useEffect} from 'react';

import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import LangSelect from "../blocks/LangSelect/LangSelect";
import {openLoginModal, openRegisterModal} from "../store/ducks/modals";

import styles from "./MainPage.module.scss";

const Header = props => {
    const {isLogged, isOpen, scrollTo} = props;

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);

    const hideScroll = () => {
        const body = document.getElementsByTagName('body')[0]
        if (body) {
            body.style.overflow = 'hidden';
        }
        const headerItems = document.getElementById('header-items');
        if (headerItems) {
            headerItems.style.overflow = 'auto';
            headerItems.style.maxHeight = 'calc(100vh - 140px)';
        }

    }

    useEffect(() => {
        return () => {
            const body = document.getElementsByTagName('body')[0]
            if (body) {
                body.style.overflow = 'auto';
            }
        }
    }, [])

    useEffect(() => {
        if (isOpen && isOpen) {
            hideScroll();
        }
    }, [isOpen, isOpen])

    return <>
        <ul className={isOpen && styles.headerMobile} id='header-items'>
            <li className={styles.item}>
                <button onClick={() => scrollTo('how')} className={styles.itemLink}>
                    {locale ? locale['Here\'s how it works'] : ''}</button>
            </li>
            <li className={styles.item}>
                <NavLink to='/search' className={styles.itemLink}>{locale ? locale['Job search'] : ''}</NavLink>
            </li>
            <li className={styles.item}>
                <button onClick={() => scrollTo('plan')} className={styles.itemLink}>
                    {locale ? locale['Pricing'] : ''}</button>
            </li>
            <li className={styles.item}>
                <button onClick={() => scrollTo('faqs')} className={styles.itemLink}>
                    {locale ? locale['FAQs'] : ''}</button>
            </li>
            <li className={styles.item}>
                {isLogged ? (
                    <NavLink to={'/job-add?active=overview'} className={styles.itemLink}>
                        {locale ? locale['My Jobs'] : ''}
                    </NavLink>
                ) : (
                    <button className={styles.itemLink} onClick={() => dispatch(openLoginModal())}>
                        {locale ? locale['Log in'] : ''}
                    </button>
                )}
            </li>
            <li className={styles.item}>
                {isLogged
                    ? <NavLink to="/job-add" className={styles.registerBtn}>
                        {locale ? locale['Publish a job'] : ''}
                    </NavLink>
                    : <button className={styles.registerBtn} onClick={() => dispatch(openRegisterModal())}>
                        {locale ? locale['Register now'] : ''}
                    </button>
                }
            </li>
            <li className={styles.item}>
                <LangSelect/>
            </li>
        </ul>
    </>
};

export default Header;
