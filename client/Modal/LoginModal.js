import React, {useEffect, useState} from 'react';
import ReactGA from "react-ga";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";

import axios from "axios";

import {faTimes} from '@fortawesome/fontawesome-free-solid';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import Modal from './Modal';
import {getUserIdSuccess} from "../store/ducks/user";
import {closeModals, openRegisterModal} from "../store/ducks/modals";

import styles from './LoginModal.module.scss';

import GoogleLogo from "../../media/Google__G__Logo.svg";
import LinkedinLogo from "../../media/linkedin.png";

const LoginModal = () => {
    const history = useHistory();

    const [getGoogleLink, setGoogleLink] = useState('');
    const [getLinkedinLink, setLinkedinLink] = useState('');

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {userId} = useSelector(state => state.userIdState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);
    const {isOpenLoginModal} = useSelector(state => state.modalsState);

    const [getErrorMsg, setErrorMsg] = useState('');
    const [getErrorStatus, setErrorStatus] = useState(false);

    const [getUserEmail, setUserEmail] = useState('');
    const [getUserPassword, setUserPassword] = useState('');

    const [getResetPasswordStatus, setResetPasswordStatus] = useState(false);
    const [getResetPasswordSuccessfully, setResetPasswordSuccessfully] = useState(false);

    const loginNow = () => {
        axios.get('/ajax.php', {
            params: {model: 'user', mode: 'login', email: getUserEmail, password: getUserPassword},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data && data.data.data) {
                    ReactGA.event({
                        label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                        action: 'login_e-mail',
                        category: 'Login'
                    });
                    dispatch(getUserIdSuccess(data.data.data));
                    dispatch(closeModals());
                } else {
                    console.error(data);
                    setErrorMsg(data.error);
                    setErrorStatus(true);
                }
            })
            .catch(err => {
                setErrorMsg(err);
                setErrorStatus(true);
                console.error(err);
            });
    };

    const forgotPass = () => {
        axios.get('/ajax.php', {
            params: {model: 'user', mode: 'reset_password', email: getUserEmail},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                setResetPasswordStatus(true);
                if (data.data) {
                    setResetPasswordSuccessfully(true);
                } else {
                    console.error(data);
                    setResetPasswordSuccessfully(false);
                }
            })
            .catch(err => console.error(err));
    };

    useEffect(() => {
        axios.get('/ajax.php', {params: {model: 'user', mode: 'get_auth_links'}})
            .then(({data}) => {
                if (data.data) {
                    setGoogleLink(data.data['google_link']);
                    setLinkedinLink(data.data['linkedin_link']);
                } else {
                    console.error(data);
                }
            }).catch(err => console.error(err));
    }, []);

    useEffect(() => {
        if (userId) {
            history.push('/job-add');
            dispatch(closeModals());
        }
    }, [userId]);

    return <Modal isOpen={isOpenLoginModal} isMainModal={true} ariaHideApp={false}>
        <div className={styles.loginModal}>
            <div className={styles.header}>
                <h2 className={styles.title}>{locale ? locale['Log in'] : ''}</h2>
                <span className={styles.closeBtn} onClick={() => dispatch(closeModals())}>
                    <FontAwesomeIcon icon={faTimes}/>
                </span>
            </div>
            <div className={styles.loginModalContent}>
                <div className={styles.loginWithWrapper}>
                    <a href={getGoogleLink} id='login_by_google' className={styles.btnLink}
                       onClick={() => {
                           ReactGA.event({
                               label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                               action: 'login_google',
                               category: 'Login'
                           })
                       }}>
                        <img src={GoogleLogo} alt="" className={styles.icon}/>
                        {locale ? locale['Login with Google'] : ''}
                    </a>
                    <a href={getLinkedinLink} className={styles.btnLink}
                       onClick={() => {
                           ReactGA.event({
                               label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                               action: 'login_linkedin',
                               category: 'Login'
                           })
                       }}>
                        <img src={LinkedinLogo} id='login_by_linkedin' alt="" className={styles.icon}/>
                        {locale ? locale['Login with LinkedIn'] : ''}
                    </a>
                </div>
                <div className={styles.loginDelimiter}>
                    <div className={styles.lineInline}/>
                    <div className={styles.delimiterText}>{locale ? locale['or'] : ''}</div>
                    <div className={styles.lineInline}/>
                </div>
                <form onSubmit={loginNow} className={styles.authForm}>
                    <label className={styles.inputLabel}>
                        <span className={styles.inputLabelText}>{locale ? locale['E-Mail Address'] : ''}</span>
                        <input type='email' name='email' placeholder='name@companyname.de' className={styles.input}
                               onChange={e => setUserEmail(e.target.value)}
                               onBlur={() => setUserEmail(getUserEmail.trim())}/>
                    </label>
                    <label className={styles.inputLabel}>
                        <span className={styles.inputLabelText}>{locale ? locale['Password'] : ''}</span>
                        <input type='password' name='password' className={styles.input}
                               onChange={e => setUserPassword(e.target.value)}
                               onBlur={() => setUserPassword(getUserPassword.trim())}/>
                    </label>
                    {getResetPasswordStatus && <div className={styles.resetPasswordMessage}>
                        {getResetPasswordSuccessfully
                            ? <span className={styles.resetPasswordMessageSuccess}>
                            {locale ? locale['Password successfully reset'] : ''}
                        </span>
                            : <span className={styles.resetPasswordMessageFail}>
                            {locale ? locale['Email not found! Please log In'] : ''}
                        </span>
                        }
                    </div>}
                    <div className={styles.buttonsWrapper}>
                        <div>
                            <button type="button" className={styles.forgotPasswordBtn} onClick={forgotPass}>
                                {locale ? locale['Forgot password?'] : ''}
                            </button>
                        </div>
                        <div className={styles.goToRegister}>
                            <button
                                type='button'
                                className={styles.forgotPasswordBtn}
                                onClick={() => dispatch(openRegisterModal())}
                            >
                                {locale ? locale['No account?'] : ''}
                                {' '}
                                {locale ? locale['Register here'] : ''}
                            </button>
                        </div>
                    </div>
                    {getErrorStatus ? <span className={styles.errorMsg}>{getErrorMsg}</span> : ''}
                    <button type='button' id='login_by_email' className={styles.authBtnSubmit} onClick={loginNow}>
                        {locale ? locale['Login now'] : ''}
                    </button>
                </form>
            </div>
        </div>
    </Modal>;
};

export default LoginModal;
