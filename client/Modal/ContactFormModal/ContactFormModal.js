import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import {faTimes} from '@fortawesome/fontawesome-free-solid';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import * as Yup from 'yup';

import Modal from '../Modal';
import {closeModals} from "../../store/ducks/modals";

import styles from './ContactFormModal.module.scss';
import {Field, Form, Formik} from "formik";
import {NavLink} from "react-router-dom";
import axios from "axios";
import {showAlert} from "../../store/ducks/alert";

const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('Required'),
    lastName: Yup.string().required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    companyName: Yup.string().required('Required'),
    phoneNumber: Yup.string().required('Required'),
    website: Yup.string().required('Required'),
    message: Yup.string().required('Required'),
});

const initialValues = {
    firstName: '',
    lastName: '',
    companyName: '',
    phoneNumber: '',
    email: '',
    website: '',
    message: '',
}

const ContactFormModal = () => {

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {isOpenContactFormModal} = useSelector(state => state.modalsState);

    const submit = ({firstName, lastName, companyName, phoneNumber, email, website, message}) => {
        axios.get('/ajax.php', {
            params: {
                model: 'contact',
                mode: 'send',
                email,
                message,
                lastName,
                firstName,
                phoneNumber,
                companyName,
                webSite: website,
            },
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    dispatch(showAlert('success', 'Message has been sent'));
                    dispatch(closeModals());
                } else {
                    dispatch(showAlert('failure', data.error));
                    console.error(data);
                }
            })
            .catch(err => console.error(err));
    }

    return <Modal isOpen={isOpenContactFormModal} isMainModal={true} ariaHideApp={false}>
        <div className={styles.contactFormModal}>
            <div className={styles.header}>
                <h2 className={styles.title}>{locale ? locale['Contact Us'] : ''}</h2>
                <span className={styles.closeBtn} onClick={() => dispatch(closeModals())}>
                    <FontAwesomeIcon icon={faTimes}/>
                </span>
            </div>
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={values => submit(values)}
            >
                {({errors, touched}) => (
                    <Form className={styles.form}>
                        <div className={styles.columnWrapper}>
                            <div className={`${styles.inputColumn} ${styles.mr10}`}>
                                <div
                                    className={`${styles.label} ${errors.firstName && touched.firstName ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['First name'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="firstName"
                                        className={`${styles.input} ${errors.firstName && touched.firstName ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.lastName && touched.lastName ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Last name'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="lastName"
                                        className={`${styles.input} ${errors.lastName && touched.lastName ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.email && touched.email ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Email'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="email"
                                        className={`${styles.input} ${errors.email && touched.email ? styles.invalidInput : ''}`}
                                    />
                                </div>
                            </div>
                            <div className={styles.inputColumn}>
                                <div
                                    className={`${styles.label} ${errors.companyName && touched.companyName ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Company name'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="companyName"
                                        className={`${styles.input} ${errors.companyName && touched.companyName ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.phoneNumber && touched.phoneNumber ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Phone number'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="phoneNumber"
                                        className={`${styles.input} ${errors.phoneNumber && touched.phoneNumber ? styles.invalidInput : ''}`}
                                    />
                                </div>
                                <div
                                    className={`${styles.label} ${errors.website && touched.website ? 'label-error' : ''}`}>
                                    <div className={styles.labelText}>
                                        {locale ? locale['Website URL'] : ''}
                                        <span className={styles.require}>*</span>
                                    </div>
                                    <Field
                                        name="website"
                                        className={`${styles.input} ${errors.website && touched.website ? styles.invalidInput : ''}`}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className={`${styles.label} ${errors.message && touched.message ? 'label-error' : ''}`}>
                            <div className={styles.labelText}>
                                {locale ? locale['Your message'] : ''}
                                <span className={styles.require}>*</span>
                            </div>
                            <Field
                                name="message"
                                component="textarea"
                                rows="4"
                                className={`${styles.input} ${errors.message && touched.message ? styles.invalidInput : ''}`}
                            />
                        </div>
                        <div className={styles.mandatory}>*{locale ? locale['Mandatory fields'] : ''}</div>
                        <button className={styles.btnSubmit}>
                            {locale ? locale['Send'] : ''}
                        </button>
                    </Form>
                )}
            </Formik>
            <div className={styles.privacyWarn}>
                {locale ? locale['By sending the contact form'] : ''}
                {' '}
                <NavLink to={`/privacy-${locale ? locale['lang'] : 'en'}`}
                         className={styles.link} target="_blank">
                    {locale ? locale['privacy policy2'] : ''}
                </NavLink>
                {locale && locale['lang'] === 'de' ? ' zu' : ''}.
            </div>
        </div>
    </Modal>
};

export default ContactFormModal;
