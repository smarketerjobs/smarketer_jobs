import React from 'react';

import ReactModal from 'react-modal';

import styles from './Modal.module.scss';

const Modal = props => {
    return (
        <ReactModal {...props} overlayClassName={styles.overlay} className={styles.modal}>
            <div className={props.isMainModal ? styles.mainModalContent : styles.modalContent}>
                {props.children}
            </div>
        </ReactModal>
    );
};

export default Modal;
