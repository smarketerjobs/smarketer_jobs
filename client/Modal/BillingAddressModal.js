import React, {useEffect, useState} from 'react';

import axios from "axios";
import {faTimes} from "@fortawesome/fontawesome-free-solid";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useDispatch, useSelector} from "react-redux";

import Modal from "./Modal";
import {showAlert} from "../store/ducks/alert";
import {fetchBills} from "../store/ducks/bills";
import {closeModals} from "../store/ducks/modals";
import {fetchCountries} from "../store/ducks/countries";
import {fetchUserBillingData} from "../store/ducks/user";

import styles from './BillingAddressModal.module.scss';

const BillingAddressModal = () => {

    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {countries} = useSelector(state => state.countriesState);
    const {isOpenBillingInfoModal} = useSelector(state => state.modalsState);

    const [getCity, setCity] = useState('');
    const [getEmail, setEmail] = useState('');
    const [getVatId, setVatId] = useState('');
    const [getLastName, setLastName] = useState('');
    const [getPostCode, setPostCode] = useState('');
    const [getFirstName, setFirstName] = useState('');
    const [getCompanyName, setCompanyName] = useState('');
    const [getCountryCode, setCountryCodeName] = useState('DE');
    const [getFirstAddressName, setFirstAddressName] = useState('');
    const [getSecondAddressName, setSecondAddressName] = useState('');

    const [loadingSaveBillingInfo, setLoadingSaveBillingInfo] = useState(false);

    const [isSaveFall, setSaveFall] = useState(false);

    const billingInfoSuccessfullySaved = () => {
        dispatch(showAlert('success', 'billing info successfully saved'));
        dispatch(fetchUserBillingData());
        dispatch(fetchBills());
        dispatch(closeModals());
    };

    const handleSaveBillingInfo = () => {
        if (getCompanyName && getFirstName && getLastName && getCountryCode && getEmail) {
            setLoadingSaveBillingInfo(true);
            axios.get('/ajax.php', {
                params: {
                    model: 'user',
                    mode: 'save_billing_info',
                    vat_id: getVatId,
                    company_name: getCompanyName,
                    first_name: getFirstName,
                    last_name: getLastName,
                    address_1: getFirstAddressName,
                    address_2: getSecondAddressName,
                    post_code: getPostCode,
                    city: getCity,
                    country_code: getCountryCode,
                    email: getEmail
                },
                headers: {'Cache-Control': 'no-cache'}
            })
                .then(({data}) => {
                    if (data.data) {
                        billingInfoSuccessfullySaved();
                    } else {
                        dispatch(showAlert('failure', data.error));
                        console.error(data);
                    }
                    setLoadingSaveBillingInfo(false);
                })
                .catch(err => {
                    dispatch(showAlert('failure', 'Something went wrong'));
                    setLoadingSaveBillingInfo(false);
                    console.error('err', err);
                });
        } else {
            setSaveFall(true);
        }
    };

    useEffect(() => {
        dispatch(fetchCountries());

        axios.get('/ajax.php', {
            params: {model: 'user', mode: 'get_billing_info'},
            headers: {'Cache-Control': 'no-cache'}
        })
            .then(({data}) => {
                if (data.data) {
                    data.data.city ? setCity(data.data.city) : '';
                    data.data.email ? setEmail(data.data.email) : '';
                    data.data.vat_id ? setVatId(data.data.vat_id) : '';
                    data.data.last_name ? setLastName(data.data.last_name) : '';
                    data.data.post_code ? setPostCode(data.data.post_code) : '';
                    data.data.first_name ? setFirstName(data.data.first_name) : '';
                    data.data.address_1 ? setFirstAddressName(data.data.address_1) : '';
                    data.data.address_2 ? setSecondAddressName(data.data.address_2) : '';
                    data.data.company_name ? setCompanyName(data.data.company_name) : '';
                    data.data.country_code ? setCountryCodeName(data.data.country_code) : '';
                } else if (data.error === 'User is not logged') {
                    // user no logged
                } else {
                    console.error(data);
                }
            })
            .catch(err => console.error(err));
    }, []);

    return (
        <Modal isOpen={isOpenBillingInfoModal} ariaHideApp={false}>
            <section className={styles.modal}>
                <div className={styles.header}>
                    <h2 className={styles.title}>{locale ? locale['Adjust billing address'] : ''}</h2>
                    <span className={styles.closeBtn} onClick={() => dispatch(closeModals())}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </span>
                </div>
                <div>
                    <div className={styles.formEl}>
                        <span className={`${styles.formTitle} ${isSaveFall && !getCompanyName ? styles.fail : ''}`}>
                            {locale ? locale['Company'] : ''}*
                        </span>
                        <div className={styles.inputWrapper}>
                            <input type="text" value={getCompanyName} onChange={e => setCompanyName(e.target.value)}
                                   className={`${styles.input} ${isSaveFall && !getCompanyName ? styles.fail : ''}`}
                                   onBlur={() => setCompanyName(getCompanyName.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={styles.formTitle}>
                            {locale ? locale['VAT ID'] : ''}
                        </span>
                        <div className={styles.inputWrapper}>
                            <input
                                type="text"
                                className={styles.input}
                                value={getVatId}
                                onChange={e => setVatId(e.target.value)}
                                onBlur={() => setVatId(getVatId.trim())}
                            />
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={styles.formSection}>{locale ? locale['Contact Person'] : ''}</span>
                    </div>
                    <div className={styles.formEl}>
                        <span className={`${styles.formTitle} ${isSaveFall && !getFirstName ? styles.fail : ''}`}>
                            {locale ? locale['First name'] : ''}*
                        </span>
                        <div className={styles.inputWrapper}>
                            <input type="text" value={getFirstName} onChange={e => setFirstName(e.target.value)}
                                   className={`${styles.input} ${isSaveFall && !getFirstName ? styles.fail : ''}`}
                                   onBlur={() => setFirstName(getFirstName.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={`${styles.formTitle} ${isSaveFall && !getLastName ? styles.fail : ''}`}>
                            {locale ? locale['Surname'] : ''}*
                        </span>
                        <div className={styles.inputWrapper}>
                            <input type="text" value={getLastName} onChange={e => setLastName(e.target.value)}
                                   className={`${styles.input} ${isSaveFall && !getLastName ? styles.fail : ''}`}
                                   onBlur={() => setLastName(getLastName.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={styles.formSection}>{locale ? locale['Address'] : ''}</span>
                    </div>
                    <div className={styles.formEl}>
                        <span className={`${styles.formTitle} ${isSaveFall && !getEmail ? styles.fail : ''}`}>
                            {locale ? locale['E-Mail'] : ''}*
                        </span>
                        <div className={styles.inputWrapper}>
                            <input type="email" value={getEmail} onChange={e => setEmail(e.target.value)}
                                   className={`${styles.input} ${isSaveFall && !getEmail ? styles.fail : ''}`}
                                   onBlur={() => setEmail(getEmail.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={`${styles.formTitle} ${isSaveFall && !getCountryCode ? styles.fail : ''}`}>
                            {locale ? locale['Country'] : ''}*
                        </span>
                        <div className={`${styles.inputWrapper} ${styles.selectWrapper}`}>
                            <select name='countries' value={getCountryCode}
                                    onChange={e => setCountryCodeName(e.target.value)}
                                    className={`${styles.input} ${isSaveFall && !getCountryCode ? styles.fail : ''} ${styles.select}`}>
                                {countries.map(country => (
                                    <option value={country.country_code} key={country.country_code}>
                                        {country.country}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={`${styles.formTitle}`}>{locale ? locale['City'] : ''}</span>
                        <div className={styles.inputWrapper}>
                            <input type="text" className={styles.input} value={getCity}
                                   onChange={e => setCity(e.target.value)}
                                   onBlur={() => setCity(getCity.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={styles.formTitle}>{locale ? locale['Post Code'] : ''}</span>
                        <div className={styles.inputWrapper}>
                            <input type="text" className={styles.input} value={getPostCode}
                                   onChange={e => setPostCode(e.target.value)}
                                   onBlur={() => setPostCode(getPostCode.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={styles.formTitle}>{locale ? locale['Street + house number'] : ''}</span>
                        <div className={styles.inputWrapper}>
                            <input type="text" className={styles.input} value={getFirstAddressName}
                                   onChange={e => setFirstAddressName(e.target.value)}
                                   onBlur={() => setFirstAddressName(getFirstAddressName.trim())}/>
                        </div>
                    </div>
                    <div className={styles.formEl}>
                        <span className={styles.formTitle}>{locale ? locale['Address addition'] : ''}</span>
                        <div className={styles.inputWrapper}>
                            <input type="text" className={styles.input} value={getSecondAddressName}
                                   onChange={e => setSecondAddressName(e.target.value)}
                                   onBlur={() => setSecondAddressName(getSecondAddressName.trim())}/>
                        </div>
                    </div>
                    {isSaveFall && (!getCompanyName || !getFirstName || !getLastName || !getEmail || !getCountryCode)
                        ? <div className={styles.messageWrapper}>
                            <span className={styles.failMsg}>{locale ? locale['Please fill all fields'] : ''}</span>
                        </div>
                        : ''
                    }
                    <button
                        type="button"
                        disabled={loadingSaveBillingInfo}
                        className={styles.button}
                        onClick={handleSaveBillingInfo}
                    >
                        {locale ? locale['to save'] : ''}
                    </button>
                </div>
            </section>
        </Modal>
    );
};

export default BillingAddressModal;
