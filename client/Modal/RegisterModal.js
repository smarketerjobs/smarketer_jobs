import React, {useEffect, useState} from 'react';

import axios from "axios";
import ReactGA from "react-ga";
import {faTimes} from "@fortawesome/fontawesome-free-solid";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useDispatch, useSelector} from "react-redux";

import Modal from "./Modal";
import Register from "../Register/Register";
import {closeModals, openLoginModal} from "../store/ducks/modals";

import styles from "./RegisterModal.module.scss";
import GoogleLogo from "../../media/Google__G__Logo.svg";
import LinkedinLogo from "../../media/linkedin.png";

const RegisterModal = () => {
    const dispatch = useDispatch();
    const {locale} = useSelector(state => state.localeState);
    const {userBillingData} = useSelector(state => state.userBillingDataState);
    const {isOpenRegisterModal} = useSelector(state => state.modalsState);

    const [getGoogleLink, setGoogleLink] = useState('');
    const [getLinkedinLink, setLinkedinLink] = useState('');

    const closeModal = () => {
        dispatch(closeModals());
    };

    const ContentDescription = () => <div className={styles.contentDescription}>
        <div className={styles.authSwitch}>
            <a href={getGoogleLink} id='registration_by_google' className={styles.btnLink}
               onClick={() => {
                   ReactGA.event({
                       label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                       action: 'registration_google',
                       category: 'Registration'
                   })
               }}>
                <img src={GoogleLogo} alt="" className={styles.icon}/>
                {locale ? locale['Register with Google'] : ''}
            </a>
            <a href={getLinkedinLink} id='registration_by_linkedin' className={styles.btnLink}
               onClick={() => {
                   ReactGA.event({
                       label: userBillingData ? userBillingData['company_name'] : 'user_not_logged',
                       action: 'registration_linkedin',
                       category: 'Registration'
                   })
               }}>
                <img src={LinkedinLogo} alt="" className={styles.icon}/>
                {locale ? locale['Register with Linkedin'] : ''}
            </a>
        </div>
        <span className={styles.login}>
            {locale ? locale['Already registered?'] : ''}
            {' '}
            <button className={styles.loginBtn} onClick={() => dispatch(openLoginModal())}>
                <strong>{locale ? locale['Login here'] : ''}</strong>
            </button>
        </span>
    </div>;

    useEffect(() => {
        axios.get('/ajax.php', {params: {model: 'user', mode: 'get_auth_links'}})
            .then(({data}) => {
                if (data.data) {
                    setGoogleLink(data.data['google_link']);
                    setLinkedinLink(data.data['linkedin_link']);
                } else {
                    console.error(data);
                }
            }).catch(err => console.error(err));
    }, []);

    return (
        <Modal isOpen={isOpenRegisterModal} isMainModal={true} ariaHideApp={false}>
            <div className={styles.registerModalContent}>
                <div className={styles.header}>
                    <h2 className={styles.title}>
                        {locale ? locale['Create a free account & test it for 30 days without obligation.'] : ''}
                    </h2>
                    <div className={styles.closeBtn} onClick={closeModal}><FontAwesomeIcon icon={faTimes}/></div>
                </div>
                <Register/>
                <div className={styles.separateBlock}>
                    <div className={styles.separateLine}/>
                    <div className={styles.separateText}>{locale ? locale['or register with'] : ''}</div>
                    <div className={styles.separateLine}/>
                </div>
                <ContentDescription/>
            </div>
        </Modal>
    );
};

export default RegisterModal;
