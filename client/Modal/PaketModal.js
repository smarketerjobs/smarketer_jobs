import React from 'react';

import Toggle from "react-toggle";
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faTimes} from "@fortawesome/fontawesome-free-solid";

import {planPrice} from "../utils/planPrice";

import {closeModals} from "../store/ducks/modals";
import {setPeriod, setPlan} from "../store/ducks/cart";

import Modal from "./Modal";

import styles from './PaketModal.module.scss';

const PaketModal = () => {

    const history = useHistory();

    const dispatch = useDispatch();
    const {period} = useSelector(state => state.cartState);
    const {locale} = useSelector(state => state.localeState);
    const {isOpenPaketModal} = useSelector(state => state.modalsState);

    const handleSelectPlan = name => {
        dispatch(setPlan(name));
        dispatch(closeModals());
        history.push(`/cart`);
    };

    return <>
        <Modal isOpen={isOpenPaketModal} ariaHideApp={false}>
            <section className={styles.modal}>
                <div className={styles.header}>
                    <h2 className={styles.title}>{locale ? locale['Choose a Plan'] : ''}</h2>
                    <span className={styles.closeBtn} onClick={() => dispatch(closeModals())}>
                        <FontAwesomeIcon icon={faTimes}/>
                    </span>
                </div>
                <p className={styles.modalDescribe}>{locale ? locale['Select Package sub titel'] : ''}</p>
                <div className={styles.content}>
                    <div className={styles.plan}>
                        <div className={styles.wrapper}>
                            <div className={styles.content}>
                                <div className={styles.choosePlanSection}>
                                    <span
                                        className={`${styles.monthPlan} ${styles.planPeriod} ${period === '_month' ? styles.bold : ''}`}>
                                        {locale ? locale['Monthly subscription'] : ''}
                                    </span>
                                    <label className={styles.toggleBtn}>
                                        <Toggle checked={period === '_year'} icons={false}
                                                onChange={e => dispatch(setPeriod(e.target.checked ? '_year' : '_month'))}/>
                                    </label>
                                    <span
                                        className={`${styles.yearPlan} ${styles.planPeriod} ${period === '_year' ? styles.bold : ''}`}>
                                        {locale ? locale['Annual subscription'] : ''}
                                    </span>
                                    <span
                                        className={styles.tipForPlan}>{locale ? locale['2 months free'] : ''}</span>
                                </div>
                                <div className={styles.selectPlan}>
                                    <ul className={styles.listOfPlans}>
                                        <li className={`${styles.planItem} ${styles.starterPlan}`}>
                                            <span className={styles.planName}>{planPrice.starter.name}</span>
                                            <div className={styles.planPricing}>
                                                <span className={styles.planCurrency}>&#8364;</span>
                                                <span className={styles.planValue}>{planPrice.starter.montPrice}</span>
                                                <span className={styles.planPeriod}>/mt</span>
                                                <span className={`${styles.planPriceDetail} ${styles.hidden}`}>0</span>
                                            </div>
                                            <ul className={styles.listOfFeatures}>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['1 job ad for free'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Online for 30 days'] : ''}</span>
                                                </li>
                                                <li className={`${styles.planFeature} ${styles.hidden}`}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>Hidden</span>
                                                </li>
                                                <li className={`${styles.planFeature} ${styles.hidden}`}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>Hidden</span>
                                                </li>
                                            </ul>
                                            <button type='button' className={styles.getPlanBtn}
                                                    onClick={() => handleSelectPlan('starter')}>
                                                {locale ? locale['Choose'] : ''}
                                            </button>
                                        </li>
                                        <li className={styles.planItem}>
                                            <span className={styles.planName}>{planPrice.basic.name}</span>
                                            <div className={styles.planPricing}>
                                                <span className={styles.planCurrency}>&#8364;</span>
                                                <span className={styles.planValue}>
                                                    {period === '_year' ? planPrice.basic.monthPrice : planPrice.basic.yearPrice}
                                                </span>
                                                <span className={styles.planPeriod}>
                                                    /mt
                                                    {period === '_year' && <span className={styles.rechtek}>
                                                        &#8364;{planPrice.basic.yearPrice}
                                                    </span>}
                                                </span>
                                                <span className={styles.planPriceDetail}>
                                                    {period === '_year' && planPrice.basic.monthPrice * 12}
                                                    {period === '_year' && locale && locale['annual payment']}
                                                </span>
                                            </div>
                                            <ul className={styles.listOfFeatures}>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['5 jobs included'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Optimization of your data'] : ''}</span>
                                                </li>
                                                <li className={`${styles.planFeature} ${styles.hidden}`}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>Hidden</span>
                                                </li>
                                                <li className={`${styles.planFeature} ${styles.hidden}`}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>Hidden</span>
                                                </li>
                                            </ul>
                                            <button type='button' className={styles.getPlanBtn}
                                                    onClick={() => handleSelectPlan('basic')}>
                                                {locale ? locale['Choose'] : ''}
                                            </button>
                                        </li>
                                        <li className={`${styles.planItem} ${styles.topsellerPlan}`}>
                                            <span className={styles.topSelletContent}>
                                                {locale ? locale['Topseller'] : ''}
                                            </span>
                                            <span className={styles.planName}>{planPrice.plus.name}</span>
                                            <div className={styles.planPricing}>
                                                <span className={styles.planCurrency}>&#8364;</span>
                                                <span className={styles.planValue}>
                                                    {period === '_year' ? planPrice.plus.monthPrice : planPrice.plus.yearPrice}
                                                </span>
                                                <span className={styles.planPeriod}>
                                                    /mt
                                                    {period === '_year' && <span
                                                        className={styles.rechtek}>&#8364;{planPrice.plus.yearPrice}</span>}
                                                </span>
                                                <span className={styles.planPriceDetail}>
                                                    {period === '_year' && planPrice.plus.monthPrice * 12}
                                                    {period === '_year' && locale && locale['annual payment']}
                                                </span>
                                            </div>
                                            <ul className={styles.listOfFeatures}>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['10 jobs included'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Optimization of your data'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Reporting included'] : ''}</span>
                                                </li>
                                                <li className={`${styles.planFeature} ${styles.hidden}`}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>Hidden</span>
                                                </li>
                                                <li className={`${styles.planFeature} ${styles.hidden}`}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>Hidden</span>
                                                </li>
                                            </ul>
                                            <button type='button' className={styles.getPlanBtn}
                                                    onClick={() => handleSelectPlan('plus')}>
                                                {locale ? locale['Choose'] : ''}
                                            </button>
                                        </li>
                                        <li className={styles.planItem}>
                                            <span className={styles.planName}>{planPrice.professional.name}</span>
                                            <div className={styles.planPricing}>
                                                <span className={styles.planCurrency}>&#8364;</span>
                                                <span className={styles.planValue}>
                                                    {period === '_year' ? planPrice.professional.monthPrice : planPrice.professional.yearPrice}</span>
                                                <span className={styles.planPeriod}>
                                                    /mt
                                                    {period === '_year' && <span className={styles.rechtek}>
                                                        &#8364;{planPrice.professional.yearPrice}
                                                    </span>}
                                                </span>
                                                <span className={styles.planPriceDetail}>
                                                    {period === '_year' && planPrice.professional.monthPrice * 12}
                                                    {period === '_year' && locale && locale['annual payment']}
                                                </span>
                                            </div>
                                            <ul className={styles.listOfFeatures}>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['unlimited job ads'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Reporting included'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Support included'] : ''}</span>
                                                </li>
                                                <li className={styles.planFeature}>
                                                    <FontAwesomeIcon icon={faCheck} className={styles.checkIcon}/>
                                                    <span>{locale ? locale['Branded Landingpage & Video'] : ''}</span>
                                                </li>
                                            </ul>
                                            <button type='button' className={styles.getPlanBtn}
                                                    onClick={() => handleSelectPlan('professional')}>
                                                {locale ? locale['Choose'] : ''}
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <p className={styles.planNotice}>
                                    {locale ? locale['Prices ex VAT; *Number of active Job Postings'] : ''}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Modal>
    </>;
};

export default PaketModal;
